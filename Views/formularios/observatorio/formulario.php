<div class="col-xs-12"><br><br></div>
<div class="col-xs-12">
	<div class="steps f2">
		<?php include 'inc/linea-avance.php'; ?>
	</div>
</div>
<div class="col-md-12"><br></div>
<?php
$linea = 0;
if ($_GET['linea']=='investigacion-aplicada') {
	$linea = 1;
	$presupuesto = 95000000;
}else{
	if ($_GET['linea']=='innovacion-y-desarrollo-tecnologico') {
		$linea = 2;
		$presupuesto = 100000000;
	}else{
		if ($_GET['linea']=='fortalecimiento-de-la-ofeta-de-servicios-tecnologicos-a-las-empresas') {
			$linea = 3;
			$presupuesto = 400000000;
		}else{
			if ($_GET['linea']=='actualizacion-y-modernizacion-tecnologica') {
				$linea = 4;
				$presupuesto = 500000000;
			}
		}
	}
}
$var = htmlentities(addslashes($_GET['registro']));

if (!empty($_GET['form']=='fase-1') ) {

	$datosPersonales = $clase->datosPersonalesRadicador($var);
	if ($datosPersonales!=false) {
		while ($datos = mysqli_fetch_object($datosPersonales)) {
			include 'inc/datos-personales-directo.php';
		}
	}else{
		echo '<div class="contentDatos_fase_1">
			<div class="col-xs-12">
				<form name="datos-fase_1" enctype="multipart/form-data" method="post">
					<input type="hidden" id="accesoRegistro" value="0">
					<input type="hidden" name="presupuesto" id="presupuesto" value="'.$presupuesto.'" placeholder="">
					<input type="hidden" name="linea_programatica" value="'.$linea.'" >
					';
					include 'fase_1/form-datos-personales.php';
				echo '</form>
				<div class="col-xs-12">
					<br>
					<div id="messageAlert-fase_1-inscripcion" style="display: none;"></div>
					<div class="pull-right">
						<button class="btn btn-info" id="guardarDatosFase_1" onclick="enviarDatosForm(4,'.$_GET['ac'].',\''.$_GET['linea'].'\','.$_GET['registro'].')">Guardar datos</button>
					</div>
				</div>
				<div class="col-md-12"><br><br><br><br><br></div>
			</div>
		</div>';
	}
}

?>