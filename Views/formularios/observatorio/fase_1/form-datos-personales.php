<div class="col-xs-12">
	<div class="col-xs-10 col-md-9">
		<h3>Datos personales</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-DatosPersonales" onclick="multiDespliegue('btn-DatosPersonales','valorrDatosPersonales','contentDatosDatosPersonales')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorrDatosPersonales" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosDatosPersonales" style="display: none;">
		<div class="col-xs-12 col-md-3">
			<p>Regional</p>
			<input type="text" name="regional" id="regional" class="form-control" onclick="validarCampoLleno('regional')" onkeyup="validarCampoLleno('regional')" value="CEDRUM N.D.S">
			<input type="hidden" id="progress_regional" value="0.823529412">
		</div>
		<div class="col-xs-12 col-md-6">
			<p>Programa de Formacion</p>
			<input type="text" name="formacion" id="formacion" class="form-control" onclick="validarCampoLleno('formacion')" 
				onkeyup="validarCampoLleno('formacion');">
			<input type="hidden" id="progress-formacion" value="0.823529412">
		</div>
		<div class="col-xs-12 col-md-3">
			<p>Ficha</p>
			<input type="number" name="ficha" id="ficha" class="form-control" onclick="validarCampoLleno('ficha')" onkeyup="validarCampoLleno('ficha');" min="0" onkeypress="return event.charCode >= 48">
			<input type="hidden" id="progress-ficha" value="0.823529412">
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-xs-12 col-md-6">
			<p>Nombres</p>
			<input type="text" name="nombre" id="nombre" class="form-control" onclick="validarCampoLleno('nombre');" onkeyup="validarCampoLleno('nombre')" >
			<input type="hidden" id="progress-nombre" value="0.823529412" >
		</div>
		<div class="col-xs-12 col-md-6">
			<p>Apellidos</p>
			<input type="text" name="apellido" id="apellido" class="form-control" onclick="validarCampoLleno('apellido');" onkeyup="validarCampoLleno('apellido')" >
			<input type="hidden" id="progress_primerApellido" value="0.823529412">
		</div>
		

		<div class="col-md-12"><br></div>
		<div class="col-xs-12 col-md-4">
			<p>Identificación</p> 
			<input type="number" name="identificacion" id="identificacion" class="form-control" onclick="validarCampoLleno('identificacion')" onkeyup="validarCampoLleno('identificacion')" >
			<input type="hidden" id="progress_identificacion" value="0.823529412">
		</div>
		<div class="col-xs-12 col-md-5">
			<p>Correo</p>
			<input type="email" name="email" id="email" class="form-control" onclick="validarEmail('email','mesageValEmail');" onkeyup="validarEmail('email','mesageValEmail');">
			<p id="mesageValEmail"></p>
		</div>
		<div class="col-xs-12 col-md-3">
			<p>Telefono</p>
			<input type="number" name="telefono" id="telefono" class="form-control"  onclick="validarCampoLleno('telefono')" onkeyup="validarCampoLleno('telefono')">
			<input type="hidden" id="progress_telefono" value="0.823529412">
		</div>
	</div>
</div>
<div class="col-xs-12"><hr></div>
<div class="col-xs-12">
	<div class="col-xs-10 col-md-9">
		<h3>Inscripción idea</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-ideaProyecto" onclick="multiDespliegue('btn-ideaProyecto','valorideaProyecto','contentDatosideaProyecto')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorideaProyecto" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosideaProyecto" style="display: none;">
		<?php include 'idea.php';?>
	</div>
</div>
<div class="col-xs-12"><hr></div>
