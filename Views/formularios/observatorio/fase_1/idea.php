<div class="col-xs-12">
	<div class="col-xs-12"><br></div>
		<p>Linea de Investigación</p>
	<div class="col-xs-12">

		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="agricola_pesca" id="opcion_1" onclick="unaOpcion(1)">
		    <label for="opcion_1">AGRÍCOLA Y DE PESCA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="agricola" id="opcion_2" onclick="unaOpcion(2)">
		    <label for="opcion_2">AGRÍCOLA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="ambiental" id="opcion_3" onclick="unaOpcion(3)">
		    <label for="opcion_3">AMBIENTAL</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="administrativa" id="opcion_4" onclick="unaOpcion(4)">
		    <label for="opcion_4">GESTIÓN ADMINISTRATIVA Y FINANCIERA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="mineria" id="opcion_5" onclick="unaOpcion(5)">
		    <label for="opcion_5">MINERÍA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="pecuaria" id="opcion_6" onclick="unaOpcion(6)">
		    <label for="opcion_6">PECUARIA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="informatica" id="opcion_7" onclick="unaOpcion(7)">
		    <label for="opcion_7">INFORMÁTICA, DISEÑO Y DESARROLLO DE SOFTWARE.</label>
		</fieldset>
		
	</div>
</div>
<div class="col-xs-12">
	<p>Idea</p>
	<textarea name="idea" id="idea" class="form-control" cols="30" rows="3" onclick="validarCampoLleno('idea')" onkeyup="validarCampoLleno('idea')" placeholder="Descripcion corta del proyecto a desarrollar"></textarea>
</div>
<div class="col-xs-12">
	<p>Planteamiento del problema</p>
	<textarea name="planteamiento" id="planteamiento" class="form-control" cols="30" rows="5" onclick="validarCampoLleno('planteamiento')" onkeyup="validarCampoLleno('planteamiento')" placeholder="A qué problemática daría solución la IDEA"></textarea>
</div>
<div class="col-xs-12">
	<p>Sector productivo al que impactará la idea</p>
	<textarea name="sectorProductivo" id="sectorProductivo" class="form-control" cols="30" rows="2" onclick="validarCampoLleno('sectorProductivo')" onkeyup="validarCampoLleno('sectorProductivo')"></textarea>
</div>
<div class="col-xs-12">
	<p>Municipios de Norte de Santander donde desarrollará la idea</p>
	<div class="col-xs-3">
		<input type="number" name="municipios" id="municipios" class="form-control" 
					onclick="validarCampoLleno('municipios');numeroMunicipios();"
					onkeyup="validarCampoLleno('municipios');numeroMunicipios();" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Municipios" >
	</div>
	<div class="col-xs-12" id="contentMunicipios"></div>
	<div class="col-xs-12"><hr></div>
</div>
<div class="col-xs-12">
	<p>¿Con quien(es) desarrollará la idea?</p>
	<textarea name="acompannantes" id="acompannantes" class="form-control"  cols="30" rows="3" onclick="validarCampoLleno('acompannantes')" onkeyup="validarCampoLleno('acompannantes')" placeholder="Otros instructores, otros Semilleros, SENA o de otras Instituciones, Sector Productivo, Sector Gubernamental, Entes Descentralizadas, etc."></textarea>
</div>
<div class="col-xs-12 col-md-4">
	<p>¿Cuanto piensa que costaría desarrollar la idea?</p>
	<input type="number" name="valor-idea" id="valor-idea" class="valor-idea form-control" onkeyup="validarPresupuesto(<?php echo $presupuesto;?>);validarCampoLleno('valor-idea')" placeholder="$ valor idea" onclick="validarPresupuesto(<?php echo $presupuesto;?>);validarCampoLleno('valor-idea')" min="0" onkeypress="return event.charCode >= 48">
	<small><p style="color:red;" id="mesageValPresupuesto"></p></small>
</div>
<div class="col-xs-12 col-md-8">
	<p>Link video</p>
	<textarea name="link-video" id="link-video" class="form-control" cols="30" rows="2" onclick="validarCampoLleno('link-video')" onkeyup="validarCampoLleno('link-video')" placeholder="En un corto video de maximo 5 minutos describa su idea."></textarea>
</div>