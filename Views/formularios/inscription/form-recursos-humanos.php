<div class="col-xs-12">
	<div class="col-xs-12">
		<h2>Recursos Humanos
	        <a class="btn-floating btn-small red right" id="btn-recursosH" onclick="desplegarRecursosHumanos()" id="btnrg2to1" style="float: right;">
	        	<i class="fa fa-angle-down"></i>
	        </a>
	        <input type="hidden" id="valorReHu" value="0">
	    </h2>
		<div class="col-xs-12" id="contentRecursosHumanos" style="display: none;">
			<div class="col-xs-12">
				<h3>Personal Externo</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="perslExt" id="perslExt" class="radio-form" value="1" onclick="si_no(1,'numPersonalExt','contentPersonalExterno')"> Si
					<input type="radio" name="perslExt" id="perslExt" class="radio-form" value="2" onclick="si_no(2,'numPersonalExt','contentPersonalExterno')" > No
				</div>
				<div class="col-xs-12 col-md-3">
					<input type="number" name="numPersonalExt" id="numPersonalExt" class="form-control" onclick="validarCampoLleno('numPersonalExt');numeroPersonaExternas()" onkeyup="validarCampoLleno('numPersonalExt');numeroPersonaExternas()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de personas" style="display: none;" />
				</div>
			</div>
			<div id="contentPersonalExterno" style="display: none;">
				<div class="col-xs-3">
				</div>
				<div class="col-xs-12" id="contentNumPersonasExternas"></div>
			</div>

			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Aprendices Sin Contrato de apredizaje)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="persIntASC" id="persIntASC" class="radio-form" value="1" onclick="si_no(1,'numPersonalIntASC','contentPersonalInternoASC')"> Si
					<input type="radio" name="persIntASC" id="persIntASC" class="radio-form" value="2" onclick="si_no(2,'numPersonalIntASC','contentPersonalInternoASC')" > No
				</div>
				<div class="col-xs-12 col-md-3">
					<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" onclick="validarCampoLleno('numPersonalIntASC');numeroPersonaInternasASC()" onkeyup="validarCampoLleno('numPersonalIntASC');numeroPersonaInternasASC()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de personas" style="display: none;">
				</div>				
			</div>
			<div  id="contentPersonalInternoASC" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasASC"></div>
			</div>

			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Aprendices Con Contrato de apredizaje)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="persIntACC" id="persIntACC" class="radio-form" value="1" onclick="si_no(1,'numPersonalIntACC','contentPersonalInternoACC')"> Si
					<input type="radio" name="persIntACC" id="persIntACC" class="radio-form" value="2" onclick="si_no(2,'numPersonalIntACC','contentPersonalInternoACC')" > No
				</div>
				<div class="col-xs-12 col-md-3">
					<input type="number" name="numPersonalIntACC" id="numPersonalIntACC" class="form-control" onclick="validarCampoLleno('numPersonalIntACC');numeroPersonaInternasACC()" onkeyup="validarCampoLleno('numPersonalIntACC');numeroPersonaInternasACC()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de personas" style="display: none;">
				</div>
			</div>
			<div  id="contentPersonalInternoACC" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasACC"></div>
			</div>
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Instructores)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="persIntInstructores" id="persIntInstructores" class="radio-form" value="1" onclick="si_no(1,'numPersonalIntInstructores','contentPersonalInternoInstructores')"> Si
					<input type="radio" name="persIntInstructores" id="persIntInstructores" class="radio-form" value="2" onclick="si_no(2,'numPersonalIntInstructores','contentPersonalInternoInstructores')" > No					
				</div>
				<div class="col-xs-12 col-md-3">
					<input type="number" name="numPersonalIntInstructores" id="numPersonalIntInstructores" class="form-control" onclick="validarCampoLleno('numPersonalIntInstructores');numeroPersonaInternasInstructores()" onkeyup="validarCampoLleno('numPersonalIntInstructores');numeroPersonaInternasInstructores()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de personas" style="display: none;">
				</div>
			</div>
			<div  id="contentPersonalInternoInstructores" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasInstructores"></div>
			</div>

			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Otros-TP-TA-SENNOVA)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="persIntOtros" id="persIntOtros" class="radio-form" value="1" onclick="si_no(1,'numPersonalIntOtros','contentPersonalInternoOtros')"> Si
					<input type="radio" name="persIntOtros" id="persIntOtros" class="radio-form" value="2" onclick="si_no(2,'numPersonalIntOtros','contentPersonalInternoOtros')" > No
				</div>
				<div class="col-xs-3">
					<input type="number" name="numPersonalIntOtros" id="numPersonalIntOtros" class="form-control" onclick="validarCampoLleno('numPersonalIntOtros');numeroPersonaInternasOtros()" onkeyup="validarCampoLleno('numPersonalIntOtros');numeroPersonaInternasOtros()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de personas" style="display: none;">
				</div>
			</div>
			<div  id="contentPersonalInternoOtros" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasOtros"></div>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
		
	<div class="col-xs-12">
		<h2>Aliados Externos y/o Contrapartida
			<a class="btn-floating btn-small red right" id="btn-aliados" onclick="desplegarAliados()" id="btnrg2to1" style="float: right;">
	        	<i class="fa fa-angle-down"></i>
	        </a>
	        <input type="hidden" id="valorAliados" value="0">
	    </h2>
		<div class="col-xs-12" id="contenAliados" style="display: none;">
			<div class="col-xs-12">
				<p>Aliados Externos</p>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="AliadosExt" id="AliadosExt" class="radio-form" value="1" onclick="si_no(1,'numAliadosExternos','contentNumAliadosExternos')"> Si
						<input type="radio" name="AliadosExt" id="AliadosExt" class="radio-form" value="2" onclick="si_no(2,'numAliadosExternos','contentNumAliadosExternos')" > No
					</div>
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numAliadosExternos" id="numAliadosExternos" class="form-control" onclick="numeroAliadosExternos()" onkeyup="numeroAliadosExternos()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Aliados externos" style="display: none;">
					</div>
				</div>
				<div class="col-xs-12" id="contentNumAliadosExternos"></div>
			</div>
			<div class="col-xs-12"><hr></div>

			<div class="col-xs-12">
				<p>Aliados Internos</p>
				<div class="col-xs-12 col-md-12">
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="AliadosInternos" id="AliadosInternos" class="radio-form" value="1" onclick="si_no(1,'numAliadosInternos','contentNumAliadosInternos')"> Si
						<input type="radio" name="AliadosInternos" id="AliadosInternos" class="radio-form" value="2" onclick="si_no(2,'numAliadosInternos','contentNumAliadosInternos')" > No
					</div>
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numAliadosInternos" id="numAliadosInternos" class="form-control" onclick="numeroAliadosInternos()" onkeyup="numeroAliadosInternos()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Aliados internos" style="display: none;">
					</div>
				</div>
				<div class="col-xs-12" id="contentNumAliadosInternos"></div>
			</div>
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<p>Ciudades y/o municipios de influencia</p>
				<div class="col-xs-12 col-md-12">
					<p>Ciudades</p>
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="ciudadesInfluencia" id="ciudadesInfluencia" class="radio-form" value="1" onclick="si_no(1,'numCiudadesInfluencia','contentNumCiudadesInfluencia')"> Si
						<input type="radio" name="ciudadesInfluencia" id="ciudadesInfluencia" class="radio-form" value="2" onclick="si_no(2,'numCiudadesInfluencia','contentNumCiudadesInfluencia')" > No
					</div>
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numCiudadesInfluencia" id="numCiudadesInfluencia" class="form-control" onclick="numeroCiudadesInfluencia()" onkeyup="numeroCiudadesInfluencia()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Ciudades" style="display: none;">
					</div>
				</div>
				<div class="col-xs-12" id="contentNumCiudadesInfluencia"></div>
				<div class="col-xs-12"><hr></div>
				<div class="col-xs-12 col-md-12">
					<p>Municipios</p>
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="municipiosInfluencia" id="municipiosInfluencia" class="radio-form" value="1" onclick="si_no(1,'numMunicipiosInfluencia','contentNumMunicipiosInfliencia')"> Si
						<input type="radio" name="municipiosInfluencia" id="municipiosInfluencia" class="radio-form" value="2" onclick="si_no(2,'numMunicipiosInfluencia','contentNumMunicipiosInfliencia')" > No
					</div>
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numMunicipiosInfluencia" id="numMunicipiosInfluencia" class="form-control" onclick="numeroMunicipiosInfluencia()" onkeyup="numeroMunicipiosInfluencia()" min="0" onkeypress="return event.charCode >= 48" placeholder="# de municipios" style="display: none;">
					</div>
				</div>
				<div class="col-xs-12" id="contentNumMunicipiosInfliencia"></div>
			</div>
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<p>Descripción de la alianza y objetivos</p>
				<textarea name="descripcionAlianza" id="descripcionAlianza" class="form-control" cols="30" rows="3"></textarea>
			</div>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-12">
		<h2>Recursos Sennova
			<a class="btn-floating btn-small red right" id="btn-recursosS" onclick="deplegarRecursosSennova()" id="btnrg2to1" style="float: right;">
	        	<i class="fa fa-angle-down"></i>
	        </a>
	        <input type="hidden" id="valorReSe" value="0">
		</h2>
		<div class="col-xs-12" id="contentRecursosSennova" style="display: none;">

			<!-- Investigacion & innovacion-->
			<?php if ($linea == 1 || $linea==2 ) { $dis_1='block';}else{$dis_1='none';} ?>

			<div class="conten_1" style="display: <?php echo $dis_1;?>">
			<div class="col-xs-12">
				<p>Otros servicios profesionales indirectos (Aprendices)</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optOtrosServPersIndirectos" id="optOtrosServPersIndirectos" class="radio-form" value="1" onclick="sinoRadios('contenoptOtrosServPersIndirectos',1)"> Si
					<input type="radio" name="optOtrosServPersIndirectos" id="optOtrosServPersIndirectos" class="radio-form" value="2" onclick="sinoRadios('contenoptOtrosServPersIndirectos',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" onclick="validarCampoLleno('otrosservPersonalesIndirectos');" onkeyup="validarCampoLleno('otrosservPersonalesIndirectos');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
			

			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea==4 || $linea == 5 ) { $dis_2='block';}else{$dis_2='none';} ?>
			
			<div class="conten_2" style="display: <?php echo $dis_2;?>">
			<div class="col-xs-12">
				<p>Servicios Personales Indirectos</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="ServPersIndirectos" id="ServPersIndirectos" class="radio-form" value="1" onclick="sinoRadios('contenServPersIndirectos',1)"> Si
					<input type="radio" name="ServPersIndirectos" id="ServPersIndirectos" class="radio-form" value="2" onclick="sinoRadios('contenServPersIndirectos',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenServPersIndirectos" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="servPersonalesIndirectos" id="servPersonalesIndirectos" class="form-control" onclick="validarCampoLleno('servPersonalesIndirectos');" onkeyup="validarCampoLleno('servPersonalesIndirectos');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionservPersonalesIndirectos" id="descripcionservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>


			


			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_3='block';}else{$dis_3='none';} ?>
			
			<div class="conten_3" style="display: <?php echo $dis_3;?>">
			<div class="col-xs-12">
				<p>Equipo de sistemas</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optequipoSistemas" id="optequipoSistemas" class="radio-form" value="1" onclick="sinoRadios('contenoptEquipoSistemas',1)"> Si
					<input type="radio" name="optequipoSistemas" id="optequipoSistemas" class="radio-form" value="2" onclick="sinoRadios('contenoptEquipoSistemas',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenoptEquipoSistemas" style="display: none;">
					<div class="col-xs-3">
						<p>Valor Equipo</p>				
						<input type="number" name="equipoSistemas" id="equipoSistemas" class="form-control" onclick="validarCampoLleno('equipoSistemas');" onkeyup="validarCampoLleno('equipoSistemas');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionSistemas" id="descripcionSistemas" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
	
			
			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_4='block';}else{$dis_4='none';} ?>
			
			<div class="conten_4" style="display: <?php echo $dis_4;?>">
			<div class="col-xs-12">
				<p>Software</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="software" id="software" class="radio-form" value="1" onclick="sinoRadios('contentSoftware',1)"> Si
					<input type="radio" name="software" id="software" class="radio-form" value="2" onclick="sinoRadios('contentSoftware',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentSoftware" style="display: none;">
					<div class="col-xs-3">
						<p>Valor software</p>				
						<input type="number" name="valorSoftware" id="valorSoftware" class="form-control" onclick="validarCampoLleno('valorSoftware');" onkeyup="validarCampoLleno('valorSoftware');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionSoftware" id="descripcionSoftware" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_5='block';}else{$dis_5='none';} ?>
			
			<div class="conten_5" style="display: <?php echo $dis_5;?>">
			<div class="col-xs-12">
				<p>Maquinaria Industrial</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="maquinariaInd" id="maquinariaInd" class="radio-form" value="1" onclick="sinoRadios('contentmaquinariaInd',1)"> Si
					<input type="radio" name="maquinariaInd" id="maquinariaInd" class="radio-form" value="2" onclick="sinoRadios('contentmaquinariaInd',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentmaquinariaInd" style="display: none;">
					<div class="col-xs-3">
						<p>Valor maquinaria</p>				
						<input type="number" name="valormaquinariaInd" id="valormaquinariaInd" class="form-control" onclick="validarCampoLleno('valormaquinariaInd');" onkeyup="validarCampoLleno('valormaquinariaInd');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionmaquinariaInd" id="descripcionmaquinariaInd" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>


			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_6='block';}else{$dis_6='none';} ?>
			
			<div class="conten_6" style="display: <?php echo $dis_6;?>">
			<div class="col-xs-12">
				<p>Otras Compras de equipos</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="OtrosEquipos" id="OtrosEquipos" class="radio-form" value="1" onclick="sinoRadios('contentOtrosEquipos',1)"> Si
					<input type="radio" name="OtrosEquipos" id="OtrosEquipos" class="radio-form" value="2" onclick="sinoRadios('contentOtrosEquipos',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentOtrosEquipos" style="display: none;">
					<div class="col-xs-3">
						<p>Valor maquinaria</p>				
						<input type="number" name="valorOtrosEquipos" id="valorOtrosEquipos" class="form-control" onclick="validarCampoLleno('valorOtrosEquipos');" onkeyup="validarCampoLleno('valorOtrosEquipos');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionOtrosEquipos" id="descripcionOtrosEquipos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
			

			<!--investigacion & innovacion & modenizacion & divulgacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 || $linea == 4) { $dis_7='block';}else{$dis_7='none';} ?>
			
			<div class="conten_7" style="display: <?php echo $dis_7;?>">
			<div class="col-xs-12">
				<p>Material para formacion profesional</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="matFormProfesional" id="matFormProfesional" class="radio-form" value="1" onclick="sinoRadios('contentmatFormProfesional',1)"> Si
					<input type="radio" name="matFormProfesional" id="matFormProfesional" class="radio-form" value="2" onclick="sinoRadios('contentmatFormProfesional',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentmatFormProfesional" style="display: none;">
					<div class="col-xs-3">
						<p>Valor maquinaria</p>				
						<input type="number" name="valormatFormProfesional" id="valormatFormProfesional" class="form-control" onclick="validarCampoLleno('valormatFormProfesional');" onkeyup="validarCampoLleno('valormatFormProfesional');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionmatFormProfesional" id="descripcionmatFormProfesional" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_8='block';}else{$dis_8='none';} ?>
			
			<div class="conten_8" style="display: <?php echo $dis_8;?>">
			<div class="col-xs-12">
				<p>Mantenimiento de maquinaria, equipo, transporte y software</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="mantenimientoMMETS" id="mantenimientoMMETS" class="radio-form" value="1" onclick="sinoRadios('contentmantenimientoMMETS',1)"> Si
					<input type="radio" name="mantenimientoMMETS" id="mantenimientoMMETS" class="radio-form" value="2" onclick="sinoRadios('contentmantenimientoMMETS',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentmantenimientoMMETS" style="display: none;">
					<div class="col-xs-3">
						<p>Valor maquinaria</p>				
						<input type="number" name="valormantenimientoMMETS" id="valormantenimientoMMETS" class="form-control" onclick="validarCampoLleno('valormantenimientoMMETS');" onkeyup="validarCampoLleno('valormantenimientoMMETS');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionmantenimientoMMETS" id="descripcionmantenimientoMMETS" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>


			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_9='block';}else{$dis_9='none';} ?>
			
			<div class="conten_9" style="display: <?php echo $dis_9;?>">
			<div class="col-xs-12">
				<p>Otras comunicaciones y transporte</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="comunicTransporte" id="comunicTransporte" class="radio-form" value="1" onclick="sinoRadios('contentcomunicTransporte',1)"> Si
					<input type="radio" name="comunicTransporte" id="comunicTransporte" class="radio-form" value="2" onclick="sinoRadios('contentcomunicTransporte',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentcomunicTransporte" style="display: none;">
					<div class="col-xs-3">
						<p>Valor maquinaria</p>				
						<input type="number" name="valorcomunicTransporte" id="valorcomunicTransporte" class="form-control" onclick="validarCampoLleno('valorcomunicTransporte');" onkeyup="validarCampoLleno('valorcomunicTransporte');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcioncomunicTransporte" id="descripcioncomunicTransporte" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_10='block';}else{$dis_10='none';} ?>
			
			<div class="conten_10" style="display: <?php echo $dis_10;?>">
			<div class="col-xs-12">
				<p>Adecuaciones y contrucciones</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="adecuacionesContrucciones" id="adecuacionesContrucciones" class="radio-form" value="1" onclick="sinoRadios('contentadecuacionesContrucciones',1)"> Si
					<input type="radio" name="adecuacionesContrucciones" id="adecuacionesContrucciones" class="radio-form" value="2" onclick="sinoRadios('contentadecuacionesContrucciones',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contentadecuacionesContrucciones" style="display: none;">
					<div class="col-xs-3">
						<p>Valor maquinaria</p>				
						<input type="number" name="valoradecuacionesContrucciones" id="valoradecuacionesContrucciones" class="form-control" onclick="validarCampoLleno('valoradecuacionesContrucciones');" onkeyup="validarCampoLleno('valoradecuacionesContrucciones');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionadecuacionesContrucciones" id="descripcionadecuacionesContrucciones" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
			

			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea == 4 || $linea==5) { $dis_11='block';}else{$dis_11='none';} ?>
			
			<div class="conten_11" style="display: <?php echo $dis_11;?>">
			<div class="col-xs-12">
				<p>Otros gastos por impresos y publicaciones</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="otrosGastosImpPub" id="otrosGastosImpPub" class="radio-form" value="1" onclick="sinoRadios('contenotrosGastosImpPub',1)"> Si
					<input type="radio" name="otrosGastosImpPub" id="otrosGastosImpPub" class="radio-form" value="2" onclick="sinoRadios('contenotrosGastosImpPub',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenotrosGastosImpPub" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="valorOtrosGastosImpPub" id="valorOtrosGastosImpPub" class="form-control" onclick="validarCampoLleno('valorOtrosGastosImpPub');" onkeyup="validarCampoLleno('valorOtrosGastosImpPub');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionvalorOtrosGastosImpPub" id="descripcionvalorOtrosGastosImpPub" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>




			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea == 4 || $linea==5) { $dis_12='block';}else{$dis_12='none';} ?>
			
			<div class="conten_12" style="display: <?php echo $dis_12;?>">
			<div class="col-xs-12">
				<p>Divulgación de actividades de gestion Institucional</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optDivulgacionActividades" id="optDivulgacionActividades" class="radio-form" value="1" onclick="sinoRadios('contenoptDivulgacionActividades',1)"> Si
					<input type="radio" name="optDivulgacionActividades" id="optDivulgacionActividades" class="radio-form" value="2" onclick="sinoRadios('contenoptDivulgacionActividades',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenoptDivulgacionActividades" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="valorDivulgacionGestionInst" id="valorDivulgacionGestionInst" class="form-control" onclick="validarCampoLleno('valorDivulgacionGestionInst');" onkeyup="validarCampoLleno('valorDivulgacionGestionInst');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionvalorDivulgacionGestionInst" id="descripcionvalorDivulgacionGestionInst" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>

			
			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea == 4 || $linea==5) { $dis_13='block';}else{$dis_13='none';} ?>
			
			<div class="conten_13" style="display: <?php echo $dis_13;?>">
			<div class="col-xs-12">
				<p>Viaticos y gastos de viajes al interior formacion profesional</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optViaticosFormProf" id="optViaticosFormProf" class="radio-form" value="1" onclick="sinoRadios('contenoptViaticosFormProf',1)"> Si
					<input type="radio" name="optViaticosFormProf" id="optViaticosFormProf" class="radio-form" value="2" onclick="sinoRadios('contenoptViaticosFormProf',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenoptViaticosFormProf" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="valorViaticosFormPro" id="valorViaticosFormPro" class="form-control" onclick="validarCampoLleno('valorViaticosFormPro');" onkeyup="validarCampoLleno('valorViaticosFormPro');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionvalorViaticosFormPro" id="descripcionvalorViaticosFormPro" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>

	

			<!-- investigacion & innovacion -->
			<?php if ($linea==1 || $linea==2 ) { $dis_14='block';}else{$dis_14='none';} ?>
			
			<div class="conten_14" style="display: <?php echo $dis_14;?>">
			<div class="col-xs-12">
				<p>Gastos bienestar Alumnos</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optBienestarAlumnos" id="optBienestarAlumnos" class="radio-form" value="1" onclick="sinoRadios('contenoptBienestarAlumnos',1)"> Si
					<input type="radio" name="optBienestarAlumnos" id="optBienestarAlumnos" class="radio-form" value="2" onclick="sinoRadios('contenoptBienestarAlumnos',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenoptBienestarAlumnos" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="valorBienestarAlumnos" id="valorBienestarAlumnos" class="form-control" onclick="validarCampoLleno('valorBienestarAlumnos');" onkeyup="validarCampoLleno('valorBienestarAlumnos');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionvalorBienestarAlumnos" id="descripcionvalorBienestarAlumnos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion -->
			<?php if ($linea==1  ) { $dis_15='block';}else{$dis_15='none';} ?>
			
			<div class="conten_15" style="display: <?php echo $dis_15;?>">
			<div class="col-xs-12">
				<p>Monitorias</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optMonitorias" id="optMonitorias" class="radio-form" value="1" onclick="sinoRadios('contenoptMonitorias',1)"> Si
					<input type="radio" name="optMonitorias" id="optMonitorias" class="radio-form" value="2" onclick="sinoRadios('contenoptMonitorias',2)"> No
				</div>
				<div class="col-xs-12 col-md-3">
				</div>
				<div class="col-xs-12" id="contenoptMonitorias" style="display: none;">
					<div class="col-xs-3">
						<p>Valor</p>				
						<input type="number" name="valorMonitorias" id="valorMonitorias" class="form-control" onclick="validarCampoLleno('valorMonitorias');" onkeyup="validarCampoLleno('valorMonitorias');" min="0" placeholder="$COP" />
					</div>
					<div class="col-xs-12">
						<p>Descripcion</p>
						<textarea name="descripcionvalorMonitorias" id="descripcionvalorMonitorias" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"></textarea>
					</div>
				</div>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>
</div>
