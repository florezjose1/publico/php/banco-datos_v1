<div class="col-xs-12"><br><br></div>
<div class="col-xs-12">
	<div class="steps f2">
		<?php include 'inc/linea-avance.php'; ?>
	</div>
</div>
<div class="col-md-12"><br></div>

<?php
$linea = 0;
if ($_GET['linea']=='investigacion') {
	$linea = 1;
}else{
	if ($_GET['linea']=='innovacion') {
		$linea = 2;
	}else{
		if ($_GET['linea']=='modernizacion') {
			$linea = 3;
		}else{
			if ($_GET['linea']=='divulgacion') {
				$linea = 4;
			}else{
				if ($_GET['linea']=='servicios-tecnologicos') {
					$linea = 5;
				}else{
					if ($_GET['linea']=='concursos') {
						$linea = 6;
					}
				}
			}
		}
	}
}
$var = htmlentities(addslashes($_GET['registro']));

if (!empty($_GET['form']=='datos-personales') ) {
    $datosPersonales = $clase->datosPersonalesRadicador($var);
	if ($datosPersonales!=false) {
		while ($datos = mysqli_fetch_object($datosPersonales)) {
			include 'inc/datos-personales-directo.php';
		}
	}else{
    	echo '<div class="contentDatosPersonales">
			<div class="col-xs-12">
				<form name="datos-personales" enctype="multipart/form-data" method="post">
					<input type="hidden" id="accesoRegistro" value="0">
					<input type="hidden" name="id_r" value="'.$_GET['registro'].'" >';
					include 'form-datos-personales.php';
				echo '</form>
				<div class="col-xs-12">
					<br>
					<div id="messageAlert-datosPersonales-inscripcion" style="display: none;"></div>
					<div class="pull-right">
						<button class="btn btn-info" id="guardarDatosPersonales" onclick="enviarDatosForm(1,'.$_GET['ac'].',\''.$_GET['linea'].'\','.$_GET['registro'].')">Guardar datos</button>
					</div>
				</div>
				<div class="col-md-12"><br><br><br><br><br></div>
			</div>
		</div>';
	}

}else{
	if (!empty($_GET['form']=='informacion-proyecto') ) {
		$datosPersonales = $clase->datosPersonalesRadicador($var);
		if ($datosPersonales!=false) {
			$sql = "`dpr_id` = '$var' ";
			$datosProyecto = $clase->datosProyecto($sql);
			if ($datosProyecto!=false) {
				while ($datos = mysqli_fetch_object($datosProyecto)) {
					$cod = $datos->ip_id;
					include 'inc/informacion-proyecto-directo.php';
				}
			}else{
				echo '
					<div class="contentInformacionProyecto" >
						<div class="col-xs-12">
							<form name="datos-proyecto" enctype="multipart/form-data" method="post">
								<input type="hidden" id="accesoRegistro" value="0">
								<input type="hidden" name="linea" value="'.$linea.'">
								<input type="hidden" name="codRadicador" value="'.$_GET['registro'].'">
								';
								include 'form-informacion-proyecto.php';
							echo '</form>
							<div class="col-xs-12">
								<br>
								<div id="messageAlert-informacion_proyecto" style="display: none;"></div>
								<div class="pull-right">
									<button class="btn btn-info" id="guardarDatosPersonales" onclick="enviarDatosForm(2,'.$_GET['ac'].',\''.$_GET['linea'].'\','.$_GET['registro'].')">Guardar datos</button>
								</div>
							</div>
						</div>
						<div class="col-md-12"><br><br><br><br><br></div>
					</div>
				';
			}
		}else{
			echo '
				<div class="contentInformacionProyecto" >
					<div class="col-xs-12">
						<form name="datos-proyecto" enctype="multipart/form-data" method="post">
							<input type="hidden" id="accesoRegistro" value="1">
							<input type="hidden" name="linea" value="'.$linea.'">
							<input type="hidden" name="codRadicador" value="'.$_GET['registro'].'">
								';
							include 'form-informacion-proyecto.php';
						echo '</form>
						<div class="col-xs-12">
							<br>
							<div id="messageAlert-informacion_proyecto" style="display: none;"></div>
							<div class="pull-right">
								<button class="btn btn-info" id="guardarDatosPersonales" onclick="enviarDatosForm(2,'.$_GET['ac'].',\''.$_GET['linea'].'\','.$_GET['registro'].')">Guardar datos</button>
							</div>
							</div>
					</div>
					<div class="col-md-12"><br><br><br><br><br></div>
				</div>
			';
		}
	}else{
		if (!empty($_GET['form']=='recursos-humanos')) {
			$datosPersonales = $clase->datosPersonalesRadicador($var);
			$sql = "`dpr_id` = '$var' ";
			$datosProyecto = $clase->datosProyecto($sql);
			if ($datosPersonales!=false && $datosProyecto!=false) {
				echo '<div class="contentRecursosHumanos" >
					<div class="col-xs-12">
						<form name="recursos-humanos" enctype="multipart/form-data" method="post">
							<input type="hidden" id="accesoRegistro" value="0">
							<input type="hidden" name="linea" value="'.$_GET['linea'].'">
							<input type="hidden" name="codRadicador" value="'.$_GET['registro'].'">
							';
							include 'form-recursos-humanos.php';
						echo '</form>
						<div class="col-xs-12">
							<br>
							<div id="messageAlert-recursosHumanos" style="display: none;"></div>
							<div class="pull-right">
								<button class="btn btn-info" id="guardarDatosPersonales" onclick="enviarDatosForm(3,'.$_GET['ac'].',\''.$_GET['linea'].'\','.$_GET['registro'].')">Guardar datos</button>
							</div>
						</div>
					</div>
					<div class="col-md-12"><br><br><br><br><br></div>
				</div>';
			}else{
				echo '<div class="contentRecursosHumanos" >
					<div class="col-xs-12">
						<form name="recursos-humanos" enctype="multipart/form-data" method="post">
							<input type="hidden" id="accesoRegistro" value="1">
							<input type="hidden" name="linea" value="'.$_GET['linea'].'">
							<input type="hidden" name="codRadicador" value="'.$_GET['registro'].'">
							';
							include 'form-recursos-humanos.php';
						echo '</form>
						<div class="col-xs-12">
							<br>
							<div id="messageAlert-recursosHumanos" style="display: none;"></div>
							<div class="pull-right">
								<button class="btn btn-info" id="guardarDatosPersonales" onclick="enviarDatosForm(3,'.$_GET['ac'].',\''.$_GET['linea'].'\','.$_GET['registro'].')">Guardar datos</button>
							</div>
						</div>
					</div>
					<div class="col-md-12"><br><br><br><br><br></div>
				</div>';
			}

		}
	}
}



?>