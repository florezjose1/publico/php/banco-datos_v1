<div class="col-xs-12">
	<div class="col-xs-12 col-md-12">
		<div class="col-xs-10 col-md-9">
			<h3>Datos de quien Radica el Proyecto</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-autoRadicador" onclick="multiDespliegue('btn-autoRadicador','valorAutorRadicador','contentDatosRadicador')" id="btnrg2to1" style="float: right;">
		       	<i class="fa fa-angle-down"></i>
		    </a>
		    <input type="hidden" id="valorAutorRadicador" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosRadicador" style="display: none;">
			<div class="col-xs-12 col-md-6">
				<p>Nombre(s) </p>
				<input type="text" name="nombreRadicador" id="nombreRadicador" class="form-control" onclick="validarCampoLleno('nombreRadicador')" onkeyup="validarCampoLleno('nombreRadicador')">
			</div>
			<div class="col-xs-12 col-md-6">
				<p>Apellidos(s)</p>
				<input type="text" name="apellidoRadicador" id="apellidoRadicador" class="form-control" onclick="validarCampoLleno('apellidoRadicador')" onkeyup="validarCampoLleno('apellidoRadicador')">
			</div>
			<div class="col-xs-12 col-md-4">
				<p>Identificacion</p>
				<input type="number" name="identificacionRadicador" id="identificacionRadicador" class="form-control" min="0" onkeypress="return event.charCode >= 48" onclick="validarCampoLleno('identificacionRadicador')" onkeyup="validarCampoLleno('identificacionRadicador')">
			</div>
			<div class="col-xs-12 col-md-4">
				<p>Email</p>
				<input type="email" name="emailRadicador" id="emailRadicador" class="form-control" onclick="validarEmail('emailRadicador','mesageValEmailRa')" onkeyup="validarEmail('emailRadicador','mesageValEmailRa')">
				<p id="mesageValEmailRa"></p>
			</div>
			<div class="col-xs-12 col-md-4">
				<p>Telefono</p>
				<input type="number" name="telefonoRadicador" id="telefonoRadicador" class="form-control" min="0" onkeypress="return event.charCode >= 48" onclick="validarCampoLleno('telefonoRadicador')" onkeyup="validarCampoLleno('telefonoRadicador')">
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-12 col-md-12">
		<div class="col-xs-10 col-md-9">
			<h3>Autores del proyecto</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-autores" onclick="multiDespliegue('btn-autores','valorAutores','contentDatosAutor')"" id="btnrg2to1" style="float: right;">
		       	<i class="fa fa-angle-down"></i>
		    </a>
		    <input type="hidden" id="valorAutores" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosAutor" style="display: none;">
			<divc class="col-xs-12 col-md-3">
				<input type="number" name="autores" id="autores" class="form-control" onclick="numeroAutores();validarCampoLleno('autores')" onkeyup="numeroAutores();validarCampoLleno('autores')" min="0" onkeypress="return event.charCode >= 48" placeholder="# de autores">
			</divc>
			<div class="col-xs-12" id="contentAutores"></div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
</div>
