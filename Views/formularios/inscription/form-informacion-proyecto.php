<div class="col-xs-12 col-md-12">
	<div class="col-xs-10 col-md-9">
		<h2>Título del proyecto</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-tituloPro" onclick="multiDespliegue('btn-tituloPro','valorTituloPro','contentTituloPro')" id="btnrg2to1" style="float: right;">
		       	<i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorTituloPro" value="0">
	</div>
	<div class="col-xs-12" id="contentTituloPro" style="display: none;">
		<input type="text" name="titulo" id="titulo" class="form-control" placeholder="Título del proyecto" onclick="validarCampoLleno('titulo')" onkeyup="validarCampoLleno('titulo')">
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h2>Aplica al posconflicto</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-posConflicto" onclick="multiDespliegue('btn-posConflicto','valorposConflicto','contentposConflicto')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorposConflicto" value="0">
	</div>
	<div class="col-xs-12" id="contentposConflicto" style="display: none;">
		<div class="col-xs-12">
			<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)"> Si
			<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)"> No
		</div>
		<div class="col-xs-12" id="contentPosConflicto">
			<div class="col-xs-3">
				<h5>Municipios a Impactar</h5>
				<input type="number" name="municipios" id="municipios" class="form-control" 
					onclick="validarCampoLleno('municipios');numeroMunicipios();"
					onkeyup="validarCampoLleno('municipios');numeroMunicipios();" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Municipios" >
			</div>
			<div class="col-xs-12" id="contentMunicipios"></div>
			<div class="col-xs-12">
				<h5>Descripcion de la Estrategia</h5>
				<textarea name="estrategiaPosConflicto" id="estrategiaPosConflicto" class="form-control" cols="30" rows="5" placeholder="Descripción de la estrategia y por qué impacta al posconficto?"  onclick="validarCampoLleno('estrategiaPosConflicto')" onkeyup="validarCampoLleno('estrategiaPosConflicto')"></textarea>
			</div>
			<div class="col-xs-12">
				<p>¿Cuénta con recursos del Posconflicto?</p>
				<div class="col-xs-12 col-md-2">
					<input type="radio" name="optPosconflicto" class="radio-form" value="1" onclick="recursosPos(1)"> Si
					<input type="radio" name="optPosconflicto" class="radio-form" value="2" onclick="recursosPos(2)"> No
				</div>
				<div class="col-xs-3" id="contentRecursosPosconflicto" style="display: none;">
					<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)"  onclick="validarCampoLleno('recursosPosconflicto')" onkeyup="validarCampoLleno('recursosPosconflicto')">
				</div>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h2>Jusificación y planteamiento del problema</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion4" onclick="multiDespliegue('btn-agrupacion4','valoragrupacion4','contentagrupacion4')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion4" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion4" style="display: none;">
		<div class="col-xs-12">
			<h4>Antecedentes y justificacion del proyecto</h4>
			<textarea name="antecedentesProyecto" id="antecedentesProyecto" class="form-control" cols="30" rows="5"  onclick="validarCampoLleno('antecedentesProyecto')" onkeyup="validarCampoLleno('antecedentesProyecto')"></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Link video</h4>
			<textarea name="linkVideo" id="linkVideo" class="form-control" cols="30" rows="2" placeholder="En un corto video de maximo 5 minutos describa su idea." onclick="validarCampoLleno('linkVideo')" onkeyup="validarCampoLleno('linkVideo')"></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Plantemiento del problema y/o necesidades</h4>
			<textarea name="plantProNecesidades" id="plantProNecesidades" class="form-control" cols="30" rows="5"  onclick="validarCampoLleno('plantProNecesidades')" onkeyup="validarCampoLleno('plantProNecesidades')"></textarea>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>
	
	<div class="col-xs-10 col-md-9">
		<h2>Objetivos</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-objetivos" onclick="multiDespliegue('btn-objetivos','valorobjetivos','contentobjetivos')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorobjetivos" value="0">
	</div>
	<div class="col-xs-12" id="contentobjetivos" style="display: none;">
		<div class="col-xs-12">
			<h4>Objetivo General</h4>
			<textarea name="objetivoGeneral" id="objetivoGeneral" class="form-control" cols="30" rows="3"  onclick="validarCampoLleno('objetivoGeneral')" onkeyup="validarCampoLleno('objetivoGeneral')"></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-3">
			<h4>Objetivo Específico</h4>
			<input type="number" name="objEspecifico" id="objEspecifico" class="form-control" onclick="numeroObjEsp();validarCampoLleno('objEspecifico')" onkeyup="numeroObjEsp();validarCampoLleno('objEspecifico')" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Obj. Especificos" >
		</div>
		<div class="col-xs-12" id="contentObjetivosEspecificos"></div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h2>Fechas</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-Fechas" onclick="multiDespliegue('btn-Fechas','valorFechas','contentFechas')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorFechas" value="0">
	</div>
	<div class="col-xs-12" id="contentFechas" style="display: none;">
		<div class="col-xs-12 col-md-6">
			<h4>Fecha Inicio del proyecto</h4>
			<input type="date" name="fechaInicioPro" id="fechaInicioPro" class="form-control"  onclick="validarCampoLleno('fechaInicioPro')" onkeyup="validarCampoLleno('fechaInicioPro')">
		</div>
		<div class="col-xs-12 col-md-6">
			<h4>Fecha Fin del proyecto</h4>
			<input type="date" name="fechaFinPro" id="fechaFinPro" class="form-control" onclick="validarCampoLleno('fechaFinPro')" onkeyup="validarCampoLleno('fechaFinPro')">
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h2>Datos Adicionales</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion5" onclick="multiDespliegue('btn-agrupacion5','valoragrupacion5','contentagrupacion5')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion5" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion5" style="display: none;">
		<div class="col-xs-12">
			<h4>Nombre grupo de investigación</h4>
			<input type="text" name="grupoInvestigacion" id="grupoInvestigacion" class="form-control"  onclick="validarCampoLleno('grupoInvestigacion')" onkeyup="validarCampoLleno('grupoInvestigacion')">
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Codigo de GrupLAC</h4>
			<input type="text" name="grupoLac" id="grupoLac" class="form-control" placeholder="Ejemplo:COL0001048"  onclick="validarCampoLleno('grupoLac')" onkeyup="validarCampoLleno('grupoLac')" value="COL0171834">
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-4">
			<h4>Número de semilleros beneficiados</h4>
			<input type="number" name="semillerosBeneficiados" id="semillerosBeneficiados" class="form-control" onclick="numeroSemilleros();validarCampoLleno('semillerosBeneficiados');" onkeyup="numeroSemilleros();validarCampoLleno('semillerosBeneficiados');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Semilleros">
		</div>
		<div class="col-xs-12" id="contentSemilleros"></div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Numeros de programas de formacion beneficiados</h4>
			<div class="col-md-3">
				<input type="number" name="formacionesBenefiadas" id="formacionesBenefiadas" class="form-control" onclick="numeroFormaciones();validarCampoLleno('formacionesBenefiadas');" onkeyup="numeroFormaciones();validarCampoLleno('formacionesBenefiadas');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Formaciones">
			</div>
		</div>
		<div class="col-xs-12" id="contentFormacionesBenefiadas"></div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h2>Metodologia, resultados e impacto esperado</h2>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion6" onclick="multiDespliegue('btn-agrupacion6','valoragrupacion6','contentagrupacion6')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion6" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion6" style="display: none;">
		<div class="col-xs-12">
			<h4>Descripción de metodologia del proyecto</h4>
			<textarea name="descripMetodologia" id="descripMetodologia" class="form-control" cols="30" rows="5"  onclick="validarCampoLleno('descripMetodologia')" onkeyup="validarCampoLleno('descripMetodologia')"></textarea>
		</div>

		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Resultados esperados</h4>
		</div>
		<div class="col-xs-12" id="contentResultadosEsperados"></div>

		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Productos</h4>
		</div>
		<div class="col-xs-12" id="contentProductosResultado"></div>


		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Impacto esperado</h4>
			<textarea name="impactoEsperado" id="impactoEsperado" class="form-control" cols="30" rows="5" onclick="validarCampoLleno('impactoEsperado')" onkeyup="validarCampoLleno('impactoEsperado')" placeholder="Agregar impacto, el numero de prioridad segun documento guia de modernizacion CAPITULO 8.2.1 Criterios de priorización." ></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

</div>