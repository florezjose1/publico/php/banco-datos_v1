<?php 


$sql2 = " `fase_2` WHERE `f2_id`='$p'";
$datosPr_f2 = $clase->detallesProyectoObservatorio($sql2);


$sql3 = " `fase_3` WHERE `f3_id`='$p'";
$datosPr_f3 = $clase->detallesProyectoObservatorio($sql3);

$sql4 = " `fase_4` WHERE `f4_id`='$p'";
$datosPr_f4 = $clase->detallesProyectoObservatorio($sql4);


include 'inc/linea-avance.php';


$estado = $proy->estado_fase;
if ($_SESSION['rol']==1) {
	$var = 0;
}else{
	$var = 1;
}

if (!empty($_GET['fase'])) {
	if (is_numeric($_GET['fase'])) {
		if ($_GET['fase']==1) {
			$block = ' disabled="disabled"  ';
			include 'inc/fase_1/form-datos-personales.php';
			if ($var == 0 && $estado==0) {
				echo '
				<div class="col-xs-12"><hr></div>
				<div class="col-xs-12">
					<h3>Espacio de Indicaciones</h3>
					<textarea name="indicacionesFase_1" id="indicacionesFase_1" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto."></textarea>
					<div class="pull-right">
						<button class="btn btn-info" id="guardarDatosFase_2" onclick="alertaAprobarFase('.$_GET['id'].','.$_GET['fase'].')">Aprobar Fase</button>
						</div>
				</div>';
			}
		}else{
			if ($_GET['fase']==2) {
				if ($datosPr_f2==false && $var == 1) {
					$block = '';
					echo '<div class="contentDatos_fase_2" >
						<div class="col-xs-12">
							<form name="datos-fase_2" enctype="multipart/form-data" method="post">';
								if ($estado==0) {
									echo '<input type="hidden" id="accesoRegistro" value="1">';
								}else{
									echo '<input type="hidden" id="accesoRegistro" value="0">';
								}
								echo '
								<input type="hidden" name="id_registro" value="'.$_GET['id'].'" >
								';
								include 'inc/fase_2/form-fase-2.php';
							echo '</form>
							<div class="col-xs-12">
								<br>
								<div id="messageAlert-fase_2-inscripcion" style="display: none;"></div>
								<div class="pull-right">
									<button class="btn btn-info" id="guardarDatosFase_2" onclick="enviarDatosForm(5)">Guardar datos</button>
								</div>
							</div>
						</div>
						<div class="col-md-12"><br><br><br><br><br></div>
					</div>';
				}else{
					$block = ' disabled="disabled"  ';
					
					if ($datosPr_f2!=false) {
						$datos = mysqli_fetch_object($datosPr_f2);
						include 'inc/fase_2/form-fase-2.php';
						$estado = $datos->estado_fase;
						if ($var == 0 && $estado==0) {
							echo '
							<div class="col-xs-12"><hr></div>
							<div class="col-xs-12">
								<h3>Espacio de Indicaciones</h3>
								<textarea name="indicacionesFase_2" id="indicacionesFase_2" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto."></textarea>
								<div class="pull-right">
									<button class="btn btn-info" id="guardarDatosFase_2" onclick="alertaAprobarFase('.$_GET['id'].','.$_GET['fase'].')">Aprobar Fase</button>
									</div>
							</div>';
						}
					}else{
						include 'inc/fase_2/form-fase-2.php';
					}

				}
			}else{
				if ($_GET['fase']==3) {

					if ($datosPr_f2!=false && $datosPr_f3==false && $var == 1) {
						$fase_2 = mysqli_fetch_object($datosPr_f2);
						$estado = $fase_2->estado_fase;
						$block = '';
						echo '<div class="contentDatos_fase_3" >
							<div class="col-xs-12">
								<form name="datos-fase_3" enctype="multipart/form-data" method="post">';
									if ($estado==0) {
										echo '<input type="hidden" id="accesoRegistro" value="2">';
									}else{
										echo '<input type="hidden" id="accesoRegistro" value="0">';
									}
									echo '
									<input type="hidden" name="id_registro" value="'.$_GET['id'].'" >
									';
									include 'inc/fase_3/form-fase-3.php';
								echo '</form>
								<div class="col-xs-12">
									<br>
									<div id="messageAlert-fase_3-inscripcion" style="display: none;"></div>
									<div class="pull-right">
										<button class="btn btn-info" id="guardarDatosFase_3" onclick="enviarDatosForm(6)">Guardar datos</button>
									</div>
								</div>
							</div>
							<div class="col-md-12"><br><br><br><br><br></div>
						</div>';
					}else{
						$block = ' disabled="disabled"  ';
						if ($datosPr_f3!=false) {
							$datos = mysqli_fetch_object($datosPr_f3);
							include 'inc/fase_3/form-fase-3.php';
							$estado = $datos->estado_fase;
							if ($var == 0 && $estado==0) {
								echo '
								<div class="col-xs-12"><hr></div>
								<div class="col-xs-12">
									<h3>Espacio de Indicaciones</h3>
									<textarea name="indicacionesFase_3" id="indicacionesFase_3" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto."></textarea>
									<div class="pull-right">
										<button class="btn btn-info" id="guardarDatosFase_2" onclick="alertaAprobarFase('.$_GET['id'].','.$_GET['fase'].')">Aprobar Fase</button>
										</div>
								</div>';
							}
						}else{
							include 'inc/fase_3/form-fase-3.php';
						}
					}
				}else{
					if ($_GET['fase']==4) {

						if ($datosPr_f3!=false && $datosPr_f4==false && $var == 1) {
							$fase_3 = mysqli_fetch_object($datosPr_f3);
							$estado = $fase_3->estado_fase;
							$block = '';
							echo '<div class="contentDatos_fase_4" >
								<div class="col-xs-12">
									<form name="datos-fase_4" enctype="multipart/form-data" method="post">';
										if ($estado==0) {
											echo '<input type="hidden" id="accesoRegistro" value="3">';
										}else{
											echo '<input type="hidden" id="accesoRegistro" value="0">';
										}
										echo '
										<input type="hidden" name="id_registro" value="'.$_GET['id'].'" >
										';
										include 'inc/fase_4/form-fase-4.php';
									echo '</form>
									<div class="col-xs-12">
										<br>
										<div id="messageAlert-fase_4-inscripcion" style="display: none;"></div>
										<div class="pull-right">
											<button class="btn btn-info" id="guardarDatosFase_4" onclick="enviarDatosForm(7)">Guardar datos</button>
										</div>
									</div>
								</div>
								<div class="col-md-12"><br><br><br><br><br></div>
							</div>';
						}else{
							$block = ' disabled="disabled"  ';
							if ($datosPr_f4!=false) {
								$datos = mysqli_fetch_object($datosPr_f4);
								include 'inc/fase_4/form-fase-4.php';
								$estado = $datos->estado_fase;
								if ($_SESSION['rol']==1 && $estado==0) {
									echo '
									<div class="col-xs-12"><hr></div>
									<div class="col-xs-12">
										<h3>Espacio de Indicaciones</h3>
										<textarea name="indicacionesFase_4" id="indicacionesFase_4" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto."></textarea>
										<div class="pull-right">
											<button class="btn btn-info" id="guardarDatosFase_2" onclick="alertaAprobarFase('.$_GET['id'].','.$_GET['fase'].')">Aprobar Fase</button>
											</div>
									</div>';
								}
							}else{
								include 'inc/fase_4/form-fase-4.php';
							}
						}
					}else{
						echo '<script> window.location="../404.php"; </script>';
					}
				}
			}
		}
	}
}else{
	$block = ' disabled="disabled"  ';
	if ($datosPr_f4!=false) {
		//include 'inc/fase_4/form-fase-4.php';
		echo '<script> window.location="login.php?ac='.$_GET['ac'].'&proyectos='.$_GET['proyectos'].'&id='.$_GET['id'].'&autor='.$_GET['autor'].'&fase=4";</script>';
	}else{
		if ($datosPr_f3!=false) {
			//include 'inc/fase_3/form-fase-3.php';
			echo '<script> window.location="login.php?ac='.$_GET['ac'].'&proyectos='.$_GET['proyectos'].'&id='.$_GET['id'].'&autor='.$_GET['autor'].'&fase=3";</script>';
		}else{
			if ($datosPr_f2!=false) {
				echo '<script> window.location="login.php?ac='.$_GET['ac'].'&proyectos='.$_GET['proyectos'].'&id='.$_GET['id'].'&autor='.$_GET['autor'].'&fase=2";</script>';

				//include 'inc/fase_2/form-fase-2.php';
			}else{
				echo '<script> window.location="login.php?ac='.$_GET['ac'].'&proyectos='.$_GET['proyectos'].'&id='.$_GET['id'].'&autor='.$_GET['autor'].'&fase=1";</script>';

				//include 'inc/fase_1/form-datos-personales.php';
			}
		}
	}
}




?>

