<div class="col-xs-12">
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-10 col-md-9">
		<h3>Linea Programática</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-lineas" onclick="multiDespliegue('btn-lineas','valorLineas','contentDatosLineas')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorLineas" value="0">
	</div>
	
	<?php

		if ($datosPr_f4!=false) {
			$li = $datos->f4_linea_programatica;
		}else{
			if ($datosPr_f3!=false) {
				if ( $var != 1) {
					$fase_3 = mysqli_fetch_object($datosPr_f3);
				}
				$li = $fase_3->f3_linea_programatica;
			}else{
				$li = "";
			}
		} 

	?>

	<div class="col-xs-12" id="contentDatosLineas" style="display: none;">
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="investigacion" id="opcion_1" onclick="unaOpcion(<?php if ($datosPr_f4!=false) {echo $datos->f4_linea_programatica;}else{echo '1';}?>)" <?php if ($li==1) {echo "checked";} ?> >
		   <label for="opcion_1">INVESTIGACIÓN APLICADA (Máximo $95.000.000).</label>
		</fieldset>
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="innovacion" id="opcion_2" onclick="unaOpcion(<?php if ($datosPr_f4!=false) {echo $datos->f4_linea_programatica;}else{echo '2';}?>)" <?php if ($li==2) {echo "checked";} ?>>
		   <label for="opcion_2">INNOVACIÓN Y DESARROLLO TECNOLÓGICO (Máximo $100.000.000).</label>
		</fieldset>
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="servicios" id="opcion_3" onclick="unaOpcion(<?php if ($datosPr_f4!=false) {echo $datos->f4_linea_programatica;}else{echo '3';}?>)" <?php if ($li==3) {echo "checked";} ?>>
		   <label for="opcion_3">FORTALECIMIENTO DE LA OFERTA DE SERVICIOS TECNOLÓGICOS A LAS EMPRESAS (Máximo $500.000.000).</label>
		</fieldset>
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="modernizacion" id="opcion_4" onclick="unaOpcion(<?php if ($datosPr_f4!=false) {echo $datos->f4_linea_programatica;}else{echo '4';}?>)" <?php if ($li==4) {echo "checked";} ?>>
		   <label for="opcion_4">ACTUALIZACIÓN Y MODERNIZACIÓN TECNOLÓGICA DE CENTRO (Máximo $400.000.000).</label>
		</fieldset>
	</div>
	<div class="col-xs-12"><hr></div>

	
	<div class="col-xs-10 col-md-9">
		<h3>Areas de conocimiento</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-AreasCon" onclick="multiDespliegue('btn-AreasCon','valorAreas','contentDatosAreas')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorAreas" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosAreas" style="display: none;">
		<div class="col-md-6">
			<p>Area #1 </p>
			<select name="area_conocimiento_1" id="" class="form-control">
				<?php 
				if ($datosPr_f4!=false) {
					echo '<option value="'.$datos->f4_area_conocimiento_1.'">'.$datos->f4_area_conocimiento_1.'</option>';
				}else{
					if ($datosPr_f3!=false) {
						echo '<option value="'.$fase_3->f3_area_conocimiento_1.'">'.$fase_3->f3_area_conocimiento_1.'</option>';
					}
				?>
				<option value="1. AGRONOMIA VETERINARIA Y AFINES">1. AGRONOMIA VETERINARIA Y AFINES</option>
				<option value="2. BELLAS ARTES">2. BELLAS ARTES</option>
				<option value="3. CIENCIAS DE LA EDUCACION">3. CIENCIAS DE LA EDUCACION</option>
				<option value="4. CIENCIAS DE LA SALUD">4. CIENCIAS DE LA SALUD</option>
				<option value="5. CIENCIAS SOCIALES Y HUMANAS">5. CIENCIAS SOCIALES Y HUMANAS</option>
				<option value="6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES">6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES</option>
				<option value="7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES">7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES</option>
				<option value="9. MATEMATICAS Y CIENCIAS NATURALES">9. MATEMATICAS Y CIENCIAS NATURALES</option>
				<option value="10. SIN CLASIFICAR">10. SIN CLASIFICAR</option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-6">
			<p>Area #2</p>
			<select name="area_conocimiento_2" id="" class="form-control">
				<?php 
				if ($datosPr_f4!=false) {
					echo '<option value="'.$datos->f4_area_conocimiento_1.'">'.$datos->f4_area_conocimiento_2.'</option>';
				}else{
					if ($datosPr_f3!=false) {
						echo '<option value="'.$fase_3->f3_area_conocimiento_1.'">'.$fase_3->f3_area_conocimiento_1.'</option>';
					}
				?>
				<option value="1. AGRONOMIA VETERINARIA Y AFINES">1. AGRONOMIA VETERINARIA Y AFINES</option>
				<option value="2. BELLAS ARTES">2. BELLAS ARTES</option>
				<option value="3. CIENCIAS DE LA EDUCACION">3. CIENCIAS DE LA EDUCACION</option>
				<option value="4. CIENCIAS DE LA SALUD">4. CIENCIAS DE LA SALUD</option>
				<option value="5. CIENCIAS SOCIALES Y HUMANAS">5. CIENCIAS SOCIALES Y HUMANAS</option>
				<option value="6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES">6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES</option>
				<option value="7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES">7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES</option>
				<option value="9. MATEMATICAS Y CIENCIAS NATURALES">9. MATEMATICAS Y CIENCIAS NATURALES</option>
				<option value="10. SIN CLASIFICAR">10. SIN CLASIFICAR</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Aplica al posconflicto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-posConflicto" onclick="multiDespliegue('btn-posConflicto','valorposConflicto','contentposConflicto')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorposConflicto" value="0">
	</div>
	<div class="col-xs-12" id="contentposConflicto" style="display: none;">
		<div class="col-xs-12">
			<?php 
			if ($datosPr_f4!=false) {
				if ($datos->f4_descrip_estra!=false) {
					echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" checked '.$block.'>  Si';
					echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" '.$block.'>  No';
				}else{
					echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" checked '.$block.'> Si';
					echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" checked '.$block.'> No';
				}
			}else{
				if ($datosPr_f3!=false) {
					if ($fase_3->f3_descrip_estra>0) {
						if ($fase_3->f3_descrip_estra!=false) {
							echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" checked '.$block.'> Si';
							echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" '.$block.'> No';
						}else{
							echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" '.$block.'> Si';
							echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" checked '.$block.'> No';
						}
					}else{
						echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" checked '.$block.'> Si';
						echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" checked '.$block.'> No';
					}
				}else{
					echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" checked '.$block.'> Si';
					echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" checked '.$block.'> No';
				}
			}
			?>
			
		</div>
		<?php 
			if ($datosPr_f4!=false) {
				$display = "block";
			}else{
				if ($datosPr_f3!=false) {
					if ($fase_3->f3_descrip_estra>0) {
						$display = "block";
					}else{
						$display = "none";
					}
				}else{
					$display = "none";

				}
			}
		?>

		<div class="col-xs-12" id="contentPosConflicto" style="display: <?php echo $display; ?>">
			<div class="col-xs-12">
				<h5>Descripcion de la Estrategia</h5>
				<textarea name="estrategiaPosConflicto" id="estrategiaPosConflicto" class="form-control" cols="30" rows="5" <?php echo $block; ?> placeholder="Descripción de la estrategia y por qué impacta al posconficto?"  onclick="validarCampoLleno('estrategiaPosConflicto')" onkeyup="validarCampoLleno('estrategiaPosConflicto')"><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_descrip_estra;
			}else{
				if ($datosPr_f3!=false) {
					if ($fase_3->f3_descrip_estra>0) {
						echo $fase_3->f4_descrip_estra;
					}
				}

			}
			?></textarea>
			</div>
			<div class="col-xs-12">
				<p>¿Cuénta con recursos del Posconflicto?</p>
				<?php 
				if ($datosPr_f4!=false) {
					if ($datos->f4_recursos_pos>0) {
						echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="optPosconflicto" class="radio-form" value="1" '.$block.' checked disabled="disabled"> Si
								<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="2" disabled="disabled"> No
							</div>
							<div class="col-xs-3" id="contentRecursosPosconflicto" style="">
								<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" '.$block.' class="form-control" placeholder="$(COP)" value="'.$datos->f4_recursos_pos.'">
							</div>
						';
					}else{
						echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="1" disabled="disabled"> Si
								<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="2" checked disabled="disabled"> No
							</div>
						';
					}
				}else{
					if ($datosPr_f3!=false) {
						if ($datosPr_f3!=false) {
							if ($fase_3->f3_recursos_pos>0) {
								echo '
									<div class="col-xs-12 col-md-2">
										<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="1" checked disabled="disabled"> Si
										<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="2" disabled="disabled"> No
									</div>
									<div class="col-xs-3" id="contentRecursosPosconflicto" style="">
										<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)" '.$block.' value="'.$fase_3->f3_recursos_pos.'">
									</div>
								';
							}else{
								echo '
									<div class="col-xs-12 col-md-2">
										<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="1" onclick="recursosPos(1)"> Si
										<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="2" onclick="recursosPos(2)" checked> No
									</div>
									<div class="col-xs-3" id="contentRecursosPosconflicto" style="display: none;">
										<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)" '.$block.' value="">
									</div>
								';
							}
						}else{
				?>
				<div class="col-xs-12 col-md-3">
					<input type="radio" name="optPosconflicto" id="optPosconflicto" class="radio-form" value="1" <?php echo $block; ?> onclick="recursosPos(1)"> Si
					<input type="radio" name="optPosconflicto" id="optPosconflicto" class="radio-form" value="2" <?php echo $block; ?> onclick="recursosPos(2)"> No
				</div>
				<div class="col-xs-3" id="contentRecursosPosconflicto" style="display: none;">
					<input type="number" <?php echo $block; ?> name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)"  onclick="validarCampoLleno('recursosPosconflicto')" onkeyup="validarCampoLleno('recursosPosconflicto')">
				</div>
				<?php } } }?>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>



	<div class="col-xs-10 col-md-9">
		<h3>Municipios donde desarrollará la idea</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-municipios" onclick="multiDespliegue('btn-municipios','valorMunicipios','contentDatosMunicipios')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorMunicipios" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosMunicipios" style="display: none;">
		<div class="col-xs-3">
			<h5>Municipios</h5>
			<input type="number" name="municipios" id="municipios" <?php echo $block; ?> class="form-control" onclick="validarCampoLleno('municipios');numeroMunicipios();" onkeyup="validarCampoLleno('municipios');numeroMunicipios();" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Municipios" value="<?php if ($datosPr_f4!=false) {	echo $datos->f4_municipios_idea;}else{echo $fase_3->f3_municipios_idea;}?>" >
		</div>
		<div class="col-xs-12" id="contentMunicipios">
			<?php 
			if ($datosPr_f4!=false || $datosPr_f3!=false ) {
				if ($datosPr_f4!=false) {
					$sql = " `f4_idea` = '$datos->f4_id' ";
					$var = 'readonly="readonly"' ;
				}else{
					if ($datosPr_f3!=false) {
						if ($datosPr_f3!=false) {	
							$var = '';
							$sql = " `f3_idea` = '$fase_3->f3_id' ";
						}
					}
				}
				$municipios = $clase->detallesMunicipios($sql);

				if ($municipios!=false) {
					$o = 1;
					while ($mun = mysqli_fetch_object($municipios)) {
						echo '
							<div class="col-xs-12 col-md-3">
					               <h5>Municipio #'.$o.'</h5>
					               <input type="text" name="nombreMunicipio_'.$o.'" id="nombreMunicipio_'.$o.'" '.$block.' class="form-control" '.$var.' value="'.$mun->mu_nombre.'" >
					           </div>
						';
						$o = $o + 1;
					}
				}

			}
			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Sector productivo al que impacta el proyecto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-sector" onclick="multiDespliegue('btn-sector','valorSector','contentDatosSector')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorSector" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosSector" style="display: none;">
		<div class="col-xs-12">
			<textarea name="sectorProImpacto" id="sectorProImpacto" <?php echo $block; ?> class="form-control" cols="30" rows="2" onclick="validarCampoLleno('sectorProImpacto')" onkeyup="validarCampoLleno('sectorProImpacto')"  ><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_sector_impacto;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_sector_impacto;
				}

			}?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Semilleros y formaciones beneficiadas e impacto esperado</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-semilleros" onclick="multiDespliegue('btn-semilleros','valorSemilleros','contentDatosSermilleros')" id="btnrg2to1" style="float: right;">
		   <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorSemilleros" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosSermilleros" style="display: none;">
		<div class="col-xs-4">
			<h4>Número de semilleros beneficiados</h4>
			<input type="number" name="semillerosBeneficiados" id="semillerosBeneficiados" class="form-control" <?php echo $block; ?> onclick="numeroSemilleros();validarCampoLleno('semillerosBeneficiados');" onkeyup="numeroSemilleros();validarCampoLleno('semillerosBeneficiados');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Semilleros" value="<?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_semilleros;
			}else{echo $fase_3->f3_semilleros; }?>">
		</div>
		<div class="col-xs-12" id="contentSemilleros">
			<?php 
			if ($datosPr_f4!=false||$datosPr_f3!=false) {
				if ($datosPr_f4!=false) {
					$var = 'readonly="readonly"' ;
					$sql = " `semilleros_ben` WHERE `f4_id` = '$datos->f4_id' ";
				}else{
					if ($datosPr_f3!=false) {
						$var = '' ;
						$sql = " `semilleros_ben` WHERE `f3_id` = '$fase_3->f3_id' ";
					}

				}
				$semilleros = $clase->detallesSemiFormaciones($sql);

				if ($semilleros!=false) {
					$o = 1;
					while ($sem = mysqli_fetch_object($semilleros)) {
						echo '
							<div class="col-xs-12 col-md-3">
					         <h5>Semillero #'.$o.'</h5>
					         <input type="text" name="nombreSemillero_'.$o.'" id="nombreSemillero_'.$o.'" '.$block.' class="form-control" value="'.$sem->sb_nombre.'"  '.$var.'>
					     	</div>
						';
						$o = $o + 1;
					}
				}
			}
			?>
		</div>


		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Numeros de programas de formacion beneficiados</h4>
			<div class="col-md-3">
				<input type="number" name="formacionesBenefiadas" id="formacionesBenefiadas" <?php echo $block; ?> class="form-control" onclick="numeroFormaciones();validarCampoLleno('formacionesBenefiadas');" onkeyup="numeroFormaciones();validarCampoLleno('formacionesBenefiadas');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Formaciones"  value="<?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_formaciones;
			}else{ echo $fase_3->f3_formaciones; }?>">
			</div>
		</div>
		<div class="col-xs-12" id="contentFormacionesBenefiadas">
			<?php 
			if ($datosPr_f4!=false||$datosPr_f3!=false) {
				if ($datosPr_f4!=false) {
					$var = 'readonly="readonly"' ;
					$sql = " `formaciones_ben` WHERE `f4_id` = '$datos->f4_id' ";		
				}else{
					if ($datosPr_f3!=false) {
						$var = '' ;
						$sql = " `formaciones_ben` WHERE `f3_id` = '$fase_3->f3_id' ";
					}
				}
				
				$formacion = $clase->detallesSemiFormaciones($sql);

				if ($formacion!=false) {
					$o = 1;
					while ($form = mysqli_fetch_object($formacion)) {
						echo '
							<div class="col-xs-12 col-md-3">
					         <h5>Formación #'.$o.'</h5>
					         <input type="text" name="nombreFormacion_'.$o.'" id="nombreFormacion_'.$o.'" '.$block.' class="form-control" value="'.$form->fb_nombre.'"  '.$var.'>
					      </div>
						';
						$o = $o + 1;
					}
				}
			}
			?>
		</div>

		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Impacto esperado</h4>
			<textarea name="impactoEsperado" id="impactoEsperado" class="form-control" cols="30" rows="3" <?php echo $block; ?> onclick="validarCampoLleno('impactoEsperado')" onkeyup="validarCampoLleno('impactoEsperado')"  ><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_impacto;
			}else{if ($datosPr_f3!=false) {echo $fase_3->f3_impacto;} }?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Datos Proyecto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-DatosProyecto" onclick="multiDespliegue('btn-DatosProyecto','valorrDatosProyecto','contentDatosDatosProyecto')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorrDatosProyecto" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosDatosProyecto" style="display: none;">
		<div class="col-xs-12">
			<p>Titulo</p>
			<textarea name="titulo" id="titulo" <?php echo $block; ?> class="form-control" cols="30" rows="3" placeholder="Afirmación precisa que hace referencia al tema en torno al cual gira el proyecto de investigación. Tenga en cuenta las siguientes preguntas: ¿Qué se va hacer?, ¿Cómo se va a realizar? y ¿A qué se le va a aplicar?"  <?php echo $block; ?>><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_titulo;
			}else{
				if ($datosPr_f3!=false) {
					
					echo $fase_3->f3_titulo;
				}else{
					echo $proy->f1_idea;
				}
			}
			?></textarea>
		</div>
		<div class="col-xs-12">
			<p>Introducción</p>
			<textarea name="introduccion" id="introduccion" <?php echo $block; ?> class="form-control" cols="30" rows="5" onclick="validarCampoLleno('introduccion')" onkeyup="validarCampoLleno('introduccion')" placeholder="Descripción de la situación del problema que soporta al estudio, además de la relevancia, pertinencia e impacto del proyecto de investigación." <?php echo $block; ?>><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_introduccion;
			}
			?></textarea>
		</div>
		<div class="col-xs-12">
			<p>Planteamiento del problema</p>
			<textarea name="planteamiento" id="planteamiento" <?php echo $block; ?> class="form-control" cols="30" rows="5" placeholder="Descripción de la situación del problema que soporta al estudio, además de la relevancia, pertinencia e impacto del proyecto de investigación." <?php echo $block; ?>><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_planteamiento;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_planteamiento;
				}else{
					echo $proy->f1_idea;
				}
			}
			?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Objetivos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-objetivos" <?php echo $block; ?> onclick="multiDespliegue('btn-objetivos','valorobjetivos','contentobjetivos')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorobjetivos" value="0">
	</div>
	<div class="col-xs-12" id="contentobjetivos" style="display: none;">
		<div class="col-xs-12">
			<h4>Objetivo General</h4>
			<textarea name="objetivoGeneral" id="objetivoGeneral" class="form-control" cols="30" rows="3" <?php echo $block; ?> onclick="validarCampoLleno('objetivoGeneral')" onkeyup="validarCampoLleno('objetivoGeneral')" placeholder="Los objetivos generales corresponden a las finalidades genéricas de un proyecto o entidad. No señalan resultados concretos ni directamente medibles por medio de indicadores pero si que expresan el propósito central del proyecto. Tienen que ser coherentes con la misión de la entidad."  <?php echo $block; ?>><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_obj_general;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_obj_general;
				}
			}
			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Objetivos Específicos</h4>
			<div class="col-xs-12">
				<small>Expresión cualitativa de un propósito particular. Se diferencia del objetivo general por su nivel de detalle y complementariedad. La característica principal de éste, es que debe permitir cuantificarse para poder expresarse en metas.</small>
				
			</div>
			<div class="col-xs-3">
			<input type="hidden" name="objEspecifico" id="objEspecifico" value="<?php if ($datosPr_f4!=false) {echo $datos->f4_obj_especifico;}else{if ($datosPr_f3!=false) {echo $fase_3->f3_obj_especifico;}} ?>" >

				<input type="number" disabled="disabled" name="objEspecificoMostrario" id="objEspecificoMostrario" class="form-control"  onclick="numeroObjEsp();validarCampoLleno('objEspecificoMostrario')" onkeyup="numeroObjEsp();validarCampoLleno('objEspecificoMostrario')" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Obj. Especificos" value="<?php if ($datosPr_f4!=false) {echo $datos->f4_obj_especifico;}else{if ($datosPr_f3!=false) {echo $fase_3->f3_obj_especifico;}} ?>" >
				
			</div>
		</div>
		<div class="col-xs-12" id="contentObjetivosEspecificos">
			<?php 

			if ($datosPr_f4!=false || $datosPr_f3!=false) {

				if ($datosPr_f4!=false) {
					$cod = $datos->f4_id;
					$sql = " `f4_id`='$cod' ";
				}else{
					$cod = $fase_3->f3_id;
					$sql = " `f3_id`='$cod' ";
				}


				$obj = $clase->detallesObjetivos($sql);
				if ($obj!=false) {
					$a = 1;
					while ($ob = mysqli_fetch_object($obj)) {
						echo '
						<div class="col-xs-12">
		               		<h5>Objetivo #'.$a.'</h5>
		                	<textarea name="objetivoEsp_'.$a.'" id="objetivoEsp_'.$a.'" class="form-control" cols="30" rows="2" '.$block.' >'.$ob->oe_objetivo.'</textarea>
		           		</div>
		           		<div class="col-xs-12">
							<h5>Resultados del Objetivo Especifico #'.$a.'</h5>
							<textarea name="resultadoObjetivo_'.$a.'" id="resultadoObjetivo_'.$a.'" class="form-control" '.$block.'onclick="validarCampoLleno(\'resultadoObjetivo_'.$a.'\')" onkeyup="validarCampoLleno(\'resultadoObjetivo_'.$a.'\')" cols="30" rows="2">'.$ob->oe_resultado.'</textarea>
						</div>
						<div class="col-xs-12">
							<h5>Producto del Resultado #'.$a.'</h5>
							<textarea name="productoResultado_'.$a.'" id="productoResultado_'.$a.'" class="form-control" '.$block.' onclick="validarCampoLleno(\'productoResultado_'.$a.'\')" onkeyup="validarCampoLleno(\'productoResultado_'.$a.'\')" cols="30" rows="2">'.$ob->oe_producto.'</textarea>
						</div>
						';
						$a = $a + 1;
					}
				}
			}

			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>



	<div class="col-xs-10 col-md-9">
		<h3>Jusificación</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion4" onclick="multiDespliegue('btn-agrupacion4','valoragrupacion4','contentagrupacion4')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion4" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion4" style="display: none;">
		<div class="col-xs-12">
			<h4>Justificación</h4>
			<textarea name="justificacion" id="justificacion" <?php echo $block; ?> class="form-control" cols="30" rows="5"  onclick="validarCampoLleno('justificacion')" onkeyup="validarCampoLleno('justificacion')"><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_justificacion;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_justificacion;
				}
			}

			?></textarea>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>



	<div class="col-xs-10 col-md-9">
		<h3>Marco Teórico</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion5" onclick="multiDespliegue('btn-agrupacion5','valoragrupacion5','contentagrupacion5')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion5" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion5" style="display: none;">
		<div class="col-xs-12">
			<h4>Antecedentes</h4>
			<textarea name="antecedentesProyecto" id="antecedentesProyecto" class="form-control" <?php echo $block; ?> cols="30" rows="5"  onclick="validarCampoLleno('antecedentesProyecto')" onkeyup="validarCampoLleno('antecedentesProyecto')" placeholder="Copiar el Abstract o resumen con citación por lo menos de 3 a 5 de los estudios previos y tesis de grado relacionadas con el problema planteado, es decir, investigaciones realizadas anteriormente y que guardan alguna vinculación con el problema en estudio."><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_antecedentes;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_antecedentes;
				}
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Bases Teóricas o Marco Conceptual</h4>
			<textarea name="basesTeoricas" id="basesTeoricas" class="form-control" cols="30" rows="5" <?php echo $block; ?> onclick="validarCampoLleno('basesTeoricas')" onkeyup="validarCampoLleno('basesTeoricas')" placeholder="Describa los conceptos y proposiciones (glosario de términos) que constituyen un punto de vista o enfoque determinado, dirigido a explicar el fenómeno o problema planteado."><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_bases_teoricas;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_bases_teoricas;
				}
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>

		<div class="col-xs-12">
			<h4>Marco Normativo</h4>
			<textarea name="marcoNormativo" id="marcoNormativo" class="form-control" cols="30" rows="7" <?php echo $block; ?>  onclick="validarCampoLleno('marcoNormativo')" onkeyup="validarCampoLleno('marcoNormativo')" placeholder="Son las bases legales que sustente el objeto de estudio, aquí se debe comenzar por hacer referencia a las Leyes según la pirámide de Kelsen 
•	Artículos de la Constitución que se refiere al tema investigado y análisis de los mismos. 
•	Convenios o tratados internacionales que se refieren al tema investigado y análisis de los mismos.
•	Leyes Orgánicas relacionadas con el tema investigado y análisis de los mismos. 
•	Leyes ordinarias relacionadas con el tema investigado y análisis de las mismas 
•	Decretos, providencias, resoluciones, o sentencias relacionadas con el tema investigado y análisis de los mismos. Políticas Públicas de Organismos tanto nacionales como internacionales que tienen disposiciones o normativas relacionadas con el tema de estudio y análisis de los mismos"><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_marco_normativo;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_marco_normativo;
				}
			}

			?></textarea>
		</div>

	</div>	
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Recursos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion6" onclick="multiDespliegue('btn-agrupacion6','valoragrupacion6','contentagrupacion6')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion6" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion6" style="display: none;">
		<div class="col-xs-12">
			<p>• Recursos Materiales: equipos, dispositivos, material de laboratorio, etc.</p>
			<p>• Recursos Humanos: asistentes de investigación, especialistas o cualquier otro personal de apoyo.</p>
			<p>• Recursos financieros: se indican a través de un presupuesto.</p>
			<a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal-ayuda1">Ver Ejemplo</a>
		</div>
		<div class="col-xs-12">
			<br>
		</div>
		<div class="col-xs-12">
			<p>Subir archivos</p>
			<div class="col-xs-12" id="contentArchivos">
				<input type="file" name="recursos_1" class="form-control">
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Cronograma de Actividades</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion7" onclick="multiDespliegue('btn-agrupacion7','valoragrupacion7','contentagrupacion7')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion7" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion7" style="display: none;">
		<div class="col-xs-12">
			<p>• Se describe la secuencia de las actividades de toda la investigación en función de tiempo de modo que se permita el control y el seguimiento de cada una de ellas.</p>
			<a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal-ayuda2">Ver Ejemplo</a>
		</div>
		<div class="col-xs-12">
			<br>
		</div>
		<div class="col-xs-12">
			<p>Subir archivos</p>
			<div class="col-xs-12" id="contentArchivos">
				<input type="file" name="recursos_1" class="form-control">
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>





	<div class="col-xs-10 col-md-9">
		<h3>Diseño Metodológico</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion10" onclick="multiDespliegue('btn-agrupacion10','valoragrupacion10','contentagrupacion10')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion10" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion10" style="display: none;">
		<div class="col-xs-12">
			<h4>Tipo de investigación</h4>
			<textarea name="tipoInvestigacion" id="tipoInvestigacion" class="form-control" cols="30" rows="8" <?php echo $block; ?>  onclick="validarCampoLleno('tipoInvestigacion')" onkeyup="validarCampoLleno('tipoInvestigacion')" placeholder="Se refiere a la clase de estudio que se va a realizar. Orienta sobre la finalidad general del estudio y sobre la manera de recoger la información o datos necesarios. El nivel de investigación se refiere al grado de profundidad con que se aborda
un objeto o fenómeno. Aquí se indicará si se trata de una investigación exploratoria, descriptiva o explicativa. En cualquiera de los casos es recomendable justificar el nivel adoptado. Se clasifica en:
- Investigación Exploratoria: es aquella que se efectúa sobre un tema u objeto poco conocido o estudiado, por lo que sus resultados constituyen una visión aproximada de dicho objeto. 
- Investigación Descriptiva: consiste en la caracterización de un hecho, fenómeno o supo con establecer su estructura o comportamiento.
- Investigación Explicativa: se encarga de buscar el por qué de los hechos mediante el establecimiento de relaciones causa-efecto. "><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_tipo_investigacion;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Diseño del método de investigación</h4>
			<textarea name="disennoMetodoInvestigacion" id="disennoMetodoInvestigacion" class="form-control" cols="30" rows="8" <?php echo $block; ?> onclick="validarCampoLleno('disennoMetodoInvestigacion')" onkeyup="validarCampoLleno('disennoMetodoInvestigacion')" placeholder="El diseño de investigación es la estrategia que adopta el investigador para responder al problema planteado. En esta sección se definirá y se justificará el tipo de según el diseño o estrategia por emplear y se clasifica en:
- Investigación Documental: es aquella que se basa en la obtención y análisis de datos provenientes de materiales impresos u otros tipos de documentos. 
-Investigación de Campo: consiste en la recolección de datos directamente de la realidad donde ocurren los hechos, sin manipular o controlar variable alguna.
-Investigación Experimental: proceso que consiste en someter a un objeto o grupo de individuos a determinadas condiciones o estímulos (variable independiente), para observar los efectos que se producen (variable dependiente). "><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_disenno_inv;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Universo, población y muestra</h4>
			<textarea name="universo" id="universo" class="form-control" cols="30" rows="5" <?php echo $block; ?> onclick="validarCampoLleno('universo')" onkeyup="validarCampoLleno('universo')" placeholder="La población o universo se refiere al conjunto para el cual serán válidas las conclusiones que se obtengan: a los elementos o unidades (personas,instituciones o cosas) involucradas en la investigación. Así como el tamaño y forma de selección.
La muestra es un 'subconjunto representativo de un universo o población'. "><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_universo_muestra;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Técnicas e instrumentos de recolección y muestra</h4>
			<textarea name="tecnicaMuestra" id="tecnicaMuestra" class="form-control" cols="30" rows="5" <?php echo $block; ?>  onclick="validarCampoLleno('tecnicaMuestra')" onkeyup="validarCampoLleno('tecnicaMuestra')" placeholder="Las técnicas de recolección de datos son las distintas formas o maneras de obtener la información. Son ejemplos de técnicas; la observación directa, la encuesta en sus dos modalidades (entrevista o cuestionario), el análisis documental, análisis de contenido, etc. Los instrumentos son los medios materiales que se emplean para recoger y almacenar la información. Ejemplo: fichas, formatos de cuestionario, guías de entrevista, lkta de cotejo, grabadores, escalas de actitudes u opinión (tipo likert), etc."><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_tecnicas_inst_rec_muestra;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Técnicas de procesamiento y análisis</h4>
			<textarea name="procesamientoAnalisis" id="procesamientoAnalisis" class="form-control" cols="30" rows="5" <?php echo $block; ?> onclick="validarCampoLleno('procesamientoAnalisis')" onkeyup="validarCampoLleno('procesamientoAnalisis')" placeholder="En este punto se describen las distintas operaciones a las que serán sometidos los datos que se obtengan: clasificación, registro, tabulación y codificación si fuere el caso. 
Un lo referente al análisis, se definirán las técnicas lógicas (inducción, deducción, análisis, síntesis), o estadísticas (descriptivas o inferenciales), que serán empleadas para descifrar lo que revelan los datos que sean recogidos. "><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_tecnica_procesamiento_analisis;
			}

			?></textarea>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Blibliografia</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-Bibliografia" onclick="multiDespliegue('btn-Bibliografia','valor-bibliografia','contentBibliografia')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valor-bibliografia" value="0">
	</div>
	<div class="col-xs-12" id="contentBibliografia" style="display: none;">
		<div class="col-xs-12 col-md-12">
			<div class="col-xs-12">
				<p>Bibliografía de la investigación</p>
				<textarea name="bibliografia" id="bibliografia" class="form-control" cols="30" rows="5" <?php echo $block; ?> onclick="validarCampoLleno('bibliografia')" onkeyup="validarCampoLleno('bibliografia')" placeholder="Citar y referenciar los materiales consultados ya sea página web, revistas,  y libros, de donde se obtuvo la información." <?php echo $block; ?>><?php 
			if ($datosPr_f4!=false) {
				echo $datos->f4_bibliografia;
			}else{
				if ($datosPr_f3!=false) {
					echo $fase_3->f3_bibliografia;
				}
			}

			?></textarea>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Anexos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion11" onclick="multiDespliegue('btn-agrupacion11','valoragrupacion11','contentagrupacion11')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion11" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion11" style="display: none;">
		<div class="col-xs-12">
			<p>Los anexos constituyen los elementos adicionales que se excluyen del texto del trabajo y que se agregan al fin del mismo. ANEXO A: Borrador de estudios previos.</p>
			<p>ANEXO B: Fichas Técnicas si aplica mantenimiento o rubros para compra de equipos.</p>
		</div>
		<div class="col-xs-12">
			<br>
		</div>
		<div class="col-xs-12">
			<p>Subir archivos</p>
			<div class="col-xs-12" id="contentArchivos">
				<input type="file" name="recursos_1" class="form-control">
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<?php 

	if ($datosPr_f4!=false) {
		if ($datos->indicaciones!=false) {
			
	?>


	<div class="col-xs-12"><hr></div>
	
	<div class="col-xs-12">
		<div class="col-xs-10 col-md-9">
			<h3>Indicaiones</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-indicaciones" onclick="multiDespliegue('btn-indicaciones','valorindicaciones','contentDatosindicaciones')" id="btnrg2to1" style="float: right;">
			       <i class="fa fa-angle-down"></i>
			   </a>
			   <input type="hidden" id="valorindicaciones" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosindicaciones" style="display: none;">
			<textarea name="indicacionesFase_1" id="indicacionesFase_1" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto." <?php echo $block; ?>><?php 
			echo $datos->indicaciones;

			?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<?php } }?>
	
</div>