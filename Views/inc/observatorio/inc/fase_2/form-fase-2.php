<div class="col-xs-12">
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-10 col-md-9">
		<h3>Datos Autor</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-DatosPersonales" onclick="multiDespliegue('btn-DatosPersonales','valorrDatosPersonales','contentDatosDatosPersonales')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorrDatosPersonales" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosDatosPersonales" style="display: none;">
		<div class="col-xs-12 col-md-3">
			<p>ROL DEL PROPONENTE </p>
			<?php 
			if ($datosPr_f2!=false) {
				echo '<input type="text" name="rol-exponente" id="rol-exponente" class="form-control" value="'.$datos->rol_exponente.'" placeholder="" '.$block.' >';
			}else{
				echo '
					<select name="rol-exponente" id="rol-exponente" class="form-control">
						<option value="Aprendiz CEDRUM">Aprendiz CEDRUM</option>
						<option value="Intructor CEDRUM">Intructor CEDRUM</option>
						<option value="Administrativo CEDRUM">Administrativo CEDRUM</option>
						<option value="Otro">Otro</option>
					</select>
				';
			}
			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Datos Proyecto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-DatosProyecto" onclick="multiDespliegue('btn-DatosProyecto','valorrDatosProyecto','contentDatosDatosProyecto')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorrDatosProyecto" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosDatosProyecto" style="display: none;">
		<div class="col-xs-12">
			<p>Titulo</p>
			<textarea name="idea" id="idea" class="form-control" cols="30" rows="3" onclick="validarCampoLleno('idea')" onkeyup="validarCampoLleno('idea')" placeholder="Descripcion corta del proyecto a desarrollar"  <?php echo $block; ?>><?php 
			if ($datosPr_f2!=false) {
				echo $datos->f2_titulo;
			}else{
				echo $proy->f1_idea;
			}
			?></textarea>
		</div>
		<div class="col-xs-12">
			<p>Planteamiento del problema</p>
			<textarea name="planteamiento" id="planteamiento" class="form-control" cols="30" rows="5" onclick="validarCampoLleno('planteamiento')" onkeyup="validarCampoLleno('planteamiento')" placeholder="A qué problemática daría solución la IDEA" <?php echo $block; ?>><?php 
			if ($datosPr_f2!=false) {
				echo $datos->f2_planteamiento;
			}else{
				echo $proy->f1_planteamiento;
			}
			?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Objetivos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-objetivos" onclick="multiDespliegue('btn-objetivos','valorobjetivos','contentobjetivos')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorobjetivos" value="0">
	</div>
	<div class="col-xs-12" id="contentobjetivos" style="display: none;">
		<div class="col-xs-12">
			<h4>Objetivo General</h4>
			<textarea name="objetivoGeneral" id="objetivoGeneral" class="form-control" cols="30" rows="3"  onclick="validarCampoLleno('objetivoGeneral')" onkeyup="validarCampoLleno('objetivoGeneral')"  <?php echo $block; ?>><?php 
			if ($datosPr_f2!=false) {
				echo $datos->f2_objetivo_general;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-3">
			<h4>Objetivos Específicos</h4>
			<input type="number" name="objEspecifico" id="objEspecifico" class="form-control"  <?php echo $block; ?> onclick="numeroObjEsp();validarCampoLleno('objEspecifico')" onkeyup="numeroObjEsp();validarCampoLleno('objEspecifico')" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Obj. Especificos" value="<?php if ($datosPr_f2!=false) {echo $datos->f2_objetivo_especifico;}?>" >
		</div>
		<div class="col-xs-12" id="contentObjetivosEspecificos">
			<?php 

			if ($datosPr_f2!=false) {

				$cod = $datos->f2_id;
				$sql = " `f2_id`='$cod' ";

				$obj = $clase->detallesObjetivos($sql);
				if ($obj!=false) {
					$a = 1;
					while ($ob = mysqli_fetch_object($obj)) {
						echo '
						<div class="col-xs-12">
		               		<h5>Objetivo #'.$a.'</h5>
		                	<textarea name="objetivoEsp_'.$a.'" id="objetivoEsp_'.$a.'" class="form-control" cols="30" rows="2" '.$block.' >'.$ob->oe_objetivo.'</textarea>
		           		</div>
						';
						$a = $a + 1;
					}
				}
			}

			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Justificación</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-justificacion" onclick="multiDespliegue('btn-justificacion','valorJustificacion','contentJustificacion')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorJustificacion" value="0">
	</div>
	<div class="col-xs-12" id="contentJustificacion" style="display: none;">
		<div class="col-xs-12 col-md-12">
			<div class="col-xs-12">
				<p>Describa la Justificación de la investigación</p>
				<textarea name="justificacion" id="justificacion" class="form-control" cols="30" rows="5" onclick="validarCampoLleno('justificacion')" onkeyup="validarCampoLleno('justificacion')" placeholder="Indicaciones de las motivaciones que se llevarán para el desarrollo del proyecto. Porque es conveniente llevar a cabo dicha investigación y cuáles son los beneficios que se derivarán de ella" <?php echo $block; ?>><?php 
			if ($datosPr_f2!=false) {
				echo $datos->f2_jusificacion;
			}

			?></textarea>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>



	<div class="col-xs-10 col-md-9">
		<h3>Blibliografia</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-Bibliografia" onclick="multiDespliegue('btn-Bibliografia','valor-bibliografia','contentBibliografia')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valor-bibliografia" value="0">
	</div>
	<div class="col-xs-12" id="contentBibliografia" style="display: none;">
		<div class="col-xs-12 col-md-12">
			<div class="col-xs-12">
				<p>Bibliografía Preliminar de la investigación</p>
				<textarea name="bibliografia" id="bibliografia" class="form-control" cols="30" rows="5" onclick="validarCampoLleno('bibliografia')" onkeyup="validarCampoLleno('bibliografia')" placeholder="Citar y referenciar los materiales consultados ya sea página web, revistas,  y libros, de donde se obtuvo la información." <?php echo $block; ?>><?php 
					if ($datosPr_f2!=false) {
						echo $datos->f2_bibliografia;
					}
				?></textarea>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	


	<div class="col-xs-10 col-md-9">
		<h3>Tiempo y presupuesto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-presupuesto" onclick="multiDespliegue('btn-presupuesto','valorPresupuesto','contentPresupuesto')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorPresupuesto" value="0">
	</div>
	<div class="col-xs-12" id="contentPresupuesto" style="display: none;">
		<div class="col-xs-12 col-md-12">
			<div class="col-xs-6">
				<p>Cuánto tiempo dura la ejecución de su proyecto de investigación(Tener en cuenta que solo se puede desarrollar en mínimo 6 meses y máximo 10)</p>
				<?php 
					if ($datosPr_f2!=false) {
						echo '<input type="text" name="tiempoEjecucion" id="tiempoEjecucion" value="'.$datos->f2_tiempo_ejecucion.'" class="form-control" '.$block.' placeholder="">';
					}else{
						echo '
						<select name="tiempoEjecucion" id="tiempoEjecucion" class="form-control">
							<option value="6 meses">6 meses</option>
							<option value="7 meses">7 meses</option>
							<option value="8 meses">8 meses</option>
							<option value="9 meses">9 meses</option>
							<option value="10 meses">10 meses</option>
						</select>
						';
					}
				?>
			</div>
			<div class="col-xs-6">
				<p>TENIENDO EN CUENTA EL VALOR PRESENTADO EN LA PRIMERA FASE INDIQUE CUÁNTO PIENSA QUE COSTARÍA DESARROLLAR LA IDEA</p>
				<input type="number" min="0" name="valorPropuesta" id="valorPropuesta" onclick="validarCampoLleno('valorPropuesta')" onkeyup="validarCampoLleno('valorPropuesta')"class="form-control" <?php echo $block;?>

				value="<?php  if ($datosPr_f2!=false) { echo $datos->f2_valor_idea;}else{echo $proy->f1_valor;} ?>">
				
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
	
	<?php 

	if ($datosPr_f2!=false) {
		if ($datos->indicaciones!=false) {
			
	?>


	<div class="col-xs-12"><hr></div>
	
	<div class="col-xs-12">
		<div class="col-xs-10 col-md-9">
			<h3>Indicaiones</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-indicaciones" onclick="multiDespliegue('btn-indicaciones','valorindicaciones','contentDatosindicaciones')" id="btnrg2to1" style="float: right;">
			       <i class="fa fa-angle-down"></i>
			   </a>
			   <input type="hidden" id="valorindicaciones" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosindicaciones" style="display: none;">
			<textarea name="indicacionesFase_1" id="indicacionesFase_1" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto." <?php echo $block; ?>><?php 
			echo $datos->indicaciones;

			?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<?php } }?>


</div>
