<div class="col-xs-12">
	<div class="col-xs-12"><br></div>
		<p>Linea de Investigación</p>
	<div class="col-xs-12">
        
		<?php 
			$n = $proy->li_linea_investigacion;
		?>
		<fieldset class="form-group">
		    <input type="checkbox" class="disable filled-in" name="agricola_pesca" id="opcion_1" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==1) {echo 'checked';}  ?>>
		    <label for="opcion_1">AGRÍCOLA Y DE PESCA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="agricola" id="opcion_2" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==2) {echo 'checked';}  ?> >
		    <label for="opcion_2">AGRÍCOLA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="ambiental" id="opcion_3" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==3) {echo 'checked';}  ?> >
		    <label for="opcion_3">AMBIENTAL</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="administrativa" id="opcion_4" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==4) {echo 'checked';}  ?> >
		    <label for="opcion_4">GESTIÓN ADMINISTRATIVA Y FINANCIERA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="mineria" id="opcion_5" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==5) {echo 'checked';}  ?> >
		    <label for="opcion_5">MINERÍA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="pecuaria" id="opcion_6" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==6) {echo 'checked';}  ?> >
		    <label for="opcion_6">PECUARIA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="informatica" id="opcion_7" onclick="unaOpcion(<?php echo $n;?>)" <?php if ($n==7) {echo 'checked';}  ?> >
		    <label for="opcion_7">INFORMÁTICA, DISEÑO Y DESARROLLO DE SOFTWARE.</label>
		</fieldset>
		
	</div>
</div>
<div class="col-xs-12">
	<p>Idea</p>
	<textarea name="idea" id="idea" class="form-control" cols="30" rows="3" placeholder="Descripcion corta del proyecto a desarrollar" <?php echo $block; ?>><?php echo $proy->f1_idea ?></textarea>
</div>
<div class="col-xs-12">
	<p>Planteamiento del problema</p>
	<textarea name="planteamiento" id="planteamiento" class="form-control" cols="30" rows="5" placeholder="A qué problemática daría solución la IDEA" <?php echo $block; ?>><?php echo $proy->f1_planteamiento ?></textarea>
</div>
<div class="col-xs-12">
	<p>Sector productivo al que impactará la idea</p>
	<textarea name="sectorProductivo" id="sectorProductivo" class="form-control" cols="30" rows="2" <?php echo $block; ?>><?php echo $proy->f1_sector_productivo ?></textarea>
</div>
<div class="col-xs-12">
	<p>Municipios de Norte de Santander donde desarrollará la idea</p>
	<div class="col-xs-3">
		<input type="number" name="municipios" id="municipios" class="form-control" value="<?php echo $proy->f1_municipios ?>" <?php echo $block; ?> >
	</div>
	<div class="col-xs-12" id="contentMunicipios">
		<?php

			$cod = $proy->f1_id;
			$sql = "`f1_id` = '$cod' ";
			$mun = $clase->detallesMunicipios($sql);
			if ($mun!=false) {
				$a = 1;
				while ($datos = mysqli_fetch_object($mun)) {
					echo '
					<div class="col-xs-4">
		                <h5>Municipio #'.$a.'</h5>
		                <input type="text" name="nombreMunicipio_` + municipio + `" id="nombreMunicipio_'.$a.'" class="form-control" value="'.$datos->mu_nombre.'" '.$block.'/>
		            </div>
					';
				}
			}
		?>
	</div>
	<div class="col-xs-12"><hr></div>
</div>
<div class="col-xs-12">
	<p>¿Con quien(es) desarrollará la idea?</p>
	<textarea name="acompannantes" id="acompannantes" class="form-control"  cols="30" rows="3" placeholder="Otros instructores, otros Semilleros, SENA o de otras Instituciones, Sector Productivo, Sector Gubernamental, Entes Descentralizadas, etc." <?php echo $block; ?>><?php echo $proy->f1_acompanante ?></textarea>
</div>
<div class="col-xs-12 col-md-4">
	<p>¿Cuanto piensa que costaría desarrollar la idea?</p>
	<input type="number" name="valor-idea" id="valor-idea" class="form-control" placeholder="$ valor idea" min="0" onkeypress="return event.charCode >= 48" value="<?php echo $proy->f1_valor ?>" <?php echo $block; ?>>
</div>
<div class="col-xs-12 col-md-8">
	<p>Link video</p>
	<textarea name="link-video" id="link-video" class="form-control" cols="30" rows="2" placeholder="En un corto video de maximo 5 minutos describa su idea." <?php echo $block; ?>><?php echo $proy->f1_link ?></textarea>
</div>