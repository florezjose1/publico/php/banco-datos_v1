<div class="col-xs-12">
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-10 col-md-9">
		<h3>Datos personales</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-DatosPersonales" onclick="multiDespliegue('btn-DatosPersonales','valorrDatosPersonales','contentDatosDatosPersonales')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorrDatosPersonales" value="0" >
	</div>
	<div class="col-xs-12" id="contentDatosDatosPersonales" style="display: none;">
		<div class="col-xs-12 col-md-3">
			<p>Regional</p>
			<input type="text" name="regional" id="regional" class="form-control" value="<?php echo $aut->dpr_regional ?>" <?php echo $block; ?>>
			<input type="hidden" id="progress_regional" value="0.823529412">
		</div>
		<div class="col-xs-12 col-md-6">
			<p>Programa de Formacion</p>
			<input type="text" name="formacion" id="formacion" class="form-control" value="<?php echo $aut->dpr_formacion ?>" <?php echo $block; ?>>
			<input type="hidden" id="progress-formacion" value="0.823529412">
		</div>
		<div class="col-xs-12 col-md-3">
			<p>Ficha</p>
			<input type="number" name="ficha" id="ficha" class="form-control" value="<?php echo $aut->dpr_ficha ?>" min="0" onkeypress="return event.charCode >= 48"  <?php echo $block; ?>>
			<input type="hidden" id="progress-ficha" value="0.823529412">
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-xs-12 col-md-6">
			<p>Nombres</p>
			<input type="text" name="nombre" id="nombre" class="form-control" value="<?php echo $aut->dpr_nombre ?>" <?php echo $block; ?>>
			<input type="hidden" id="progress-nombre" value="0.823529412" >
		</div>
		<div class="col-xs-12 col-md-6">
			<p>Apellidos</p>
			<input type="text" name="apellido" id="apellido" class="form-control" value="<?php echo $aut->dpr_apellido ?>" <?php echo $block; ?>>
			<input type="hidden" id="progress_primerApellido" value="0.823529412">
		</div>
		

		<div class="col-md-12"><br></div>
		<div class="col-xs-12 col-md-4">
			<p>Identificación</p> 
			<input type="number" name="identificacion" id="identificacion" class="form-control" value="<?php echo $aut->dpr_identificacion ?>" <?php echo $block; ?> >
			<input type="hidden" id="progress_identificacion" value="0.823529412">
		</div>
		<div class="col-xs-12 col-md-5">
			<p>Correo</p>
			<input type="email" name="email" id="email" class="form-control" value="<?php echo $aut->dpr_email ?>" <?php echo $block; ?>>
			<p id="mesageValEmail"></p>
		</div>
		<div class="col-xs-12 col-md-3">
			<p>Telefono</p>
			<input type="number" name="telefono" id="telefono" class="form-control" value="<?php echo $aut->dpr_telefono ?>" <?php echo $block; ?>>
			<input type="hidden" id="progress_telefono" value="0.823529412">
		</div>
	</div>
</div>
<div class="col-xs-12"><hr></div>
<div class="col-xs-12">
	<div class="col-xs-10 col-md-9">
		<h3>Inscripción idea</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-ideaProyecto" onclick="multiDespliegue('btn-ideaProyecto','valorideaProyecto','contentDatosideaProyecto')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorideaProyecto" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosideaProyecto" style="display: none;">
		<?php include 'idea.php';?>
	</div>
</div>
<div class="col-xs-12"><hr></div>
<div class="col-xs-12"><hr></div>
<div class="col-xs-12">
	<div class="col-xs-10 col-md-9">
		<h3>Indicaiones</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-indicaciones" onclick="multiDespliegue('btn-indicaciones','valorindicaciones','contentDatosindicaciones')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorindicaciones" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosindicaciones" style="display: none;">
		<textarea name="indicacionesFase_1" id="indicacionesFase_1" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto." <?php echo $block; ?>><?php 
		echo $proy->indicaciones;

		?></textarea>
	</div>
</div>
<div class="col-xs-12"><hr></div>
