<div class="col-xs-12">
	<div class="col-xs-12"><hr></div>
	<div class="col-xs-10 col-md-9">
		<h3>Linea Programática</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-lineas" onclick="multiDespliegue('btn-lineas','valorLineas','contentDatosLineas')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorLineas" value="0">
	</div>
	
	<?php if ($datosPr_f3!=false) {$cod = $datos->f3_linea_programatica;}else{echo '';} ?>

	<?php $li = $proy->li_linea_programatica ?>
	<div class="col-xs-12" id="contentDatosLineas" style="display: none;">
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="investigacion" id="opcion_1" onclick="unaOpcion(<?php if ($datosPr_f3!=false) {echo $datos->f3_linea_programatica;}else{echo '1';}?>)" <?php if ($li==1) {echo "checked";} ?> >
		   <label for="opcion_1">INVESTIGACIÓN APLICADA (Máximo $95.000.000).</label>
		</fieldset>
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="innovacion" id="opcion_2" onclick="unaOpcion(<?php if ($datosPr_f3!=false) {echo $datos->f3_linea_programatica;}else{echo '2';}?>)" <?php if ($li==2) {echo "checked";} ?>>
		   <label for="opcion_2">INNOVACIÓN Y DESARROLLO TECNOLÓGICO (Máximo $100.000.000).</label>
		</fieldset>
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="servicios" id="opcion_3" onclick="unaOpcion(<?php if ($datosPr_f3!=false) {echo $datos->f3_linea_programatica;}else{echo '3';}?>)" <?php if ($li==3) {echo "checked";} ?>>
		   <label for="opcion_3">FORTALECIMIENTO DE LA OFERTA DE SERVICIOS TECNOLÓGICOS A LAS EMPRESAS (Máximo $500.000.000).</label>
		</fieldset>
		<fieldset class="form-group">
		   <input type="checkbox" class="filled-in" name="modernizacion" id="opcion_4" onclick="unaOpcion(<?php if ($datosPr_f3!=false) {echo $datos->f3_linea_programatica;}else{echo '4';}?>)" <?php if ($li==4) {echo "checked";} ?>>
		   <label for="opcion_4">ACTUALIZACIÓN Y MODERNIZACIÓN TECNOLÓGICA DE CENTRO (Máximo $400.000.000).</label>
		</fieldset>
	</div>
	<div class="col-xs-12"><hr></div>

	
	<div class="col-xs-10 col-md-9">
		<h3>Areas de conocimiento</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-AreasCon" onclick="multiDespliegue('btn-AreasCon','valorAreas','contentDatosAreas')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorAreas" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosAreas" style="display: none;">
		<div class="col-md-6">
			<p>Area #1 </p>
			<select name="area_conocimiento_1" id="" class="form-control">
				<?php 
				if ($datosPr_f3!=false) {
					echo '<option value="'.$datos->f3_area_conocimiento_1.'">'.$datos->f3_area_conocimiento_1.'</option>';
				}else{
				?>
				<option value="1. AGRONOMIA VETERINARIA Y AFINES">1. AGRONOMIA VETERINARIA Y AFINES</option>
				<option value="2. BELLAS ARTES">2. BELLAS ARTES</option>
				<option value="3. CIENCIAS DE LA EDUCACION">3. CIENCIAS DE LA EDUCACION</option>
				<option value="4. CIENCIAS DE LA SALUD">4. CIENCIAS DE LA SALUD</option>
				<option value="5. CIENCIAS SOCIALES Y HUMANAS">5. CIENCIAS SOCIALES Y HUMANAS</option>
				<option value="6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES">6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES</option>
				<option value="7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES">7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES</option>
				<option value="9. MATEMATICAS Y CIENCIAS NATURALES">9. MATEMATICAS Y CIENCIAS NATURALES</option>
				<option value="10. SIN CLASIFICAR">10. SIN CLASIFICAR</option>
				<?php } ?>
			</select>
		</div>
		<div class="col-md-6">
			<p>Area #2</p>
			<select name="area_conocimiento_2" id="" class="form-control">
				<?php 
				if ($datosPr_f3!=false) {
					echo '<option value="'.$datos->f3_area_conocimiento_1.'">'.$datos->f3_area_conocimiento_2.'</option>';
				}else{
				?>
				<option value="1. AGRONOMIA VETERINARIA Y AFINES">1. AGRONOMIA VETERINARIA Y AFINES</option>
				<option value="2. BELLAS ARTES">2. BELLAS ARTES</option>
				<option value="3. CIENCIAS DE LA EDUCACION">3. CIENCIAS DE LA EDUCACION</option>
				<option value="4. CIENCIAS DE LA SALUD">4. CIENCIAS DE LA SALUD</option>
				<option value="5. CIENCIAS SOCIALES Y HUMANAS">5. CIENCIAS SOCIALES Y HUMANAS</option>
				<option value="6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES">6. ECONOMIA, ADMINISTRACION, CONTADURIA Y AFINES</option>
				<option value="7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES">7. INGENIERIA, ARQUITECTURA, URBANISMO Y AFINES</option>
				<option value="9. MATEMATICAS Y CIENCIAS NATURALES">9. MATEMATICAS Y CIENCIAS NATURALES</option>
				<option value="10. SIN CLASIFICAR">10. SIN CLASIFICAR</option>
				<?php } ?>
			</select>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Aplica al posconflicto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-posConflicto" onclick="multiDespliegue('btn-posConflicto','valorposConflicto','contentposConflicto')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorposConflicto" value="0">
	</div>
	<div class="col-xs-12" id="contentposConflicto" style="display: none;">
		<div class="col-xs-12">
			<?php 
			if ($datosPr_f3!=false) {
				if ($datos->f3_descrip_estra!=false) {
					echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" checked '.$block.'> Si';
					echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" '.$block.'> No';
				}else{
					echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" '.$block.'> Si';
					echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" checked '.$block.'> No';
				}
			}else{
				echo '<input type="radio" name="posconflicto" class="radio-form" value="1" onclick="apliPos(1)" '.$block.'> Si';
				echo '<input type="radio" name="posconflicto" class="radio-form" value="2" onclick="apliPos(2)" checked '.$block.'> No';
			}

			?>
		</div>
		<?php
		if ($datosPr_f3!=false) {
			if ($datos->f3_descrip_estra!=false) {
				$display = "block";
			}else{
				$display = "none";
			}
		}else{
			$display = "none";
		}
		?>
		<div class="col-xs-12" id="contentPosConflicto" style="display: <?php echo $display; ?>">
			<div class="col-xs-12">
				<h5>Descripcion de la Estrategia</h5>
				<textarea name="estrategiaPosConflicto" <?php echo $block; ?> id="estrategiaPosConflicto" class="form-control" cols="30" rows="5" placeholder="Descripción de la estrategia y por qué impacta al posconficto?"  onclick="validarCampoLleno('estrategiaPosConflicto')" onkeyup="validarCampoLleno('estrategiaPosConflicto')"><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_descrip_estra;
			}
			?></textarea>
			</div>
			<div class="col-xs-12">
				<p>¿Cuénta con recursos del Posconflicto?</p>
				<?php 
				if ($datosPr_f3!=false) {
					if ($datos->f3_recursos_pos>0) {
						echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="1" checked disabled="disabled"> Si
								<input type="radio" name="optPosconflicto" class="radio-form" '.$block.' value="2" disabled="disabled"> No
							</div>
							<div class="col-xs-3" id="contentRecursosPosconflicto" style="">
								<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)" '.$block.' value="'.$datos->f3_recursos_pos.'">
							</div>
						';
					}else{
						echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="optPosconflicto" class="radio-form" value="1" disabled="disabled"> Si
								<input type="radio" name="optPosconflicto" class="radio-form" value="2" checked disabled="disabled"> No
							</div>
						';
					}
				}else{
				?>
				<div class="col-xs-12 col-md-3">
					<input type="radio" name="optPosconflicto" id="optPosconflicto" class="radio-form" value="1" onclick="recursosPos(1)"> Si
					<input type="radio" name="optPosconflicto" id="optPosconflicto" class="radio-form" value="2" onclick="recursosPos(2)"> No
				</div>
				<div class="col-xs-3" id="contentRecursosPosconflicto" style="display: none;">
					<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)"  onclick="validarCampoLleno('recursosPosconflicto')" onkeyup="validarCampoLleno('recursosPosconflicto')">
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	
	<div class="col-xs-10 col-md-9">
		<h3>Municipios donde desarrollará la idea</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-municipios" onclick="multiDespliegue('btn-municipios','valorMunicipios','contentDatosMunicipios')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorMunicipios" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosMunicipios" style="display: none;">
		<div class="col-xs-3">
			<h5>Municipios</h5>
			<input type="number" name="municipios" id="municipios" <?php echo $block; ?> class="form-control" onclick="validarCampoLleno('municipios');numeroMunicipios();" onkeyup="validarCampoLleno('municipios');numeroMunicipios();" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Municipios" value="<?php if ($datosPr_f3!=false) {	echo $datos->f3_municipios_idea;}?>" >
		</div>
		<div class="col-xs-12" id="contentMunicipios">
			<?php 
			if ($datosPr_f3!=false) {	
				$sql = " `f3_idea` = '$datos->f3_id' ";
				$municipios = $clase->detallesMunicipios($sql);

				if ($municipios!=false) {
					$o = 1;
					while ($mun = mysqli_fetch_object($municipios)) {
						echo '
							<div class="col-xs-12 col-md-3">
					               <h5>Municipio #'.$o.'</h5>
					               <input type="text" '.$block.' name="nombreMunicipio_'.$o.'" id="nombreMunicipio_'.$o.'" class="form-control" value="'.$mun->mu_nombre.'" readonly="readonly">
					           </div>
						';
						$o = $o + 1;
					}
				}

			}
			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Sector productivo al que impacta el proyecto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-sector" onclick="multiDespliegue('btn-sector','valorSector','contentDatosSector')" id="btnrg2to1" style="float: right;">
		      <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorSector" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosSector" style="display: none;">
		<div class="col-xs-12">
			<textarea name="sectorProImpacto" id="sectorProImpacto" class="form-control" cols="30" rows="2" onclick="validarCampoLleno('sectorProImpacto')" onkeyup="validarCampoLleno('sectorProImpacto')" <?php echo $block; ?> ><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_sector_impacto;
			}?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Semilleros y formaciones beneficiadas e impacto esperado</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-semilleros" onclick="multiDespliegue('btn-semilleros','valorSemilleros','contentDatosSermilleros')" id="btnrg2to1" style="float: right;">
		   <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorSemilleros" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosSermilleros" style="display: none;">
		<div class="col-xs-4">
			<h4>Número de semilleros beneficiados</h4>
			<input type="number" name="semillerosBeneficiados" id="semillerosBeneficiados" class="form-control" onclick="numeroSemilleros();validarCampoLleno('semillerosBeneficiados');" onkeyup="numeroSemilleros();validarCampoLleno('semillerosBeneficiados');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Semilleros" <?php echo $block; ?> value="<?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_semilleros;
			}?>">
		</div>
		<div class="col-xs-12" id="contentSemilleros">
			<?php 
			if ($datosPr_f3!=false) {	
				$sql = " `semilleros_ben` WHERE `f3_id` = '$datos->f3_id' ";
				$semilleros = $clase->detallesSemiFormaciones($sql);

				if ($semilleros!=false) {
					$o = 1;
					while ($sem = mysqli_fetch_object($semilleros)) {
						echo '
							<div class="col-xs-12 col-md-3">
					         <h5>Semillero #'.$o.'</h5>
					         <input type="text" '.$block.' name="nombreSemillero_'.$o.'" id="nombreSemillero_'.$o.'" class="form-control" value="'.$sem->sb_nombre.'"  disabled="disabled">
					     	</div>
						';
						$o = $o + 1;
					}
				}
			}
			?>
		</div>


		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Numeros de programas de formacion beneficiados</h4>
			<div class="col-md-3">
				<input type="number" name="formacionesBenefiadas" <?php echo $block; ?> id="formacionesBenefiadas" class="form-control" onclick="numeroFormaciones();validarCampoLleno('formacionesBenefiadas');" onkeyup="numeroFormaciones();validarCampoLleno('formacionesBenefiadas');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Formaciones"  value="<?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_formaciones;
			}?>">
			</div>
		</div>
		<div class="col-xs-12" id="contentFormacionesBenefiadas">
			<?php 
			if ($datosPr_f3!=false) {	
				$sql = " `formaciones_ben` WHERE `f3_id` = '$datos->f3_id' ";
				$formacion = $clase->detallesSemiFormaciones($sql);

				if ($formacion!=false) {
					$o = 1;
					while ($form = mysqli_fetch_object($formacion)) {
						echo '
							<div class="col-xs-12 col-md-3">
					         <h5>Formación #'.$o.'</h5>
					         <input type="text" '.$block.' name="nombreFormacion_'.$o.'" class="form-control" id="nombreFormacion_'.$o.'" value="'.$form->fb_nombre.'"  disabled="disabled">
					      </div>
						';
						$o = $o + 1;
					}
				}
			}
			?>
		</div>

		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Impacto esperado</h4>
			<textarea name="impactoEsperado" id="impactoEsperado" <?php echo $block; ?> class="form-control" cols="30" rows="3" onclick="validarCampoLleno('impactoEsperado')" onkeyup="validarCampoLleno('impactoEsperado')"  ><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_impacto;
			}?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	
	<div class="col-xs-10 col-md-9">
		<h3>Datos Proyecto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-DatosProyecto" onclick="multiDespliegue('btn-DatosProyecto','valorrDatosProyecto','contentDatosDatosProyecto')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorrDatosProyecto" value="0">
	</div>
	<div class="col-xs-12" id="contentDatosDatosProyecto" style="display: none;">
		<div class="col-xs-12">
			<p>Titulo</p>
			<textarea name="titulo" id="titulo" <?php echo $block; ?> class="form-control" cols="30" rows="3" placeholder="Afirmación precisa que hace referencia al tema en torno al cual gira el proyecto de investigación. Tenga en cuenta las siguientes preguntas: ¿Qué se va hacer?, ¿Cómo se va a realizar? y ¿A qué se le va a aplicar?"  <?php echo $block; ?>><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_titulo;
			}else{
				if ($datosPr_f2!=false) {
					if ($var != 1) {
						$fase_2 = mysqli_fetch_object($datosPr_f2);
					}
					echo $fase_2->f2_titulo;
				}else{
					echo $proy->f1_idea;
				}
			}
			?></textarea>
		</div>
		<div class="col-xs-12">
			<p>Planteamiento del problema</p>
			<textarea name="planteamiento" id="planteamiento"  class="form-control" cols="30" rows="5" <?php echo $block; ?> placeholder="Descripción de la situación del problema que soporta al estudio, además de la relevancia, pertinencia e impacto del proyecto de investigación." <?php echo $block; ?>><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_planteamiento;
			}else{
				if ($datosPr_f2!=false) {
					echo $fase_2->f2_planteamiento;
				}else{
					echo $proy->f1_idea;
				}
			}
			?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Objetivos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-objetivos" onclick="multiDespliegue('btn-objetivos','valorobjetivos','contentobjetivos')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorobjetivos" value="0">
	</div>
	<div class="col-xs-12" id="contentobjetivos" style="display: none;">
		<div class="col-xs-12">
			<h4>Objetivo General</h4>
			<textarea name="objetivoGeneral" id="objetivoGeneral" class="form-control" cols="30" rows="3" <?php echo $block; ?> onclick="validarCampoLleno('objetivoGeneral')" onkeyup="validarCampoLleno('objetivoGeneral')" placeholder="Los objetivos generales corresponden a las finalidades genéricas de un proyecto o entidad. No señalan resultados concretos ni directamente medibles por medio de indicadores pero si que expresan el propósito central del proyecto. Tienen que ser coherentes con la misión de la entidad."  <?php echo $block; ?>><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_obj_general;
			}else{
				if ($datosPr_f2!=false) {
					echo $fase_2->f2_objetivo_general;
				}
			}
			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Objetivos Específicos</h4>
			<div class="col-xs-12">
				<small>Expresión cualitativa de un propósito particular. Se diferencia del objetivo general por su nivel de detalle y complementariedad. La característica principal de éste, es que debe permitir cuantificarse para poder expresarse en metas.</small>
				
			</div>
			<div class="col-xs-3">
			<input type="hidden" name="objEspecifico" id="objEspecifico" value="<?php if ($datosPr_f3!=false) {echo $datos->f3_obj_especifico;}else{echo $fase_2->f2_objetivo_especifico;} ?>" >

				<input type="number" disabled="disabled" name="objEspecifico" id="objEspecifico" class="form-control"  onclick="numeroObjEsp();validarCampoLleno('objEspecifico')" onkeyup="numeroObjEsp();validarCampoLleno('objEspecifico')" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Obj. Especificos" value="<?php if ($datosPr_f3!=false) {echo $datos->f3_obj_especifico;}else{echo $fase_2->f2_objetivo_especifico;} ?>" >
				
			</div>
		</div>
		<div class="col-xs-12" id="contentObjetivosEspecificos">
			<?php 

			if ($datosPr_f2!=false) {

				if ($datosPr_f3!=false) {
					$cod = $datos->f3_id;
					$sql = " `f3_id`='$cod' ";
				}else{
					$cod = $fase_2->f2_id;
					$sql = " `f2_id`='$cod' ";
				}


				$obj = $clase->detallesObjetivos($sql);
				if ($obj!=false) {
					$a = 1;
					while ($ob = mysqli_fetch_object($obj)) {
						echo '
						<div class="col-xs-12">
		               <h5>Objetivo #'.$a.'</h5>
		               <textarea name="objetivoEsp_'.$a.'" id="objetivoEsp_'.$a.'" class="form-control" '.$block.' cols="30" rows="2" '.$block.' >'.$ob->oe_objetivo.'</textarea>
		           	</div>
						';
						$a = $a + 1;
					}
				}
			}

			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Jusificación</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion4" onclick="multiDespliegue('btn-agrupacion4','valoragrupacion4','contentagrupacion4')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion4" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion4" style="display: none;">
		<div class="col-xs-12">
			<h4>Justificación</h4>
			<textarea name="justificacion" id="justificacion" class="form-control" <?php echo $block; ?>  cols="30" rows="5"  onclick="validarCampoLleno('justificacion')" onkeyup="validarCampoLleno('justificacion')"><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_justificacion;
			}else{
				if ($datosPr_f2!=false) {
					echo $fase_2->f2_jusificacion;
				}
			}

			?></textarea>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Marco Teórico</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion5" onclick="multiDespliegue('btn-agrupacion5','valoragrupacion5','contentagrupacion5')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion5" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion5" style="display: none;">
		<div class="col-xs-12">
			<h4>Antecedentes</h4>
			<textarea name="antecedentesProyecto" id="antecedentesProyecto" class="form-control" cols="30" rows="5" <?php echo $block; ?>  onclick="validarCampoLleno('antecedentesProyecto')" onkeyup="validarCampoLleno('antecedentesProyecto')" placeholder="Copiar el Abstract o resumen con citación por lo menos de 3 a 5 de los estudios previos y tesis de grado relacionadas con el problema planteado, es decir, investigaciones realizadas anteriormente y que guardan alguna vinculación con el problema en estudio."><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_antecedentes;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Bases Teóricas o Marco Conceptual</h4>
			<textarea name="basesTeoricas" id="basesTeoricas" class="form-control" cols="30" rows="5" <?php echo $block; ?>  onclick="validarCampoLleno('basesTeoricas')" onkeyup="validarCampoLleno('basesTeoricas')" placeholder="Describa los conceptos y proposiciones (glosario de términos) que constituyen un punto de vista o enfoque determinado, dirigido a explicar el fenómeno o problema planteado."><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_bases_teoricas;
			}

			?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>

		<div class="col-xs-12">
			<h4>Marco Normativo</h4>
			<textarea name="marcoNormativo" id="marcoNormativo" class="form-control" cols="30" rows="7" <?php echo $block; ?>  onclick="validarCampoLleno('marcoNormativo')" onkeyup="validarCampoLleno('marcoNormativo')" placeholder="Son las bases legales que sustente el objeto de estudio, aquí se debe comenzar por hacer referencia a las Leyes según la pirámide de Kelsen 
•	Artículos de la Constitución que se refiere al tema investigado y análisis de los mismos. 
•	Convenios o tratados internacionales que se refieren al tema investigado y análisis de los mismos.
•	Leyes Orgánicas relacionadas con el tema investigado y análisis de los mismos. 
•	Leyes ordinarias relacionadas con el tema investigado y análisis de las mismas 
•	Decretos, providencias, resoluciones, o sentencias relacionadas con el tema investigado y análisis de los mismos. Políticas Públicas de Organismos tanto nacionales como internacionales que tienen disposiciones o normativas relacionadas con el tema de estudio y análisis de los mismos"><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_marco_normativo;
			}

			?></textarea>
		</div>

	</div>	
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Recursos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion6" onclick="multiDespliegue('btn-agrupacion6','valoragrupacion6','contentagrupacion6')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion6" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion6" style="display: none;">
		<div class="col-xs-12">
			<p>• Recursos Materiales: equipos, dispositivos, material de laboratorio, etc.</p>
			<p>• Recursos Humanos: asistentes de investigación, especialistas o cualquier otro personal de apoyo.</p>
			<p>• Recursos financieros: se indican a través de un presupuesto.</p>
			<a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal-ayuda1">Ver Ejemplo</a>
		</div>
		<div class="col-xs-12">
			<br>
		</div>
		<div class="col-xs-12">
			<p>Subir archivos</p>
			<div class="col-xs-12" id="contentArchivos">
				<form name="datos-fase" enctype="multipart/form-data" method="post"></form>
				<input type="file" name="archivos[]" id="recursos_1" class="form-control" onclick="tamannno()"  multiple />
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Cronograma de Actividades</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion7" onclick="multiDespliegue('btn-agrupacion7','valoragrupacion7','contentagrupacion7')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion7" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion7" style="display: none;">
		<div class="col-xs-12">
			<p>• Se describe la secuencia de las actividades de toda la investigación en función de tiempo de modo que se permita el control y el seguimiento de cada una de ellas.</p>
			<a href="#" class="btn btn-default" data-toggle="modal" data-target="#modal-ayuda2">Ver Ejemplo</a>
		</div>
		<div class="col-xs-12">
			<br>
		</div>
		<div class="col-xs-12">
			<p>Subir archivos</p>
			<div class="col-xs-12" id="contentArchivos">
				<input type="file" name="recursos_1" class="form-control">
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>


	<div class="col-xs-10 col-md-9">
		<h3>Blibliografia</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-Bibliografia" onclick="multiDespliegue('btn-Bibliografia','valor-bibliografia','contentBibliografia')" id="btnrg2to1" style="float: right;">
		       <i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valor-bibliografia" value="0">
	</div>
	<div class="col-xs-12" id="contentBibliografia" style="display: none;">
		<div class="col-xs-12 col-md-12">
			<div class="col-xs-12">
				<p>Bibliografía Preliminar de la investigación</p>
				<textarea name="bibliografia" id="bibliografia" class="form-control" cols="30" rows="5" <?php echo $block; ?>  onclick="validarCampoLleno('bibliografia')" onkeyup="validarCampoLleno('bibliografia')" placeholder="Citar y referenciar los materiales consultados ya sea página web, revistas,  y libros, de donde se obtuvo la información." <?php echo $block; ?>><?php 
			if ($datosPr_f3!=false) {
				echo $datos->f3_bibliografia;
			}else{
				if ($datosPr_f2!=false) {
					echo $fase_2->f2_bibliografia;
				}

			}

			?></textarea>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<?php 

	if ($datosPr_f3!=false) {
		if ($datos->indicaciones!=false) {
			
	?>


	<div class="col-xs-12"><hr></div>
	
	<div class="col-xs-12">
		<div class="col-xs-10 col-md-9">
			<h3>Indicaiones</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-indicaciones" onclick="multiDespliegue('btn-indicaciones','valorindicaciones','contentDatosindicaciones')" id="btnrg2to1" style="float: right;">
			       <i class="fa fa-angle-down"></i>
			   </a>
			   <input type="hidden" id="valorindicaciones" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosindicaciones" style="display: none;">
			<textarea name="indicacionesFase_1" id="indicacionesFase_1" class="form-control" cols="30" rows="5"  placeholder="Espacio de indicaciones y/o correcciones para el postulante de la idea de proyecto." <?php echo $block; ?>><?php 
			echo $datos->indicaciones;

			?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<?php } }?>
	

</div>