<div class="col-xs-12">
		<div class="col-xs-10 col-md-9">
			<h3>Datos de quien Radica el Proyecto</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-autoRadicador" onclick="multiDespliegue('btn-autoRadicador','valorAutorRadicador','contentDatosRadicador')" id="btnrg2to1" style="float: right;">
		       	<i class="fa fa-angle-down"></i>
		    </a>
		    <input type="hidden" id="valorAutorRadicador" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosRadicador" style="display: none;">
			<div class="col-xs-12 col-md-6">
				<p>Nombre(s) </p>
				<input type="text" name="nombreRadicador" id="nombreRadicador" class="form-control" onclick="validarCampoLleno('nombreRadicador')" onkeyup="validarCampoLleno('nombreRadicador')" value="<?php echo $datos->dpr_nombre ?>" readonly="readonly" >
			</div>
			<div class="col-xs-12 col-md-6">
				<p>Apellidos(s)</p>
				<input type="text" name="apellidoRadicador" id="apellidoRadicador" class="form-control" onclick="validarCampoLleno('apellidoRadicador')" onkeyup="validarCampoLleno('apellidoRadicador')" value="<?php echo $datos->dpr_apellido ?>" readonly="readonly">
			</div>
			<div class="col-xs-12 col-md-4">
				<p>Identificacion</p>
				<input type="number" name="identificacionRadicador" id="identificacionRadicador" class="form-control" min="0" onkeypress="return event.charCode >= 48" onclick="validarCampoLleno('identificacionRadicador')" onkeyup="validarCampoLleno('identificacionRadicador')" value="<?php echo $datos->dpr_identificacion ?>" readonly="readonly">
			</div>
			<div class="col-xs-12 col-md-4">
				<p>Email</p>
				<input type="email" name="emailRadicador" id="emailRadicador" class="form-control" onclick="validarEmail('emailRadicador','mesageValEmailRa')" onkeyup="validarEmail('emailRadicador','mesageValEmailRa')" value="<?php echo $datos->dpr_email ?>" readonly="readonly">
				<p id="mesageValEmailRa"></p>
			</div>
			<div class="col-xs-12 col-md-4">
				<p>Telefono</p>
				<input type="number" name="telefonoRadicador" id="telefonoRadicador" class="form-control" min="0" onkeypress="return event.charCode >= 48" onclick="validarCampoLleno('telefonoRadicador')" onkeyup="validarCampoLleno('telefonoRadicador')" value="<?php echo $datos->dpr_telefono ?>" readonly="readonly">
			</div>
		</div>
	<div class="col-xs-12"><hr></div>
		<div class="col-xs-10 col-md-9">
			<h3>Autores del proyecto</h3>
		</div>
		<div class="col-xs-2 col-md-3">
			<a class="btn-floating btn-small red right" id="btn-autores" onclick="multiDespliegue('btn-autores','valorAutores','contentDatosAutor')"" id="btnrg2to1" style="float: right;">
		       	<i class="fa fa-angle-down"></i>
		    </a>
		    <input type="hidden" id="valorAutores" value="0">
		</div>
		<div class="col-xs-12" id="contentDatosAutor" style="display: none;">
			<div class="col-xs-12 col-md-3">
				<input type="hidden" name="autores" id="autores" class="form-control" onclick="numeroAutores();validarCampoLleno('autores')" onkeyup="numeroAutores();validarCampoLleno('autores')" min="0" onkeypress="return event.charCode >= 48" placeholder="# de autores" value="<?php echo $datos->dpr_autores ?>" readonly="readonly">
			</div>
			<?php 
			
			$datosAutores = $clase->datosAutores($a);

			if ($datosAutores!=false) {
				$autor = 1;
				while ($datosA = mysqli_fetch_object($datosAutores)) {

echo '
<div class="col-xs-12">
	<p>Autor '.$autor.'</p>
	<div class="col-xs-12 col-md-12 contentAutor_'.$autor.'">
		<div class="col-xs-12 col-md-6">
			<p>Nombre(s)</p>
			<input type="text" name="nombreAutor_'.$autor.'" id="nombreAutor_'.$autor.'" class="form-control" onclick="validarCampoLleno(\'nombreAutor_'.$autor.'\')" onkeyup="validarCampoLleno(\'nombreAutor_'.$autor.'\')" value="'.$datosA->dpa_nombre.'" readonly="readonly" />
		</div>
		<div class="col-xs-12 col-md-6">
			<p>Apellido(s)</p>
			<input type="text" name="apellidosAutor_'.$autor.'" id="apellidosAutor_'.$autor.'" class="form-control" onclick="validarCampoLleno(\'apellidosAutor_'.$autor.'\')" onkeyup="validarCampoLleno(\'apellidosAutor_'.$autor.'\')"  value="'.$datosA->dpa_apellido.'"  readonly="readonly" />
		</div>
		<div class="col-xs-12 col-md-4">
			<p>Identificacion</p>
			<input type="number" name="identificacionAutor_'.$autor.'" id="identificacionAutor_'.$autor.'" class="form-control" onclick="validarCampoLleno(\'identificacionAutor_'.$autor.'\')" onkeyup="validarCampoLleno(\'identificacionAutor_'.$autor.'\')" min="0" onkeypress="return event.charCode >= 48"  value="'.$datosA->dpa_identificacion.'" readonly="readonly"/>
		</div>
		<div class="col-xs-12 col-md-4">
			<p>Email</p>
			<input type="email" name="emailAutor_'.$autor.'" id="emailAutor_'.$autor.'" class="form-control"  onclick="
				validarEmail(\'emailAutor_'.$autor.'\',\'mesageValEmailAutores\');
				validarCampoLleno(\'emailAutor_'.$autor.'\')" 
				onkeyup="validarEmail(\'emailAutor_'.$autor.'\',\'mesageValEmailAutores\');
				validarCampoLleno(\'emailAutor_'.$autor.'\')" value="'.$datosA->dpa_email.'" readonly="readonly"/>
			<p id="mesageValEmailAutores"></p>
		</div>
		<div class="col-xs-12 col-md-4">
			<p>Telefono</p>
			<input type="number" name="telefonoAutor_'.$autor.'" id="telefonoAutor_'.$autor.'" class="form-control" onclick="validarCampoLleno(\'telefonoAutor_'.$autor.'\')" onkeyup="validarCampoLleno(\'telefonoAutor_'.$autor.'\')" min="0" onkeypress="return event.charCode >= 48" value="'.$datosA->dpa_telefono.'" readonly="readonly"/>
		</div>
	</div>
</div>
<div class="col-xs-12"><hr /></div>
					';

					$autor= $autor + 1;
				}

			}else{
				echo 'sin datos';
			}
			?>
			<div class="col-xs-12" id="contentAutores"></div>
		</div>
	<div class="col-xs-12"><hr></div>
</div>