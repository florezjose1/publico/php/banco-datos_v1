<div class="col-xs-12">
	<div class="col-xs-10 col-md-9">
		<h3>Título del proyecto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-tituloPro" onclick="multiDespliegue('btn-tituloPro','valorTituloPro','contentTituloPro')" id="btnrg2to1" style="float: right;">
		       	<i class="fa fa-angle-down"></i>
		   </a>
		   <input type="hidden" id="valorTituloPro" value="0">
	</div>
	<div class="col-xs-12" id="contentTituloPro" style="display: none;">
		<input type="text" name="titulo" id="titulo" class="form-control" placeholder="Título del proyecto" value="<?php echo $datos->ip_titulo ?>" readonly="readonly">
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Aplica al posconflicto</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-posConflicto" onclick="multiDespliegue('btn-posConflicto','valorposConflicto','contentposConflicto')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorposConflicto" value="0">
	</div>
	<div class="col-xs-12" id="contentposConflicto" style="display: none;">
		<div class="col-xs-12">
			<?php 
			if ($datos->ip_n_munip_posC > 0) {
				echo '
				<input type="radio" name="posconflicto" class="disabled radio-form" value="1" onclick="" checked> Si
				<input type="radio" name="posconflicto" class="disabled radio-form" value="2" onclick=""> No
				';
				echo '
				<div class="col-xs-12" >
				<div class="col-xs-3">
					<h5>Municipios a Impactar</h5>
					<input type="number" name="municipios" id="municipios" class="form-control" 
						onclick="validarCampoLleno(\'municipios\');" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Municipios"  value="'.$datos->ip_n_munip_posC.'" readonly="readonly">
				</div>
				<div class="col-xs-12" id="contentMunicipios">';

					$municipios = $clase->municipios($p);

					if ($municipios!=false) {
						$o = 1;
						while ($mun = mysqli_fetch_object($municipios)) {
							echo '
								<div class="col-xs-12 col-md-3">
					                <h5>Municipio #'.$o.'</h5>
					                <input type="text" name="nombreMunicipio_'.$o.'" class="form-control" value="'.$mun->mu_nombre.'" readonly="readonly">
					            </div>
							';
							$o = $o + 1;
						}
					}

				echo '</div>
				<div class="col-xs-12">
					<h5>Descripcion de la Estrategia</h5>
					<textarea name="estrategiaPosConflicto" id="estrategiaPosConflicto" class="form-control" cols="30" rows="5" placeholder="Descripción de la estrategia y por qué impacta al posconficto?" readonly="readonly">'.$datos->ip_estrategia_posC.'</textarea>
				</div>
				<div class="col-xs-12">
					<p>¿Cuénta con recursos del Posconflicto?</p>
					';
					if ($datos->ip_recursos_posC>0) {
						echo '
						<div class="col-xs-12 col-md-2">
							<input type="radio" name="optPosconflicto" class="disabled radio-form" value="1" checked> Si
							<input type="radio" name="optPosconflicto" class="disabled radio-form" value="2"> No
						</div>
						';
						echo '
						<div class="col-xs-3" id="contentRecursosPosconflicto">
							<input type="number" name="recursosPosconflicto" id="recursosPosconflicto" class="form-control" placeholder="$(COP)"  value="'.$datos->ip_recursos_posC.'" readonly="readonly">
						</div>
						';
					}else{
						echo '
						<div class="col-xs-12 col-md-2">
							<input type="radio" name="optPosconflicto" class="disabled radio-form" value="1"> Si
							<input type="radio" name="optPosconflicto" class="disabled radio-form" value="2" checked> No
						</div>
						';
					}
				echo '</div>
			</div>
				';
			}else{
				echo '
				<input type="radio" name="posconflicto" class="disabled radio-form" value="1" onclick=""> Si
				<input type="radio" name="posconflicto" class="disabled radio-form" value="2" onclick="" checked> No

				';
			}
			?>
		</div>
		
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Jusificación y planteamiento del problema</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion4" onclick="multiDespliegue('btn-agrupacion4','valoragrupacion4','contentagrupacion4')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion4" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion4" style="display: none;">
		<div class="col-xs-12">
			<h4>Antecedentes y justificacion del proyecto</h4>
			<textarea value="hola" name="antecedentesProyecto" id="antecedentesProyecto" class="form-control" cols="30" rows="5"  readonly="readonly"><?php echo $datos->ip_justificacion ?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Link video</h4>
			<textarea name="linkVideo" id="linkVideo" class="form-control" cols="30" rows="2" placeholder="En un corto video de maximo 5 minutos describa su idea." readonly="readonly"><?php echo $datos->ip_link_video ?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Plantemiento del problema y/o necesidades</h4>
			<textarea name="plantProNecesidades" id="plantProNecesidades" class="form-control" cols="30" rows="5"  readonly="readonly"><?php echo $datos->ip_planteamiento ?></textarea>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>
	
	<div class="col-xs-10 col-md-9">
		<h3>Objetivos</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-objetivos" onclick="multiDespliegue('btn-objetivos','valorobjetivos','contentobjetivos')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorobjetivos" value="0">
	</div>
	<div class="col-xs-12" id="contentobjetivos" style="display: none;">
		<div class="col-xs-12">
			<h4>Objetivo General</h4>
			<textarea name="objetivoGeneral" id="objetivoGeneral" class="form-control" cols="30" rows="3"  readonly="readonly"><?php echo $datos->ip_obj_general ?></textarea>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-3">
			<h4>Objetivo Específico</h4>
			<input type="number" name="objEspecifico" id="objEspecifico" class="form-control" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Obj. Especificos"  value="<?php echo $datos->ip_n_obj_especifico ?>" readonly="readonly">
		</div>
		<div class="col-xs-12" id="contentObjetivosEspecificos">
			<?php 

			$objetivosEsp = $clase->objetivosEspecificos($p);

			if ($objetivosEsp!=false) {
				$o = 1;
				while ($obj = mysqli_fetch_object($objetivosEsp)) {
					echo '
						<div class="col-xs-12">
			                <h5>Objetivo #'.$o.'</h5>
			                <textarea name="objetivoEsp_'.$o.'" id="objetivoEsp_'.$o.'" class="form-control" cols="30" rows="2" readonly="readonly">'.$obj->oe_objetivo.'</textarea>
			            </div>
					';
					$o = $o+1;
				}
			}


			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Fechas</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-Fechas" onclick="multiDespliegue('btn-Fechas','valorFechas','contentFechas')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valorFechas" value="0">
	</div>
	<div class="col-xs-12" id="contentFechas" style="display: none;">
		<div class="col-xs-12 col-md-6">
			<h4>Fecha Inicio del proyecto</h4>
			<input type="date" name="fechaInicioPro" id="fechaInicioPro" class="form-control"  value="<?php echo $datos->ip_fecha_inicio ?>" readonly="readonly">
		</div>
		<div class="col-xs-12 col-md-6">
			<h4>Fecha Fin del proyecto</h4>
			<input type="date" name="fechaFinPro" id="fechaFinPro" class="form-control" value="<?php echo $datos->ip_fecha_fin ?>" readonly="readonly">
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Datos Adicionales</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion5" onclick="multiDespliegue('btn-agrupacion5','valoragrupacion5','contentagrupacion5')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion5" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion5" style="display: none;">
		<div class="col-xs-12">
			<h4>Nombre grupo de investigación</h4>
			<input type="text" name="grupoInvestigacion" id="grupoInvestigacion" class="form-control"  value="<?php echo $datos->ip_grupo_investigacion ?>" readonly="readonly">
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Codigo de GrupLAC</h4>
			<input type="text" name="grupoLac" id="grupoLac" class="form-control" placeholder="Ejemplo:COL0001048"  value="<?php echo $datos->ip_grupo_lac ?>" readonly="readonly">
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-4">
			<h4>Número de semilleros beneficiados</h4>
			<input type="number" name="semillerosBeneficiados" id="semillerosBeneficiados" class="form-control""" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Semilleros" value="<?php echo $datos->ip_n_semilleros_beneficiados ?>" readonly="readonly">
		</div>
		<div class="col-xs-12" id="contentSemilleros">
			<?php 

			$semilleros = $clase->semilleros($p);

			if ($semilleros!=false) {
				$o = 1;
				while ($obj = mysqli_fetch_object($semilleros)) {
					echo '
						<div class="col-xs-12">
			                <h5>Nombre semillero #'.$o.'</h5>
			                <textarea name="nombreSemillero_'.$o.'" id="nombreSemillero_'.$o.'" class="form-control" cols="30" rows="2" readonly="readonly">'.$obj->sb_nombre.'</textarea>
			            </div>
					';
					$o = $o + 1;
				}
			}
			?>
		</div>
		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Numeros de programas de formacion beneficiados</h4>
			<div class="col-md-3">
				<input type="number" name="formacionesBenefiadas" id="formacionesBenefiadas" class="form-control""" min="0" onkeypress="return event.charCode >= 48" placeholder="# de Formaciones" value="<?php echo $datos->ip_n_formaciones_beneficiadas ?>" readonly="readonly">
			</div>
		</div>
		<div class="col-xs-12" id="contentFormacionesBenefiadas">
			<?php 

			$formaciones = $clase->formaciones($p);

			if ($formaciones!=false) {
				$o = 1;
				while ($obj = mysqli_fetch_object($formaciones)) {
					echo '
						<div class="col-xs-12">
			                <h5>Nombre de la  Formacion #'.$o.'</h5>
			                <textarea name="nombreFormacion_'.$o.'" id="nombreFormacion_'.$o.'" class="form-control" cols="30" rows="2" readonly="readonly">'.$obj->fb_nombre.'</textarea>
			            </div>
					';
					$o = $o + 1;
				}
			}
			?>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

	<div class="col-xs-10 col-md-9">
		<h3>Metodologia, productos, resultados e impacto esperado</h3>
	</div>
	<div class="col-xs-2 col-md-3">
		<a class="btn-floating btn-small red right" id="btn-agrupacion6" onclick="multiDespliegue('btn-agrupacion6','valoragrupacion6','contentagrupacion6')" id="btnrg2to1" style="float: right;">
			<i class="fa fa-angle-down"></i>
		</a>
		<input type="hidden" id="valoragrupacion6" value="0">
	</div>
	<div class="col-xs-12" id="contentagrupacion6" style="display: none;">
		<div class="col-xs-12">
			<h4>Descripción de metodologia del proyecto</h4>
			<textarea name="descripMetodologia" id="descripMetodologia" class="form-control" cols="30" rows="5"  readonly="readonly"><?php echo $datos->ip_metodologia ?></textarea>
		</div>

		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Resultados esperados</h4>
		</div>
		<div class="col-xs-12" id="contentResultadosEsperados">
			<?php 

			$objetivosEsp = $clase->objetivosEspecificos($p);

			if ($objetivosEsp!=false) {
				$o = 1;
				while ($obj = mysqli_fetch_object($objetivosEsp)) {
					echo '
						<div class="col-xs-12">
			                <h5>Resultados del Objetivo Especifico #'.$o.'</h5>
			                <textarea name="resultadoObjetivo_'.$o.'" id="resultadoObjetivo_'.$o.'" class="form-control" cols="30" rows="2" readonly="readonly">'.$obj->oe_resultado.'</textarea>
			            </div>
					';
					$o = $o + 1;
				}
			}
			?>
		</div>

		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Productos</h4>
		</div>
		<div class="col-xs-12" id="contentProductosResultado">
			<?php 

			$objetivosEsp = $clase->objetivosEspecificos($p);

			if ($objetivosEsp!=false) {
				$o = 1;
				while ($obj = mysqli_fetch_object($objetivosEsp)) {
					echo '
						<div class="col-xs-12">
			                <h5>Producto del Resultado #'.$o.'</h5>
			                <textarea name="productoResultado_'.$o.'" id="productoResultado_'.$o.'" class="form-control" cols="30" rows="2" readonly="readonly">'.$obj->oe_producto.'</textarea>
			            </div>
					';
					$o = $o + 1;
				}
			}
			?>
		</div>


		<div class="col-xs-12"><hr></div>
		<div class="col-xs-12">
			<h4>Impacto esperado</h4>
			<textarea name="impactoEsperado" id="impactoEsperado" class="form-control" cols="30" rows="5" placeholder="Agregar impacto, el numero de prioridad segun documento guia de modernizacion CAPITULO 8.2.1 Criterios de priorización." readonly="readonly"><?php echo $datos->ip_impacto ?></textarea>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>

</div>