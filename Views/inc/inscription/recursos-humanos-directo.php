<div class="col-xs-12">
	<div class="col-xs-12">
		<h3>Recursos Humanos
		
	        <a class="btn-floating btn-small red right" id="btn-recursosH" onclick="desplegarRecursosHumanos()" id="btnrg2to1" style="float: right;">
	        	<i class="fa fa-angle-down"></i>
	        </a>
	        <input type="hidden" id="valorReHu" value="0">

	    </h3>
		<div class="col-xs-12" id="contentRecursosHumanos" style="display: none;">
			<div class="col-xs-12">
				<h3>Personal Externo</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<?php 
				if ($datos->rh_personal_ext>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="perslExt" id="perslExt" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="perslExt" id="perslExt" class="disabled radio-form" value="2" > No
					</div>
					';
					$sql = "SELECT * FROM `personal_externo` WHERE `rh_id` = '$cod' ";
					$personalExterno = $clase->detallesRecursosHumanos($sql);
					$n = mysqli_num_rows($personalExterno);
					
					echo '
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numPersonalExt" id="numPersonalExt" class="form-control" readonly="readonly" value="'.$n.'" />
					</div>
					';
					$num = 1;
					while ($per = mysqli_fetch_object($personalExterno)){
						echo '
						<div class="col-xs-12">
            				<p>Persona # '.$num.'</p>
            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
                				<div class="col-xs-12 col-md-4">
                    				<p>Nombre(s)</p>
                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->pe_nombre.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Apellido(s)</p>
                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->pe_apellido.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Identificacion</p>
                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->pe_identificacion.'" readonly="readonly"/>
                				</div>
            				</div>
        				</div>
        				<div class="col-xs-12"><hr /></div>
						';
						$num = $num + 1;
					}
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="disabled radio" name="perslExt" id="perslExt" class="radio-form" value="1"> Si
						<input type="radio" name="perslExt" id="perslExt" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div id="contentPersonalExterno" style="display: none;">
				<div class="col-xs-3">
				</div>
				<div class="col-xs-12" id="contentNumPersonasExternas"></div>
			</div>

			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Aprendices Sin Contrato de apredizaje)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<?php 
				if ($datos->rh_personal_int_asc>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntASC" id="persIntASC" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="persIntASC" id="persIntASC" class=disabled "radio-form" value="2" > No
					</div>
					';
					$sql = "SELECT * FROM `personal_internoasc` WHERE `rh_id` = '$cod' ";
					$personalIntASC = $clase->detallesRecursosHumanos($sql);
					$n = mysqli_num_rows($personalIntASC);
					
					echo '
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
					</div>
					';
					$num = 1;
					while ($per = mysqli_fetch_object($personalIntASC)){
						echo '
						<div class="col-xs-12">
            				<p>Aprendiz # '.$num.'</p>
            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
                				<div class="col-xs-12 col-md-4">
                    				<p>Nombre(s)</p>
                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->pi_nombre.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Apellido(s)</p>
                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->pi_apellido.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Identificacion</p>
                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->pi_identificacion.'" readonly="readonly"/>
                				</div>
            				</div>
        				</div>
        				<div class="col-xs-12"><hr /></div>
						';
						$num = $num + 1;
					}
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntASC" id="persIntASC" class="disabled radio-form" value="1"> Si
						<input type="radio" name="persIntASC" id="persIntASC" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div  id="contentPersonalInternoASC" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasASC"></div>
			</div>

			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Aprendices Con Contrato de apredizaje)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<?php 
				if ($datos->rh_personal_int_acc>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntACC" id="persIntACC" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="persIntACC" id="persIntACC" class=disabled "radio-form" value="2" > No
					</div>
					';
					$sql = "SELECT * FROM `personal_internoacc` WHERE `rh_id` = '$cod' ";
					$personalIntASC = $clase->detallesRecursosHumanos($sql);
					$n = mysqli_num_rows($personalIntASC);
					
					echo '
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
					</div>
					';
					$num = 1;
					while ($per = mysqli_fetch_object($personalIntASC)){
						echo '
						<div class="col-xs-12">
            				<p>Aprendiz # '.$num.'</p>
            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
                				<div class="col-xs-12 col-md-4">
                    				<p>Nombre(s)</p>
                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->pi_nombre.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Apellido(s)</p>
                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->pi_apellido.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Identificacion</p>
                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->pi_identificacion.'" readonly="readonly"/>
                				</div>
            				</div>
        				</div>
        				<div class="col-xs-12"><hr /></div>
						';
						$num = $num + 1;
					}
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntACC" id="persIntACC" class="disabled radio-form" value="1"> Si
						<input type="radio" name="persIntACC" id="persIntACC" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div  id="contentPersonalInternoACC" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasACC"></div>
			</div>
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Instructores)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<?php 
				if ($datos->rh_personal_int_instructores>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntinst" id="persIntinst" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="persIntinst" id="persIntinst" class=disabled "radio-form" value="2" > No
					</div>
					';
					$sql = "SELECT * FROM `personal_internoinstructores` WHERE `rh_id` = '$cod' ";
					$personalIntASC = $clase->detallesRecursosHumanos($sql);
					$n = mysqli_num_rows($personalIntASC);
					
					echo '
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
					</div>
					';
					$num = 1;
					while ($per = mysqli_fetch_object($personalIntASC)){
						echo '
						<div class="col-xs-12">
            				<p>Instructor # '.$num.'</p>
            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
                				<div class="col-xs-12 col-md-4">
                    				<p>Nombre(s)</p>
                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->pi_nombre.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Apellido(s)</p>
                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->pi_apellido.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Identificacion</p>
                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->pi_identificacion.'" readonly="readonly"/>
                				</div>
            				</div>
        				</div>
        				<div class="col-xs-12"><hr /></div>
						';
						$num = $num + 1;
					}
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntinst" id="persIntinst" class="disabled radio-form" value="1"> Si
						<input type="radio" name="persIntinst" id="persIntinst" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div  id="contentPersonalInternoInstructores" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasInstructores"></div>
			</div>

			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<h3>Personal Interno (Otros-TP-TA-SENNOVA)</h3>
			</div>
			<div class="col-xs-12 col-md-12">
				<?php 
				if ($datos->rh_personal_int_otros>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntOtros" id="persIntOtros" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="persIntOtros" id="persIntOtros" class=disabled "radio-form" value="2" > No
					</div>
					';
					$sql = "SELECT * FROM `personal_internootros` WHERE `rh_id` = '$cod' ";
					$personalIntASC = $clase->detallesRecursosHumanos($sql);
					$n = mysqli_num_rows($personalIntASC);
					
					echo '
					<div class="col-xs-12 col-md-3">
						<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
					</div>
					';
					$num = 1;
					while ($per = mysqli_fetch_object($personalIntASC)){
						echo '
						<div class="col-xs-12">
            				<p>Persona # '.$num.'</p>
            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
                				<div class="col-xs-12 col-md-4">
                    				<p>Nombre(s)</p>
                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->pe_nombre.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Apellido(s)</p>
                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->pe_apellido.'" readonly="readonly"/>
                				</div>
                				<div class="col-xs-12 col-md-4">
                    				<p>Identificacion</p>
                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->pe_identificacion.'" readonly="readonly"/>
                				</div>
            				</div>
        				</div>
        				<div class="col-xs-12"><hr /></div>
						';
						$num = $num + 1;
					}
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="persIntOtros" id="persIntOtros" class="disabled radio-form" value="1"> Si
						<input type="radio" name="persIntOtros" id="persIntOtros" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div  id="contentPersonalInternoOtros" style="display: none;">
				<div class="col-xs-12" id="contentNumPersonasInternasOtros"></div>
			</div>
		</div>
	</div>
	<div class="col-xs-12"><hr></div>
		
	<div class="col-xs-12">
		<h3>Aliados Externos y/o Contrapartida
			<a class="btn-floating btn-small red right" id="btn-aliados" onclick="desplegarAliados()" id="btnrg2to1" style="float: right;">
	        	<i class="fa fa-angle-down"></i>
	        </a>
	        <input type="hidden" id="valorAliados" value="0">
	    </h3>
		<div class="col-xs-12" id="contenAliados" style="display: none;">
			<div class="col-xs-12">
				<p>Aliados Externos</p>
				<div class="col-xs-12 col-md-12">
					<?php 
					if ($datos->rh_aliados_ext>0) {
						echo '
						<div class="col-xs-12 col-md-2">
							<input type="radio" name="alidadosExt" id="alidadosExt" class="disabled radio-form" value="1" checked> Si
							<input type="radio" name="alidadosExt" id="alidadosExt" class=disabled "radio-form" value="2" > No
						</div>
						';
						$sql = "SELECT * FROM `aliados_externos` WHERE `rh_id` = '$cod' ";
						$personalIntASC = $clase->detallesRecursosHumanos($sql);
						$n = mysqli_num_rows($personalIntASC);
						
						echo '
						<div class="col-xs-12 col-md-3">
							<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
						</div>
						';
						$num = 1;
						while ($per = mysqli_fetch_object($personalIntASC)){
							echo '
							<div class="col-xs-12">
	            				<p>Aliado # '.$num.'</p>
	            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
	                				<div class="col-xs-12 col-md-4">
	                    				<p>Nombre(s)</p>
	                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->ae_nombre.'" readonly="readonly"/>
	                				</div>
	                				<div class="col-xs-12 col-md-4">
	                    				<p>NIT(s)</p>
	                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->ae_nit.'" readonly="readonly"/>
	                				</div>
	                				<div class="col-xs-12 col-md-4">
	                    				<p>Recursos en Especie ($COP):</p>
	                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->ae_recursos.'" readonly="readonly"/>
	                				</div>
	            				</div>
	        				</div>
	        				<div class="col-xs-12"><hr /></div>
							';
							$num = $num + 1;
						}
					}else{
						echo '
						<div class="col-xs-12 col-md-2">
							<input type="radio" name="alidadosExt" id="alidadosExt" class="disabled radio-form" value="1"> Si
							<input type="radio" name="alidadosExt" id="alidadosExt" class="disabled radio-form" value="2" checked > No
						</div>
						';
					}
					?>
				</div>
			</div>
			<div class="col-xs-12"><hr></div>

			<div class="col-xs-12">
				<p>Aliados Internos</p>
				<div class="col-xs-12 col-md-12">
					<?php 
					if ($datos->rh_aliados_int>0) {
						echo '
						<div class="col-xs-12 col-md-2">
							<input type="radio" name="aliadosInt" id="aliadosInt" class="disabled radio-form" value="1" checked> Si
							<input type="radio" name="aliadosInt" id="aliadosInt" class=disabled "radio-form" value="2" > No
						</div>
						';
						$sql = "SELECT * FROM `aliados_internos` WHERE `rh_id` = '$cod' ";
						$personalIntASC = $clase->detallesRecursosHumanos($sql);
						$n = mysqli_num_rows($personalIntASC);
						
						echo '
						<div class="col-xs-12 col-md-3">
							<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
						</div>
						';
						$num = 1;
						while ($per = mysqli_fetch_object($personalIntASC)){
							echo '
							<div class="col-xs-12">
	            				<p>Aliado # '.$num.'</p>
	            				<div class="col-xs-12 col-md-12 contentAutor_'.$num.'">
	                				<div class="col-xs-12 col-md-4">
	                    				<p>Nombre(s)</p>
	                    				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->ai_nombre.'" readonly="readonly"/>
	                				</div>
	                				<div class="col-xs-12 col-md-4">
	                    				<p>NIT(s)</p>
	                    				<input type="text" name="apellidosPersExt_'.$num.'" id="apellidosPersExt_'.$num.'" class="form-control" value="'.$per->ai_nit.'" readonly="readonly"/>
	                				</div>
	                				<div class="col-xs-12 col-md-4">
	                    				<p>Recursos en Especie ($COP):</p>
	                    				<input type="number" name="identificacionPersExt_'.$num.'" id="identificacionPersExt_'.$num.'" class="form-control" value="'.$per->ai_recursos.'" readonly="readonly"/>
	                				</div>
	            				</div>
	        				</div>
	        				<div class="col-xs-12"><hr /></div>
							';
							$num = $num + 1;
						}
					}else{
						echo '
						<div class="col-xs-12 col-md-2">
							<input type="radio" name="aliadosInt" id="aliadosInt" class="disabled radio-form" value="1"> Si
							<input type="radio" name="aliadosInt" id="aliadosInt" class="disabled radio-form" value="2" checked > No
						</div>
						';
					}
					?>
				</div>
				<div class="col-xs-12" id="contentNumAliadosInternos"></div>
			</div>
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<p>Ciudades y/o municipios de inlfuencia</p>
				<div class="col-xs-12 col-md-12">
					<p>Ciudades</p>
					<div class="col-xs-12 col-md-12">
						<?php 
						if ($datos->rh_ciudades>0) {
							echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="ciudades" id="ciudades" class="disabled radio-form" value="1" checked> Si
								<input type="radio" name="ciudades" id="ciudades" class=disabled "radio-form" value="2" > No
							</div>
							';
							$sql = "SELECT * FROM `ciudades` WHERE `rh_id` = '$cod' ";
							$personalIntASC = $clase->detallesRecursosHumanos($sql);
							$n = mysqli_num_rows($personalIntASC);
							
							echo '
							<div class="col-xs-12 col-md-3">
								<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
							</div>
							<div class="col-xs-12">
							';
							$num = 1;
							while ($per = mysqli_fetch_object($personalIntASC)){
								echo '
								<div class="col-xs-12 col-md-6">
		            				<p>Ciudad # '.$num.'</p>
		                    		<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->ci_nombre.'" readonly="readonly"/>
		                		</div>
								';
								$num = $num + 1;
							}
							echo '</div>';
						}else{
							echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="ciudades" id="ciudades" class="disabled radio-form" value="1"> Si
								<input type="radio" name="ciudades" id="ciudades" class="disabled radio-form" value="2" checked > No
							</div>
							';
						}
						?>
					</div>
				</div>
				<div class="col-xs-12" id="contentNumCiudadesInfluencia"></div>
				<div class="col-xs-12"><hr></div>
				<div class="col-xs-12 col-md-12">
					<p>Municipios</p>
					<div class="col-xs-12 col-md-12">
						<?php 
						if ($datos->rh_municipios>0) {
							echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="municipios" id="municipios" class="disabled radio-form" value="1" checked> Si
								<input type="radio" name="municipios" id="municipios" class=disabled "radio-form" value="2" > No
							</div>
							';
							$sql = "SELECT * FROM `municipios` WHERE `rh_id` = '$cod' ";
							$personalIntASC = $clase->detallesRecursosHumanos($sql);
							$n = mysqli_num_rows($personalIntASC);
							
							echo '
							<div class="col-xs-12 col-md-3">
								<input type="number" name="numPersonalIntASC" id="numPersonalIntASC" class="form-control" readonly="readonly" value="'.$n.'" />
							</div>
							<div class="col-xs-12">
							';
							$num = 1;
							while ($per = mysqli_fetch_object($personalIntASC)){
								echo '
								<div class="col-xs-12 col-md-6">
		            				<p>Municipio # '.$num.'</p>
		            				<input type="text" name="nombrePersExt_'.$num.'" id="nombrePersExt_'.$num.'" class="form-control" value="'.$per->mu_nombre.'" readonly="readonly"/>
		        				</div>
								';
								$num = $num + 1;
							}
							echo '</div>';
						}else{
							echo '
							<div class="col-xs-12 col-md-2">
								<input type="radio" name="municipios" id="municipios" class="disabled radio-form" value="1"> Si
								<input type="radio" name="municipios" id="municipios" class="disabled radio-form" value="2" checked > No
							</div>
							';
						}
						?>
					</div>
				</div>
				<div class="col-xs-12" id="contentNumMunicipiosInfliencia"></div>
			</div>
			<div class="col-xs-12"><hr></div>
			<div class="col-xs-12">
				<p>Descripción de la alianza y objetivos</p>
				<textarea name="descripcionAlianza" id="descripcionAlianza" class="form-control" cols="30" rows="3"><?php echo $datos->rh_descipcion_alianza_obj; ?></textarea>
			</div>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>

	<?php 

	$linea = 0;
	if ($_GET['linea']=='investigacion') {
		$linea = 1;
	}else{
		if ($_GET['linea']=='innovacion') {
			$linea = 2;
		}else{
			if ($_GET['linea']=='modernizacion') {
				$linea = 3;
			}else{
				if ($_GET['linea']=='divulgacion') {
					$linea = 4;
				}else{
					if ($_GET['linea']=='servicios-tecnologicos') {
						$linea = 5;
					}else{
						if ($_GET['linea']=='concursos') {
							$linea = 6;
						}
					}
				}
			}
		}
	}

	?>

	<div class="col-xs-12">
		<h3>Recursos Sennova
			<a class="btn-floating btn-small red right" id="btn-recursosS" onclick="deplegarRecursosSennova()" id="btnrg2to1" style="float: right;">
	        	<i class="fa fa-angle-down"></i>
	        </a>
	        <input type="hidden" id="valorReSe" value="0">
		</h3>
		<div class="col-xs-12" id="contentRecursosSennova" style="display: none;">

			<!-- Investigacion & innovacion-->
			<?php if ($linea == 1 || $linea==2 ) { $dis_1='block';}else{$dis_1='none';} ?>

			<div class="conten_1" style="display: <?php echo $dis_1;?>">
			<div class="col-xs-12">
				<p>Otros servicios profesionales indirectos (Aprendices)</p>
				<?php 
				if ($datos->rh_otros_serv_pers_ind_aprend_valor > 0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrosServPerInd" id="otrosServPerInd" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="otrosServPerInd" id="otrosServPerInd" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos">
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_otros_serv_pers_ind_aprend_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_otros_serv_pers_ind_aprend_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrosServPerInd" id="otrosServPerInd" class="disabled radio-form" value="1"> Si
						<input type="radio" name="otrosServPerInd" id="otrosServPerInd" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
			

			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea==4 || $linea == 5 ) { $dis_2='block';}else{$dis_2='none';} ?>
			
			<div class="conten_2" style="display: <?php echo $dis_2;?>">
			<div class="col-xs-12">
				<p>Servicios Personales Indirectos</p>
				<?php 
				if ($datos->rh_otros_serv_pers_ind_aprend_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="serviPersInd" id="serviPersInd" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="serviPersInd" id="serviPersInd" class="disabled radio-form" value="2" > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos">
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_serv_pers_indirectos_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_serv_pers_indirectos_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="serviPersInd" id="serviPersInd" class="disabled radio-form" value="1"> Si
						<input type="radio" name="serviPersInd" id="serviPersInd" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>


			


			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_3='block';}else{$dis_3='none';} ?>
			
			<div class="conten_3" style="display: <?php echo $dis_3;?>">
			<div class="col-xs-12">
				<p>Equipo de sistemas</p>
				<?php 
				if ($datos->rh_eq_sis_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="equipSistemas" id="equipSistemas" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="equipSistemas" id="equipSistemas" class="disabled radio-form" value="2"> No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_eq_sis_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_eq_sis_descripcion_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="equipSistemas" id="equipSistemas" class="disabled radio-form" value="1"> Si
						<input type="radio" name="equipSistemas" id="equipSistemas" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
	
			
			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_4='block';}else{$dis_4='none';} ?>
			
			<div class="conten_4" style="display: <?php echo $dis_4;?>">
			<div class="col-xs-12">
				<p>Software</p>
				<?php 
				if ($datos->rh_eq_soft_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="software" id="software" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="software" id="software" class="disabled radio-form" value="2"> No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_eq_soft_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_eq_soft_descripcion_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="software" id="software" class="disabled radio-form" value="1"> Si
						<input type="radio" name="software" id="software" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_5='block';}else{$dis_5='none';} ?>
			
			<div class="conten_5" style="display: <?php echo $dis_5;?>">
			<div class="col-xs-12">
				<p>Maquinaria Industrial</p>
				<?php 
				if ($datos->rh_eq_mind_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="maquinariaInd" id="maquinariaInd" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="maquinariaInd" id="maquinariaInd" class="disabled radio-form" value="2" > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor Maquinaria</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_eq_mind_valor.'" readonly="readonly"/>
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"  readonly="readonly">'.$datos->rh_eq_mind_descripcion_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="maquinariaInd" id="maquinariaInd" class="disabled radio-form" value="1"> Si
						<input type="radio" name="maquinariaInd" id="maquinariaInd" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>


			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_6='block';}else{$dis_6='none';} ?>
			
			<div class="conten_6" style="display: <?php echo $dis_6;?>">
			<div class="col-xs-12">
				<p>Otras Compras de equipos</p>
				<?php 
				if ($datos->rh_eq_otras_compras_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrasCompras" id="otrasCompras" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="otrasCompras" id="otrasCompras" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor Maquinaria</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_eq_otras_compras_valor.'" readonly="readonly"/>
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_eq_otras_compras_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrasCompras" id="otrasCompras" class="disabled radio-form" value="1"> Si
						<input type="radio" name="otrasCompras" id="otrasCompras" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
			

			<!--investigacion & innovacion & modenizacion & divulgacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 || $linea == 4) { $dis_7='block';}else{$dis_7='none';} ?>
			
			<div class="conten_7" style="display: <?php echo $dis_7;?>">
			<div class="col-xs-12">
				<p>Material para formacion profesional</p>
				<?php 
				if ($datos->rh_formacion_prof_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="matformProf" id="matformProf" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="matformProf" id="matformProf" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor Material</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_formacion_prof_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_formacion_pro_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="matformProf" id="matformProf" class="disabled radio-form" value="1"> Si
						<input type="radio" name="matformProf" id="matformProf" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_8='block';}else{$dis_8='none';} ?>
			
			<div class="conten_8" style="display: <?php echo $dis_8;?>">
			<div class="col-xs-12">
				<p>Mantenimiento de maquinaria, equipo, transporte y software</p>
				<?php 
				if ($datos->rh_mantenimiento_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="mantMaquinaria" id="mantMaquinaria" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="mantMaquinaria" id="mantMaquinaria" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_mantenimiento_valor.'"  readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"  readonly="readonly">'.$datos->rh_mantenimiento_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="mantMaquinaria" id="mantMaquinaria" class="disabled radio-form" value="1"> Si
						<input type="radio" name="mantMaquinaria" id="mantMaquinaria" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>


			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_9='block';}else{$dis_9='none';} ?>
			
			<div class="conten_9" style="display: <?php echo $dis_9;?>">
			<div class="col-xs-12">
				<p>Otras comunicaciones y transporte</p>
				<?php 
				if ($datos->rh_otras_comun_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrasComunicaciones" id="otrasComunicaciones" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="otrasComunicaciones" id="otrasComunicaciones" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_otras_comun_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_otras_comun_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrasComunicaciones" id="otrasComunicaciones" class="disabled radio-form" value="1"> Si
						<input type="radio" name="otrasComunicaciones" id="otrasComunicaciones" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion & innovacion & modernizacion-->
			<?php if ($linea==1 || $linea==2 || $linea==3 ) { $dis_10='block';}else{$dis_10='none';} ?>
			
			<div class="conten_10" style="display: <?php echo $dis_10;?>">
			<div class="col-xs-12">
				<p>Adecuaciones y contrucciones</p>
				<?php 
				if ($datos->rh_adecuaciones_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="adecuacionesContr" id="adecuacionesContr" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="adecuacionesContr" id="adecuacionesContr" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_adecuaciones_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_adecuaciones_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="adecuacionesContr" id="adecuacionesContr" class="disabled radio-form" value="1"> Si
						<input type="radio" name="adecuacionesContr" id="adecuacionesContr" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
			

			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea == 4 || $linea==5) { $dis_11='block';}else{$dis_11='none';} ?>
			
			<div class="conten_11" style="display: <?php echo $dis_11;?>">
			<div class="col-xs-12">
				<p>Otros gastos por impresos y publicaciones</p>
				<?php 
				if ($datos->rh_otros_gastos_impre_public_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrosGastos" id="otrosGastos" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="otrosGastos" id="otrosGastos" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_otros_gastos_impre_public_valor.'"   readonly="readonly"/>
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?"  readonly="readonly">'.$datos->rh_otros_gastos_impre_public_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="otrosGastos" id="otrosGastos" class="disabled radio-form" value="1"> Si
						<input type="radio" name="otrosGastos" id="otrosGastos" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>




			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea == 4 || $linea==5) { $dis_12='block';}else{$dis_12='none';} ?>
			
			<div class="conten_12" style="display: <?php echo $dis_12;?>">
			<div class="col-xs-12">
				<p>Divulgación de actividades de gestion Institucional</p>
				<?php 
				if ($datos->rh_divulgacion_act_gestion_inst_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="divulgAct" id="divulgAct" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="divulgAct" id="divulgAct" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_divulgacion_act_gestion_inst_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_divulgacion_act_gestion_inst_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="divulgAct" id="divulgAct" class="disabled radio-form" value="1"> Si
						<input type="radio" name="divulgAct" id="divulgAct" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>

			
			<!-- investigacion & innovacion & divulgacion & servicios-tecnologicos -->
			<?php if ($linea == 1 || $linea==2 || $linea == 4 || $linea==5) { $dis_13='block';}else{$dis_13='none';} ?>
			
			<div class="conten_13" style="display: <?php echo $dis_13;?>">
			<div class="col-xs-12">
				<p>Viaticos y gastos de viajes al interior formacion profesional</p>
				<?php 
				if ($datos->rh_viaticos_form_prof_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="viaticos" id="viaticos" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="viaticos" id="viaticos" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_viaticos_form_prof_valor.'"  readonly="readonly"/>
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_viaticos_form_prof_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="viaticos" id="viaticos" class="disabled radio-form" value="1"> Si
						<input type="radio" name="viaticos" id="viaticos" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>

	

			<!-- investigacion & innovacion -->
			<?php if ($linea==1 || $linea==2 ) { $dis_14='block';}else{$dis_14='none';} ?>
			
			<div class="conten_14" style="display: <?php echo $dis_14;?>">
			<div class="col-xs-12">
				<p>Gastos bienestar Alumnos</p>
				<?php 
				if ($datos->rh_gastos_bienestar_alum_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="bienestar" id="bienestar" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="bienestar" id="bienestar" class="disabled radio-form" value="2"  > No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos" >
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_gastos_bienestar_alum_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_gastos_bienestar_alum_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="bienestar" id="bienestar" class="disabled radio-form" value="1"> Si
						<input type="radio" name="bienestar" id="bienestar" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>



			<!-- investigacion -->
			<?php if ($linea==1  ) { $dis_15='block';}else{$dis_15='none';} ?>
			
			<div class="conten_15" style="display: <?php echo $dis_15;?>">
			<div class="col-xs-12">
				<p>Monitorias</p>
				<?php 
				if ($datos->rh_monitorias_valor>0) {
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="monitorias" id="monitorias" class="disabled radio-form" value="1" checked> Si
						<input type="radio" name="monitorias" id="monitorias" class="disabled radio-form" value="2"> No
					</div>
					<div class="col-xs-12" id="contenoptOtrosServPersIndirectos">
						<div class="col-xs-3">
							<p>Valor</p>				
							<input type="number" name="otrosservPersonalesIndirectos" id="otrosservPersonalesIndirectos" class="form-control" placeholder="$COP" value="'.$datos->rh_monitorias_valor.'" readonly="readonly" />
						</div>
						<div class="col-xs-12">
							<p>Descripcion</p>
							<textarea name="descripcionotrosservPersonalesIndirectos" id="descripcionotrosservPersonalesIndirectos" class="form-control" cols="30" rows="5" placeholder="¿Que es? y ¿Para qué es?" readonly="readonly">'.$datos->rh_monitorias_que_para.'
							</textarea>
						</div>
					</div>
					';
				}else{
					echo '
					<div class="col-xs-12 col-md-2">
						<input type="radio" name="monitorias" id="monitorias" class="disabled radio-form" value="1"> Si
						<input type="radio" name="monitorias" id="monitorias" class="disabled radio-form" value="2" checked > No
					</div>
					';
				}
				?>
			</div>
			<div class="col-md-12"><hr></div>
			</div>
		</div>
	</div>	
	<div class="col-xs-12"><hr></div>
		
</div>