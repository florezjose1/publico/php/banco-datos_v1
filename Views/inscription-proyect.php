<?php
	require_once '../Controllers/principalControllers.php';

	$clase = new PrincipalController();
	if (!empty($_GET['ac']) && !empty($_GET['registro'])) {
		if ($_GET['ac']==1 || $_GET['ac']==2) {
		  	session_start();
		  	if(isset($_SESSION['registro'])) {
		  		if (is_numeric($_GET['registro'])) {
		  			if ($_SESSION['registro'] == $_GET['registro']) {
		  				if (!empty($_GET['linea'])) {

?>

<!DOCTYPE html>
<html lang="es">

<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
   <title>Formulario Banco Datos</title>
   <link rel="stylesheet" href="../Public/bootstrap/css/fonts.css">
   <link rel="stylesheet" href="../Public/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" href="../Public/bootstrap/css/mdb.min.css">
   <link rel="stylesheet" href="../Public/css/style.css">
   <link rel="stylesheet" href="../Public/bootstrap/css/ionicons.min.css">
   <link rel="stylesheet" href="../Public/fonts/font-awesome/css/font-awesome.min.css">
   <link rel="stylesheet" href="../Public/fonts/fuentes.css">
   <?php 
      if ($_GET['ac']==1) {?>
   <link rel="stylesheet" href="../Public/css/style-form-step-by-step.css">
   <?php }else{ ?>
   <link rel="stylesheet" href="../Public/css/style-form-direct.css">
   <?php } ?>
</head>


<body>

   <nav class="navbar navbar-default navbar-fixed-top" id="header-banco" style="background-color: #107b71;    border-color: #107b71;">
      <div class="container">
      	<div class="row">
      		<div class="col-xs-2">
		         <a href="../lineas-programaticas.php?registro=<?php echo $_SESSION['registro']?>&ac=<?php echo $_GET['ac']?>" class="" type="submit">
						<button class="btn btn-default pull-left " style="background-color: #359e8d; border-color: #359e8d;">
                     <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                  </button>
					</a>
      		</div>
      		<div class="col-xs-8">
		         <h1 style="text-align: center;color:#fff;margin-top: 15px;font-size: 30px;">
		         	Linea de <?php

                     $li = str_replace('-', ' ', $_GET['linea']);

                   echo $li ;?>
		         </h1>
      		</div>
      	</div>
      </div>
   </nav>

   <div class="container">
      <div class="row">
         <div class="col-md-12"><br></div>
         <div class="col-xs-12">
            <?php 
				// validamos de acuerdo a la seleccion de incripcion del usuario
				if ($_GET['ac']==1) {
               if (!empty($_GET['linea']=='investigacion-aplicada') || !empty($_GET['linea']=='innovacion-y-desarrollo-tecnologico') || !empty($_GET['linea']=='fortalecimiento-de-la-ofeta-de-servicios-tecnologicos-a-las-empresas') || !empty($_GET['linea']=='actualizacion-y-modernizacion-tecnologica')  ) {

                  if ($_GET['form']=='fase-1' ) {
                     
                     include 'formularios/observatorio/formulario.php';

                  }else{
                     echo '<script> window.location="../404.php"; </script>';
                  }
               }else{
                  echo '<script> window.location="../404.php"; </script>';
               }
            }else{
               if ( !empty($_GET['linea']=='investigacion') || !empty($_GET['linea']=='innovacion') || !empty($_GET['linea']=='modernizacion') || !empty($_GET['linea']=='divulgacion') || !empty($_GET['linea']=='servicios-tecnologicos') || !empty($_GET['linea']=='concursos') ) {

                  if ( $_GET['form']=='datos-personales' || $_GET['form']=='informacion-proyecto' || $_GET['form']=='recursos-humanos') {
                     
                     include 'formularios/inscription/formulario.php';

                  }else{
                     echo '<script> window.location="../404.php"; </script>';
                  }
               }else{
                  echo '<script> window.location="../404.php"; </script>';
               }
            }
            ?>
         </div>
      </div>
   </div>


   <!-- modal descartar -->
   <div style="margin-top: 50px;" class="modal fade" id="alertaRegistroDenegado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-xs" role="document">
         <div class="modal-content">
            <div class="modal-body bodyModalAlerta" style="text-align: center;">
               <img src="../Public/img/acceso_denegado.png" alt="">
               <h2>Acceso denegado.! </h2>
               <p>Todavía no esta habilitada esta parte del registro.</p>
               <p><small>"Debes seguir en secuencia"</small></p>
               
               <button style="display: none;" id="cerrarAcceso" data-dismiss="modal" data-toggle="modal"> </button>
            </div>
         </div>
      </div>
   </div>

   <script src="../Public/plugins/jquery/jquery-2.2.3.min.js"></script>
   <script src="../Public/bootstrap/js/bootstrap.min.js"></script>
   <script src="../Public/js/main.js"></script>
   <script>

      $(function () {

      });

   </script>

</body>


<?php 
						}else{
							echo '<script> window.location="../404.php"; </script>';
						}
					}else{
						echo '<script> window.location="../404.php"; </script>';
					}
				}else{
					echo '<script> window.location="../404.php"; </script>';
				}
			}else{
				echo '<script> window.location="../404.php"; </script>';
			}
		}else{
			echo '<script> window.location="../404.php"; </script>';
		}
	}else{
		echo '<script> window.location="../404.php"; </script>';
	}
?>