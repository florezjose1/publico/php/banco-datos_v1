<?php
session_start();
if (!empty($_GET['ac'])) {
  if (is_numeric($_GET['registro'])) {
    if ($_SESSION['registro'] == $_GET['registro']) {

    	if (is_numeric($_GET['ac'])==1 || is_numeric($_GET['ac'])==2) {
        if ($_GET['ac']==1) {
          $href = 'inscription-proyect';
        }
        if ($_GET['ac']==2) {
          $href = 'inscription-proyect';
        }
?>


<!DOCTYPE html>
<html lang="en">
<head>
  	<title>Lineas programaticas</title>
  	<meta charset="utf-8">
  	<link rel="stylesheet" href="../Public/css/style-lineas.css">
  	<link rel="stylesheet" href="../Public/bootstrap/css/fonts.css">
  	<link rel="stylesheet" href="../Public/bootstrap/css/bootstrap.min.css">
  	<link rel="stylesheet" href="../Public/bootstrap/css/mdb.min.css">
  	<link rel="stylesheet" href="../Public/bootstrap/css/compiled.css">
  	<link rel="stylesheet" href="../Public/bootstrap/css/ionicons.min.css">
  	<link rel="stylesheet" href="../Public/fonts/font-awesome/css/font-awesome.min.css">
</head>





<body style="background-color:#eee;">

	<nav class="navbar navbar-default navbar-fixed-top" id="header-banco" style="background-color: #2f3e5d;    border-color: #2f3e5d;text-align: center;">
      <div class="row">
        	<div class="col-xs-2 col-md-2">
          	<a href="../index.php">
              <button class="btn btn-info pull-left btn-regreso" type="submit">
                	<i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
              </button>
            </a>
        	</div>
        	<div class="col-xs-8 col-md-8">
          	<h1 style="color:#fff;margin-top: 15px;font-size: 30px;">Lineas Programaticas</h1>
        	</div>
      </div>
   </nav>
   <br>
   <br>
   <br>
  
    <?php 
      if ($_GET['ac']==1) {
    ?>
    <br><br>
   
    <div class="container">
      <!--Collection card-->
            <div class="card collection-card" id="abreinvestigacion">
              <!--Card image-->
              <div class="view  hm-zoom">
                <img src="../Public/img/a1.jpg" class="img-lineas img-fluid" alt="">
                <div class="stripe light "> <!-- dark -->
                  <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=investigacion-aplicada&form=fase-1">
                    <p style="font-size: 15px;">INVESTIGACION APLICADA. <!--<i class="fa fa-chevron-right"></i>--></p>
                  </a>
                  <p> (Máximo $95.000.000).</p>
                </div>
              </div>
              <!--/.Card image-->
            </div>
            <!--/.Collection card-->
<!--Collection card-->
            <div class="card collection-card caja" id="abreinnovacion">
              <!--Card image-->
              <div class="view  hm-zoom">
                <img src="../Public/img/investigacion/innovacion.jpg" class="img-lineas img-fluid" alt="">
                <div class="stripe light"> <!-- dark -->
                  <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=innovacion-y-desarrollo-tecnologico&form=fase-1">
                    <p style="font-size: 15px;">INNOVACIÓN Y DESARROLLO TECNOLÓGICO. <!--<i class="fa fa-chevron-right"></i>--></p>
                  </a>
                  <p> (Máximo $100.000.000).</p>
                </div>
              </div>
              <!--/.Card image-->
            </div>
            <!--/.Collection card-->
<!--Collection card-->
            <div class="card collection-card" id="abreservicios"  >
              <!--Card image-->
              <div class="view  hm-zoom">
                <img src="../Public/img/investigacion/fortaleza.png" class="img-lineas img-fluid" alt="">
                <div class="stripe light "> <!-- dark -->
                  <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=fortalecimiento-de-la-ofeta-de-servicios-tecnologicos-a-las-empresas&form=fase-1">
                    <p style="font-size: 15px;">  
                    FORTALECIMIENTO DE LA OFERTA DE SERVICIOS TECNOLÓGICOS A LAS EMPRESAS. <!--<i class="fa fa-chevron-right"></i>--></p>
                  </a>
                  <p>  (Máximo $500.000.000).</p>
                </div>
              </div>
              <!--/.Card image-->
            </div>
            <!--/.Collection card-->
<!--Collection card-->
            <div class="card collection-card" id="abremodernizacion">
              <!--Card image-->
              <div class="view  hm-zoom">
                <img src="../Public/img/investigacion/actualizacion.jpg" class="img-lineas img-fluid" alt="">
                <div class="stripe light"> <!-- dark -->
                  <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=actualizacion-y-modernizacion-tecnologica&form=fase-1">
                    <p style="font-size: 15px;">ACTUALIZACIÓN Y MODERNIZACIÓN TECNOLÓGICA DE CENTRO. <!--<i class="fa fa-chevron-right"></i>--></p>
                  </a>
                  <p> (Máximo $400.000.000).</p>
                </div>
              </div>
              <!--/.Card image-->
            </div>
            <!--/.Collection card-->
    </div>

    <?php }else{ ?>
    <!-- #gallery -->
  	<section id="gallery">
  	<div class="container">
    	<ul id="myRoundabout">
         <li>
        		<a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=investigacion&form=datos-personales">
        			<img src="../Public/img/lineas/lineas/investigacion.png" alt="">
        		</a>
        	</li>
      	<li>
      		<a href="inscription-proyect.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=innovacion&form=datos-personales">
      			<img src="../Public/img/lineas/lineas/innovacion.png" alt="">
      		</a>
      	</li>


      	<li>
      		<a href="inscription-proyect.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=modernizacion&form=datos-personales">
      			<img src="../Public/img/lineas/lineas/modernizacion.png" alt="">
      		</a>
      	</li>


      	<li>
      		<a href="inscription-proyect.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=servicios-tecnologicos&form=datos-personales">
      			<img src="../Public/img/lineas/lineas/servicios.png" alt="">
      		</a>
      	</li>


      	<li>
      		<a href="inscription-proyect.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=divulgacion&form=datos-personales">
      			<img src="../Public/img/lineas/lineas/divulgacion.png" alt="">
      		</a>
      	</li>
      </ul>
  	</div>
  	</section>
  	<!-- /#gallery -->
    <?php } ?>
	

<!-- Modal Bienvenida -->
<div class="modal fade modal-ext" id="modal-investigacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <!--Content-->
    <div class="modal-content modal-md" style="margin-top: 100px;">
      <img src="../Public/img/bienvenido.png" alt="" class="img-responsive" style="width: 100%;">   
    </div>
    <!--/.Content-->
  </div>
</div>

	<script type="text/javascript" src="../Public/plugins/jquery/jquery-1.9.1.min.js" ></script>
  <script src="../Public/bootstrap/js/bootstrap.js"></script>
	<script type="text/javascript" src="../Public/plugins/galeria/roundabout.js"></script>
	<script type="text/javascript" src="../Public/plugins/galeria/roundabout_shapes.js"></script>
	<script type="text/javascript" src="../Public/plugins/galeria/gallery_init.js"></script>
  <script>
    $(function(){
      /*$('.card').hover(function(){
        $(this).modal('show');
      });
      $('.card').mouseleave(function(){
        $(this).removeClass('modal-investigacion');
      });
      $("html").hover(function() {
          alert("Click!");
      });
      $('.caja').hover(function (e) {
          e.stopPropagation();
      });*/
    });
  </script>
</body>



<?php 
    	}else{
    		echo '<script> window.location="../404.php"; </script>';
    	}
    }else{
      echo '<script> window.location="../404.php"; </script>';
    }
  }else{
    echo '<script> window.location="../404.php"; </script>';
  }
}else{
	echo '<script> window.location="../404.php"; </script>';
}
?>