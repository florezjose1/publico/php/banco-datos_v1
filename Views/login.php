<?php
  	session_start();
  	if( isset($_SESSION['id']) && isset($_SESSION['user']) && isset($_SESSION['rol']) ) {

   	$id = $_SESSION['id'];
   	$user = htmlentities(addslashes($_SESSION['user']));
   	require_once '../Controllers/principalControllers.php';
   	$clase = new PrincipalController();

   	$error = '
			<center>
				<img src="../Public/img/acceso_denegado.png" alt="">
				<h1>Error.! 404</h2><h4>page not found</h4></h1>
			</center>
      ';

   	if ($_SESSION['rol']==1) {
   		include 'login/index-admin.php';
		}else{
   		include 'login/index-user.php';
		}


	}else{
		echo '<script> window.location="../index.php"; </script>';
	}

?>