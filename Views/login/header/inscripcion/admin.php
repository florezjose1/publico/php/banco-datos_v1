<li class="treeview">
  <a href="#">
    <i class="fa fa-search"></i> <span>Investigación</span>
    <span class="pull-right-container">
      <?php 
      if ($investigacion>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $investigacion .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li>
      <a href="login.php?ac=aprobados&linea=investigacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-green"></i> 
        <span>Aprobados</span>
          <?php 
          if ($aprobadosInvestigacion!=false) {
            $n = mysqli_num_rows($aprobadosInvestigacion);
            echo '
              <span class="label label-success pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=rechazados&linea=investigacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-red"></i>
        <span>Rechazados</span>
        <?php 
          if ($rechazadosInvestigacion!=false) {
            $n = mysqli_num_rows($rechazadosInvestigacion);
            echo '
              <span class="label label-danger pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=inscriptos&linea=investigacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-blue"></i>
        <span>Inscriptos</span>
        <?php 
          if ($inscritosInvestigacion!=false) {
            $n = mysqli_num_rows($inscritosInvestigacion);
            echo '
              <span class="label label-primary pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <!--<li>
      <a href="login.php?ac=fases&linea=investigacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-aqua"></i> 
        <span>R. Fases</span>
        <span class="label label-primary pull-right">4</span>
      </a>
    </li>
    <li>
      <a href="login.php?ac=directo&linea=investigacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-yellow"></i> 
        <span>R. Directo</span>
        <span class="label label-warning pull-right">4</span>
      </a>
    </li>-->
  </ul> 
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-lightbulb-o"></i>
    <span>Innovación</span>
    <span class="pull-right-container">
      <?php 
      if ($innovacion>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $innovacion .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li>
      <a href="login.php?ac=aprobados&linea=innovacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-green"></i> 
        <span>Aprobados</span>
          <?php 
          if ($aprobadosInnovacion!=false) {
            $n = mysqli_num_rows($aprobadosInnovacion);
            echo '
              <span class="label label-success pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=rechazados&linea=innovacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-red"></i>
        <span>Rechazados</span>
        <?php 
          if ($rechazadosInnovacion!=false) {
            $n = mysqli_num_rows($rechazadosInnovacion);
            echo '
              <span class="label label-danger pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=inscriptos&linea=innovacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-blue"></i>
        <span>Inscriptos</span>
        <?php 
          if ($inscritosInnovacion!=false) {
            $n = mysqli_num_rows($inscritosInnovacion);
            echo '
              <span class="label label-primary pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
  </ul> 
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-laptop"></i> <span>Modernización</span>
    <span class="pull-right-container">
      <?php 
      if ($modernizacion>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $modernizacion .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li>
      <a href="login.php?ac=aprobados&linea=modernizacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-green"></i> 
        <span>Aprobados</span>
          <?php 
          if ($aprobadosModernizacion!=false) {
            $n = mysqli_num_rows($aprobadosModernizacion);
            echo '
              <span class="label label-success pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=rechazados&linea=modernizacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-red"></i>
        <span>Rechazados</span>
        <?php 
          if ($rechazadosModernizacion!=false) {
            $n = mysqli_num_rows($rechazadosModernizacion);
            echo '
              <span class="label label-danger pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=inscriptos&linea=modernizacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-blue"></i>
        <span>Inscriptos</span>
        <?php 
          if ($inscritosModernizacion!=false) {
            $n = mysqli_num_rows($inscritosModernizacion);
            echo '
              <span class="label label-primary pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-bullhorn"></i> <span>Divulgación</span>
    <span class="pull-right-container">
      <?php 
      if ($divulgacion>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $divulgacion .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li>
      <a href="login.php?ac=aprobados&linea=divulgacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-green"></i> 
        <span>Aprobados</span>
          <?php 
          if ($aprobadosDivulgacion!=false) {
            $n = mysqli_num_rows($aprobadosDivulgacion);
            echo '
              <span class="label label-success pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=rechazados&linea=divulgacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-red"></i>
        <span>Rechazados</span>
        <?php 
          if ($rechazadosDivulgacion!=false) {
            $n = mysqli_num_rows($rechazadosDivulgacion);
            echo '
              <span class="label label-danger pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=inscriptos&linea=divulgacion&proyectos=inscriptos">
        <i class="fa fa-circle-o text-blue"></i>
        <span>Inscriptos</span>
        <?php 
          if ($inscritosDivulgacion!=false) {
            $n = mysqli_num_rows($inscritosDivulgacion);
            echo '
              <span class="label label-primary pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
  </ul>
</li>

<li class="treeview">
  <a href="#">
    <i class="fa fa-gamepad"></i> <span>Servicios tecnológicos</span>
    <span class="pull-right-container">
      <?php 
      if ($servicios>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $servicios .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
  <ul class="treeview-menu">
    <li>
      <a href="login.php?ac=aprobados&linea=servicios-tecnologicos&proyectos=inscriptos">
        <i class="fa fa-circle-o text-green"></i> 
        <span>Aprobados</span>
          <?php 
          if ($aprobadosServicios!=false) {
            $n = mysqli_num_rows($aprobadosServicios);
            echo '
              <span class="label label-success pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=rechazados&linea=servicios-tecnologicos&proyectos=inscriptos">
        <i class="fa fa-circle-o text-red"></i>
        <span>Rechazados</span>
        <?php 
          if ($rechazadosServicios!=false) {
            $n = mysqli_num_rows($rechazadosServicios);
            echo '
              <span class="label label-danger pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
    <li>
      <a href="login.php?ac=inscriptos&linea=servicios-tecnologicos&proyectos=inscriptos">
        <i class="fa fa-circle-o text-blue"></i>
        <span>Inscriptos</span>
        <?php 
          if ($inscritosServicios!=false) {
            $n = mysqli_num_rows($inscritosServicios);
            echo '
              <span class="label label-primary pull-right">
              '. $n .'
              </span>

            ';
          }
          ?>
      </a>
    </li>
  </ul>
</li>
       
