<li class="treeview">
  <a href="login.php?ac=aprobados&proyectos=observatorio">
    <i class="fa fa-circle-o text-green"></i>  <span>Aprobados</span>
    <span class="pull-right-container">
      <?php 
      if ($proyectosAprobados!=false) {
        $n = mysqli_num_rows($proyectosAprobados);
        echo '
          <span class="label label-primary pull-right">
            '. $n .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
</li>

<li class="treeview">
  <a href="login.php?ac=rechazados&proyectos=observatorio">
    <i class="fa fa-circle-o text-red"></i> <span>Rechazados</span>
    <span class="pull-right-container">
      <?php 
      if ($proyectosRechazados!=false) {
            $n = mysqli_num_rows($proyectosRechazados);
        echo '
          <span class="label label-primary pull-right">
            '. $n .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
</li>

<li class="treeview">
  <a href="login.php?ac=inscriptos&proyectos=observatorio">
    <i class="fa fa-circle-o text-blue"></i> <span>Inscriptos</span>
    <span class="pull-right-container">
      <?php 
      if ($proyectosInscriptos!=false) {
        $n = mysqli_num_rows($proyectosInscriptos);
        echo '
          <span class="label label-primary pull-right">
            '. $n .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
</li>
