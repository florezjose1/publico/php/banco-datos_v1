<li class="treeview">
  <a href="login.php?ac=aprobados&proyectos=observatorio">
    <i class="fa fa-circle-o text-green"></i>  <span>Aprobados</span>
    <span class="pull-right-container">
      <?php 
      if ($a>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $a .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
</li>

<li class="treeview">
  <a href="login.php?ac=rechazados&proyectos=observatorio">
    <i class="fa fa-circle-o text-red"></i> <span>Rechazados</span>
    <span class="pull-right-container">
      <?php 
      if ($r>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $r .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
</li>

<li class="treeview">
  <a href="login.php?ac=inscriptos&proyectos=observatorio">
    <i class="fa fa-circle-o text-blue"></i> <span>Inscriptos</span>
    <span class="pull-right-container">
      <?php 
      if ($i>0) {
        echo '
          <span class="label label-primary pull-right">
            '. $i .'
          </span>
        ';
      }else{
        echo '
          <i class="fa fa-angle-left pull-right"></i>
        ';
      }
      ?>
      </span>
    </span>
  </a>
</li>
