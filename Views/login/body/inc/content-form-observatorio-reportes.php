<div class="col-xs-4" style="border-right: 1px solid #eee;">
		<h3>Fase</h3>
	<div class="col-xs-12">
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="fase_1" onclick="unaOpcionReportes(2)" id="fase_1" <?php if (!empty($_GET['fase1'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="fase_1">Fase 1</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="fase_2" onclick="unaOpcionReportes(2)" id="fase_2" <?php if (!empty($_GET['fase2'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="fase_2">Fase 2</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="fase_3" onclick="unaOpcionReportes(2)" id="fase_3" <?php if (!empty($_GET['fase3'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="fase_3">Fase 3</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="fase_4" onclick="unaOpcionReportes(2)" id="fase_4" <?php if (!empty($_GET['fase4'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="fase_4">Fase 4</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="todas_fases" id="todas_fases" onclick="unaOpcionReportes(1)" <?php if (!empty($_GET['fase1'])&&!empty($_GET['fase2'])&&!empty($_GET['fase3'])&&!empty($_GET['fase4'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="todas_fases">Todas las Fases</label>
		</fieldset>
	</div>
</div>

<div class="col-xs-6">
	<h3>Linea programática</h3>
	<div class="col-xs-12">
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="investigacion" onclick="unaOpcionReportes(3)" id="linea_1" <?php if (!empty($_GET['linea1'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="linea_1">INVESTIGACIÓN APLICADA</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="innovacion" onclick="unaOpcionReportes(3)" id="linea_2" <?php if (!empty($_GET['linea2'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="linea_2">INNOVACIÓN Y DESARROLLO TECNOLÓGICO</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="servicios" onclick="unaOpcionReportes(3)" id="linea_3" <?php if (!empty($_GET['linea3'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="linea_3">FORTALECIMIENTO DE LA OFERTA DE SERVICIOS TECNOLÓGICOS A LAS EMPRESAS</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="modernizacion" onclick="unaOpcionReportes(3)" id="linea_4" <?php if (!empty($_GET['linea4'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="linea_4">ACTUALIZACIÓN Y MODERNIZACIÓN TECNOLÓGICA DE CENTRO</label>
		</fieldset>
		<fieldset class="form-group">
		    <input type="checkbox" class="filled-in" name="todas_lineas" id="todas_lineas" onclick="unaOpcionReportes(4)" <?php if (!empty($_GET['linea1'])&&!empty($_GET['linea2'])&&!empty($_GET['linea3'])&&!empty($_GET['linea4'])) {echo 'checked';} ?>>
		    <label style="font-size: 12px;" for="todas_lineas">Todas las lineas</label>
		</fieldset>
	</div>
	<a onclick="generarReporte()" href="#" class="btn btn-default pull-right" style="background-color: #fefefe;color: #333;border-color: #adadad;box-shadow: 0 5px 11px 0 rgba(0,0,0,.18), 0 4px 15px 0 rgba(0,0,0,.15);">Generar Reporte</a>
</div>