<center>
	<h2>Lista de usuarios
		
		<a class="btn-floating btn-small red right pull-right" id="btn-DatosPersonales btnrg2to1" onclick="busquedaLinea('btn-DatosPersonales','valorrDatosPersonales','contentDatosDatosPersonales')" >
		    <i class="fa fa-search"></i>
		</a>
		<a class="btn-floating btn-small red right pull-right" id="btn-DatosPersonales btnrg2to1" data-toggle="modal" data-target="#modal-registro" >
		    <i class="fa fa-user-plus"></i>
		</a>
	</h2>
	<div class="row form-buscar-x-linea" style="display: none;">

		<div class="col-xs-12 col-md-offset-2 col-md-6">
		 	<form action="" method="post" name="search_form" id="search_form">
            <input type="text" name="campoBuscar" id="search" class="form-control" placeholder="Valor a buscar" style="font-size: 16px;" >
         </form>
		</div>
		<div class="col-xs-12 col-md-2">
			
			<button type="button" style="margin-top: 0px;" class="btn btn-success btn-flat" onclick="selectEstadisticas(2)">Buscar</button>
		</div>
	</div>
	<br>
</center>

<table class="table table-bordered" id="mytable" style=" background-color: #fff;text-align: center;font-size:20px;">
	<thead style="background-color: #5099c3;color: #fff;">
		<tr>
			<th style="text-align: center;">#</th>
			<th style="text-align: center;">Nombre</th>
			<th style="text-align: center;">Email</th>
			<th style="text-align: center;" colspan="2">Opciones</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$n = 1;
		$usuarios = $clase->listUsuarios();
		while ($row = mysqli_fetch_object($usuarios)) {
			echo '<tr>
				<td>'.$n.'</td>
				<td>'.$row->lg_nombre.'</td>
				<td>'.$row->lg_user.'</td>
				<td><a href="#" ><i class="fa fa-pencil-square-o"></i></a></td>
				<td><a href="#" onclick="alertEliminar('.$row->lg_id.')" ><i class="fa fa-trash"></i></a></td>
	
			</tr>';
			$n = $n + 1;
		}

		?>
	</tbody>
</table>