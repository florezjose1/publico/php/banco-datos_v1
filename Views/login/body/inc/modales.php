<!-- Modal Inicio -->
<div class="modal fade modal-ext" id="modal-registro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content modal-md">
      <!--Header-->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="w-100"><i class="fa fa-user"></i> Registrar Usuario</h3>
      </div>
      <!--Body-->
      <div class="modal-body">
        <div class="form">
          <div class="md-form">
            <p>Nombre</p>
            <input type="email" id="nombre" name="nombre"  class="form-control">
          </div>
          <div class="md-form">
            <p>email</p>
            <input type="email" id="email" name="email" onkeyup="verificarEmail()" class="form-control">
            <div class="alert-email" style="display: none;">
              <img style="float: left; margin-right: 10px;" src="../Public/img/loader.gif" alt="" width="20px">
              <p></p>
            </div>
          </div>
          <div class="md-form">
            <p>Clave</p>
            <input type="password" id="clave" name="clave"  class="form-control">
          </div>

          <div class="md-form">
            <div id="alert" style="display: none;"></div>
          </div>
          <input type="reset" style="display: none;" id="resetRegistro" class="form-control">
            
          <div class="text-center">
            <center>
              <button type="button" onclick="registrarUsuario()" class="btn-registrar btn btn-success btn-sm">Registrar</button>
                    
            </center>
          </div>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" id="cerrarRegistro" class="btn btn-warning btn-sm ml-auto" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>



<!-- Modal Inicio -->
<div class="modal fade modal-ext" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content modal-md">
      <!--Header-->
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="w-100"><i class="fa fa-user"></i> Actualizar Usuario</h3>
      </div>
      <!--Body-->
      <div class="modal-body">
        <div class="form">
          <div class="md-form">
            <p>Nombre</p>
            <input type="email" id="nombreU" name="nombreU"  class="form-control">
            <input type="hidden" id="cod" name="cod"  class="form-control">
          </div>
          <div class="md-form">
            <p>email</p>
            <input type="email" id="emailU" name="emailU" onkeyup="verificarEmail()" class="form-control">
            <div class="alert-email" style="display: none;">
              <img style="float: left; margin-right: 10px;" src="../Public/img/loader.gif" alt="" width="20px">
              <p></p>
            </div>
          </div>
          <div class="md-form">
            <p>Clave</p>
            <input type="password" id="claveU" name="claveU"  class="form-control">
          </div>

          <div class="md-form">
            <div id="alert" style="display: none;"></div>
          </div>
          <input type="reset" style="display: none;" id="resetRegistro" class="form-control">
            
          <div class="text-center">
            <center>
              <button type="button" onclick="actualizarUsuario()" class="btn-registrar btn btn-success btn-sm">Actualizar</button>
                    
            </center>
          </div>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer">
        <button type="button" id="cerrarRegistro" class="btn btn-warning btn-sm ml-auto" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>