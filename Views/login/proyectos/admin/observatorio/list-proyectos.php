<?php 
if ($_GET['ac']    =='inscriptos') $estado = 0;
if ($_GET['ac']    =='aprobados') $estado = 1;
if ($_GET['ac']    =='rechazados') $estado = 2;
?>

<br>
<center>
	<h2>Proyectos <?php echo $_GET['ac'] . ' Observatorio'?>
	
		<a href="#" onclick="busquedaLinea()" class="pull-right">
			<i class="fa fa-search" aria-hidden="true"></i>
		</a>

	</h2>
	<div class="row form-buscar-x-linea" style="display: none;">

		<div class="col-xs-12 col-md-offset-2 col-md-6">
		 	<form action="" method="post" name="search_form" id="search_form">
            <input type="text" name="campoBuscar" id="search" class="form-control" placeholder="Valor a buscar" style="font-size: 16px;" >
         </form>
		</div>
		<div class="col-xs-12 col-md-2">
			<button type="button" style="margin-top: 0px;" class="btn btn-success btn-flat" onclick="selectEstadisticas(2)">Buscar</button>
		</div>
	</div>
	<br>
</center>

<?php 
$infoP = $clase->infoProyectosObservatio($estado);

if ($infoP!=false) {
	$html = '
	<table class="table table-bordered" id="mytable" style=" background-color: #fff;text-align: center;font-size:20px;">
		<thead style="background-color: darkcyan;">
			<tr>
				<th style="text-align: center;">Idea del Proyecto</th>
				<th style="text-align: center;">Autor</th>
				<th style="text-align: center;">Email</th>
				<th style="text-align: center;">Telefono</th>
				<th style="text-align: center;">Formación</th>';
				if ($_GET['ac']=='inscriptos') {
					$html.= '<th colspan="3" style="text-align: center; ">Opciones</th>';
				}else{
					$html.= '<th colspan="2" style="text-align: center; ">Opciones</th>';
					
				}
			$html.= '</tr>
		</thead>
		<tbody id="cuerpoListProyectos">
	';

	while ($row = mysqli_fetch_object($infoP)) {
		// datos de autor - radicador del proyecto
		$autor = $row->dpr_id;
		$infoAutor = $clase->datosPersonalesRadicador($autor);
		$inf = mysqli_fetch_object($infoAutor);
		$nombre = $inf->dpr_nombre;
		$email = $inf->dpr_email;
		$telefono = $inf->dpr_telefono;
		
		$cod = $row->f1_id;
		if ($row->estado==0) {
			$color = 'rgba(21, 31, 88, 0.1)';
		}else{
			$color = 'rgba(226, 226, 226, 0.1)';
		}

		$html.= '
		<tr style="background-color:'.$color.';">
			<td>'.$row->f1_idea.'</td>
			<td>'.$nombre.'</td>
			<td>'.$email.'</td>
			<td>'.$telefono.'</td>
			<td>'.$inf->dpr_formacion.'</td>';
			if ($_GET['ac']=='inscriptos') {
				$html.= '
				<td style="width: 50px;">
		         <a href="login.php?ac='.$_GET['ac'].'&proyectos='.$_GET['proyectos'].'&id='.$row->f1_id.'&autor='.$autor.'" class="btn-floating btn-small blue right">
		            <i class="fa fa-low-vision" ></i>
		         </a>
		      </td>
				<td style="width: 50px;">
		         <a href="#" class="btn-floating btn-small green right" onclick="abrirModal_aprobar('.$cod.')">
		            <i class="fa fa-check"></i>
		         </a>
		      </td>
		      <td style="width: 50px;">
		         <a href="#" class="btn-floating btn-small red right" onclick="abrirModal_descartar('.$cod.')">
		            <i class="fa fa-close"></i>
		         </a>
		      </td>
			</tr>
			';
			}else{
				$html.='
				<td style="width: 20px;">
	            <a href="login.php?ac='.$_GET['ac'].'&proyectos='.$_GET['proyectos'].'&id='.$row->f1_id.'&autor='.$autor.'" class="btn-floating btn-small blue right">
	               <i class="fa fa-low-vision" ></i>
	            </a>
	         </td>';
	         if ($_GET['ac']=='rechazados') {
	            $html.='
	            <td style="width: 20px;">
		            <a href="#" class="btn-floating btn-small green right" onclick="abrirModal_aprobar('.$cod.')">
		               <i class="fa fa-check"></i>
		            </a>
		         </td>
				</tr>';
	         }else{
					$html.='
					<td style="width: 20px;">
		            <a href="#" class="btn-floating btn-small red right" onclick="abrirModal_descartar('.$cod.')">
		              	<i class="fa fa-close"></i>
		            </a>
		         </td>
					</tr>';
	         }
			}
	}
	$html.= '</tbody>
		</table>';

   echo '<div class="contenedorLineas">';
	echo $html;
   echo '</div>';
}


?>