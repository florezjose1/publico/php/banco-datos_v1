<?php
  if( isset($_SESSION['id']) && isset($_SESSION['user']) && isset($_SESSION['rol']) ) {

    // lineas PROGRAMATICAS 

      // investigacion
      $inscritosInvestigacion  = $clase->proyectosinscriptosInvestigacion();
      $aprobadosInvestigacion  = $clase->proyectosAprobadoInvestigacion();
      $rechazadosInvestigacion = $clase->proyectosRechazadosInvestigacion();
      $investigacion = 0;
      if ($inscritosInvestigacion  !=false) $investigacion = $investigacion + mysqli_num_rows($inscritosInvestigacion);
      if ($aprobadosInvestigacion  !=false) $investigacion = $investigacion + mysqli_num_rows($aprobadosInvestigacion);
      if ($rechazadosInvestigacion !=false) $investigacion = $investigacion + mysqli_num_rows($rechazadosInvestigacion);
      // innovacion
      $inscritosInnovacion     = $clase->proyectosinscriptosInnovacion();
      $aprobadosInnovacion     = $clase->proyectosAprobadoInnovacion();
      $rechazadosInnovacion    = $clase->proyectosRechazadosInnovacion();
      $innovacion = 0;
      if ($inscritosInnovacion  !=false) $innovacion = $innovacion + mysqli_num_rows($inscritosInnovacion);
      if ($aprobadosInnovacion  !=false) $innovacion = $innovacion + mysqli_num_rows($aprobadosInnovacion);
      if ($rechazadosInnovacion !=false) $innovacion = $innovacion + mysqli_num_rows($rechazadosInnovacion);
      // modernizacion
      $inscritosModernizacion  = $clase->proyectosinscriptosModernizacion();
      $aprobadosModernizacion  = $clase->proyectosAprobadoModernizacion();
      $rechazadosModernizacion = $clase->proyectosRechazadosModernizacion();
      $modernizacion = 0;
      if ($inscritosModernizacion  !=false) $modernizacion = $modernizacion + mysqli_num_rows($inscritosModernizacion);
      if ($aprobadosModernizacion  !=false) $modernizacion = $modernizacion + mysqli_num_rows($aprobadosModernizacion);
      if ($rechazadosModernizacion !=false) $modernizacion = $modernizacion + mysqli_num_rows($rechazadosModernizacion);
      // divulgacion
      $inscritosDivulgacion    = $clase->proyectosinscriptosDivulgacion();
      $aprobadosDivulgacion    = $clase->proyectosAprobadoDivulgacion();
      $rechazadosDivulgacion   = $clase->proyectosRechazadosDivulgacion();
      $divulgacion = 0;
      if ($inscritosDivulgacion  !=false) $divulgacion = $divulgacion + mysqli_num_rows($inscritosDivulgacion);
      if ($aprobadosDivulgacion  !=false) $divulgacion = $divulgacion + mysqli_num_rows($aprobadosDivulgacion);
      if ($rechazadosDivulgacion !=false) $divulgacion = $divulgacion + mysqli_num_rows($rechazadosDivulgacion);
      // servicios tecnológicos
      $inscritosServicios      = $clase->proyectosinscriptosServicios();
      $aprobadosServicios      = $clase->proyectosAprobadoServicios();
      $rechazadosServicios     = $clase->proyectosRechazadosServicios();
      $servicios = 0;
      if ($inscritosServicios  !=false) $servicios = $servicios + mysqli_num_rows($inscritosServicios);
      if ($aprobadosServicios  !=false) $servicios = $servicios + mysqli_num_rows($aprobadosServicios);
      if ($rechazadosServicios !=false) $servicios = $servicios + mysqli_num_rows($rechazadosServicios);

      // OBSERVATORIO

      $proyectosInscriptos = $clase->infoProyectosObservatio(0);
      $proyectosAprobados = $clase->infoProyectosObservatio(1);
      $proyectosRechazados = $clase->infoProyectosObservatio(2);



?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Banco Proyectos</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="../Public/bootstrap/css/mdb.min.css">
  <link rel="stylesheet" href="../Public/css/main.css">
  
  <link rel="stylesheet" href="../Public/css/style.css">
  <link rel="stylesheet" href="../Public/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../Public/fonts/font-awesome/css/font-awesome.min.css">
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">-->
  <!-- Ionicons -->
  <link rel="stylesheet" href="../Public/bootstrap/css/ionicons.min.css">
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
  <!-- Theme style -->
  <link rel="stylesheet" href="../Public/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../Public/dist/css/skins/_all-skins.min.css">

  <link rel="stylesheet" href="../Public/plugins/alerts/css/sweetalert.css">

  <link rel="stylesheet" href="../Public/css/style-form-step-by-step.css">
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php 
    if (!empty($_GET['ac'])) {

  ?>

  <div class="wrapper">
    <header class="main-header" style="position: fixed;width: 100%;">
      <!-- Logo -->
      <a href="index2.html" class="logo" style="background-color: #359e8d;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>B</b>P</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Banco</b>Proyectos</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top"  style="background-color: #107b71;">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
           
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="../Public/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
                <span class="hidden-xs"><?php echo $user ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header" style="background-color: #0b685a;">
                  <img src="../Public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                  <p>
                    <?php echo $user ?> - Web Developer
                    <small>Member since Nov. 2016</small>
                  </p>
                </li>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left" >
                    <a href="login.php?ac=<?php echo $user;?>" class="btn btn-default btn-flat" style="font-size: 10px;">Ver perfil</a>
                  </div>
                  <div class="pull-right">
                    <a href="#" onclick="cerrarSesion()" class="btn btn-default btn-flat" style="font-size: 10px;">Cerrar Sesion</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar" style="position: fixed;background-color: #043e35;">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            <img src="../Public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
          </div>
          <div class="pull-left info">
            <p><?php echo $user ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- search form 
        <form action="#" method="get" class="sidebar-form">
          <div class="input-group">
            <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                  <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                  </button>
                </span>
          </div>
        </form>
        <!- /.search form -->
        
        <?php 
          echo '
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
              <!--<li class="header">Inscripciones</li>-->
              <li class="treeview">
                <a href="login.php?ac=inicio">
                  <i class="fa fa-home"></i>
                  <span>Inicio</span>
                  <span class="pull-right-container">
                  <!--<i class="fa fa-angle-left pull-right"></i>
                    <span class="label label-primary pull-right">4</span>-->
                  </span>
                </a>
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-search"></i> <span>Observatorio</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </span>
                </a>
                <ul class="treeview-menu">
                '; include "header/observatorio/admin.php";
                  
                echo '</ul> 
              </li>
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-eyedropper"></i> <span>Inscripciones</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </span>
                </a>
                <ul class="treeview-menu">
                  '; include "header/inscripcion/admin.php";
                  echo '
                </ul> 
              </li>
              <li class="treeview">
                <a href="login.php?ac=reportes">
                  <i class="fa fa-file-pdf-o"></i>
                  <span>Reportes</span>
                  <span class="pull-right-container">
                    <!--<i class="fa fa-angle-left pull-right"></i>
                    <span class="label label-primary pull-right">4</span>-->
                  </span>
                </a>
              </li>
              <li class="treeview">
                <a href="login.php?ac=usuarios">
                  <i class="fa fa-user"></i>
                  <span>Usuarios</span>
                  <span class="pull-right-container">
                    <!--<i class="fa fa-angle-left pull-right"></i>
                    <span class="label label-primary pull-right">4</span>-->
                  </span>
                </a>
              </li>
            </ul>
          ';
        ?>




      </section>
      <!-- /.sidebar -->
    </aside>
    
    <?php 
      include 'body/admin.php';
      include 'body/inc/modales.php';
    ?>

   

    <!-- /.content-wrapper -->
    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Banco Proyectos</b> v.1.0
      </div>
      <strong>Copyright &copy; <?php echo date('Y'); ?> 
        <a href="">Sennova Cedrum</a>.</strong> Todos los derechos reservados.
    </footer>
  </div>
  <?php
    }else{
      echo '<script> window.location="login.php?ac=inicio"; </script>';
    }
  ?>

  <!-- jQuery 2.2.3 -->
  <script src="../Public/plugins/jquery/jquery-2.2.3.js"></script>
  <script src="../Public/bootstrap/js/bootstrap.min.js"></script>
  <script src="../Public/dist/js/app.min.js"></script>
  <script src="../Public/js/main.js"></script>
  <script src="../Public/js/usuarios.js"></script>

  <script src="../Public/plugins/alerts/js/functions.js"></script>
  <script src="../Public/plugins/alerts/js/sweetalert.min.js"></script>

  <script src="../Public/plugins/Highcharts-5.0.12/code/highcharts.js"></script>
  <script src="../Public/plugins/Highcharts-5.0.12/code/highcharts-3d.js"></script>
  <script src="../Public/plugins/Highcharts-5.0.12/code/modules/exporting.js"></script>
  <script src="../Public/plugins/Highcharts-5.0.12/code/modules/drilldown.js"></script>

  <script>
  // Write on keyup event of keyword input element
  $(document).ready(function(){
    $("#search").keyup(function(){
      _this = this;
      // Show only matching TR, hide rest of them
      $.each($("#mytable tbody tr"), function() {
        if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
          $(this).hide();
        else
          $(this).show();                
        });
      });
  });
  </script>

  
    <script type="text/javascript">
      $(document).ready(function() {
        //selectEstadisticas(1);
      });
    </script>
  

</body>
</html>

<?php
  }else{
    echo '<script> window.location="../index.php"; </script>';
  }
  
?>