<?php 

	require_once 'principalControllers.php';

	$clase = new PrincipalController();


	$id_r           = time();
	$id_registro = $_POST['id_registro'];

	if ( isset($_POST['investigacion'])	)	{ $lineaP = 1;}
	if ( isset($_POST['innovacion'])	)	{ $lineaP = 2;}
	if ( isset($_POST['servicios'])		)	{ $lineaP = 3;}
	if ( isset($_POST['modernizacion'])	)	{ $lineaP = 4;}



	$area1 = $_POST['area_conocimiento_1'];
	$area2 = $_POST['area_conocimiento_2'];
	$estrategiaPosConfl   = htmlentities(addslashes($_POST['estrategiaPosConflicto']));
	$recursos             = htmlentities(addslashes($_POST['recursosPosconflicto']));

	$municipios           = htmlentities(addslashes($_POST['municipios']));
	$acomuladorMunicipios = "";
		for ($mun=1; $mun <= $municipios ; $mun++) { 
			$mu_id  = time().$mun;
			$nombre = htmlentities(addslashes($_POST['nombreMunicipio_'.$mun]));

			$acomuladorMunicipios.= "('$mu_id','$nombre','$id_registro'),";
		}



	$sectorProImpacto = $_POST['sectorProImpacto'];

	$semillerosBeneficiados = htmlentities(addslashes($_POST['semillerosBeneficiados']));
		$acomuladorSemilleros = "";
		for ($semillero=1; $semillero <= $semillerosBeneficiados ; $semillero++) { 
			$sb_id  = time().$semillero;
			$nombre = htmlentities(addslashes($_POST['nombreSemillero_'.$semillero]));

			$acomuladorSemilleros.= "('$sb_id','$nombre','$id_registro'),";

		}
	$formacionesBeneficiadas = htmlentities(addslashes($_POST['formacionesBenefiadas']));
		$acomuladorFormaciones = "";
		for ($formacion=1; $formacion <= $formacionesBeneficiadas ; $formacion++) { 
			$fb_id  = time().$formacion;
			$nombre = htmlentities(addslashes($_POST['nombreFormacion_'.$formacion]));
			
			$acomuladorFormaciones.= "('$fb_id','$nombre','$id_registro'),";
		}




	$impactoEsperado = $_POST['impactoEsperado'];
	$titulo = $_POST['titulo'];
	$planteamiento = $_POST['planteamiento'];

	// objetivos
	$objetivoGeneral = htmlentities(addslashes($_POST['objetivoGeneral']));
	$obj_especifico = htmlentities(addslashes($_POST['objEspecifico']));
		$acomuladorObjetivos = "";
		for ($obejtivo=1; $obejtivo <=$obj_especifico ; $obejtivo++) { 
			$oe_id         = time().$obejtivo;
			$objEspecifico = htmlentities(addslashes($_POST['objetivoEsp_'.$obejtivo]));

			$acomuladorObjetivos .= "('$oe_id','$objEspecifico','$id_registro'),";
		}





	$justificacion = $_POST['justificacion'];
	$antecedentesProyecto = $_POST['antecedentesProyecto'];
	$basesTeoricas = $_POST['basesTeoricas'];
	$marcoNormativo = $_POST['marcoNormativo'];
	$bibliografia = $_POST['bibliografia'];

	$fecha = date('Y-m-d H:i:s');

	$sql = "INSERT INTO `fase_3`(`f3_id`, `f3_fecha_registro`, `estado`, `f3_linea_programatica`, `f3_area_conocimiento_1`, `f3_area_conocimiento_2`, `f3_descrip_estra`, `f3_recursos_pos`, `f3_municipios_idea`, `f3_sector_impacto`, `f3_semilleros`, `f3_formaciones`, `f3_impacto`, `f3_titulo`, `f3_planteamiento`, `f3_obj_general`, `f3_obj_especifico`, `f3_justificacion`, `f3_antecedentes`, `f3_bases_teoricas`, `f3_marco_normativo`, `f3_recursos`, `f3_cronograma`, `f3_bibliografia`, `f2_id`) 
	VALUES (
	'$id_registro','$fecha','0','$lineaP','$area1','$area2','$estrategiaPosConfl','$recursos','$municipios','$sectorProImpacto','$semillerosBeneficiados','$formacionesBeneficiadas','$impactoEsperado','$titulo','$planteamiento','$objetivoGeneral','$obj_especifico','$justificacion','$antecedentesProyecto','$basesTeoricas','$marcoNormativo','verificar','verificar','$bibliografia','$id_r')";


	$clase->insertregistros($sql);
	echo '<pre>'; print_r($sql); echo '</pre>';

	$objetivos = "INSERT INTO `objetivos_especificos`(`oe_id`, `oe_objetivo`, `f3_id`) VALUES $acomuladorObjetivos w";
	$queryObjetivos = str_replace(', w', '', $objetivos);
	$clase->insertregistros($queryObjetivos);
	echo '<pre-objetivos>'; print_r($objetivos); echo '</pre-objetivos>';

	$semillero = "INSERT INTO `semilleros_ben`(`sb_id`, `sb_nombre`, `f3_id`) VALUES $acomuladorSemilleros w ";
	$querySemilleros = str_replace(', w', '', $semillero);
	$clase->insertregistros($querySemilleros);


	$formaciones = "INSERT INTO `formaciones_ben`(`fb_id`, `fb_nombre`, `f3_id`) VALUES  $acomuladorFormaciones w ";
	$queryFormaciones = str_replace(', w', '', $formaciones);
	$clase->insertregistros($queryFormaciones);

	$municipios = "INSERT INTO `municipios`(`mu_id`, `mu_nombre`, `f3_idea`) VALUES $acomuladorMunicipios w";
	$queryMunicipios = str_replace(', w', '', $municipios);
	$clase->insertregistros($queryMunicipios);
