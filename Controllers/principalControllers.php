<?php 

	//include 'C:\xampp\htdocs\proyectos\banco_v1\Database\conexion\conexion.php ';
	include '/opt/lampp/htdocs/proyectos/banco_v2/Database/conexion/conexion.php';

	date_default_timezone_set('America/Bogota');

	class PrincipalController extends Conexion
	{
		public function datosPersonalesRadicador($cod) {
			$query = "SELECT * FROM `datos_personales_radicador` WHERE `dpr_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function datosAutores($cod) {
			$query = "SELECT * FROM `datos_personales_autores` WHERE `dpr_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function datosProyecto($sql) {
			$query = "SELECT * FROM `informacion_proyecto` WHERE $sql ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function vistoProyecto($cod){
			$query = "UPDATE `informacion_proyecto` SET `ip_visto`= '1' WHERE `ip_id` =  '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
		}
		public function rechazarProyecto($request) {
			$cod = htmlentities(addslashes($request->cod));

			$query ="UPDATE `informacion_proyecto` SET `ip_estado` = '2' WHERE `informacion_proyecto`.`ip_id` = '$cod'   ";
			$result = $this->ejectuarBancoProyectos($query);
			if ($result) {
				$query = "UPDATE `informacion_proyecto` SET `ip_visto`= '1' WHERE `ip_id` =  '$cod' ";
				$result = $this->ejectuarBancoProyectos($query);
				if ($result) {
					echo true;
				}
			}else{
				echo false;
			}
		}
		public function aprobarProyecto($request) {
			$cod = htmlentities(addslashes($request->cod));

			$query ="UPDATE `informacion_proyecto` SET `ip_estado` = '1' WHERE `informacion_proyecto`.`ip_id` = '$cod'   ";
			$result = $this->ejectuarBancoProyectos($query);
			if ($result) {
				$query = "UPDATE `informacion_proyecto` SET `ip_visto`= '1' WHERE `ip_id` =  '$cod' ";
				$result = $this->ejectuarBancoProyectos($query);
				if ($result) {
					echo true;
				}
			}else{
				echo false;
			}
		}
		public function aprobarFase($request) {
			$cod = htmlentities(addslashes($request->cod));
			$f = htmlentities(addslashes($request->fase));
			$indicaciones = htmlentities(addslashes($request->indicaciones));

			$fase = 'fase_'.$f;

			$if = 'f'.$f.'_id';

			$query ="UPDATE `$fase` SET `estado_fase` = '1', `indicaciones`='$indicaciones' WHERE `$fase`.`$if` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			if ($result) {
				echo true;
			}else{
				echo false;
			}
		}

		public function recursosHumanos($cod) {
			$query = "SELECT * FROM `recursos_humanos` WHERE `dpr_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function municipios($cod) {
			$query = "SELECT * FROM `municipios` WHERE `ip_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function objetivosEspecificos($cod) {
			$query = "SELECT * FROM `objetivos_especificos` WHERE `ip_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function semilleros($cod) {
			$query = "SELECT * FROM `semilleros_ben` WHERE `ip_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function formaciones($cod) {
			$query = "SELECT * FROM `formaciones_ben` WHERE `ip_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		// detalles de recursos humanos
		public function detallesRecursosHumanos($sql) {
			$query = "$sql";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}


		// registros -- 
		public function insertregistros($query){
			$result = $this->ejectuarBancoProyectos($query);
			if($result){
			 	return true;
			}else{
				return false;
			}
		}
		// registros usuario "si estos no existen"
		public function existeUsuario($email) {
			$query = "SELECT * FROM `datos_personales_radicador` WHERE `dpr_email` = '$email' ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}

		// inicio sesion
		public function sessionBanco($request){
			$usuario = htmlentities(addslashes($request->usuario));
			$pass = htmlentities(addslashes($request->pass));
			$p = MD5($pass);
			$query ="SELECT * FROM login WHERE `lg_user` = '$usuario' AND `lg_pass` = '$p'";
			$result = $this->ejectuarBancoProyectos($query);
	        $post = array();
            if(mysqli_num_rows($result) > 0){
	            while ($row = mysqli_fetch_object($result)) {
		            session_start();
		            $page = 'Views/login.php?ac=inicio';
	                $_SESSION['user'] = $row->lg_user;
	                $_SESSION['id'] = $row->lg_id;
	                $_SESSION['rol'] = $row->lg_rol;
		            
		            $post[] = array( 'detalle'=>1, 'id'=>$row->lg_id, 'page'=>$page);
	            }
            }else{
            	$post[] = array( 'detalle'=>0);
            }
	        echo json_encode($post);
		}
		// informacion de usuario
		public function datosUsuario($usuario){
			$query="SELECT * FROM login WHERE lg_id = '$usuario' ";
			$result = $this->ejectuarBancoProyectos($query);
			if(mysqli_num_rows($result) > 0){
			 	return $result;
			}else{
				return false;
			}
		}
		public function listUsuarios(){
			$query = "SELECT * FROM `login` WHERE `lg_rol` = 1 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function verificarEmail($request){
			$usuario = htmlentities(addslashes($request->email));
			$query ="SELECT * FROM login WHERE `lg_user` = '$usuario'   ";
			$result = $this->ejectuarBancoProyectos($query);
            if(mysqli_num_rows($result) > 0){
	            echo true;
            }else{
            	echo false;
            }
		}
		public function registrarUser($request){
			$nombre = htmlentities(addslashes($request->nombre));
			$usuario = htmlentities(addslashes($request->email));
			$clave = htmlentities(addslashes($request->clave));
			$p = MD5($clave);
			$id = time();
			$fecha = date('Y-m-d H:i:s');
			$query ="INSERT INTO `login`(`lg_id`, `lg_nombre`, `lg_user`, `lg_pass`, `fecha_registro`, `lg_rol`) VALUES ('$id','$nombre','$usuario','$p', '$fecha', '1')   ";
			$result = $this->ejectuarBancoProyectos($query);
            if($result){
	            echo true;
            }else{
            	echo false;
            }
		}
		public function actualizarUsuario($request){
			$cod = htmlentities(addslashes($request->cod));
			$nombre = htmlentities(addslashes($request->nombre));
			$usuario = htmlentities(addslashes($request->email));
			$clave = htmlentities(addslashes($request->clave));
			$p = MD5($clave);
			$id = time();
			$fecha = date('Y-m-d H:i:s');
			$query ="UPDATE `login` SET `lg_nombre`='$nombre', `lg_user`='$usuario' , `lg_pass` = '$clave' , `fecha_update` WHERE `lg_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
            if($result){
	            echo true;
            }else{
            	echo false;
            }
		}
		public function eliminarUsuario($request){
			$cod = htmlentities(addslashes($request->cod));
			$query = "DELETE FROM `login` WHERE `lg_id` = '$cod' ";
			$result = $this->ejectuarBancoProyectos($query);
            if($result){
	            echo true;
            }else{
            	echo false;
            }
		}


		#///////////////////////////////////////////////////////////
		#////////////////////-- INSCRIPCION --//////////////////////
		#///////////  INFORMACION PROYECTOS POR TIPO   /////////////
		#///////////////////////////////////////////////////////////
		#///////////////////////////////////////////////////////////
		public function proyectosinscriptosInvestigacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 0 AND `ip_linea`=1 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosAprobadoInvestigacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 1 AND `ip_linea`=1 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosRechazadosInvestigacion(){
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 2 AND `ip_linea`=1 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosinscriptosInnovacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 0 AND `ip_linea`=2 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosAprobadoInnovacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 1 AND `ip_linea`=2 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosRechazadosInnovacion(){
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 2 AND `ip_linea`=2 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosinscriptosModernizacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 0 AND `ip_linea`=3 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosAprobadoModernizacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 1 AND `ip_linea`=3 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosRechazadosModernizacion(){
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 2 AND `ip_linea`=3 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosinscriptosDivulgacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 0 AND `ip_linea`=4 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosAprobadoDivulgacion() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 1 AND `ip_linea`=4 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosRechazadosDivulgacion(){
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 2 AND `ip_linea`=4 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosinscriptosServicios() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 0 AND `ip_linea`=5 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosAprobadoServicios() {
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 1 AND `ip_linea`=5 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function proyectosRechazadosServicios(){
			$query = "SELECT * FROM `informacion_proyecto` WHERE `ip_estado` = 2 AND `ip_linea`=5 ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		
		// observatorio
		public function infoProyectosObservatio($estado){
			$query = "SELECT * FROM `fase_1` WHERE `estado`= '$estado'  ";
			$result = $this->ejectuarBancoProyectos($query);
			$post = array();
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}

		public function detallesProyectoObservatorio($sql) {
			$query = "SELECT * FROM $sql  ";
			$result = $this->ejectuarBancoProyectos($query);
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function detallesMunicipios($sql){
			$query = "SELECT * FROM `municipios` WHERE $sql  ";
			$result = $this->ejectuarBancoProyectos($query);
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function detallesObjetivos($sql){
			$query = "SELECT * FROM `objetivos_especificos` WHERE $sql  ";
			$result = $this->ejectuarBancoProyectos($query);
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function detallesSemiFormaciones($sql){
			$query = " SELECT * FROM  $sql ";
			$result = $this->ejectuarBancoProyectos($query);
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}


		// usuario
		public function radicador($sql){
			$query = " SELECT * FROM  $sql ";
			$result = $this->ejectuarBancoProyectos($query);
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}
		public function misProyectos($sql){
			$query = " SELECT * FROM  $sql ";
			$result = $this->ejectuarBancoProyectos($query);
			if (mysqli_num_rows($result)>0) {
				return $result;
			}else{
				return false;
			}
		}

		
	}
?>