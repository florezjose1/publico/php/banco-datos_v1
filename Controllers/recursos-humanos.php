<?php 

	require_once 'principalControllers.php';

	$clase = new PrincipalController();

	$rh_id = time();

	$codRadicador = htmlentities(addslashes($_POST['codRadicador']));
	$linea        = htmlentities(addslashes($_POST['linea']));
	print_r('2');

	$ip = rand(1,5000);
	$newCod = time().$ip;

	$queryRadicador = "UPDATE `datos_personales_radicador` SET `dpr_id`= '$newCod' WHERE `dpr_id` = '$codRadicador' ";
	$clase->insertregistros($queryRadicador);

	$personalExt   = htmlentities(addslashes($_POST['numPersonalExt']));
	$personalASC   = htmlentities(addslashes($_POST['numPersonalIntASC']));
	$personalACC   = htmlentities(addslashes($_POST['numPersonalIntACC']));
	$personalinst  = htmlentities(addslashes($_POST['numPersonalIntInstructores']));
	$personalOtros = htmlentities(addslashes($_POST['numPersonalIntOtros']));

	$aliadosExt           = htmlentities(addslashes($_POST['numAliadosExternos']));
	$aliadosInt           = htmlentities(addslashes($_POST['numAliadosInternos']));
	$ciudades             = htmlentities(addslashes($_POST['numCiudadesInfluencia']));
	$municipios           = htmlentities(addslashes($_POST['numMunicipiosInfluencia']));
	$descripcionObjetivos = htmlentities(addslashes($_POST['descripcionAlianza']));

	$otrosservPersonalesIndirectos = htmlentities(addslashes($_POST['otrosservPersonalesIndirectos']));
	$descripcionotrosservPersonalesIndirectos = htmlentities(addslashes($_POST['descripcionotrosservPersonalesIndirectos']));

	$servPersonalesIndirectos = htmlentities(addslashes($_POST['servPersonalesIndirectos']));
	$descripcionservPersonalesIndirectos = htmlentities(addslashes($_POST['descripcionservPersonalesIndirectos']));

	$valorOtrosGastosImpPub = htmlentities(addslashes($_POST['valorOtrosGastosImpPub']));
	$descripcionvalorOtrosGastosImpPub = htmlentities(addslashes($_POST['descripcionvalorOtrosGastosImpPub']));

	$valorDivulgacionGestionInst = htmlentities(addslashes($_POST['valorDivulgacionGestionInst']));
	$descripcionvalorDivulgacionGestionInst = htmlentities(addslashes($_POST['descripcionvalorDivulgacionGestionInst']));

	$valorViaticosFormPro = htmlentities(addslashes($_POST['valorViaticosFormPro']));
	$descripcionvalorViaticosFormPro = htmlentities(addslashes($_POST['descripcionvalorViaticosFormPro']));

	$valorBienestarAlumnos = htmlentities(addslashes($_POST['valorBienestarAlumnos']));
	$descripcionvalorBienestarAlumnos = htmlentities(addslashes($_POST['descripcionvalorBienestarAlumnos']));

	$valorMonitorias = htmlentities(addslashes($_POST['valorMonitorias']));
	$descripcionvalorMonitorias = htmlentities(addslashes($_POST['descripcionvalorMonitorias']));


	$equipoSis        = htmlentities(addslashes($_POST['equipoSistemas']));
	$descripSis       = htmlentities(addslashes($_POST['descripcionSistemas']));
	$equipoSoft       = htmlentities(addslashes($_POST['valorSoftware']));
	$descripSoft      = htmlentities(addslashes($_POST['descripcionSoftware']));
	$equiMaqInd       = htmlentities(addslashes($_POST['valormaquinariaInd']));
	$descMaqInd       = htmlentities(addslashes($_POST['descripcionmaquinariaInd']));
	$otrosEquip       = htmlentities(addslashes($_POST['valorOtrosEquipos']));
	$descOtrosEq      = htmlentities(addslashes($_POST['descripcionOtrosEquipos']));
	$formProf         = htmlentities(addslashes($_POST['valormatFormProfesional']));
	$descProf         = htmlentities(addslashes($_POST['descripcionmatFormProfesional']));
	$MMETS            = htmlentities(addslashes($_POST['valormantenimientoMMETS']));
	$descMMETS        = htmlentities(addslashes($_POST['descripcionmantenimientoMMETS']));
	$transp           = htmlentities(addslashes($_POST['valorcomunicTransporte']));
	$desctransp       = htmlentities(addslashes($_POST['descripcioncomunicTransporte']));
	$adecuaciones     = htmlentities(addslashes($_POST['valoradecuacionesContrucciones']));
	$descAdecuaciones = htmlentities(addslashes($_POST['descripcionadecuacionesContrucciones']));


	$query = "INSERT INTO `recursos_humanos`(

		`rh_id`, 
		`rh_personal_ext`, 
		`rh_personal_int_asc`, 
		`rh_personal_int_acc`,

		`rh_personal_int_instructores`, 
		`rh_personal_int_otros`, 

		`rh_aliados_ext`, 
		`rh_aliados_int`, 

		`rh_ciudades`, 
		`rh_municipios`, 

		`rh_descipcion_alianza_obj`, 

		`rh_otros_serv_pers_ind_aprend_valor`, 
		`rh_otros_serv_pers_ind_aprend_que_para`,

		`rh_serv_pers_indirectos_valor`, 
		`rh_serv_pers_indirectos_que_para`,

		`rh_otros_gastos_impre_public_valor`, 
		`rh_otros_gastos_impre_public_que_para`, 

		`rh_divulgacion_act_gestion_inst_valor`, 
		`rh_divulgacion_act_gestion_inst_que_para`, 

		`rh_viaticos_form_prof_valor`, 
		`rh_viaticos_form_prof_que_para`, 

		`rh_gastos_bienestar_alum_valor`, 
		`rh_gastos_bienestar_alum_que_para`, 

		`rh_monitorias_valor`, 
		`rh_monitorias_que_para`,

		`rh_eq_sis_valor`, 
		`rh_eq_sis_descripcion_que_para`, 

		`rh_eq_soft_valor`, 
		`rh_eq_soft_descripcion_que_para`, 

		`rh_eq_mind_valor`, 
		`rh_eq_mind_descripcion_que_para`, 

		`rh_eq_otras_compras_valor`, 
		`rh_eq_otras_compras_que_para`, 

		`rh_formacion_prof_valor`, 
		`rh_formacion_pro_que_para`, 

		`rh_mantenimiento_valor`, 
		`rh_mantenimiento_que_para`, 

		`rh_otras_comun_valor`, 
		`rh_otras_comun_que_para`, 

		`rh_adecuaciones_valor`, 
		`rh_adecuaciones_que_para`, 

		`dpr_id`) 


		VALUES (
		'$rh_id',
		'$personalExt',
		'$personalASC',
		'$personalACC',

		'$personalinst',
		'$personalOtros',

		'$aliadosExt',
		'$aliadosInt',

		'$ciudades',
		'$municipios',


		'$descripcionObjetivos',

		'$otrosservPersonalesIndirectos',
		'$descripcionotrosservPersonalesIndirectos',

		'$servPersonalesIndirectos',
		'$descripcionservPersonalesIndirectos',

		'$valorOtrosGastosImpPub',
		'$descripcionvalorOtrosGastosImpPub',

		'$valorDivulgacionGestionInst',
		'$descripcionvalorDivulgacionGestionInst',

		'$valorViaticosFormPro',
		'$descripcionvalorViaticosFormPro',

		'$valorBienestarAlumnos',
		'$descripcionvalorBienestarAlumnos',

		'$valorMonitorias',
		'$descripcionvalorMonitorias',

		'$equipoSis',
		'$descripSis',

		'$equipoSoft',
		'$descripSoft',

		'$equiMaqInd',
		'$descMaqInd',

		'$otrosEquip',
		'$descOtrosEq',

		'$formProf',
		'$descProf',

		'$MMETS',
		'$descMMETS',

		'$transp',
		'$desctransp',

		'$adecuaciones',
		'$descAdecuaciones',
		
		'$newCod')";
	echo '<pre>'; print_r($query); echo '</pre>';
	$clase->insertregistros($query);

	if ($personalExt>0) {
		$acomuladorExt = "";
		for ($p=1; $p <=$personalExt ; $p++) {
			$pe_id  = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombrePersExt_'.$p]));
			$apell  = htmlentities(addslashes($_POST['apellidosPersExt_'.$p]));
			$identi = htmlentities(addslashes($_POST['identificacionPersExt_'.$p]));

			$acomuladorExt.= "('$pe_id','$nombre','$apell','$identi','$rh_id'),";
		}
		$personalExt = "INSERT INTO `personal_externo`(`pe_id`, `pe_nombre`, `pe_apellido`, `pe_identificacion`, `rh_id`) VALUES  $acomuladorExt w";
		$queryPE = str_replace(', w', '', $personalExt);
		$clase->insertregistros($queryPE);
	}
	if ($personalASC>0) {
		$acomuladorInt = "";
		for ($p=1; $p <=$personalASC ; $p++) {
			$pi_id = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombrePersIntASC_'.$p]));
			$apell  = htmlentities(addslashes($_POST['apellidosPersIntASC_'.$p]));
			$identi = htmlentities(addslashes($_POST['identificacionPersIntASC_'.$p]));
			$acomuladorInt.= "('$pi_id','$nombre','$apell','$identi','$rh_id'),";
		}
		$personalintASC = "INSERT INTO `personal_internoacc`(`pi_id`, `pi_nombre`, `pi_apellido`, `pi_identificacion`, `rh_id`) VALUES $acomuladorInt w";
		$queryPI_ASC = str_replace(', w', '', $personalintASC);
		$clase->insertregistros($queryPI_ASC);
	}
	if ($personalACC>0) {
		$acomuladorIntACC = "";
		for ($p=1; $p <=$personalACC ; $p++) {
			$pi_id  = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombrePersIntACC_'.$p]));
			$apell  = htmlentities(addslashes($_POST['apellidosPersIntACC_'.$p]));
			$identi = htmlentities(addslashes($_POST['identificacionPersIntACC_'.$p]));
			$acomuladorIntACC.= "('$pi_id','$nombre','$apell','$identi','$rh_id'),";
		}
		$personalintASC = "INSERT INTO `personal_internoasc`(`pi_id`, `pi_nombre`, `pi_apellido`, `pi_identificacion`, `rh_id`) VALUES $acomuladorIntACC w";
		$queryPI_ACC = str_replace(', w', '', $personalintASC);
		$clase->insertregistros($queryPI_ACC);
	}
	if ($personalinst>0) {
		$acomuladorIntInst = "";
		for ($p=1; $p <=$personalinst ; $p++) {
			$pi_id  = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombrePersIntInstructores_'.$p]));
			$apell  = htmlentities(addslashes($_POST['apellidosPersIntInstructores_'.$p]));
			$identi = htmlentities(addslashes($_POST['identificacionPersIntInstructores_'.$p]));

			$acomuladorIntInst.= "('$pi_id','$nombre','$apell','$identi','$rh_id'),";
		}
		$personalintInst = "INSERT INTO `personal_internoinstructores`(`pi_id`, `pi_nombre`, `pi_apellido`, `pi_identificacion`, `rh_id`) VALUES $acomuladorIntInst w";
		$queryPI_inst = str_replace(', w', '', $personalintInst);
		$clase->insertregistros($queryPI_inst);
	}
	if ($personalOtros>0) {
		$acomuladorIntOtros = "";
		for ($p=1; $p <=$personalOtros ; $p++) {
			$pi_id  = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombrePersIntOtros_'.$p]));
			$apell  = htmlentities(addslashes($_POST['apellidosPersIntOtros_'.$p]));
			$identi = htmlentities(addslashes($_POST['identificacionPersIntOtros_'.$p]));

			$acomuladorIntOtros.= "('$pi_id','$nombre','$apell','$identi','$rh_id'),";
		}
		$personalintOtros = "INSERT INTO `personal_internootros`(`pe_id`, `pe_nombre`, `pe_apellido`, `pe_identificacion`, `rh_id`) VALUES  $acomuladorIntOtros w";
		$queryPI_otros = str_replace(', w', '', $personalintOtros);
		$clase->insertregistros($queryPI_otros);
	}
	if ($aliadosExt>0) {
		$acomuladorAliadosExt = "";
		for ($p=1; $p <=$aliadosExt ; $p++) {
			$pi_id   = time().$p;
			$nombre  = htmlentities(addslashes($_POST['nombreAliadoExt_'.$p]));
			$nit     = htmlentities(addslashes($_POST['nitAliadoExt_'.$p]));
			$recurso = htmlentities(addslashes($_POST['recursoAliadoExt_'.$p]));

			$acomuladorAliadosExt.= "('$pi_id','$nombre','$nit','$recurso','$rh_id'),";
		}
		$personalAliadosExt = "INSERT INTO `aliados_externos`(`ae_id`, `ae_nombre`, `ae_nit`, `ae_recursos`, `rh_id`) VALUES $acomuladorAliadosExt w";
		$queryAliadosInt = str_replace(', w', '', $personalAliadosExt);
		$clase->insertregistros($queryAliadosInt);
	}
	if ($aliadosInt>0) {
		$acomuladorAliadosInt = "";
		for ($p=1; $p <=$aliadosInt ; $p++) {
			$pi_id   = time().$p;
			$nombre  = htmlentities(addslashes($_POST['nombreAliadoInterno_'.$p]));
			$nit     = htmlentities(addslashes($_POST['nitAliadoInterno_'.$p]));
			$recurso = htmlentities(addslashes($_POST['recursoAliadoInterno_'.$p]));

			$acomuladorAliadosInt.= "('$pi_id','$nombre','$nit','$recurso','$rh_id'),";
		}
		$personalAliadosInt = "INSERT INTO `aliados_internos`(`ai_id`, `ai_nombre`, `ai_nit`, `ai_recursos`, `rh_id`) VALUES $acomuladorAliadosInt w";
		$queryAliados = str_replace(', w', '', $personalAliadosInt);
		$clase->insertregistros($queryAliados);
	}
	if ($ciudades>0) {
		$acomuladorCiudad = "";
		for ($p=1; $p <=$ciudades ; $p++) {
			$ci_id  = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombreCiudadInfluencia_'.$p]));
			
			$acomuladorCiudad.= "('$ci_id','$nombre','$rh_id'),";
		}
		$ciudad = "INSERT INTO `ciudades`(`ci_id`, `ci_nombre`, `rh_id`) VALUES $acomuladorCiudad w";
		$queryCiudad = str_replace(', w', '', $ciudad);
		$clase->insertregistros($queryCiudad);
	}
	if ($municipios>0) {
		$acomuladorMun = "";
		for ($p=1; $p <=$municipios ; $p++) {
			$ci_id  = time().$p;
			$nombre = htmlentities(addslashes($_POST['nombreMunicipioInfluencia_'.$p]));
			$acomuladorMun.= "('$ci_id','$nombre','$rh_id'),";
		}
		$municipio = "INSERT INTO `municipios`(`mu_id`, `mu_nombre`, `rh_id`) VALUES  $acomuladorMun w";
		$queryMunicipio = str_replace(', w', '', $municipio);
		$clase->insertregistros($queryMunicipio);
	}





?>