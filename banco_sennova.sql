-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 03-08-2017 a las 03:06:18
-- Versión del servidor: 10.1.22-MariaDB
-- Versión de PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `banco_sennova`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aliados_externos`
--

CREATE TABLE `aliados_externos` (
  `ae_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ae_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ae_nit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ae_recursos` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aliados_internos`
--

CREATE TABLE `aliados_internos` (
  `ai_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ai_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ai_nit` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ai_recursos` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ciudades`
--

CREATE TABLE `ciudades` (
  `ci_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ci_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_personales_autores`
--

CREATE TABLE `datos_personales_autores` (
  `dpa_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dpa_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpa_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpa_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpa_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpa_telefono` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `datos_personales_radicador`
--

CREATE TABLE `datos_personales_radicador` (
  `dpr_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dpr_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_telefono` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_regional` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dpr_formacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dpr_ficha` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `dpr_autores` int(5) DEFAULT NULL,
  `lo_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `datos_personales_radicador`
--

INSERT INTO `datos_personales_radicador` (`dpr_id`, `dpr_nombre`, `dpr_apellido`, `dpr_identificacion`, `dpr_email`, `dpr_telefono`, `dpr_regional`, `dpr_formacion`, `dpr_ficha`, `dpr_autores`, `lo_id`) VALUES
('1501474067', 'JOSE', 'FLOREZ', '1094167940', 'jose@gmail.com', '3229008256', 'CEDRUM N.D.S', 'ADSI', '1022830', NULL, '15014740672'),
('1501512100', 'Juan', 'Peres', '10', 'juan@gmail.com', '39102312', 'CEDRUM N.D.S', 'adsi', '10931029', NULL, '15015121001'),
('1501631476', 'asjoa', 'jaosd', '12312', 'jos@gmail.com', '12312', 'CEDRUM N.D.S', 'adis', '123910', NULL, '15016314767');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estados`
--

CREATE TABLE `estados` (
  `es_id` int(10) NOT NULL,
  `es_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `estados`
--

INSERT INTO `estados` (`es_id`, `es_nombre`) VALUES
(0, 'Inscritos'),
(1, 'Aprobados'),
(2, 'Rechazados');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fase_1`
--

CREATE TABLE `fase_1` (
  `f1_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `f1_fecha_registro` datetime DEFAULT NULL,
  `estado` int(5) DEFAULT NULL,
  `estado_fase` int(5) NOT NULL DEFAULT '0',
  `li_linea_programatica` int(5) DEFAULT NULL,
  `li_linea_investigacion` int(5) DEFAULT NULL,
  `f1_idea` text COLLATE utf8_unicode_ci,
  `f1_planteamiento` text COLLATE utf8_unicode_ci,
  `f1_sector_productivo` text COLLATE utf8_unicode_ci,
  `f1_municipios` int(5) DEFAULT NULL,
  `f1_acompanante` text COLLATE utf8_unicode_ci,
  `f1_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1_link` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dpr_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `indicaciones` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fase_1`
--

INSERT INTO `fase_1` (`f1_id`, `f1_fecha_registro`, `estado`, `estado_fase`, `li_linea_programatica`, `li_linea_investigacion`, `f1_idea`, `f1_planteamiento`, `f1_sector_productivo`, `f1_municipios`, `f1_acompanante`, `f1_valor`, `f1_link`, `dpr_id`, `indicaciones`) VALUES
('15014740671716', '2017-07-30 23:07:47', 0, 1, 2, 7, 'asd', 'asd', 'asd', 1, 'Jose', '132', '13123', '1501474067', 'Hola Juan como estas, mira.! el proyecto tiene una idea excelente pero debes redactar un poco mejor, puesto que dejas muchas deficiencias y/o dudas que deben ser resolvidas.... saludos...'),
('15015121002197', '2017-07-31 09:41:40', 0, 1, 2, 3, 'adas asdaskd aksdnkja', 'idhaisd askdjan akjsdnaskjd', 'asdjasd asdnjaksd jaskdnak', 1, 'adjasd asdjasd', '123', '123123', '1501512100', ''),
('1501631476652', '2017-08-01 18:51:16', 0, 1, 2, 1, 'asda', 'ads', 'asd', 1, 'asda', '123', 'asdas', '1501631476', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fase_2`
--

CREATE TABLE `fase_2` (
  `f2_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `f2_fecha_registro` datetime DEFAULT NULL,
  `estado` int(5) DEFAULT NULL,
  `estado_fase` int(5) NOT NULL DEFAULT '0',
  `rol_exponente` varchar(100) COLLATE latin1_spanish_ci NOT NULL,
  `f2_titulo` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `f2_planteamiento` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `f2_objetivo_general` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `f2_objetivo_especifico` int(5) DEFAULT NULL,
  `f2_jusificacion` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `f2_bibliografia` text COLLATE latin1_spanish_ci,
  `f2_tiempo_ejecucion` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `f2_valor_idea` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `f1_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `indicaciones` text COLLATE latin1_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `fase_2`
--

INSERT INTO `fase_2` (`f2_id`, `f2_fecha_registro`, `estado`, `estado_fase`, `rol_exponente`, `f2_titulo`, `f2_planteamiento`, `f2_objetivo_general`, `f2_objetivo_especifico`, `f2_jusificacion`, `f2_bibliografia`, `f2_tiempo_ejecucion`, `f2_valor_idea`, `f1_id`, `indicaciones`) VALUES
('15014740671716', '2017-07-31 20:45:16', 0, 1, 'Aprendiz CEDRUM', 'asd asd', 'asd', 'hola asnd asdk aksjd askjd a', 2, 'asdasdasda ad asd asd da', 'as a ads adsasd asdasd dasd as as asd', '6 meses', '132', '1501551916', 'Hola, veo que acertaste en las indicaciones, has realizado un excelente trabajo, felicitaciones y esperemos como formulas la tercera fase.... cu&iacute;date...');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fase_3`
--

CREATE TABLE `fase_3` (
  `f3_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `f3_fecha_registro` datetime NOT NULL,
  `estado` int(5) NOT NULL,
  `estado_fase` int(5) NOT NULL DEFAULT '0',
  `f3_linea_programatica` int(10) DEFAULT NULL,
  `f3_area_conocimiento_1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_area_conocimiento_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_descrip_estra` text COLLATE utf8_unicode_ci,
  `f3_recursos_pos` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_municipios_idea` int(10) DEFAULT NULL,
  `f3_sector_impacto` text COLLATE utf8_unicode_ci,
  `f3_semilleros` int(10) DEFAULT NULL,
  `f3_formaciones` int(10) DEFAULT NULL,
  `f3_impacto` text COLLATE utf8_unicode_ci,
  `f3_titulo` text COLLATE utf8_unicode_ci,
  `f3_planteamiento` text COLLATE utf8_unicode_ci,
  `f3_obj_general` text COLLATE utf8_unicode_ci,
  `f3_obj_especifico` int(10) DEFAULT NULL,
  `f3_justificacion` text COLLATE utf8_unicode_ci,
  `f3_antecedentes` text COLLATE utf8_unicode_ci,
  `f3_bases_teoricas` text COLLATE utf8_unicode_ci,
  `f3_marco_normativo` text COLLATE utf8_unicode_ci,
  `f3_recursos` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_cronograma` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_bibliografia` text COLLATE utf8_unicode_ci,
  `f2_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `indicaciones` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `fase_3`
--

INSERT INTO `fase_3` (`f3_id`, `f3_fecha_registro`, `estado`, `estado_fase`, `f3_linea_programatica`, `f3_area_conocimiento_1`, `f3_area_conocimiento_2`, `f3_descrip_estra`, `f3_recursos_pos`, `f3_municipios_idea`, `f3_sector_impacto`, `f3_semilleros`, `f3_formaciones`, `f3_impacto`, `f3_titulo`, `f3_planteamiento`, `f3_obj_general`, `f3_obj_especifico`, `f3_justificacion`, `f3_antecedentes`, `f3_bases_teoricas`, `f3_marco_normativo`, `f3_recursos`, `f3_cronograma`, `f3_bibliografia`, `f2_id`, `indicaciones`) VALUES
('15014740671716', '2017-07-31 22:20:07', 0, 1, 3, '10. SIN CLASIFICAR', '1. AGRONOMIA VETERINARIA Y AFINES', '', '', 1, 'asdas', 1, 1, '12asdasdsad', 'asd asd adas asd ', 'asd as sssssssssss', 'hola asnd asdk aksjd askjd a', 2, 'asdasdasda ad asd asd da wwwwwwwww', 'asd  asd', 'asdasd ', 'asdada', 'verificar', 'verificar', 'as a ads adsasd asdasd dasd as as asd asd a asdasd asdasd', '1501557607', 'sdafghmdsdfgh');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fase_4`
--

CREATE TABLE `fase_4` (
  `f4_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `f4_fecha_registro` datetime NOT NULL,
  `estado` int(5) NOT NULL,
  `estado_fase` int(5) NOT NULL DEFAULT '0',
  `f4_linea_programatica` int(10) DEFAULT NULL,
  `f4_area_conocimiento_1` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_area_conocimiento_2` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_descrip_estra` text COLLATE utf8_unicode_ci,
  `f4_recursos_pos` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_municipios_idea` int(10) DEFAULT NULL,
  `f4_sector_impacto` text COLLATE utf8_unicode_ci,
  `f4_semilleros` int(10) DEFAULT NULL,
  `f4_formaciones` int(10) DEFAULT NULL,
  `f4_impacto` text COLLATE utf8_unicode_ci,
  `f4_titulo` text COLLATE utf8_unicode_ci,
  `f4_introduccion` text COLLATE utf8_unicode_ci,
  `f4_planteamiento` text COLLATE utf8_unicode_ci,
  `f4_obj_general` text COLLATE utf8_unicode_ci,
  `f4_obj_especifico` int(10) DEFAULT NULL,
  `f4_justificacion` text COLLATE utf8_unicode_ci,
  `f4_antecedentes` text COLLATE utf8_unicode_ci,
  `f4_bases_teoricas` text COLLATE utf8_unicode_ci,
  `f4_marco_normativo` text COLLATE utf8_unicode_ci,
  `f4_recursos` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_cronograma` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_tipo_investigacion` text COLLATE utf8_unicode_ci,
  `f4_disenno_inv` text COLLATE utf8_unicode_ci,
  `f4_universo_muestra` text COLLATE utf8_unicode_ci,
  `f4_tecnicas_inst_rec_muestra` text COLLATE utf8_unicode_ci,
  `f4_tecnica_procesamiento_analisis` text COLLATE utf8_unicode_ci,
  `f4_bibliografia` text COLLATE utf8_unicode_ci,
  `f4_anexos` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `indicaciones` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `formaciones_ben`
--

CREATE TABLE `formaciones_ben` (
  `fb_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fb_nombre` text COLLATE utf8_unicode_ci,
  `ip_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `formaciones_ben`
--

INSERT INTO `formaciones_ben` (`fb_id`, `fb_nombre`, `ip_id`, `f3_id`, `f4_id`) VALUES
('15015576071', 'adsas', NULL, '15014740671716', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `informacion_proyecto`
--

CREATE TABLE `informacion_proyecto` (
  `ip_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ip_linea` int(5) NOT NULL,
  `ip_estado` int(5) NOT NULL,
  `ip_visto` int(5) NOT NULL,
  `ip_fecha_registro` datetime NOT NULL,
  `ip_titulo` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `ip_n_munip_posC` int(5) NOT NULL,
  `ip_estrategia_posC` text COLLATE utf8_unicode_ci NOT NULL,
  `ip_recursos_posC` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ip_justificacion` text COLLATE utf8_unicode_ci NOT NULL,
  `ip_link_video` text COLLATE utf8_unicode_ci NOT NULL,
  `ip_planteamiento` text COLLATE utf8_unicode_ci NOT NULL,
  `ip_obj_general` text COLLATE utf8_unicode_ci NOT NULL,
  `ip_n_obj_especifico` int(5) NOT NULL,
  `ip_fecha_inicio` date NOT NULL,
  `ip_fecha_fin` date NOT NULL,
  `ip_grupo_investigacion` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ip_grupo_lac` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ip_n_semilleros_beneficiados` int(5) NOT NULL,
  `ip_n_formaciones_beneficiadas` int(5) NOT NULL,
  `ip_metodologia` text COLLATE utf8_unicode_ci NOT NULL,
  `ip_impacto` text COLLATE utf8_unicode_ci NOT NULL,
  `dpr_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas_investigacion`
--

CREATE TABLE `lineas_investigacion` (
  `li_id` int(5) NOT NULL,
  `li_nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `lineas_investigacion`
--

INSERT INTO `lineas_investigacion` (`li_id`, `li_nombre`) VALUES
(1, 'INVESTIGACION'),
(2, 'INNOVACI?N'),
(3, 'MODERNIZACI?N'),
(4, 'DIVULGACI?N'),
(5, 'SERVICIOS TECNOL?GICOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas_investigacion_observatorio`
--

CREATE TABLE `lineas_investigacion_observatorio` (
  `li_id` int(5) NOT NULL,
  `li_nombre` varchar(100) CHARACTER SET latin1 COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `lineas_investigacion_observatorio`
--

INSERT INTO `lineas_investigacion_observatorio` (`li_id`, `li_nombre`) VALUES
(1, 'AGRÍCOLA Y DE PESCA'),
(2, 'AGRÍCOLA'),
(3, 'AMBIENTAL'),
(4, 'GESTIÓN ADMINISTRATIVA Y FINANCIERA'),
(5, 'MINERÍA'),
(6, 'PECUARIA'),
(7, 'INFORMÁTICA, DISEÑO Y DESARROLLO DE SOFTWARE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lineas_programaticas`
--

CREATE TABLE `lineas_programaticas` (
  `li_id` int(11) NOT NULL,
  `li_nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `li_descripcion` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `lineas_programaticas`
--

INSERT INTO `lineas_programaticas` (`li_id`, `li_nombre`, `li_descripcion`) VALUES
(1, 'INVESTIGACION APLICADA', ''),
(2, 'INNOVACIÓN Y DESARROLLO TECNOLÓGICO', ''),
(3, 'FORTALECIMIENTO DE LA OFERTA DE SERVICIOS TECNOLÓGICOS A LAS EMPRESAS', ''),
(4, 'ACTUALIZACIÓN Y MODERNIZACIÓN TECNOLÓGICA DE CENTRO', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `login`
--

CREATE TABLE `login` (
  `lg_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lg_nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `lg_user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lg_pass` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fecha_registro` datetime NOT NULL,
  `lg_rol` int(5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `login`
--

INSERT INTO `login` (`lg_id`, `lg_nombre`, `lg_user`, `lg_pass`, `fecha_registro`, `lg_rol`) VALUES
('1', 'Jose', 'jose', '662eaa47199461d01a623884080934ab', '0000-00-00 00:00:00', 1),
('15014740672', '', 'jose@gmail.com', '662eaa47199461d01a623884080934ab', '0000-00-00 00:00:00', 2),
('15015121001', '', 'juan@gmail.com', '32db5d8e46e75cb5a9a65f5c4d0ffbdb', '0000-00-00 00:00:00', 2),
('1501568958', 'rojose', 'rojsoe@gmail.com', '202cb962ac59075b964b07152d234b70', '2017-08-01 01:29:18', 1),
('15016314767', 'asjoa', 'jos@gmail.com', 'dfa09fd5282ef0d895649b4ab1832a79', '0000-00-00 00:00:00', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `municipios`
--

CREATE TABLE `municipios` (
  `mu_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mu_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ip_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f1_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_idea` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_idea` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `municipios`
--

INSERT INTO `municipios` (`mu_id`, `mu_nombre`, `rh_id`, `ip_id`, `f1_id`, `f3_idea`, `f4_idea`) VALUES
('15014740671', 'Cucuta', NULL, NULL, '15014740671716', NULL, NULL),
('15015121001', 'Villa Caro', NULL, NULL, '15015121002197', NULL, NULL),
('15015576071', 'asd', NULL, NULL, NULL, '15014740671716', NULL),
('15016314761', 'asdasd', NULL, NULL, '1501631476652', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `objetivos_especificos`
--

CREATE TABLE `objetivos_especificos` (
  `oe_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `oe_objetivo` text COLLATE utf8_unicode_ci,
  `oe_resultado` text COLLATE utf8_unicode_ci,
  `oe_producto` text COLLATE utf8_unicode_ci,
  `ip_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f2_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `objetivos_especificos`
--

INSERT INTO `objetivos_especificos` (`oe_id`, `oe_objetivo`, `oe_resultado`, `oe_producto`, `ip_id`, `f2_id`, `f3_id`, `f4_id`) VALUES
('15015519161', 'asd ajsdka asjkdas askdjasd ', NULL, NULL, NULL, '15014740671716', NULL, NULL),
('15015519162', 'ajsdk ajsdka sajsdk asdakjsdasd', NULL, NULL, NULL, '15014740671716', NULL, NULL),
('15015576071', 'asd ajsdka asjkdas askdjasd  vvvvvvvv', NULL, NULL, NULL, NULL, '15014740671716', NULL),
('15015576072', 'ajsdk ajsdka sajsdk asdakjsdasd xxxxxxxxxxxxx', NULL, NULL, NULL, NULL, '15014740671716', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_externo`
--

CREATE TABLE `personal_externo` (
  `pe_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pe_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pe_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pe_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_internoacc`
--

CREATE TABLE `personal_internoacc` (
  `pi_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pi_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pi_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pi_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_internoasc`
--

CREATE TABLE `personal_internoasc` (
  `pi_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pi_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pi_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pi_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_internoinstructores`
--

CREATE TABLE `personal_internoinstructores` (
  `pi_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pi_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pi_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pi_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal_internootros`
--

CREATE TABLE `personal_internootros` (
  `pe_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pe_nombre` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pe_apellido` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pe_identificacion` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursos_humanos`
--

CREATE TABLE `recursos_humanos` (
  `rh_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `rh_personal_ext` int(10) DEFAULT NULL,
  `rh_personal_int_asc` int(10) DEFAULT NULL,
  `rh_personal_int_acc` int(10) DEFAULT NULL,
  `rh_personal_int_instructores` int(10) DEFAULT NULL,
  `rh_personal_int_otros` int(10) DEFAULT NULL,
  `rh_aliados_ext` int(10) DEFAULT NULL,
  `rh_aliados_int` int(10) DEFAULT NULL,
  `rh_ciudades` int(10) DEFAULT NULL,
  `rh_municipios` int(10) DEFAULT NULL,
  `rh_descipcion_alianza_obj` text COLLATE utf8_unicode_ci,
  `rh_otros_serv_pers_ind_aprend_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_otros_serv_pers_ind_aprend_que_para` text COLLATE utf8_unicode_ci,
  `rh_serv_pers_indirectos_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_serv_pers_indirectos_que_para` text COLLATE utf8_unicode_ci,
  `rh_otros_gastos_impre_public_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_otros_gastos_impre_public_que_para` text COLLATE utf8_unicode_ci,
  `rh_divulgacion_act_gestion_inst_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_divulgacion_act_gestion_inst_que_para` text COLLATE utf8_unicode_ci,
  `rh_viaticos_form_prof_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_viaticos_form_prof_que_para` text COLLATE utf8_unicode_ci,
  `rh_gastos_bienestar_alum_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_gastos_bienestar_alum_que_para` text COLLATE utf8_unicode_ci,
  `rh_monitorias_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_monitorias_que_para` text COLLATE utf8_unicode_ci,
  `rh_eq_sis_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_eq_sis_descripcion_que_para` text COLLATE utf8_unicode_ci,
  `rh_eq_soft_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_eq_soft_descripcion_que_para` text COLLATE utf8_unicode_ci,
  `rh_eq_mind_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_eq_mind_descripcion_que_para` text COLLATE utf8_unicode_ci,
  `rh_eq_otras_compras_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_eq_otras_compras_que_para` text COLLATE utf8_unicode_ci,
  `rh_formacion_prof_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_formacion_pro_que_para` text COLLATE utf8_unicode_ci,
  `rh_mantenimiento_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_mantenimiento_que_para` text COLLATE utf8_unicode_ci,
  `rh_otras_comun_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_otras_comun_que_para` text COLLATE utf8_unicode_ci,
  `rh_adecuaciones_valor` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rh_adecuaciones_que_para` text COLLATE utf8_unicode_ci,
  `dpr_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `semilleros_ben`
--

CREATE TABLE `semilleros_ben` (
  `sb_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sb_nombre` text COLLATE utf8_unicode_ci,
  `ip_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f3_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `f4_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `semilleros_ben`
--

INSERT INTO `semilleros_ben` (`sb_id`, `sb_nombre`, `ip_id`, `f3_id`, `f4_id`) VALUES
('15015576071', 'asda', NULL, '15014740671716', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `aliados_externos`
--
ALTER TABLE `aliados_externos`
  ADD PRIMARY KEY (`ae_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `aliados_internos`
--
ALTER TABLE `aliados_internos`
  ADD PRIMARY KEY (`ai_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD PRIMARY KEY (`ci_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `datos_personales_autores`
--
ALTER TABLE `datos_personales_autores`
  ADD PRIMARY KEY (`dpa_id`),
  ADD KEY `dpr_id` (`dpr_id`);

--
-- Indices de la tabla `datos_personales_radicador`
--
ALTER TABLE `datos_personales_radicador`
  ADD PRIMARY KEY (`dpr_id`),
  ADD KEY `lo_id` (`lo_id`);

--
-- Indices de la tabla `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`es_id`);

--
-- Indices de la tabla `fase_1`
--
ALTER TABLE `fase_1`
  ADD PRIMARY KEY (`f1_id`),
  ADD KEY `dpr_id` (`dpr_id`),
  ADD KEY `li_linea_investigacion` (`li_linea_investigacion`),
  ADD KEY `li_linea_programatica` (`li_linea_programatica`);

--
-- Indices de la tabla `fase_2`
--
ALTER TABLE `fase_2`
  ADD PRIMARY KEY (`f2_id`),
  ADD KEY `f1_id` (`f1_id`);

--
-- Indices de la tabla `fase_3`
--
ALTER TABLE `fase_3`
  ADD PRIMARY KEY (`f3_id`),
  ADD KEY `f2_id` (`f2_id`);

--
-- Indices de la tabla `fase_4`
--
ALTER TABLE `fase_4`
  ADD PRIMARY KEY (`f4_id`),
  ADD KEY `f2_id` (`f3_id`);

--
-- Indices de la tabla `formaciones_ben`
--
ALTER TABLE `formaciones_ben`
  ADD PRIMARY KEY (`fb_id`),
  ADD KEY `ip_id` (`ip_id`),
  ADD KEY `f3_id` (`f3_id`),
  ADD KEY `f4_id` (`f4_id`);

--
-- Indices de la tabla `informacion_proyecto`
--
ALTER TABLE `informacion_proyecto`
  ADD PRIMARY KEY (`ip_id`),
  ADD KEY `dpr_id` (`dpr_id`),
  ADD KEY `ip_estado` (`ip_estado`),
  ADD KEY `ip_linea` (`ip_linea`);

--
-- Indices de la tabla `lineas_investigacion`
--
ALTER TABLE `lineas_investigacion`
  ADD PRIMARY KEY (`li_id`);

--
-- Indices de la tabla `lineas_investigacion_observatorio`
--
ALTER TABLE `lineas_investigacion_observatorio`
  ADD PRIMARY KEY (`li_id`);

--
-- Indices de la tabla `lineas_programaticas`
--
ALTER TABLE `lineas_programaticas`
  ADD PRIMARY KEY (`li_id`);

--
-- Indices de la tabla `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`lg_id`);

--
-- Indices de la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD KEY `f1_id` (`f1_id`),
  ADD KEY `ip_id` (`ip_id`),
  ADD KEY `rh_id` (`rh_id`),
  ADD KEY `f3_idea` (`f3_idea`),
  ADD KEY `f4_idea` (`f4_idea`);

--
-- Indices de la tabla `objetivos_especificos`
--
ALTER TABLE `objetivos_especificos`
  ADD PRIMARY KEY (`oe_id`),
  ADD KEY `f2_id` (`f2_id`),
  ADD KEY `ip_id` (`ip_id`),
  ADD KEY `f3_id` (`f3_id`),
  ADD KEY `f4_id` (`f4_id`);

--
-- Indices de la tabla `personal_externo`
--
ALTER TABLE `personal_externo`
  ADD PRIMARY KEY (`pe_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `personal_internoacc`
--
ALTER TABLE `personal_internoacc`
  ADD PRIMARY KEY (`pi_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `personal_internoasc`
--
ALTER TABLE `personal_internoasc`
  ADD PRIMARY KEY (`pi_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `personal_internoinstructores`
--
ALTER TABLE `personal_internoinstructores`
  ADD PRIMARY KEY (`pi_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `personal_internootros`
--
ALTER TABLE `personal_internootros`
  ADD PRIMARY KEY (`pe_id`),
  ADD KEY `rh_id` (`rh_id`);

--
-- Indices de la tabla `recursos_humanos`
--
ALTER TABLE `recursos_humanos`
  ADD PRIMARY KEY (`rh_id`),
  ADD KEY `dpr_id` (`dpr_id`);

--
-- Indices de la tabla `semilleros_ben`
--
ALTER TABLE `semilleros_ben`
  ADD PRIMARY KEY (`sb_id`),
  ADD KEY `ip_id` (`ip_id`),
  ADD KEY `f3_id` (`f3_id`),
  ADD KEY `f4_id` (`f4_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `estados`
--
ALTER TABLE `estados`
  MODIFY `es_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `lineas_investigacion`
--
ALTER TABLE `lineas_investigacion`
  MODIFY `li_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `lineas_investigacion_observatorio`
--
ALTER TABLE `lineas_investigacion_observatorio`
  MODIFY `li_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `lineas_programaticas`
--
ALTER TABLE `lineas_programaticas`
  MODIFY `li_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `aliados_externos`
--
ALTER TABLE `aliados_externos`
  ADD CONSTRAINT `aliados_externos_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `aliados_internos`
--
ALTER TABLE `aliados_internos`
  ADD CONSTRAINT `aliados_internos_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `ciudades`
--
ALTER TABLE `ciudades`
  ADD CONSTRAINT `ciudades_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `datos_personales_autores`
--
ALTER TABLE `datos_personales_autores`
  ADD CONSTRAINT `datos_personales_autores_ibfk_1` FOREIGN KEY (`dpr_id`) REFERENCES `datos_personales_radicador` (`dpr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `datos_personales_radicador`
--
ALTER TABLE `datos_personales_radicador`
  ADD CONSTRAINT `datos_personales_radicador_ibfk_1` FOREIGN KEY (`lo_id`) REFERENCES `login` (`lg_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fase_1`
--
ALTER TABLE `fase_1`
  ADD CONSTRAINT `fase_1_ibfk_1` FOREIGN KEY (`dpr_id`) REFERENCES `datos_personales_radicador` (`dpr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fase_1_ibfk_2` FOREIGN KEY (`li_linea_programatica`) REFERENCES `lineas_programaticas` (`li_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fase_1_ibfk_3` FOREIGN KEY (`li_linea_investigacion`) REFERENCES `lineas_investigacion_observatorio` (`li_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fase_2`
--
ALTER TABLE `fase_2`
  ADD CONSTRAINT `fase_2_ibfk_1` FOREIGN KEY (`f2_id`) REFERENCES `fase_1` (`f1_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fase_3`
--
ALTER TABLE `fase_3`
  ADD CONSTRAINT `fase_3_ibfk_2` FOREIGN KEY (`f3_id`) REFERENCES `fase_2` (`f2_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `fase_4`
--
ALTER TABLE `fase_4`
  ADD CONSTRAINT `fase_4_ibfk_1` FOREIGN KEY (`f4_id`) REFERENCES `fase_3` (`f3_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `formaciones_ben`
--
ALTER TABLE `formaciones_ben`
  ADD CONSTRAINT `formaciones_ben_ibfk_1` FOREIGN KEY (`ip_id`) REFERENCES `informacion_proyecto` (`ip_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formaciones_ben_ibfk_2` FOREIGN KEY (`f3_id`) REFERENCES `fase_3` (`f3_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `formaciones_ben_ibfk_3` FOREIGN KEY (`f4_id`) REFERENCES `fase_4` (`f4_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `informacion_proyecto`
--
ALTER TABLE `informacion_proyecto`
  ADD CONSTRAINT `informacion_proyecto_ibfk_1` FOREIGN KEY (`dpr_id`) REFERENCES `datos_personales_radicador` (`dpr_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `informacion_proyecto_ibfk_2` FOREIGN KEY (`ip_estado`) REFERENCES `estados` (`es_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `informacion_proyecto_ibfk_3` FOREIGN KEY (`ip_linea`) REFERENCES `lineas_investigacion` (`li_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `municipios`
--
ALTER TABLE `municipios`
  ADD CONSTRAINT `municipios_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `municipios_ibfk_2` FOREIGN KEY (`ip_id`) REFERENCES `informacion_proyecto` (`ip_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `municipios_ibfk_3` FOREIGN KEY (`f1_id`) REFERENCES `fase_1` (`f1_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `municipios_ibfk_5` FOREIGN KEY (`f3_idea`) REFERENCES `fase_3` (`f3_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `municipios_ibfk_7` FOREIGN KEY (`f4_idea`) REFERENCES `fase_4` (`f4_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `objetivos_especificos`
--
ALTER TABLE `objetivos_especificos`
  ADD CONSTRAINT `objetivos_especificos_ibfk_1` FOREIGN KEY (`ip_id`) REFERENCES `informacion_proyecto` (`ip_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `objetivos_especificos_ibfk_2` FOREIGN KEY (`f2_id`) REFERENCES `fase_2` (`f2_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `objetivos_especificos_ibfk_3` FOREIGN KEY (`f3_id`) REFERENCES `fase_3` (`f3_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `objetivos_especificos_ibfk_4` FOREIGN KEY (`f4_id`) REFERENCES `fase_4` (`f4_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personal_externo`
--
ALTER TABLE `personal_externo`
  ADD CONSTRAINT `personal_externo_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personal_internoacc`
--
ALTER TABLE `personal_internoacc`
  ADD CONSTRAINT `personal_internoacc_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personal_internoasc`
--
ALTER TABLE `personal_internoasc`
  ADD CONSTRAINT `personal_internoasc_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personal_internoinstructores`
--
ALTER TABLE `personal_internoinstructores`
  ADD CONSTRAINT `personal_internoinstructores_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personal_internootros`
--
ALTER TABLE `personal_internootros`
  ADD CONSTRAINT `personal_internootros_ibfk_1` FOREIGN KEY (`rh_id`) REFERENCES `recursos_humanos` (`rh_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recursos_humanos`
--
ALTER TABLE `recursos_humanos`
  ADD CONSTRAINT `recursos_humanos_ibfk_1` FOREIGN KEY (`dpr_id`) REFERENCES `datos_personales_radicador` (`dpr_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `semilleros_ben`
--
ALTER TABLE `semilleros_ben`
  ADD CONSTRAINT `semilleros_ben_ibfk_1` FOREIGN KEY (`ip_id`) REFERENCES `informacion_proyecto` (`ip_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semilleros_ben_ibfk_2` FOREIGN KEY (`f3_id`) REFERENCES `fase_3` (`f3_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semilleros_ben_ibfk_3` FOREIGN KEY (`f4_id`) REFERENCES `fase_4` (`f4_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
