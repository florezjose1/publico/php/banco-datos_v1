<?php
   session_start();
   if(isset($_SESSION['id'])) {
      echo '<script> window.location="Views/login.php"; </script>';
   }


if (!empty($_GET['ac'])) {
   if (is_numeric($_GET['registro'])) {
      if ($_SESSION['registro'] == $_GET['registro']) {

         if (is_numeric($_GET['ac'])==1 || is_numeric($_GET['ac'])==2) {
            if ($_GET['ac']==1) {
               $href = 'Views/inscription-proyect';
            }
            if ($_GET['ac']==2) {
               $href = 'Views/inscription-proyect';
            }
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Banco Proyectos Sennova</title>
	<link rel="stylesheet" href="Public/bootstrap/css/fonts.css">
	<link rel="stylesheet" href="Public/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="Public/css/style.css">
   <link rel="stylesheet" href="Public/bootstrap/css/bootstrap.min.css">
   <link rel="stylesheet" href="Public/bootstrap/css/compiled.css">
   <link rel="stylesheet" href="Public/css/style-index.css">
	<link rel="stylesheet" href="Public/plugins/alerts/css/sweetalert.css">
	<!--<link rel="stylesheet" href="Hand_Of_Sean_Demo.ttf">-->
	
	
	<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
	
</head>
<body onload="atras();" >
   
   <div class="row" style=" position: absolute; width: 100%;top:0;">
      <div class="col-xs-12">
         <h2 class="header" style=""><b>Banco</b><br> De Proyectos</h2>

         <img src="Public/img/header/2.png" id="img_2" class="animated img-responsive pull-right" alt="">
         <img src="Public/img/header/3.png" id="img_3" class="animated img-responsive pull-right" alt="">
         <img src="Public/img/header/4.png" id="img_4" class="animated img-responsive pull-right" alt="">
         <img src="Public/img/header/5.png" id="img_5" class="animated img-responsive pull-right" alt="">
      </div>
   </div>
	<div class="container-fluid menu" id="botones-inicio" >
      <ul class="list-inline">
         <li>
            <a href="index.php" id="inicio" class="btn-header active">Inicio</a>
         </li>
         <li>
            <a href="beneficios.php" id="beneficios" class="btn-header ">Beneficios</a>
         </li>
         <li>
            <a href="inscripcion.php" id="inscripcion" class="btn-header ">Inscripcion</a>
         </li>
         <li>
            <a href="#" data-toggle="modal" data-target="#modal-login" id="sesion" class="btn-header ">Iniciar Sesion</a>
         </li>
      </ul>
   </div>




	<?php 
      if ($_GET['ac']==1) {
   ?>
   
   <div class="container-fluid" style="margin-top: 170px; ">
      <div class="col-xs-2 menu-lineas">
         <ul>
            <li>
               <button class="linea_1 btn btn-defaul btn-lineas active" onclick="linea(1)">Investigacion</button>
            </li>
            <li>
               <button class="linea_2 btn btn-info-banco btn-lineas" onclick="linea(2)">Innovacion</button>
            </li>
            <li>
               <button class="linea_3 btn btn-info-banco btn-lineas" onclick="linea(3)">Modernizacion</button>
            </li>
            <li>
               <button class="linea_5 btn btn-info-banco btn-lineas" onclick="linea(5)">Servicios Tencológicos</button>
            </li>
         </ul>
      </div>
      <div class="col-xs-9 content-lineas">
         <div class="animated content-linea_1">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=investigacion-aplicada&form=fase-1">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l1.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=investigacion-aplicada&form=fase-1">
                           <p style="font-size: 25px;color:#fff;"><b>INVESTIGACIÓN APLICADA.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> (Máximo $95.000.000).</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_2" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=innovacion-y-desarrollo-tecnologico&form=fase-1">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l2.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- light -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=innovacion-y-desarrollo-tecnologico&form=fase-1">
                           <p style="font-size: 25px;color:#fff;"><b>INNOVACIÓN Y DESARROLLO TECNOLÓGICO. </b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;">(Máximo $100.000.000)</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_3" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=actualizacion-y-modernizacion-tecnologica&form=fase-1">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l3.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=actualizacion-y-modernizacion-tecnologica&form=fase-1">
                           <p style="font-size: 25px;color:#fff;"><b>ACTUALIZACIÓN Y MODERNIZACIÓN TECNOLÓGICA DE CENTRO. </b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> (Máximo $400.000.000)</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_5" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=fortalecimiento-de-la-ofeta-de-servicios-tecnologicos-a-las-empresas&form=fase-1">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l5.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=fortalecimiento-de-la-ofeta-de-servicios-tecnologicos-a-las-empresas&form=fase-1">
                           <p style="font-size: 25px;color:#fff;"><b>FORTALECIMIENTO DE LA OFERTA DE SERVICIOS TECNOLÓGICOS A LAS EMPRESAS.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;">  (Máximo $500.000.000)</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
      </div>
   </div>

   <?php }else{ ?>

   <div class="container-fluid" style="margin-top: 170px; ">
      <div class="col-xs-2 menu-lineas">
         <ul>
            <li>
               <button class="linea_1 btn btn-defaul btn-lineas active" onclick="linea(1)">Investigacion</button>
            </li>
            <li>
               <button class="linea_2 btn btn-info-banco btn-lineas" onclick="linea(2)">Innovacion</button>
            </li>
            <li>
               <button class="linea_3 btn btn-info-banco btn-lineas" onclick="linea(3)">Modernizacion</button>
            </li>
            <li>
               <button class="linea_4 btn btn-info-banco btn-lineas" onclick="linea(4)">Divulgacion</button>
            </li>
            <li>
               <button class="linea_5 btn btn-info-banco btn-lineas" onclick="linea(5)">Servicios Tencológicos</button>
            </li>
         </ul>
      </div>
      <div class="col-xs-9 content-lineas">
         <div class="animated content-linea_1">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=investigacion&form=datos-personales">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l1.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=investigacion&form=datos-personales">
                           <p style="font-size: 25px;color:#fff;"><b>INVESTIGACIÓN.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Formula tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_2" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=innovacion&form=datos-personales">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l2.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=innovacion&form=datos-personales">
                           <p style="font-size: 25px;color:#fff;"><b>INNOVACIÓN.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Formula tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_3" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=modernizacion&form=datos-personales">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l3.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=modernizacion&form=datos-personales">
                           <p style="font-size: 25px;color:#fff;"><b>MODERNIZACIÓN.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Formula tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_4" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=divulgacion&form=datos-personales">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l4.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=divulgacion&form=datos-personales">
                           <p style="font-size: 25px;color:#fff;"><b>DIVULGACIÓN.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Formula tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
         <div class="animated content-linea_5" style="display: none;">
            <!--Collection card-->
            <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=servicios-tecnologicos&form=datos-personales">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  ">
                     <img src="Public/img/lineas/l5.jpg" style="width:100%;" class="img-lineas img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="<?php echo $href?>.php?ac=<?php echo $_GET['ac']?>&registro=<?php echo $_SESSION['registro']?>&linea=servicios-tecnologicos&form=datos-personales">
                           <p style="font-size: 25px;color:#fff;"><b>SERVICIOS TECNOLÓGICOS.</b> <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Formula tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
      </div>
	</div>
   <?php } ?>





	<?php 
		include 'Public/inc/modales.php';
	?>

	<script src="Public/plugins/jquery/jquery-2.2.3.min.js"></script>


	<script src="Public/plugins/alerts/js/functions.js"></script>
  	<script src="Public/plugins/alerts/js/sweetalert.min.js"></script>

	<script src="Public/bootstrap/js/bootstrap.min.js"></script>
	<script src="Public/bootstrap/js/mdb.min.js"></script>
	<script src="Public/js/main.js"></script>
	<script src="Public/js/usuarios.js"></script>

	<script>

      function linea(cod){
         for (var i = 1; i <= 6; i++) {
            if (i==cod) {
               $('.linea_'+i).addClass('active');
               $('.content-linea_'+i).show(1500);
            }else{
               $('.linea_'+i).removeClass('active');
               $('.content-linea_'+i).hide(1300);
            }
         }
      }
        
    $(function(){
        setTimeout(function(){
            $('#img_2').addClass('fadeInRight');
            $('#img_2').css('display','block');

            setTimeout(function(){
                $('#img_3').addClass('fadeInRight');
                $('#img_3').css('display','block');

                setTimeout(function(){
                    $('#img_4').addClass('fadeInRight');
                    $('#img_4').css('display','block');

                    setTimeout(function(){
                        $('#img_5').addClass('fadeInRight');
                        $('#img_5').css('display','block');

                        /*setTimeout(function(){
                            
                            //$('#botones-inicio').addClass('sombra');
                            $('#botones-inicio').addClass('fadeInLeft');
                                       
                        },600)*/
                    },600)
                },600)
            },600)
        },600)
    });

    </script>
	
	<script type="text/javascript">
		//window.history.forward();
		function atras(){
			//window.location.hash = "no-back-button";
			//window.location.hash = "Again-No-back-button"; // chrome
			//window.onhashchange = function(){window.location.hash="no-back-button"};
			////window.history.forward();
		}
		//function adelante(){
			//window.history.back();
		//}

		$(function () {
			var html = '<img src="Public/img/facebook.png" alt="">	';
			setTimeout(function(){
				
				/*swal({
			        title: "Banco de Proyectos Sennova",
			        text: html,
			        timer: 5000,
			        showConfirmButton: false
			    });*/
			},1000);
		});
	</script>
</body>
</html>


<?php 
      }else{
        echo '<script> window.location="../404.php"; </script>';
      }
    }else{
      echo '<script> window.location="../404.php"; </script>';
    }
  }else{
    echo '<script> window.location="../404.php"; </script>';
  }
}else{
  echo '<script> window.location="../404.php"; </script>';
}
?>