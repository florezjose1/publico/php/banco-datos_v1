<div style="" class="modal fade" id="modalFormarse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-body contentModalUpdate" style="text-align: center;padding: 0;">
        <input type="text" id="cod_proyecto" name="cod_proyecto" style="display: none; ">
        <center>
          <img src="Public/img/modales/modal1.png" class="img-responsive">
          <p class="modal-title" id="myModalLabel" style="text-align: justify; padding: 10px 25px;">
          El <b>Observatorio</b>
          Es un espacio donde los miembros de la comunidad SENNOVA formulan y presentan proyectos de investigación que serán analizadas, valoradas y consolidadas para ser postulados al banco de proyectos Nacional, la estrategia parte de ir aumentado el nivel de compromiso y exigencia de acuerdo a las fases a diligenciar del proyecto y puede ser presentada por un semillero o grupo de investigación, aprendiz, instructor, administrativo o comunidad vinculada con el centro de Formación CEDRUM – SENA.
           <!-- La opción de formarte es un acompañamiento por parte de la entidad patrocinadora <i>Sennova</i> el cual se te asignará un previo acompañamiento para que así logres resolver dudas tecnicas en la incripcion del proyecto.-->
          </p>  
        </center>
        <div class="text-xs-center" style="padding-bottom: 20px;">
          <a href="lineas-programaticas.php?registro=<?php echo $_SESSION['registro']?>&ac=1" type="button" class="btn btn-info-banco" id="deleteFree"> Si, deseo formarme! </a>
          <button type="button" class="btn btn-success-banco" id="NodeleteProyecto" data-dismiss="modal" data-toggle="modal" data-target="#modalIncripcionDirecta"> Deseo escoger la otra opción! </button> 
        </div>
      </div>            
    </div>
  </div>
</div>



<div style="" class="modal fade" id="modalIncripcionDirecta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xs" role="document">
    <div class="modal-content">
      <div class="modal-body contentModalUpdate" style="text-align: center;padding: 0;">
        <input type="text" id="cod_proyecto" name="cod_proyecto" style="display: none; ">
        <center>
          <img src="Public/img/modales/modal2.png" class="img-responsive">
          <h5 class="modal-title" id="myModalLabel"  style="text-align: justify; padding: 10px 25px;">
            Al seleccionar esta idea estas consciente de que cuentas con la documentacion bien especifica y necesaría para que tu proyecto sea aprobado.
          </h4>
        </center>
        <br>
        <div class="text-xs-center" style="padding-bottom: 20px;">
          <a href="lineas-programaticas.php?registro=<?php echo $_SESSION['registro']?>&ac=2" type="button" class="btn btn-info-banco" id="deleteFree"> Si, estoy de acuerdo! </a>
          <button type="button" class="btn btn-success-banco" id="NodeleteProyecto" data-dismiss="modal" data-toggle="modal" data-target="#modalFormarse"> No, deseo formarme! </button> 
        </div>
      </div>            
    </div>
  </div>
</div>



<!-- Modal Inicio -->
<div class="modal fade modal-ext" id="modal-login" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <!--Content-->
        <div class="modal-content modal-md">
            <!--Header-->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="w-100"><i class="fa fa-user"></i> Iniciar sesion</h3>
            </div>
            <!--Body-->
            <div class="modal-body">
                      
        <div class="form" onkeypress="return accesoBanco(event)">

            <div class="md-form">
                <i class="fa fa-envelope prefix"></i>
                <input type="email" id="email_User" name="correoRU"  class="form-control">
                <label for="email_User">Correo</label>
            </div>


            <div class="md-form">
                <i class="fa fa-lock prefix"></i>
                <input type="password" id="pass_User" name="claveRU"  class="form-control">
                <label for="pass_User">Contraseña</label>
            </div>

            <div class="md-form">
              <div id="alert" style="display: none;"></div>
            </div>
            <input type="reset" style="display: none;" id="resetRegistro" class="form-control">
            
            <div class="text-center">
                <center>
                    <button type="button" class="btn btn-success btn-sm" onclick="sessionUserBancoProyectos()">Iniciar Sesion</button>
                    
                </center>
            </div>
        </div>
            </div>
            <!--Footer-->
            <div class="modal-footer">
                <button type="button" id="cerrarRegistro" class="btn btn-warning btn-sm ml-auto" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
        <!--/.Content-->
    </div>
</div>



<!-- Modal Bienvenida -->
<div class="modal fade modal-ext" id="modal-bienvenida" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <!--Content-->
    <div class="modal-content modal-md" style="padding: 100px;margin-top: 100px;">
      <img src="Public/img/bienvenido.png" alt="" class="img-responsive">   
    </div>
    <!--/.Content-->
  </div>
</div>



