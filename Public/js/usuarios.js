var success =
    `<div class="alert alert-success" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Espere! </strong>
        Iniciando sesion.!</center>
    </div>`;

var success1 =
    `<div class="alert alert-success" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Muy bien! </strong>
        Tu registro ha sido exitoso.!</center>
    </div>`;

var danger =
    `<div class="alert alert-danger" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Error! </strong>
        Usuario o contraseña incorrectos...</center>
    </div>`;

var danger1 =
    `<div class="alert alert-danger" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Error! </strong>
        Algo ha salido mal.! Vuelve a intentarlo...</center>
    </div>`;

var warning =
    `<div class="alert alert-warning" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Upps! </strong>
        Al parecer has dejado campos vacios.!</center>
    </div>`;
var info = 
    `<div class="alert alert-info" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Procesando! </strong>
        Por favor espere.!</center>
    </div>`;
var info1 = 
    `<div class="alert alert-info" role="alert">
        <!--<button type="button" class="close" data-dismiss="alert">×</button>-->
        <center><strong>Upps! </strong>
        Al parecer has dejado campos vacios.!</center>
    </div>`;

function accesoBanco(e){
    console.log('a')
    if(e.keyCode == 13){
        sessionUserBancoProyectos();
        console.log('entro...')
    }
}
function sessionUserBancoProyectos() {
    var usuario= $('#email_User').val();
    console.log("usuario", usuario);
    var pass   = $('#pass_User').val();
    console.log("pass", pass);
    if (usuario.length>0 && pass.length>0 ) {
        $('#alert').html(info);
        $('#alert').show('slow');
        var param  ={'Funcion':'sessionBanco', 'usuario':usuario, 'pass':pass};
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: 'ajax.php',
            //beforesend: function(){
            //},
            success: function(data) {
                console.log("data", data);
                setTimeout(function(){
                    var json = JSON.parse(data);
                    var a = json[0].detalle;
                    if (a > 0 ) {
                        $('#alert').html(success);
                        $('#alert').show('slow');
                        var page = json[0].page;
                        setTimeout(function(){
                            window.location=page;
                            //location.reload(true);
                        },3500);
                    }else{
                        $('#alert').html(danger);
                        $('#alert').show('slow');
                        setTimeout(function(){
                            $('#alert').hide('slow');
                        },3500);
                    }
                },2500);
            },
            error: function(){
                $('#alert').html(danget);
                $('#alert').show('slow');
                location.reload();
            }
        });
    }else{
        console.log('error 1');
        $('#alert').html(warning);
        $('#alert').show('slow');
    }
}

function verificarEmail(){
    var email = $('#email').val();
    if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email)) {

        var param  ={'Funcion':'verificarEmail', 'email':email };
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: '../ajax.php',
            //beforesend: function(){
            //},
            success: function(data) {
                console.log("data", data);

                if (data>0) {
                    $('.alert-email').show('slow'); 
                    $('.alert-email p').html('Email ya existe.! Prueba con otro'); 
                    $('.btn-registrar').attr("disabled", "disabled");
                }else{
                    $('.btn-registrar').removeAttr('disabled')
                    $('.alert-email').hide('slow');
                }

            }
        });
    }else{
        $('.alert-email').show('slow'); 
        $('.alert-email p').html('Email Invalido.!');
        $('.btn-registrar').attr("disabled", "disabled");
    }
}
function registrarUsuario(){
    var nombre = $('#nombre').val();
    console.log("nombre", nombre);
    var email = $('#email').val();
    var clave = $('#clave').val();
    if (nombre.length>0 && email.length>0 && clave.length>0) {
        console.log('Hola');
        var param  ={
            'Funcion':'registrarUser', 
            'nombre':nombre,
            'email':email,
            'clave':clave
        };
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: '../ajax.php',
            //beforesend: function(){
            //},
            success: function(data) {
                console.log("data", data);
                if (data>0) {
                    swal("¡Hecho!",
                        "Se ha registrado un nuevo usuario.",
                        "success");
                    setTimeout(function(){
                        location.reload(true);  
                    },3000);
                }else{
                    $('#alert').html(danger1);
                    $('#alert').show('slow');
                    setTimeout(function(){
                        $('#alert').hide('slow');
                    },3000);
                }
            }
        });
    }else{
        $('#alert').html(warning);
        $('#alert').show('slow');
        setTimeout(function(){
            $('#alert').hide('slow');
        },3000);
    }
}

function actualizarUsuario(){
    var cod = $('#cod').val();
    var nombre = $('#nombreU').val();
    var email = $('#emailU').val();
    var clave = $('#claveU').val();
    if (nombre.length>0 && email.length>0 && clave.length>0) {
        var param  ={
            'Funcion':'actualizarUsuario', 
            'cod':cod,
            'nombre':nombre,
            'email':email,
            'clave':clave
        };
        $.ajax({
            data: JSON.stringify(param),
            type: "JSON",
            url: '../ajax.php',
            //beforesend: function(){
            //},
            success: function(data) {
                console.log("data", data);
                if (data>0) {
                    swal("¡Hecho!",
                        "Se ha registrado un nuevo usuario.",
                        "success");
                    setTimeout(function(){
                        location.reload(true);  
                    },3000);
                }else{
                    $('#alert').html(danger1);
                    $('#alert').show('slow');
                    setTimeout(function(){
                        $('#alert').hide('slow');
                    },3000);
                }
            }
        });
    }else{
        $('#alert').html(warning);
        $('#alert').show('slow');
        setTimeout(function(){
            $('#alert').hide('slow');
        },3000);
    }
}

function alertEliminar(cod){
    swal({
        title: "¿Seguro que deseas continuar?",
        text: "Una vez eliminado no podrás recuperar el registro ...",
        type: "warning",
        showCancelButton: true,
        cancelButtonText: "Mmm... mejor no!",
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "¡Adelante!",
        closeOnConfirm: false
    },

    function () {
        eliminarUsuarioAdmin(cod);
    });
}
function eliminarUsuarioAdmin(cod){
    var param = {
        'Funcion': 'eliminarUsuario',
        'cod': cod
    };
    $.ajax({
        data: JSON.stringify(param),
        type: "JSON",
        url: '../ajax.php',
        //beforesend: function(){
        //},
        success: function (data) {
            console.log(data);
            if (data > 0) {
                swal("¡Hecho!",
                    "El registro ha sido eliminado.",
                    "success");
                setTimeout(function () {
                    location.reload(true);
                }, 2000);
            } else {
                swal("Error!",
                    "Algo ha salido mal...",
                    "error");
            }
        }
    });
}



$(function(){
    // funciones probatorias para saber el porcentaje de scroll de nuestra pagina
    $("#detectascroll").on("click", function () {
        //scroll vertical
        var sv = $(document).scrollTop();
        //scroll horizontal
        var sh = $(document).scrollLeft();
        $(this).find("span").text("(" + sh + "," + sv+ ")");
    });
    $("#scrollelemento").on("click", function () {
        var boton = $(this);
        var elemento = boton.parent();
        //scroll vertical
        var sv = elemento.scrollTop();
        //scroll horizontal
        var sh = elemento.scrollLeft();
        boton.find("span").text("(" + sh + "," + sv+ ")");
    });

    

    

    $("#plagasBoton").on("click", function(){
        var a = $( window ).width();
        if (a<768){
            deplegueMenu2();
        }
        var posicion = $("#graficasPlagas").offset().top;
        $("html, body").animate({
            scrollTop: posicion
        }, 1000);
    });

    $(document).on("scroll", function(){
        $('.header').css('padding','0px');
        var desplazamientoActual = $(document).scrollTop();
        var controlArriba = $("#irarriba");
        var title = $(".navbar-brand");
        var header = $(".navbar");

        if(

            desplazamientoActual > 100 && 
            controlArriba.css("display") == "none"  && 
            title.css({
               'margin-top' : '10px',
               'font-size' : '25px',
            }) &&
            header.css({
                'padding': '.1rem 1rem'
            })      

            ){
            controlArriba.fadeIn(500);
        }


        if(

            desplazamientoActual < 100 && 
            controlArriba.css("display") == "block" && 
            header.css('padding-top','30px') &&

            title.css({
               'margin-top' : '15px',
               'font-size' : '30px',
            }) &&
            header.css({
                'padding': '.5rem 1rem'
            })       

            ){
            controlArriba.fadeOut(500);
        }
    });
    $("#irarriba a").on("click", function(e){

        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    });
});
