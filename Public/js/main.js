var success =
	`<div class="alert alert-success" role="alert">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Muy bien! </strong>
		La informacion esta completa.!
	</div>`;
var danger =
	`<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Error! </strong>
		Verifica que la informacion este diligenciada correctamente.
	</div>`;

var warning =
	`<div class="alert alert-warning" role="alert">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<strong>Upps! </strong>
		Al parecer has dejado campos vacios.!
	</div>`;

function cambiarVista(cod,param){
	var element = $('#content_'+cod);
	for(var i = 1; i<=5; i++) {
		if ( i != cod) {
			$('#content_'+i).addClass('animated fadeInUpBig ');
			$('#content_'+i).hide('slow');
			$('#opcion_'+i).removeClass('active')
		}else{
			$('#opcion_'+i).addClass('active')
			setTimeout(function(){
				$('#content_'+cod).addClass('animated fadeInUpBig ');
				var element_1 = $('#content_'+cod);
				$(element_1).show('slow');
				//$(element_1).css({ 
					//'width': '700px',
					//'height': '400px',
					//'-webkit-transform': 'rotate(360deg)', /* Safari */
					//'transform': 'rotate(360deg)',
				  //  '-webkit-transition': 'width 2s, height 2s, -webkit-transform 2s', /* Safari */
				  //  'transition': 'width 2s, height 2s, transform 2s'
				//});
			},200)
		}
	}
}


function enviarDatosForm(val,ac,linea,registro){
	console.log("val", val);
	if (val == 1) {
		var n = $('#accesoRegistro').val();
		if (n == 0) {
		
			var nombreRa = $('#nombreRadicador').val();
			var apellidoRa = $('#apellidoRadicador').val();
			var identificacionRa = $('#identificacionRadicador').val();
			var emailRa = $('#emailRadicador').val();
			var telefonoRa = $('#telefonoRadicador').val();
			var autores = $('#autores').val();

			var sum = 0;
			if (nombreRa.length > 0) {
				$('#nombreRadicador').removeClass('input-danger');
			} else {
				sum += 1;
				$('#nombreRadicador').addClass('input-danger');
			}
			if (apellidoRa.length > 0) {
				$('#apellidoRadicador').removeClass('input-danger');
			} else {
				sum += 1;
				$('#apellidoRadicador').addClass('input-danger');
			}
			if (identificacionRa.length > 0) {
				$('#identificacionRadicador').removeClass('input-danger');
			} else {
				sum += 1;
				$('#identificacionRadicador').addClass('input-danger');
			}
			if (emailRa.length > 0) {
				$('#emailRadicador').removeClass('input-danger');
			} else {
				sum += 1;
				$('#emailRadicador').addClass('input-danger');
			}
			if (telefonoRa.length > 0) {
				$('#telefonoRadicador').removeClass('input-danger');
			} else {
				sum += 1;
				$('#telefonoRadicador').addClass('input-danger');
			}

			if (autores.length > 0) {
				$('#autores').removeClass('input-danger');
			} else {
				sum += 1;
				$('#autores').addClass('input-danger');
			}
			for (var autor = 1; autor <= autores; autor++) {
				var nombre = $('#nombreAutor_' + autor).val();
				var apellido = $('#apellidosAutor_' + autor).val();
				var identificacion = $('#identificacionAutor_' + autor).val();
				var email = $('#emailAutor_' + autor).val();
				var telefono = $('#telefonoAutor_' + autor).val();
				if (nombre.length > 0) {
					$('#nombreAutor_' + autor).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreAutor_' + autor).addClass('input-danger');
				}
				if (apellido.length > 0) {
					$('#apellidosAutor_' + autor).removeClass('input-danger');
				} else {
					sum += 1;
					$('#apellidosAutor_' + autor).addClass('input-danger');
				}
				if (identificacion.length > 0) {
					$('#identificacionAutor_' + autor).removeClass('input-danger');
				} else {
					sum += 1;
					$('#identificacionAutor_' + autor).addClass('input-danger');
				}
				if (email.length > 0) {
					$('#emailAutor_' + autor).removeClass('input-danger');
				} else {
					sum += 1;
					$('#emailAutor_' + autor).addClass('input-danger');
				}
				if (telefono.length > 0) {
					$('#telefonoAutor_' + autor).removeClass('input-danger');
				} else {
					sum += 1;
					$('#telefonoAutor_' + autor).addClass('input-danger');
				}
			}

			console.log("sum", sum);
			if (sum == 0) {
				$('#messageAlert-datosPersonales-inscripcion').html(success)
				$('#messageAlert-datosPersonales-inscripcion').show('slow')
				$('.contentDatosPersonales').hide('slow');
				$('.f2 .f1-step').removeClass('active'); // icono
				$('.f2 .f1-step').addClass('activated'); // icono
				$('.f2 .f1-step > p').removeClass('active'); // texto
				$('.f2 .f1-step > p').addClass('activated'); // texto

				$('.f2 .f1-step-min').addClass('active');
				$('.progress-line').css('width', '30%');
				setTimeout(function () {
					$('#fase2').removeClass('disabled')
					$('.f2 .f1-step-min').removeClass('active');
					$('.f2 .f1-step-min').addClass('activated');
					$('.progress-line').css('width', '35%');

					$('.f2 .f2-step').addClass('active'); // icono
					//$('.contentInformacionProyecto').show('slow');
					var formData = new FormData(document.forms.namedItem("datos-personales"));
					console.log("formData", formData);

					var parametro = {
						'formData': formData
					};
					$.ajax({
						data: formData,
						type: 'POST',
						url: '../Controllers/datos-personales.php',
						cache: false,
						contentType: false,
						processData: false,
						success: function (resultado) {
							var page = 'inscription-proyect.php?ac='+ac+'&registro='+registro+'&linea='+linea+'&form=informacion-proyecto';
							window.location = page;
						}
					});
				}, 1500);
			}else{
				if (sum > 3) {
					$('#messageAlert-datosPersonales-inscripcion').html(danger)
					$('#messageAlert-datosPersonales-inscripcion').show('slow')
				} else {
					$('#messageAlert-datosPersonales-inscripcion').html(warning)
					$('#messageAlert-datosPersonales-inscripcion').show('slow')
				}
			}
		}else {
			$('#alertaRegistroDenegado').modal('show');
			setTimeout(function () {
				$('#cerrarAcceso').click();
			}, 5000);
		}
	}
	if (val == 2) {
		var n = $('#accesoRegistro').val();
		if (n == 0) {
			var titulo = $('#titulo').val();
			var posconflicto = $('input:radio[name=posconflicto]:checked').val();
			var antecedentes = $('#antecedentesProyecto').val();
			var necesidad = $('#plantProNecesidades').val();
			var objGeneral = $('#objetivoGeneral').val();
			var objEspecifico = $('#objEspecifico').val();
			var fechaInicioPro = $('#fechaInicioPro').val();
			var fechaFinPro = $('#fechaFinPro').val();
			var grupoInvestigacion = $('#grupoInvestigacion').val();
			var grupoLac = $('#grupoLac').val();
			var s_Beneficiados = $('#semillerosBeneficiados').val();
			var descripMetodologia = $('#descripMetodologia').val();
			var form_Benefiadas = $('#formacionesBenefiadas').val();
			var impactoEsperado = $('#impactoEsperado').val();

			var sum = 0;
			if (titulo.length > 0) {
				$('#titulo').removeClass('input-danger');
			} else {
				sum += 1;
				$('#titulo').addClass('input-danger');
			}
			if (posconflicto == 1) {
				var municipios = $('#municipios').val();
				if (municipios.length > 0) {
					$('#municipios').removeClass('input-danger');
					for (var mun = 1; mun <= municipios; mun++) {
						var nom = $('#nombreMunicipio_' + mun).val();
						if (nom.length > 0) {
							$('#nombreMunicipio_' + mun).removeClass('input-danger');
						} else {
							sum += 1;
							$('#nombreMunicipio_' + mun).addClass('input-danger');
						}
					}
				} else {
					sum += 1;
					$('#municipios').addClass('input-danger');
				}
				var desEstrategia = $('#estrategiaPosConflicto').val();
				if (desEstrategia.length > 0) {
					$('#estrategiaPosConflicto').removeClass('input-danger');
				} else {
					sum += 1;
					$('#estrategiaPosConflicto').addClass('input-danger');
				}
				var reposconflicto = $('input:radio[name=optPosconflicto]:checked').val();
				console.log("reposconflicto", reposconflicto);
				if (reposconflicto == 1) {
					var recursos = $('#recursosPosconflicto').val();
					if (recursos.length > 0) {
						$('#recursosPosconflicto').removeClass('input-danger');
					} else {
						sum += 1;
						$('#recursosPosconflicto').addClass('input-danger');
					}
				}
			}
			if (antecedentes.length > 0) {
				$('#antecedentesProyecto').removeClass('input-danger');
			} else {
				sum += 1;
				$('#antecedentesProyecto').addClass('input-danger');
			}
			if (necesidad.length > 0) {
				$('#plantProNecesidades').removeClass('input-danger');
			} else {
				sum += 1;
				$('#plantProNecesidades').addClass('input-danger');
			}
			if (objGeneral.length > 0) {
				$('#objetivoGeneral').removeClass('input-danger');
			} else {
				sum += 1;
				$('#objetivoGeneral').addClass('input-danger');
			}
			if (objEspecifico.length > 0) {
				$('#objEspecifico').removeClass('input-danger');
			} else {
				sum += 1;
				$('#objEspecifico').addClass('input-danger');
			}
			for (var esp = 1; esp <= objEspecifico; esp++) {
				var objetivo = $('#objetivoEsp_' + esp).val();
				var resultado = $('#resultadoObjetivo_' + esp).val();
				var producto = $('#productoResultado_' + esp).val();
				if (objetivo.length > 0) {
					$('#objetivoEsp_' + esp).removeClass('input-danger');
				} else {
					sum += 1;
					$('#objetivoEsp_' + esp).addClass('input-danger');
				}
				if (resultado.length > 0) {
					$('#resultadoObjetivo_' + esp).removeClass('input-danger');
				} else {
					sum += 1;
					$('#resultadoObjetivo_' + esp).addClass('input-danger');
				}
				if (producto.length > 0) {
					$('#productoResultado_' + esp).removeClass('input-danger');
				} else {
					sum += 1;
					$('#productoResultado_' + esp).addClass('input-danger');
				}
			}
			if (fechaInicioPro.length > 0) {
				$('#fechaInicioPro').removeClass('input-danger');
			} else {
				sum += 1;
				$('#fechaInicioPro').addClass('input-danger');
			}
			if (fechaFinPro.length > 0) {
				$('#fechaFinPro').removeClass('input-danger');
			} else {
				sum += 1;
				$('#fechaFinPro').addClass('input-danger');
			}

			if (grupoInvestigacion.length > 0) {
				$('#grupoInvestigacion').removeClass('input-danger');
			} else {
				sum += 1;
				$('#grupoInvestigacion').addClass('input-danger');
			}
			if (grupoLac.length > 0) {
				$('#grupoLac').removeClass('input-danger');
			} else {
				sum += 1;
				$('#grupoLac').addClass('input-danger');
			}
			if (s_Beneficiados.length > 0) {
				$('#semillerosBeneficiados').removeClass('input-danger');
			} else {
				sum += 1;
				$('#semillerosBeneficiados').addClass('input-danger');
			}
			for (var semillero = 1; semillero <= s_Beneficiados; semillero++) {
				var nombre = $('#nombreSemillero_' + semillero).val();
				if (nombre.length > 0) {
					$('#nombreSemillero_' + semillero).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreSemillero_' + semillero).addClass('input-danger');
				}
			}
			if (descripMetodologia.length > 0) {
				$('#descripMetodologia').removeClass('input-danger');
			} else {
				sum += 1;
				$('#descripMetodologia').addClass('input-danger');
			}
			if (form_Benefiadas.length > 0) {
				$('#formacionesBenefiadas').removeClass('input-danger');
			} else {
				sum += 1;
				$('#formacionesBenefiadas').addClass('input-danger');
			}
			for (var form = 1; form <= form_Benefiadas; form++) {
				var nombre = $('#nombreFormacion_' + form).val();
				if (nombre.length > 0) {
					$('#nombreFormacion_' + form).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreFormacion_' + form).addClass('input-danger');
				}
			}
			if (impactoEsperado.length > 0) {
				$('#impactoEsperado').removeClass('input-danger');
			} else {
				sum += 1;
				$('#impactoEsperado').addClass('input-danger');
			}



			if (sum == 0) {
				$('#messageAlert-informacion_proyecto').html(success)
				$('#messageAlert-informacion_proyecto').show('slow')
				$('.contentInformacionProyecto').hide('slow');

				$('.f2 .f2-step').removeClass('active'); // icono
				$('.f2 .f2-step').addClass('activated'); // icono
				$('.f2 .f2-step > p').removeClass('active'); // texto
				$('.f2 .f2-step > p').addClass('activated'); // texto

				$('.f2 .f2-step-min').addClass('active');
				$('.progress-line').css('width', '60%');
				setTimeout(function () {
					$('#fase3').removeClass('disabled')
					$('.f2 .f2-step-min').removeClass('active');
					$('.f2 .f2-step-min').addClass('activated');
					$('.progress-line').css('width', '75%');

					$('.f2 .f3-step').addClass('active'); // icono
					//$('.contentRecursosHumanos').show('slow');

					var formData = new FormData(document.forms.namedItem("datos-proyecto"));

					var parametro = {
						'formData': formData
					};
					$.ajax({
						data: formData,
						type: 'POST',
						url: '../Controllers/datos-proyecto.php',
						cache: false,
						contentType: false,
						processData: false,
						success: function (resultado) {
							console.log("resultado", resultado);
							var page = 'inscription-proyect.php?ac='+ac+'&registro='+registro+'&linea='+linea+'&form=recursos-humanos';
							window.location = page;
						}
					});
				}, 1500);
			}else {
				if (sum > 3) {
					$('#messageAlert-informacion_proyecto').html(danger)
					$('#messageAlert-informacion_proyecto').show('slow')
				} else {
					$('#messageAlert-informacion_proyecto').html(warning)
					$('#messageAlert-informacion_proyecto').show('slow')
				}
			}
		}else {
			$('#alertaRegistroDenegado').modal('show');
			setTimeout(function () {
				$('#cerrarAcceso').click();
			}, 5000);
		}
	}
	if (val == 3) {
		var n = $('#accesoRegistro').val();
		console.log("n", n);
		if (n == 0) {
			var perslExt = $('input:radio[name=perslExt]:checked').val();
			var persIntASC = $('input:radio[name=persIntASC]:checked').val();
			var persIntACC = $('input:radio[name=persIntACC]:checked').val();
			var persIntInstructores = $('input:radio[name=persIntInstructores]:checked').val();
			var persIntOtros = $('input:radio[name=persIntOtros]:checked').val();
			var aliadosExt = $('input:radio[name=AliadosExt]:checked').val();
			var aliadosInternos = $('input:radio[name=AliadosInternos]:checked').val();
			var ciudadesInfluencia = $('input:radio[name=ciudadesInfluencia]:checked').val();
			var municipiosInfluencia = $('input:radio[name=municipiosInfluencia]:checked').val();
			var equipoSistemas = $('input:radio[name=equipoSistemas]:checked').val();
			var software = $('input:radio[name=software]:checked').val();
			var maquinariaInd = $('input:radio[name=maquinariaInd]:checked').val();
			var OtrosEquipos = $('input:radio[name=OtrosEquipos]:checked').val();
			var matFormProfesional = $('input:radio[name=matFormProfesional]:checked').val();
			var mantenimientoMMETS = $('input:radio[name=mantenimientoMMETS]:checked').val();
			var comunicTransporte = $('input:radio[name=comunicTransporte]:checked').val();
			var adecuacionesContrucciones = $('input:radio[name=adecuacionesContrucciones]:checked').val();

			var sum = 0;
			if (perslExt == 1) {
				var perExt = $('#numPersonalExt').val();
				if (perExt.length > 0) {
					$('#numPersonalExt').removeClass('input-danger');
					for (var personas = 1; personas <= perExt; personas++) {
						var nombre = $('#nombrePersExt_' + personas).val();
						var apell = $('#apellidosPersExt_' + personas).val();
						var identi = $('#identificacionPersExt_' + personas).val();
						if (nombre.length > 0) {
							$('#nombrePersExt_' + personas).removeClass('input-danger');
						} else {
							$('#nombrePersExt_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (apell.length > 0) {
							$('#apellidosPersExt_' + personas).removeClass('input-danger');
						} else {
							$('#apellidosPersExt_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (identi.length > 0) {
							$('#identificacionPersExt_' + personas).removeClass('input-danger');
						} else {
							$('#identificacionPersExt_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numPersonalExt').addClass('input-danger');
				}
			}
			if (persIntASC == 1) {
				var perInASC = $('#numPersonalIntASC').val();
				if (perInASC.length > 0) {
					$('#numPersonalIntASC').removeClass('input-danger');
					for (var personas = 1; personas <= perInASC; personas++) {
						var nombre = $('#nombrePersIntASC_' + personas).val();
						var apell = $('#apellidosPersIntASC_' + personas).val();
						var identi = $('#identificacionPersIntASC_' + personas).val();
						if (nombre.length > 0) {
							$('#nombrePersIntASC_' + personas).removeClass('input-danger');
						} else {
							$('#nombrePersIntASC_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (apell.length > 0) {
							$('#apellidosPersIntASC_' + personas).removeClass('input-danger');
						} else {
							$('#apellidosPersIntASC_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (identi.length > 0) {
							$('#identificacionPersIntASC_' + personas).removeClass('input-danger');
						} else {
							$('#identificacionPersIntASC_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numPersonalIntASC').addClass('input-danger');
				}
			}
			if (persIntACC == 1) {
				var perInACC = $('#numPersonalIntACC').val();
				if (perInACC.length > 0) {
					$('#numPersonalIntACC').removeClass('input-danger');
					for (var personas = 1; personas <= perInACC; personas++) {
						var nombre = $('#nombrePersIntACC_' + personas).val();
						var apell = $('#apellidosPersIntACC_' + personas).val();
						var identi = $('#identificacionPersIntACC_' + personas).val();
						if (nombre.length > 0) {
							$('#nombrePersIntACC_' + personas).removeClass('input-danger');
						} else {
							$('#nombrePersIntACC_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (apell.length > 0) {
							$('#apellidosPersIntACC_' + personas).removeClass('input-danger');
						} else {
							$('#apellidosPersIntACC_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (identi.length > 0) {
							$('#identificacionPersIntACC_' + personas).removeClass('input-danger');
						} else {
							$('#identificacionPersIntACC_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numPersonalIntACC').addClass('input-danger');
				}
			}
			if (persIntInstructores == 1) {
				var perInIns = $('#numPersonalIntInstructores').val();
				if (perInIns.length > 0) {
					$('#numPersonalIntInstructores').removeClass('input-danger');
					for (var personas = 1; personas <= perInIns; personas++) {
						var nombre = $('#nombrePersIntInstructores_' + personas).val();
						var apell = $('#apellidosPersIntInstructores_' + personas).val();
						var identi = $('#identificacionPersIntInstructores_' + personas).val();
						if (nombre.length > 0) {
							$('#nombrePersIntInstructores_' + personas).removeClass('input-danger');
						} else {
							$('#nombrePersIntInstructores_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (apell.length > 0) {
							$('#apellidosPersIntInstructores_' + personas).removeClass('input-danger');
						} else {
							$('#apellidosPersIntInstructores_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (identi.length > 0) {
							$('#identificacionPersIntInstructores_' + personas).removeClass('input-danger');
						} else {
							$('#identificacionPersIntInstructores_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numPersonalIntInstructores').addClass('input-danger');
				}
			}
			if (persIntOtros == 1) {
				var perInOtros = $('#numPersonalIntOtros').val();
				if (perInOtros.length > 0) {
					$('#numPersonalIntOtros').removeClass('input-danger');
					for (var personas = 1; personas <= perInOtros; personas++) {
						var nombre = $('#nombrePersIntOtros_' + personas).val();
						var apell = $('#apellidosPersIntOtros_' + personas).val();
						var identi = $('#identificacionPersIntOtros_' + personas).val();
						if (nombre.length > 0) {
							$('#nombrePersIntOtros_' + personas).removeClass('input-danger');
						} else {
							$('#nombrePersIntOtros_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (apell.length > 0) {
							$('#apellidosPersIntOtros_' + personas).removeClass('input-danger');
						} else {
							$('#apellidosPersIntOtros_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (identi.length > 0) {
							$('#identificacionPersIntOtros_' + personas).removeClass('input-danger');
						} else {
							$('#identificacionPersIntOtros_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numPersonalIntOtros').addClass('input-danger');
				}
			}
			if (aliadosExt == 1) {
				var aliExt = $('#numAliadosExternos').val();
				if (aliExt.length > 0) {
					$('#numAliadosExternos').removeClass('input-danger');
					for (var personas = 1; personas <= aliExt; personas++) {
						var nombre = $('#nombreAliadoExt_' + personas).val();
						var nit = $('#nitAliadoExt_' + personas).val();
						var recursos = $('#recursoAliadoExt_' + personas).val();
						if (nombre.length > 0) {
							$('#nombreAliadoExt_' + personas).removeClass('input-danger');
						} else {
							$('#nombreAliadoExt_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (nit.length > 0) {
							$('#nitAliadoExt_' + personas).removeClass('input-danger');
						} else {
							$('#nitAliadoExt_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (recursos.length > 0) {
							$('#recursoAliadoExt_' + personas).removeClass('input-danger');
						} else {
							$('#recursoAliadoExt_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numAliadosExternos').addClass('input-danger');
				}
			}
			if (aliadosInternos == 1) {
				var aliInt = $('#numAliadosInternos').val();
				if (aliInt.length > 0) {
					$('#numAliadosInternos').removeClass('input-danger');
					for (var personas = 1; personas <= aliInt; personas++) {
						var nombre = $('#nombreAliadoInterno_' + personas).val();
						var nit = $('#nitAliadoInterno_' + personas).val();
						var recurso = $('#recursoAliadoInterno_' + personas).val();
						if (nombre.length > 0) {
							$('#nombreAliadoInterno_' + personas).removeClass('input-danger');
						} else {
							$('#nombreAliadoInterno_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (nit.length > 0) {
							$('#nitAliadoInterno_' + personas).removeClass('input-danger');
						} else {
							$('#nitAliadoInterno_' + personas).addClass('input-danger');
							sum += 1;
						}
						if (recurso.length > 0) {
							$('#recursoAliadoInterno_' + personas).removeClass('input-danger');
						} else {
							$('#recursoAliadoInterno_' + personas).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numAliadosInternos').addClass('input-danger');
				}
			}
			if (ciudadesInfluencia == 1) {
				var ciudad = $('#numCiudadesInfluencia').val();
				if (ciudad.length > 0) {
					$('#numCiudadesInfluencia').removeClass('input-danger');
					for (var c = 1; c <= ciudad; c++) {
						var nombre = $('#nombreCiudadInfluencia_' + c).val();
						if (nombre.length > 0) {
							$('#nombreCiudadInfluencia_' + c).removeClass('input-danger');
						} else {
							$('#nombreCiudadInfluencia_' + c).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numCiudadesInfluencia').addClass('input-danger');
				}
			}
			if (municipiosInfluencia == 1) {
				var municipio = $('#numMunicipiosInfluencia').val();
				if (municipio.length > 0) {
					$('#numMunicipiosInfluencia').removeClass('input-danger');
					for (var m = 1; m <= municipio; m++) {
						var nombre = $('#nombreMunicipioInfluencia_' + m).val();
						if (nombre.length > 0) {
							$('#nombreMunicipioInfluencia_' + m).removeClass('input-danger');
						} else {
							$('#nombreMunicipioInfluencia_' + m).addClass('input-danger');
							sum += 1;
						}
					}
				} else {
					sum += 1;
					$('#numMunicipiosInfluencia').addClass('input-danger');
				}
			}
			if (equipoSistemas == 1) {
				var valor = $('#equipoSistemas').val();
				var desc = $('#descripcionSistemas').val();
				if (valor.length > 0) {
					$('#equipoSistemas').removeClass('input-danger');
				} else {
					sum += 1;
					$('#equipoSistemas').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionSistemas').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionSistemas').addClass('input-danger');
				}
			}
			if (software == 1) {
				var valor = $('#valorSoftware').val();
				var desc = $('#descripcionSoftware').val();
				if (valor.length > 0) {
					$('#valorSoftware').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valorSoftware').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionSoftware').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionSoftware').addClass('input-danger');
				}
			}
			if (maquinariaInd == 1) {
				var valor = $('#valormaquinariaInd').val();
				var desc = $('#descripcionmaquinariaInd').val();
				if (valor.length > 0) {
					$('#valormaquinariaInd').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valormaquinariaInd').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionmaquinariaInd').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionmaquinariaInd').addClass('input-danger');
				}
			}
			if (OtrosEquipos == 1) {
				var valor = $('#valorOtrosEquipos').val();
				var desc = $('#descripcionOtrosEquipos').val();
				if (valor.length > 0) {
					$('#valorOtrosEquipos').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valorOtrosEquipos').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionOtrosEquipos').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionOtrosEquipos').addClass('input-danger');
				}
			}
			if (matFormProfesional == 1) {
				var valor = $('#valormatFormProfesional').val();
				var desc = $('#descripcionmatFormProfesional').val();
				if (valor.length > 0) {
					$('#valormatFormProfesional').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valormatFormProfesional').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionmatFormProfesional').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionmatFormProfesional').addClass('input-danger');
				}
			}
			if (mantenimientoMMETS == 1) {
				var valor = $('#valormantenimientoMMETS').val();
				var desc = $('#descripcionmantenimientoMMETS').val();
				if (valor.length > 0) {
					$('#valormantenimientoMMETS').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valormantenimientoMMETS').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionmantenimientoMMETS').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionmantenimientoMMETS').addClass('input-danger');
				}
			}
			if (comunicTransporte == 1) {
				var valor = $('#valorcomunicTransporte').val();
				var desc = $('#descripcioncomunicTransporte').val();
				if (valor.length > 0) {
					$('#valorcomunicTransporte').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valorcomunicTransporte').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcioncomunicTransporte').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcioncomunicTransporte').addClass('input-danger');
				}
			}
			if (adecuacionesContrucciones == 1) {
				var valor = $('#valoradecuacionesContrucciones').val();
				var desc = $('#descripcionadecuacionesContrucciones').val();
				if (valor.length > 0) {
					$('#valoradecuacionesContrucciones').removeClass('input-danger');
				} else {
					sum += 1;
					$('#valoradecuacionesContrucciones').addClass('input-danger');
				}
				if (desc.length > 0) {
					$('#descripcionadecuacionesContrucciones').removeClass('input-danger');
				} else {
					sum += 1;
					$('#descripcionadecuacionesContrucciones').addClass('input-danger');
				}
			}
			if (sum == 0) {
				$('#messageAlert-recursosHumanos').html(success)
				$('#messageAlert-recursosHumanos').show('slow')

				$('.progress-line').css('width', '90%');
				setTimeout(function () {
					$('.progress-line').css('width', '100%');

					$('.f2 .f3-step').addClass('activated'); // icono

					var formData = new FormData(document.forms.namedItem("recursos-humanos"));
					console.log("formData", formData);

					var parametro = {
						'formData': formData
					};
					$.ajax({
						data: formData,
						type: 'POST',
						url: '../Controllers/recursos-humanos.php',
						cache: false,
						contentType: false,
						processData: false,
						success: function (resultado) {
							console.log("resultado", resultado);
							$('.contentRecursosHumanos').html('<h1>Felicidades.! tu proyecto ha sido inscripto</h1>');

							setTimeout(function () {
								var page = 'destruirRegistro.php';

							   window.location = page;
							}, 1700);
						}
					});

				}, 1500);
			}else {
				if (sum > 3) {
					$('#messageAlert-recursosHumanos').html(danger)
					$('#messageAlert-recursosHumanos').show('slow')
				} else {
					$('#messageAlert-recursosHumanos').html(warning)
					$('#messageAlert-recursosHumanos').show('slow')
				}
			}
		}else {
			$('#alertaRegistroDenegado').modal('show');
			setTimeout(function () {
				$('#cerrarAcceso').click();
			}, 5000);
		}
	}
	if (val == 4 ) {
		var n = $('#accesoRegistro').val();
		console.log("n", n);
		if (n == 0) {
			var regional       = $('#regional').val();
			var formacion      = $('#formacion').val();
			var ficha          = $('#ficha').val();
			var nombre         = $('#nombre').val();
			var apellido       = $('#apellido').val();
			var identificacion = $('#identificacion').val();
			var email          = $('#email').val();
			var telefono       = $('#telefono').val();

			var idea             = $('#idea').val();
			var planteamiento    = $('#planteamiento').val();
			var sectorProductivo = $('#sectorProductivo').val();
			var municipios       = $('#municipios').val();
			var acompannantes    = $('#acompannantes').val();
			var valor            = $('#valor-idea').val();
			var presupuesto       = $('#presupuesto').val();
			var link             = $('#link-video').val();
		
			var sum = 0;


			if (idea.length>0) {
				$('#idea').removeClass('input-danger');
			}else{
				sum+=1;
				$('#idea').addClass('input-danger');
			}
			if (planteamiento.length>0) {
				$('#planteamiento').removeClass('input-danger');
			}else{
				sum+=1;
				$('#planteamiento').addClass('input-danger');
			}
			if (sectorProductivo.length>0) {
				$('#sectorProductivo').removeClass('input-danger');
			}else{
				sum+=1;
				$('#sectorProductivo').addClass('input-danger');
			}
			if (municipios.length>0) {
				$('#municipios').removeClass('input-danger');
				for (var mun = 1; mun <= municipios; mun++) {
					var nom = $('#nombreMunicipio_' + mun).val();
					if (nom.length > 0) {
						$('#nombreMunicipio_' + mun).removeClass('input-danger');
					} else {
						sum += 1;
						$('#nombreMunicipio_' + mun).addClass('input-danger');
					}
				}
			}else{
				sum+=1;
				$('#municipios').addClass('input-danger');
			}
			if (acompannantes.length>0) {
				$('#acompannantes').removeClass('input-danger');
			}else{
				sum+=1;
				$('#acompannantes').addClass('input-danger');
			}
			if (valor.length>0) {
				$('#valor-idea').removeClass('input-danger');
			}else{
				sum+=1;
				$('#valor-idea').addClass('input-danger');
			}
			if (link.length>0) {
				$('#link-video').removeClass('input-danger');
			}else{
				sum+=1;
				$('#link-video').addClass('input-danger');
			}

			
			if (regional.length>0) {
				$('#regional').removeClass('input-danger');
			}else{
				sum+=1;
				$('#regional').addClass('input-danger');
			}
			if (formacion.length>0) {
				$('#formacion').removeClass('input-danger');
			}else{
				sum+=1;
				$('#formacion').addClass('input-danger');
			}
			if (ficha.length>0) {
				$('#ficha').removeClass('input-danger');
			}else{
				sum+=1;
				$('#ficha').addClass('input-danger');
			}
			if (nombre.length>0) {
				$('#nombre').removeClass('input-danger');
			}else{
				sum+=1;
				$('#nombre').addClass('input-danger');
			}
			if (apellido.length>0) {
				$('#apellido').removeClass('input-danger');
			}else{
				sum+=1;
				$('#apellido').addClass('input-danger');
			}
			if (identificacion.length>0) {
				$('#identificacion').removeClass('input-danger');
			}else{
				sum+=1;
				$('#identificacion').addClass('input-danger');
			}
			if (email.length>0) {
				$('#email').removeClass('input-danger');
			}else{
				sum+=1;
				$('#email').addClass('input-danger');
			}
			if (telefono.length>0) {
				$('#telefono').removeClass('input-danger');
			}else{
				sum+=1;
				$('#telefono').addClass('input-danger');
			}
			
			console.log("sum", sum);
			if (sum == 0) {
					console.log("presupuesto", presupuesto);
					console.log("valor", valor);
				//if (valor>presupuesto) {
					//$('#messageAlert-fase_1-inscripcion').html(
					//`<div class="alert alert-warning" role="alert">
						//<button type="button" class="close" data-dismiss="alert">×</button>
						//<strong>Uffss! </strong>
						//Presupuesto superado.!
					//</div>`);
				//}else{
					$('#messageAlert-fase_1-inscripcion').html(success)
					$('#messageAlert-fase_1-inscripcion').show('slow')
					setTimeout(function(){
						//$('.contentDatos_fase_1').hide('slow');
					},3000);

					$('.f1 .f1-step').removeClass('active'); // icono
					$('.f1 .f1-step').addClass('activated'); // icono
					$('.f1 .f1-step > p').removeClass('active'); // texto
					$('.f1 .f1-step > p').addClass('activated'); // texto

					$('.f1 .f1-step-min').addClass('active');
					$('.progress-line').css('width','22%');

					var formData = new FormData(document.forms.namedItem("datos-fase_1"));
					console.log("formData", formData);

					var parametro = {
						'formData': formData
					};
					$.ajax({
						data: formData,
						type: 'POST',
						url: '../Controllers/datos-fase_1.php',
						cache: false,
						contentType: false,
						processData: false,
						success: function (resultado) {
							console.log("resultado", resultado);


							$('.contentDatos_fase_1').html(resultado );

							setTimeout(function () {
								var page = 'destruirRegistro.php';
								updateReloj();
							}, 1700);
						}
					});
				//}
			}else{
				if (sum>3) {
					$('#messageAlert-fase_1-inscripcion').html(danger)
					$('#messageAlert-fase_1-inscripcion').show('slow')
				}else{
					$('#messageAlert-fase_1-inscripcion').html(warning)
					$('#messageAlert-fase_1-inscripcion').show('slow')
				}        
			}
		}else {
			$('#alertaRegistroDenegado').modal('show');
			setTimeout(function () {
				$('#cerrarAcceso').click();
			}, 5000);
		}
	}
	if (val == 5 ) {
		var n = $('#accesoRegistro').val();
		console.log("n", n);
		if (n == 0) {
			var idea            = $('#idea').val();
			var planteamiento   = $('#planteamiento').val();
			var objetivoGeneral = $('#objetivoGeneral').val();
			var objEspecifico   = $('#objEspecifico').val();
			var justificacion   = $('#justificacion').val();
			var bibliografia    = $('#bibliografia').val();
			var tiempoEjecucion = $('#tiempoEjecucion').val();
			var valorPropuesta  = $('#valorPropuesta').val();

			

			var sum = 0;
			if (idea.length>0) {
				$('#idea').removeClass('input-danger');
			}else{
				sum+=1;
				$('#idea').addClass('input-danger');
			}
			if (planteamiento.length>0) {
				$('#planteamiento').removeClass('input-danger');
			}else{
				sum+=1;
				$('#planteamiento').addClass('input-danger');
			}
			if (objetivoGeneral.length>0) {
				$('#objetivoGeneral').removeClass('input-danger');
			}else{
				sum+=1;
				$('#objetivoGeneral').addClass('input-danger');
			}
			if (objEspecifico.length>0) {
				$('#objEspecifico').removeClass('input-danger');
			}else{
				sum+=1;
				$('#objEspecifico').addClass('input-danger');
			}
				for (var esp = 1; esp <= objEspecifico; esp++) {
					var objetivo = $('#objetivoEsp_' + esp).val();
					if (objetivo.length > 0) {
						$('#objetivoEsp_' + esp).removeClass('input-danger');
					} else {
						sum += 1;
						$('#objetivoEsp_' + esp).addClass('input-danger');
					}
				}
			if (justificacion.length>0) {
				$('#justificacion').removeClass('input-danger');
			}else{
				sum+=1;
				$('#justificacion').addClass('input-danger');
			}
			if (bibliografia.length>0) {
				$('#bibliografia').removeClass('input-danger');
			}else{
				sum+=1;
				$('#bibliografia').addClass('input-danger');
			}
			
			if (valorPropuesta.length>0) {
				$('#valorPropuesta').removeClass('input-danger');
			}else{
				sum+=1;
				$('#valorPropuesta').addClass('input-danger');
			}
			if (sum == 0) {
				$('#messageAlert-fase_2-inscripcion').html(success)
				$('#messageAlert-fase_2-inscripcion').show('slow')
				setTimeout(function(){
					//$('.contentDatos_fase_1').hide('slow');
				},3000);

				$('.f1 .f1-step').removeClass('active'); // icono
				$('.f1 .f1-step').addClass('activated'); // icono
				$('.f1 .f1-step > p').removeClass('active'); // texto
				$('.f1 .f1-step > p').addClass('activated'); // texto

				$('.f1 .f1-step-min').addClass('active');
				$('.progress-line').css('width','22%');

				var formData = new FormData(document.forms.namedItem("datos-fase_2"));
				console.log("formData", formData);

				var parametro = {
					'formData': formData
				};
				$.ajax({
					data: formData,
					type: 'POST',
					url: '../Controllers/datos-fase_2.php',
					cache: false,
					contentType: false,
					processData: false,
					success: function (resultado) {
						//$('.contentDatos_fase_2').html(resultado );
						setTimeout(function () {
							location.reload(true);
						}, 1700);
					}
				});
			}else{
				if (sum>3) {
					$('#messageAlert-fase_2-inscripcion').html(danger)
					$('#messageAlert-fase_2-inscripcion').show('slow')
				}else{
					$('#messageAlert-fase_2-inscripcion').html(warning)
					$('#messageAlert-fase_2-inscripcion').show('slow')
				}        
			}
		}else {
			if (n == 1) {
				$('#alertaRegistroDenegado').modal('show');
				$('.fase_estado').html('primera');
				setTimeout(function () {
					$('#cerrarAcceso').click();
				}, 7000);
			}else{
				$('#alertaRegistroDenegado').modal('show');
				$('.fase_estado').html('primera');
				setTimeout(function () {
					$('#cerrarAcceso').click();
				}, 7000);
			}
		}
	}
	if (val == 6 ) {
		var n = $('#accesoRegistro').val();
		console.log("n", n);
		if (n == 0) {
			var titulo = $('#titulo').val();
			var planteamiento = $('#planteamiento').val();
			var sectorProImpacto = $('#sectorProImpacto').val();
			var s_Beneficiados = $('#semillerosBeneficiados').val();
			var form_Benefiadas = $('#formacionesBenefiadas').val();
			var impactoEsperado = $('#impactoEsperado').val();
			var objGeneral = $('#objetivoGeneral').val();
			var objEspecifico = $('#objEspecifico').val();
			var justificacion = $('#justificacion').val();
			var basesTeoricas = $('#basesTeoricas').val();
			var marcoNormativo = $('#marcoNormativo').val();
			var bibliografia = $('#bibliografia').val();

			var antecedentesProyecto = $('#antecedentesProyecto').val();
			var sum = 0;
			if (antecedentesProyecto.length > 0) {
				$('#antecedentesProyecto').removeClass('input-danger');
			} else {
				sum += 1;
				$('#antecedentesProyecto').addClass('input-danger');
			}
			if (basesTeoricas.length > 0) {
				$('#basesTeoricas').removeClass('input-danger');
			} else {
				sum += 1;
				$('#basesTeoricas').addClass('input-danger');
			}
			if (marcoNormativo.length > 0) {
				$('#marcoNormativo').removeClass('input-danger');
			} else {
				sum += 1;
				$('#marcoNormativo').addClass('input-danger');
			}

			var municipios = $('#municipios').val();
			if (municipios.length > 0) {
				$('#municipios').removeClass('input-danger');
				for (var mun = 1; mun <= municipios; mun++) {
					var nom = $('#nombreMunicipio_' + mun).val();
					if (nom.length > 0) {
						$('#nombreMunicipio_' + mun).removeClass('input-danger');
					} else {
						sum += 1;
						$('#nombreMunicipio_' + mun).addClass('input-danger');
					}
				}
			} else {
				sum += 1;
				$('#municipios').addClass('input-danger');
			}


			if (impactoEsperado.length > 0) {
				$('#impactoEsperado').removeClass('input-danger');
			} else {
				sum += 1;
				$('#impactoEsperado').addClass('input-danger');
			}
			if (s_Beneficiados.length > 0) {
				$('#semillerosBeneficiados').removeClass('input-danger');
			} else {
				sum += 1;
				$('#semillerosBeneficiados').addClass('input-danger');
			}
			for (var semillero = 1; semillero <= s_Beneficiados; semillero++) {
				var nombre = $('#nombreSemillero_' + semillero).val();
				if (nombre.length > 0) {
					$('#nombreSemillero_' + semillero).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreSemillero_' + semillero).addClass('input-danger');
				}
			}
			if (form_Benefiadas.length > 0) {
				$('#formacionesBenefiadas').removeClass('input-danger');
			} else {
				sum += 1;
				$('#formacionesBenefiadas').addClass('input-danger');
			}
			for (var form = 1; form <= form_Benefiadas; form++) {
				var nombre = $('#nombreFormacion_' + form).val();
				if (nombre.length > 0) {
					$('#nombreFormacion_' + form).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreFormacion_' + form).addClass('input-danger');
				}
			}
			if (titulo.length > 0) {
				$('#titulo').removeClass('input-danger');
			}else {
				sum += 1;
				$('#titulo').addClass('input-danger');
			}
			if (planteamiento.length > 0) {
				$('#planteamiento').removeClass('input-danger');
			}else {
				sum += 1;
				$('#planteamiento').addClass('input-danger');
			}
			if (sectorProImpacto.length > 0) {
				$('#sectorProImpacto').removeClass('input-danger');
			}else {
				sum += 1;
				$('#sectorProImpacto').addClass('input-danger');
			}
			if (objGeneral.length > 0) {
				$('#objetivoGeneral').removeClass('input-danger');
			}else {
				sum += 1;
				$('#objetivoGeneral').addClass('input-danger');
			}
			if (objEspecifico.length > 0) {
				$('#objEspecifico').removeClass('input-danger');
			}else {
				sum += 1;
				$('#objEspecifico').addClass('input-danger');
			}
				for (var esp = 1; esp <= objEspecifico; esp++) {
					var objetivo = $('#objetivoEsp_' + esp).val();
					if (objetivo.length > 0) {
						$('#objetivoEsp_' + esp).removeClass('input-danger');
					} else {
						sum += 1;
						$('#objetivoEsp_' + esp).addClass('input-danger');
					}
				}


			var posconflicto = $('input:radio[name=posconflicto]:checked').val();
			if (posconflicto == 1) {
				var desEstrategia = $('#estrategiaPosConflicto').val();
				if (desEstrategia.length > 0) {
					$('#estrategiaPosConflicto').removeClass('input-danger');
				} else {
					sum += 1;
					$('#estrategiaPosConflicto').addClass('input-danger');
				}
				var reposconflicto = $('input:radio[name=optPosconflicto]:checked').val();
				console.log("reposconflicto", reposconflicto);
				if (reposconflicto == 1) {
					var recursos = $('#recursosPosconflicto').val();
					if (recursos.length > 0) {
						$('#recursosPosconflicto').removeClass('input-danger');
					} else {
						sum += 1;
						$('#recursosPosconflicto').addClass('input-danger');
					}
				}
			}

			console.log("sum", sum);
			if (sum == 0) {
				$('#messageAlert-fase_3-inscripcion').html(success)
				$('#messageAlert-fase_3-inscripcion').show('slow')
				setTimeout(function(){
					//$('.contentDatos_fase_1').hide('slow');
				},3000);

				$('.f1 .f1-step').removeClass('active'); // icono
				$('.f1 .f1-step').addClass('activated'); // icono
				$('.f1 .f1-step > p').removeClass('active'); // texto
				$('.f1 .f1-step > p').addClass('activated'); // texto

				$('.f1 .f1-step-min').addClass('active');
				$('.progress-line').css('width','22%');

				var formData = new FormData(document.forms.namedItem("datos-fase_3"));
					console.log("formData", formData);

					var parametro = {
						'formData': formData
					};
					$.ajax({
						data: formData,
						type: 'POST',
						url: '../Controllers/datos-fase_3.php',
						cache: false,
						contentType: false,
						processData: false,
						success: function (resultado) {
							console.log("resultado", resultado);

							setTimeout(function () {
							  location.reload(true);  
							}, 1700);
						}
					});
			}else{
				if (sum>3) {
					$('#messageAlert-fase_3-inscripcion').html(danger)
					$('#messageAlert-fase_3-inscripcion').show('slow')
				}else{
					$('#messageAlert-fase_3-inscripcion').html(warning)
					$('#messageAlert-fase_3-inscripcion').show('slow')
				}        
			}
		}else {
			if (n == 2) {
				$('#alertaRegistroDenegado').modal('show');
				$('.fase_estado').html('segunda');
				setTimeout(function () {
					$('#cerrarAcceso').click();
				}, 7000);
			}else{
				$('#alertaRegistroDenegado').modal('show');
				$('.fase_estado').html('segunda');
				setTimeout(function () {
					$('#cerrarAcceso').click();
				}, 7000);
			}
		}
	}
	if (val == 7 ) {
		var n = $('#accesoRegistro').val();
		console.log("n", n);
		if (n == 0) {
			var titulo = $('#titulo').val();
			var introduccion = $('#introduccion').val();
			var planteamiento = $('#planteamiento').val();
			var sectorProImpacto = $('#sectorProImpacto').val();
			var s_Beneficiados = $('#semillerosBeneficiados').val();
			var form_Benefiadas = $('#formacionesBenefiadas').val();
			var impactoEsperado = $('#impactoEsperado').val();
			var objGeneral = $('#objetivoGeneral').val();
			var objEspecifico = $('#objEspecifico').val();
			var justificacion = $('#justificacion').val();
			var basesTeoricas = $('#basesTeoricas').val();
			var marcoNormativo = $('#marcoNormativo').val();
			var bibliografia = $('#bibliografia').val();
			var sum = 0;
			var tipoInvestigacion = $('#tipoInvestigacion').val();
			if (tipoInvestigacion.length > 0) {
				$('#tipoInvestigacion').removeClass('input-danger');
			} else {
				sum += 1;
				$('#tipoInvestigacion').addClass('input-danger');
			}
			var disennoMetodoInvestigacion = $('#disennoMetodoInvestigacion').val();
			if (disennoMetodoInvestigacion.length > 0) {
				$('#disennoMetodoInvestigacion').removeClass('input-danger');
			} else {
				sum += 1;
				$('#disennoMetodoInvestigacion').addClass('input-danger');
			}
			var universo = $('#universo').val();
			if (universo.length > 0) {
				$('#universo').removeClass('input-danger');
			} else {
				sum += 1;
				$('#universo').addClass('input-danger');
			}
			var tecnicaMuestra = $('#tecnicaMuestra').val();
			if (tecnicaMuestra.length > 0) {
				$('#tecnicaMuestra').removeClass('input-danger');
			} else {
				sum += 1;
				$('#tecnicaMuestra').addClass('input-danger');
			}
			var procesamientoAnalisis = $('#procesamientoAnalisis').val();
			if (procesamientoAnalisis.length > 0) {
				$('#procesamientoAnalisis').removeClass('input-danger');
			} else {
				sum += 1;
				$('#procesamientoAnalisis').addClass('input-danger');
			}

			var antecedentesProyecto = $('#antecedentesProyecto').val();
			
			if (antecedentesProyecto.length > 0) {
				$('#antecedentesProyecto').removeClass('input-danger');
			} else {
				sum += 1;
				$('#antecedentesProyecto').addClass('input-danger');
			}
			if (basesTeoricas.length > 0) {
				$('#basesTeoricas').removeClass('input-danger');
			} else {
				sum += 1;
				$('#basesTeoricas').addClass('input-danger');
			}
			if (marcoNormativo.length > 0) {
				$('#marcoNormativo').removeClass('input-danger');
			} else {
				sum += 1;
				$('#marcoNormativo').addClass('input-danger');
			}

			var municipios = $('#municipios').val();
			console.log("municipios", municipios);
			if (municipios.length > 0) {
				$('#municipios').removeClass('input-danger');
				for (var mun = 1; mun <= municipios; mun++) {
					var nom = $('#nombreMunicipio_' + mun).val();
					if (nom.length > 0) {
						$('#nombreMunicipio_' + mun).removeClass('input-danger');
					} else {
						sum += 1;
						$('#nombreMunicipio_' + mun).addClass('input-danger');
					}
				}
			} else {
				sum += 1;
				$('#municipios').addClass('input-danger');
			}


			if (impactoEsperado.length > 0) {
				$('#impactoEsperado').removeClass('input-danger');
			} else {
				sum += 1;
				$('#impactoEsperado').addClass('input-danger');
			}
			if (s_Beneficiados.length > 0) {
				$('#semillerosBeneficiados').removeClass('input-danger');
			} else {
				sum += 1;
				$('#semillerosBeneficiados').addClass('input-danger');
			}
			for (var semillero = 1; semillero <= s_Beneficiados; semillero++) {
				var nombre = $('#nombreSemillero_' + semillero).val();
				if (nombre.length > 0) {
					$('#nombreSemillero_' + semillero).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreSemillero_' + semillero).addClass('input-danger');
				}
			}
			if (form_Benefiadas.length > 0) {
				$('#formacionesBenefiadas').removeClass('input-danger');
			} else {
				sum += 1;
				$('#formacionesBenefiadas').addClass('input-danger');
			}
			for (var form = 1; form <= form_Benefiadas; form++) {
				var nombre = $('#nombreFormacion_' + form).val();
				if (nombre.length > 0) {
					$('#nombreFormacion_' + form).removeClass('input-danger');
				} else {
					sum += 1;
					$('#nombreFormacion_' + form).addClass('input-danger');
				}
			}
			if (titulo.length > 0) {
				$('#titulo').removeClass('input-danger');
			}else {
				sum += 1;
				$('#titulo').addClass('input-danger');
			}
			if (introduccion.length > 0) {
				$('#introduccion').removeClass('input-danger');
			}else {
				sum += 1;
				$('#introduccion').addClass('input-danger');
			}
			if (planteamiento.length > 0) {
				$('#planteamiento').removeClass('input-danger');
			}else {
				sum += 1;
				$('#planteamiento').addClass('input-danger');
			}
			if (sectorProImpacto.length > 0) {
				$('#sectorProImpacto').removeClass('input-danger');
			}else {
				sum += 1;
				$('#sectorProImpacto').addClass('input-danger');
			}
			if (objGeneral.length > 0) {
				$('#objetivoGeneral').removeClass('input-danger');
			}else {
				sum += 1;
				$('#objetivoGeneral').addClass('input-danger');
			}
			if (objEspecifico.length > 0) {
				$('#objEspecifico').removeClass('input-danger');
			}else {
				sum += 1;
				$('#objEspecifico').addClass('input-danger');
			}
			for (var esp = 1; esp <= objEspecifico; esp++) {
				var objetivo = $('#objetivoEsp_' + esp).val();
				var resultado = $('#resultadoObjetivo_' + esp).val();
				var producto = $('#productoResultado_' + esp).val();
				if (objetivo.length > 0) {
					$('#objetivoEsp_' + esp).removeClass('input-danger');
				} else {
					sum += 1;
					$('#objetivoEsp_' + esp).addClass('input-danger');
				}
				if (resultado.length > 0) {
					$('#resultadoObjetivo_' + esp).removeClass('input-danger');
				} else {
					sum += 1;
					$('#resultadoObjetivo_' + esp).addClass('input-danger');
				}
				if (producto.length > 0) {
					$('#productoResultado_' + esp).removeClass('input-danger');
				} else {
					sum += 1;
					$('#productoResultado_' + esp).addClass('input-danger');
				}
			}


			var posconflicto = $('input:radio[name=posconflicto]:checked').val();
			if (posconflicto == 1) {
				var desEstrategia = $('#estrategiaPosConflicto').val();
				if (desEstrategia.length > 0) {
					$('#estrategiaPosConflicto').removeClass('input-danger');
				} else {
					sum += 1;
					$('#estrategiaPosConflicto').addClass('input-danger');
				}
				var reposconflicto = $('input:radio[name=optPosconflicto]:checked').val();
				console.log("reposconflicto", reposconflicto);
				if (reposconflicto == 1) {
					var recursos = $('#recursosPosconflicto').val();
					if (recursos.length > 0) {
						$('#recursosPosconflicto').removeClass('input-danger');
					} else {
						sum += 1;
						$('#recursosPosconflicto').addClass('input-danger');
					}
				}
			}

			console.log("sum", sum);
			if (sum == 0) {
				$('#messageAlert-fase_4-inscripcion').html(success)
				$('#messageAlert-fase_4-inscripcion').show('slow')
				setTimeout(function(){
					//$('.contentDatos_fase_1').hide('slow');
				},3000);

				$('.f1 .f1-step').removeClass('active'); // icono
				$('.f1 .f1-step').addClass('activated'); // icono
				$('.f1 .f1-step > p').removeClass('active'); // texto
				$('.f1 .f1-step > p').addClass('activated'); // texto

				$('.f1 .f1-step-min').addClass('active');
				$('.progress-line').css('width','22%');

				var formData = new FormData(document.forms.namedItem("datos-fase_4"));
					console.log("formData", formData);

					var parametro = {
						'formData': formData
					};
					$.ajax({
						data: formData,
						type: 'POST',
						url: '../Controllers/datos-fase_4.php',
						cache: false,
						contentType: false,
						processData: false,
						success: function (resultado) {
							console.log("resultado", resultado);

							setTimeout(function () {
							  location.reload(true);  
							}, 1700);
						}
					});
			}else{
				if (sum>3) {
					$('#messageAlert-fase_4-inscripcion').html(danger)
					$('#messageAlert-fase_4-inscripcion').show('slow')
				}else{
					$('#messageAlert-fase_4-inscripcion').html(warning)
					$('#messageAlert-fase_4-inscripcion').show('slow')
				}        
			}
		}else {
			if (n == 2) {
				$('#alertaRegistroDenegado').modal('show');
				$('.fase_estado').html('tercera');
				setTimeout(function () {
					$('#cerrarAcceso').click();
				}, 7000);
			}else{
				$('#alertaRegistroDenegado').modal('show');
				$('.fase_estado').html('tercera');
				setTimeout(function () {
					$('#cerrarAcceso').click();
				}, 7000);
			}
		}
	}
}
function validarPresupuesto(lim){
	var opt = $('#valor-idea').val();
	if (opt>lim) {
		$('.valor-idea').removeClass('input-success');
		$('.valor-idea').addClass('input-danger');
		$('#mesageValPresupuesto').html('Valor Superado');
	}
}
function tamannno(){
	var formData = new FormData(document.forms.namedItem("datos-personales"));

	var parametro = {
		'formData': formData
	};
	$.ajax({
		data: formData,
		type: 'POST',
		url: '../Controllers/datos-personales.php',
		cache: false,
		contentType: false,
		processData: false,
		success: function (resultado) {

		}
	});
}
function unaOpcion(val) {
	console.log("val", val);
	if (val == 99) {

	}else{
		for (var i = 1; i<=8; i++){
			if (i != val) {
				$('#opcion_'+i).prop("checked", "");
			}
		}
	}
}

var f = 0;
var l = 0;
function unaOpcionReportes(val) {
	if (val == 1) {
		if (f == 0) {
			f = 1;
			for (var i = 1; i<=8; i++){
				$('#fase_'+i).prop("checked", true);
			}
		}else{
			f = 0;
			for (var i = 1; i<=8; i++){
				$('#fase_'+i).prop("checked", false);
			}
		}
	}else{
		if (val == 2) {
			$('#todas_fases').prop("checked", false);
			f = 0;
		}else{
			if (val == 4 ) {
				if (l == 0) {
					l = 1;
					for (var i = 1; i<=8; i++){
						$('#linea_'+i).prop("checked", true);
					}
				}else{
					l = 0;
					for (var i = 1; i<=8; i++){
						$('#linea_'+i).prop("checked", false);
					}
				}
			}else{
				if (val == 3) {
					l = 0;
					$('#todas_lineas').prop("checked", false);
				}
			}
		}
	}
}
function generarReporte(){
	var fases = [];
	for(var fase = 1; fase<=5; fase++){
		var marcado = $("#fase_"+fase).prop("checked") ? true : false;
		if (marcado==true) {
			fases.push('&fase'+fase+'=fase_'+fase)
		}
	}
	var lineas = [];
	for(var linea = 1; linea<=5; linea++){
		var marcado = $("#linea_"+linea).prop("checked") ? true : false;
		if (marcado==true) {
			lineas.push('&linea'+linea+'='+linea)
		}
	}
	//alert(fases.join("\n"));
	//alert(lineas.join("\n"));
	var a = 1;
	var page = 'login.php?ac=reportes&ruta=observatorio&reporte='+a.getTime()+'&'+fases.join()+lineas.join();
	window.location = page;

}


/* Determinamos el tiempo total en segundos */
var totalTiempo=10;
/* Determinamos la url donde redireccionar */
var url="destruirRegistro.php";
function updateReloj() {
	$('#cuenta_regresiva').html("Saliendo en "+totalTiempo+" segundos");
 
	if(totalTiempo==0)
	{
		window.location=url;
	}else{
		/* Restamos un segundo al tiempo restante */
		totalTiempo-=1;
		/* Ejecutamos nuevamente la función al pasar 1000 milisegundos (1 segundo) */
		setTimeout("updateReloj()",1000);
	}
}


	

// formlario de acuerdo al tipo de inscripcion que el postulante desee utilizar
function inscripcionIdea(info) {
	$('#opcionInscripcion').val(info);
	document.form_opcionInscripcion.submit();
}

function lineaInvestigacion(linea) {
	$('#lineaInvestigacion').val(linea);
	document.form_lineaInvestigacion.submit();
}


function busqueda(){
	$('.form-buscar').show('slow');
}
function busquedaLinea(){
	$('.form-buscar-x-linea').show('slow');  
}
/*$(function () {
	$("#search_form").submit(function(e){
		e.preventDefault();
	});
	$("#campoBuscar").keyup(function () {
		var send = $("#campoBuscar").val();
		$("#messageErrorBusqueda").html("<center>Cargando...</center>");

		setTimeout(function(){
			if (send.length>0) {
				proyectoBuscado(send);
			}
		},500);
	});
});*/

function proyectoBuscado(estado,linea){
	var valor = $("#campoBuscar").val();

	setTimeout(function(){
		var param = {
			'Funcion': 'busqueda_lineas', 'linea':linea, 'estado':estado,'valor':valor
		};
		$.ajax({
			data: JSON.stringify(param),
			type: "JSON",
			url: '../../ajax.php',
			success: function (data) {
				console.log("data", data);
				var json = JSON.parse(data);
				var html = "";
				for (var i = 0; i<json.length; i++ ) {
					html+= `
						<tr>
							<td>`+json[i].title+`</td>
							<td>`+json[i].autor+`</td>
							<td>`+json[i].grupo+`</td>
						</tr>
					`;
				}
				$('#cuerpoListProyectos').html(html)
			}
		});
	},500);
}



function selectEstadisticas(cod) {
	console.log("cod", cod);
	if (cod==1) {
		var param = {
			'Funcion': 'estadisticasAdmin', 'variable':cod
		};
	}else{
		var fecha1 = $('#fechaInicio').val();
		var fecha2 = $('#fechaFin').val();
		var param = {
			'Funcion': 'estadisticasAdmin', 'variable':cod, 'fecha1':fecha1,'fecha2':fecha2
		};
	}
	$.ajax({
		data: JSON.stringify(param),
		type: "JSON",
		url: '../../ajax.php',
		success: function (data) {
			console.log("data", data);
			var json = JSON.parse(data);
			if (json[0].detalle == 1) {
				//['Investigacion', 3],
				result = [];
				result1 = [];
				for (var i = 0; i < json.length; i++) {
					result.push({
						'name': json[i]['linea'],
						"y": json[i]['cant'],
						"drilldown": json[i]['linea']
					});
				}
				console.log(result);
				/*Highcharts.chart('conta', {
					chart: {
						type: 'pie',
						options3d: {
							enabled: true,
							alpha: 25
						}
					},
					title: {
						text: 'Estadísticas'
					},
					subtitle: {
						text: ''
					},
					plotOptions: {
						pie: {
							innerSize: 100,
							depth: 95
						}
					},

					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
					},
					series: [{
						name: 'Brands',
						colorByPoint: true,
						data: [{
							name: 'Microsoft Internet Explorer',
							y: 56.33,
							drilldown: 'Microsoft Internet Explorer'
						}, {
							name: 'Chrome',
							y: 24.03,
							drilldown: 'Chrome'
						}, {
							name: 'Firefox',
							y: 10.38,
							drilldown: 'Firefox'
						}, {
							name: 'Safari',
							y: 4.77,
							drilldown: 'Safari'
						}, {
							name: 'Opera',
							y: 0.91,
							drilldown: 'Opera'
						}, {
							name: 'Proprietary or Undetectable',
							y: 0.2,
							drilldown: null
						}]
					}],
					drilldown: {
						series: [{
							name: 'Microsoft Internet Explorer',
							id: 'Microsoft Internet Explorer',
							data: [
								['v11.0', 24.13],
								['v8.0', 17.2],
								['v9.0', 8.11],
								['v10.0', 5.33],
								['v6.0', 1.06],
								['v7.0', 0.5]
							]
						}, {
							name: 'Chrome',
							id: 'Chrome',
							data: [
								['v40.0', 5],
								['v41.0', 4.32],
								['v42.0', 3.68],
								['v39.0', 2.96],
								['v36.0', 2.53],
								['v43.0', 1.45],
								['v31.0', 1.24],
								['v35.0', 0.85],
								['v38.0', 0.6],
								['v32.0', 0.55],
								['v37.0', 0.38],
								['v33.0', 0.19],
								['v34.0', 0.14],
								['v30.0', 0.14]
							]
						}, {
							name: 'Firefox',
							id: 'Firefox',
							data: [
								['v35', 2.76],
								['v36', 2.32],
								['v37', 2.31],
								['v34', 1.27],
								['v38', 1.02],
								['v31', 0.33],
								['v33', 0.22],
								['v32', 0.15]
							]
						}, {
							name: 'Safari',
							id: 'Safari',
							data: [
								['v8.0', 2.56],
								['v7.1', 0.77],
								['v5.1', 0.42],
								['v5.0', 0.3],
								['v6.1', 0.29],
								['v7.0', 0.26],
								['v6.2', 0.17]
							]
						}, {
							name: 'Opera',
							id: 'Opera',
							data: [
								['v12.x', 0.34],
								['v28', 0.24],
								['v27', 0.17],
								['v29', 0.16]
							]
						}]
					}
				});*/
				Highcharts.chart('container1', {
					chart: {
						type: 'pie',
						options3d: {
							enabled: true,
							alpha: 35
						}
					},
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					plotOptions: {
						pie: {
							innerSize: 100,
							depth: 45
						}
					},
					series: [{
						name: 'Registrados',
						data: result
						//[
						//    ["Innovacion "+ 9+"% ", 8],
						//    ['Investigacion', 3],
						//    ['Modernizacion', 1],
						//    ['Divulgacion', 6],
						//    ['Servicios Tecnológicos', 8],
						//    ['Concursos', 4]
						//]
					}]
				});
				Highcharts.chart('container2', {
					chart: {
						type: 'column'
					},
					title: {
						text: 'Estadisticas vigentes'
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						type: 'category'
					},
					yAxis: {
						title: {
							text: 'Total percent market share'
						}

					},
					legend: {
						enabled: false
					},
					plotOptions: {
						series: {
							borderWidth: 0,
							dataLabels: {
								enabled: true,
								format: '{point.y:.0f}'
							}
						}
					},

					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b><br/>'
					},

					series: [{
						name: 'Registrados',
						colorByPoint: true,
						data: result
					}]
				});
				/*
				Highcharts.chart('container3', {
					chart: {
						type: 'pie'
					},
					title: {
						text: 'CANALES INTERNACIONALES CONTRATADOS'
					},
					subtitle: {
						text: ''
					},
					plotOptions: {
						series: {
							dataLabels: {
								enabled: true,
								format: '{point.name}: {point.y:.0f} inscripto'
							}
						}
					},
					tooltip: {
						headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
						pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>  {point.y:,.0f} </b><br/>'
					},
					series: [{
						name: 'Canales',
						data: result
					}]
				});
				*/
			} else {
				console.log('nada')
			}
		}
	});
}

function cerrarSesion() {
	swal({
		title: "Cerrando Sesion",
		text: "Espera un momento ¡Por favor!.",
		timer: 3000,
		showConfirmButton: false
	});
	setTimeout(function () {
		var page = 'logout.php';
		window.location = page;
	}, 3000);
}


function alertaAprobarFase(cod,fase){
	swal({
		title: "¿Seguro que deseas continuar?",
		text: "No podrás revocar esta fase mas adelante...",
		type: "warning",
		showCancelButton: true,
		cancelButtonText: "Mmm... mejor no!",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "¡Adelante!",
		closeOnConfirm: false
	},

	function () {
		aprobarFase(cod,fase);
	});
}
function aprobarFase(cod,fase){
    console.log("fase", fase);
    console.log("cod", cod);
	var indicaciones = $('#indicacionesFase_'+fase).val();
	console.log("indicaciones", indicaciones);
	var param = {
		'Funcion': 'aprobarFase',
		'cod': cod,
		'fase': fase,
		'indicaciones':indicaciones
	};
	$.ajax({
		data: JSON.stringify(param),
		type: "JSON",
		url: '../ajax.php',
		//beforesend: function(){
		//},
		success: function (data) {
			console.log(data);
			if (data > 0) {
				swal("¡Hecho!",
					"La fase ha sido aprobada.",
					"success");
				setTimeout(function () {
					location.reload(true);
				}, 2000);
			} else {
				swal("Error!",
					"Algo ha salido mal...",
					"error");
			}
		}
	});
}

// aprobacion final
function abrirModal_descartar(cod) {
	swal({
		title: "¿Seguro que deseas continuar?",
		text: "Después podrás aprobarlo nuevamente...",
		type: "warning",
		showCancelButton: true,
		cancelButtonText: "Mmm... mejor no",
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "¡Adelante!",
		closeOnConfirm: false
	},

	function () {
		descartarProyecto(cod);
	});
}

function descartarProyecto(cod) {
	var param = {
		'Funcion': 'rechazarProyecto',
		'cod': cod
	};
	$.ajax({
		data: JSON.stringify(param),
		type: "JSON",
		url: '../ajax.php',
		//beforesend: function(){
		//},
		success: function (data) {
			console.log(data);
			if (data > 0) {
				swal("¡Hecho!",
					"El proyecto ha sido descartado.",
					"success");
				setTimeout(function () {
					location.reload(true);
				}, 2000);
			} else {
				swal("Error!",
					"Algo ha salido mal...",
					"error");
			}
		}
	});
}

function abrirModal_aprobar(cod) {
	swal({
			title: "¿Seguro que deseas continuar?",
			text: "Después podrás rechazarlo si deseas...",
			type: "warning",
			showCancelButton: true,
			cancelButtonText: "Mmm... mejor no",
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "¡Adelante!",
			closeOnConfirm: false
		},

		function () {
			aprobarProyecto(cod);
		});
}

function aprobarProyecto(cod) {
	var param = {
		'Funcion': 'aprobarProyecto',
		'cod': cod
	};
	$.ajax({
		data: JSON.stringify(param),
		type: "JSON",
		url: '../ajax.php',
		//beforesend: function(){
		//},
		success: function (data) {
			console.log(data);
			if (data > 0) {
				swal("¡Hecho!",
					"El proyecto ha sido aprobado.",
					"success");
				setTimeout(function () {
					location.reload(true);
				}, 2000);
			} else {
				swal("Error!",
					"Algo ha salido mal...",
					"error");
			}
		}
	});
}
// validaciones
function validarCampoLleno(campo) {
	var n = $('#accesoRegistro').val();
	if (n == 0) {
		var valor = $('#' + campo).val();
		if (valor.length > 0) {

			$('#' + campo + "").removeClass('input-warning');
			$('#' + campo + "").removeClass('input-danger');
			$('#' + campo + "").addClass('input-success');

		} else {
			$('#' + campo).addClass('input-warning');
			$('#' + campo).removeClass('input-success');
		}
	} else {
		$('#' + campo).attr("readonly", "readonly");
		$('#alertaRegistroDenegado').modal('show');
		if (n == 1) {
			$('.fase_estado').html('primera');
		}else{
			if (n == 2) {
				$('.fase_estado').html('segunda');
			}else{
				$('.fase_estado').html('tercera');
			}

		}
		setTimeout(function () {
			$('#cerrarAcceso').click();
		}, 5000);
	}
}

function validarEmail(email, msj) {
	var c_email = $('#' + email).val();

	if (c_email.length > 0) {
		$('#' + email + "").removeClass('input-warning');
		$('#' + email + "").removeClass('input-danger');
		$('#' + email + "").addClass('input-success');
	} else {

		$('#' + email).addClass('input-warning');
		$('#' + email).removeClass('input-success');
	}

	if (/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(c_email)) {

		// si email es valido removemo class warning y/o error
		$('#' + email).removeClass('input-warning'); // input warning
		$('#' + email).removeClass('input-danger'); // input danger
		$('#' + msj + '').removeClass('message-warning'); // mensaje warning

		// agregamos clase success
		$('#' + email).addClass('input-success'); // input succes
		$('#' + msj + '').addClass('message-success'); // mensaje success
		$('#' + msj + '').html('Correcto.!'); // mensaje

		// ocultamos mensaje despues de cierto tiempo
		setTimeout(function () {
			$('#' + msj + '').hide('slow');
		}, 1500);

	} else {

		// si email es incorrecto removemos clase succes 
		$('#' + email).removeClass('input-success'); // input success
		$('#' + msj + '').removeClass('message-success'); // mensaje succes

		// agregamos clase warning
		$('#' + email).addClass('input-warning'); // input warning
		$('#' + msj + '').addClass('message-warning'); // mensaje warning
		$('#' + msj + '').html('Error.! Email invalido'); // mensaje
	}
}

function valor() {
	var num = $('#valor-idea').val();
	var result = formatearNumero(num);
	$('#valor-idea').val(result);
}

function formatearNumero(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? ',' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + '.' + '$2');
	}
	return x1 + x2;
}


// amplificacion de formularios
function numeroAutores() {
	var num = $('#autores').val();
	var htmlAutores = '';
	for (var autor = 1; autor <= num; autor++) {
		htmlAutores += `<div class="col-xs-12">
			<p>Autor ` + autor + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + autor + `">
				<div class="col-xs-12 col-md-6">
					<p>Nombre(s)</p>
					<input type="text" name="nombreAutor_` + autor + `" id="nombreAutor_` + autor + `" class="form-control" onclick="validarCampoLleno('nombreAutor_` + autor + `')" onkeyup="validarCampoLleno('nombreAutor_` + autor + `')" />
				</div>
				<div class="col-xs-12 col-md-6">
					<p>Apellido(s)</p>
					<input type="text" name="apellidosAutor_` + autor + `" id="apellidosAutor_` + autor + `" class="form-control" onclick="validarCampoLleno('apellidosAutor_` + autor + `')" onkeyup="validarCampoLleno('apellidosAutor_` + autor + `')"  />
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Identificacion</p>
					<input type="number" name="identificacionAutor_` + autor + `" id="identificacionAutor_` + autor + `" class="form-control" onclick="validarCampoLleno('identificacionAutor_` + autor + `')" onkeyup="validarCampoLleno('identificacionAutor_` + autor + `')" min="0" onkeypress="return event.charCode >= 48" />
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Email</p>
					<input type="email" name="emailAutor_` + autor + `" id="emailAutor_` + autor + `" class="form-control"  onclick="validarEmail('emailAutor_` + autor + `','mesageValEmailAutores');validarCampoLleno('emailAutor_` + autor + `')" onkeyup="validarEmail('emailAutor_` + autor + `','mesageValEmailAutores');validarCampoLleno('emailAutor_` + autor + `')">
					<p id="mesageValEmailAutores"></p>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Telefono</p>
					<input type="number" name="telefonoAutor_` + autor + `" id="telefonoAutor_` + autor + `" class="form-control" onclick="validarCampoLleno('telefonoAutor_` + autor + `')" onkeyup="validarCampoLleno('telefonoAutor_` + autor + `')" min="0" onkeypress="return event.charCode >= 48" />
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentAutores').html(htmlAutores)
}

// posconflicto
function apliPos(val) {
	if (val == 1) {
		$('#contentPosConflicto').show('slow');
	} else {
		$('#contentPosConflicto').hide('slow');
	}
}

function recursosPos(val) {
	if (val == 1) {
		$('#contentRecursosPosconflicto').show('slow');
	} else {
		$('#contentRecursosPosconflicto').hide('slow');
	}
}

function numeroMunicipios() {
	var num = $('#municipios').val();
	var htmlMunicipios = '';

	for (var municipio = 1; municipio <= num; municipio++) {
		htmlMunicipios += `<div class="col-xs-4">
				<h5>Municipio ` + municipio + `</h5>
				<input type="text" name="nombreMunicipio_` + municipio + `" id="nombreMunicipio_` + municipio + `" class="form-control" onclick="validarCampoLleno('nombreMunicipio_` + municipio + `')" onkeyup="validarCampoLleno('nombreMunicipio_` + municipio + `')" placeholder="Nombre del Municipio"/>
			</div>`;
	}
	$('#contentMunicipios').html(htmlMunicipios);
}

// objetivos especificos, resultados esperados y productos de resultados.
function numeroObjEsp() {
	var num = $('#objEspecifico').val();
	var htmlObjEsp = '';
	var htmlResultados = '';
	var htmlProductos = '';

	for (var objetivo = 1; objetivo <= num; objetivo++) {
		htmlObjEsp += `
			<div class="col-xs-12">
				<h5>Objetivo #` + objetivo + `</h5>
				<textarea name="objetivoEsp_` + objetivo + `" id="objetivoEsp_` + objetivo + `" class="form-control" onclick="validarCampoLleno('objetivoEsp_` + objetivo + `')" onkeyup="validarCampoLleno('objetivoEsp_` + objetivo + `')" cols="30" rows="2"></textarea>
			</div>
		`;
		htmlResultados += `
			<div class="col-xs-12">
				<h5>Resultados del Objetivo Especifico #` + objetivo + `</h5>
				<textarea name="resultadoObjetivo_` + objetivo + `" id="resultadoObjetivo_` + objetivo + `" class="form-control" onclick="validarCampoLleno('resultadoObjetivo_` + objetivo + `')" onkeyup="validarCampoLleno('resultadoObjetivo_` + objetivo + `')" cols="30" rows="2"></textarea>
			</div>
		`;
		htmlProductos += `
			<div class="col-xs-12">
				<h5>Producto del Resultado #` + objetivo + `</h5>
				<textarea name="productoResultado_` + objetivo + `" id="productoResultado_` + objetivo + `" class="form-control" onclick="validarCampoLleno('productoResultado_` + objetivo + `')" onkeyup="validarCampoLleno('productoResultado_` + objetivo + `')" cols="30" rows="2"></textarea>
			</div>
		`;
	}
	$('#contentObjetivosEspecificos').html(htmlObjEsp);
	$('#contentResultadosEsperados').html(htmlResultados);
	$('#contentProductosResultado').html(htmlProductos);
}
// semilleros beneficiados
function numeroSemilleros() {
	var num = $('#semillerosBeneficiados').val();
	var htmlsemilleros = '';

	for (var semillero = 1; semillero <= num; semillero++) {
		htmlsemilleros += `
			<div class="col-xs-12">
				<h5>Nombre semillero #` + semillero + `</h5>
				<textarea name="nombreSemillero_` + semillero + `" id="nombreSemillero_` + semillero + `" class="form-control" onclick="validarCampoLleno('nombreSemillero_` + semillero + `')" onkeyup="validarCampoLleno('nombreSemillero_` + semillero + `')" cols="30" rows="1"></textarea>
			</div>
		`;
	}
	$('#contentSemilleros').html(htmlsemilleros);
}

// formaciones benefiadas
function numeroFormaciones() {
	var num = $('#formacionesBenefiadas').val();
	var htmlFormaciones = '';

	for (var formacion = 1; formacion <= num; formacion++) {
		htmlFormaciones += `
			<div class="col-xs-12">
				<h5>Nombre de la  Formacion #` + formacion + `</h5>
				<textarea name="nombreFormacion_` + formacion + `" id="nombreFormacion_` + formacion + `" class="form-control" onclick="validarCampoLleno('nombreFormacion_` + formacion + `')" onkeyup="validarCampoLleno('nombreFormacion_` + formacion + `')" cols="30" rows="1"></textarea>
			</div>
		`;
	}
	$('#contentFormacionesBenefiadas').html(htmlFormaciones);
}


// recursos humanos

// personal externo
function si_no(val, input, content) {
	if (val == 1) {
		$('#' + content).show('slow');
		$('#' + input).show('slow');
	} else {
		$('#' + content).hide('slow');
		$('#' + input).hide('slow');
	}
}

function numeroPersonaExternas() {
	var num = $('#numPersonalExt').val();
	var htmlPersonalExterno = '';
	for (var persona = 1; persona <= num; persona++) {
		htmlPersonalExterno += `<div class="col-xs-12">
			<p>Persona # ` + persona + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + persona + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre(s)</p>
					<input type="text" name="nombrePersExt_` + persona + `" id="nombrePersExt_` + persona + `" class="form-control" onclick="validarCampoLleno('nombrePersExt_` + persona + `')" onkeyup="validarCampoLleno('nombrePersExt_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Apellido(s)</p>
					<input type="text" name="apellidosPersExt_` + persona + `" id="apellidosPersExt_` + persona + `" class="form-control" onclick="validarCampoLleno('apellidosPersExt_` + persona + `')" onkeyup="validarCampoLleno('apellidosPersExt_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Identificacion</p>
					<input type="number" name="identificacionPersExt_` + persona + `" id="identificacionPersExt_` + persona + `" class="form-control" onclick="validarCampoLleno('identificacionPersExt_` + persona + `')" onkeyup="validarCampoLleno('identificacionPersExt_` + persona + `')"/>
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumPersonasExternas').html(htmlPersonalExterno)
}

// personal interno Aprendices sin contrato "ASC"
function numeroPersonaInternasASC() {
	var num = $('#numPersonalIntASC').val();
	var htmlPersonalIntASC = '';
	for (var persona = 1; persona <= num; persona++) {
		htmlPersonalIntASC += `<div class="col-xs-12">
			<p>Aprendiz # ` + persona + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + persona + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre(s)</p>
					<input type="text" name="nombrePersIntASC_` + persona + `" id="nombrePersIntASC_` + persona + `" class="form-control" onclick="validarCampoLleno('nombrePersIntASC_` + persona + `')" onkeyup="validarCampoLleno('nombrePersIntASC_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Apellido(s)</p>
					<input type="text" name="apellidosPersIntASC_` + persona + `" id="apellidosPersIntASC_` + persona + `" class="form-control" onclick="validarCampoLleno('apellidosPersIntASC_` + persona + `')" onkeyup="validarCampoLleno('apellidosPersIntASC_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Identificacion</p>
					<input type="number" name="identificacionPersIntASC_` + persona + `" id="identificacionPersIntASC_` + persona + `" class="form-control" onclick="validarCampoLleno('identificacionPersIntASC_` + persona + `')" onkeyup="validarCampoLleno('identificacionPersIntASC_` + persona + `')"/>
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumPersonasInternasASC').html(htmlPersonalIntASC)
}
// perosonal interno Aprendices con contrato "ACC"
function numeroPersonaInternasACC() {
	var num = $('#numPersonalIntACC').val();
	var htmlPersonalIntACC = '';
	for (var persona = 1; persona <= num; persona++) {
		htmlPersonalIntACC += `<div class="col-xs-12">
			<p>Aprendiz # ` + persona + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + persona + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre(s)</p>
					<input type="text" name="nombrePersIntACC_` + persona + `" id="nombrePersIntACC_` + persona + `" class="form-control" onclick="validarCampoLleno('nombrePersIntACC_` + persona + `')" onkeyup="validarCampoLleno('nombrePersIntACC_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Apellido(s)</p>
					<input type="text" name="apellidosPersIntACC_` + persona + `" id="apellidosPersIntACC_` + persona + `" class="form-control" onclick="validarCampoLleno('apellidosPersIntACC_` + persona + `')" onkeyup="validarCampoLleno('apellidosPersIntACC_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Identificacion</p>
					<input type="number" name="identificacionPersIntACC_` + persona + `" id="identificacionPersIntACC_` + persona + `" class="form-control" onclick="validarCampoLleno('identificacionPersIntACC_` + persona + `')" onkeyup="validarCampoLleno('identificacionPersIntACC_` + persona + `')"/>
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumPersonasInternasACC').html(htmlPersonalIntACC)
}
// perosonal interno Instructores
function numeroPersonaInternasInstructores() {
	var num = $('#numPersonalIntInstructores').val();
	var htmlPersonalIntInstructores = '';
	for (var persona = 1; persona <= num; persona++) {
		htmlPersonalIntInstructores += `<div class="col-xs-12">
			<p>Instructor # ` + persona + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + persona + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre(s)</p>
					<input type="text" name="nombrePersIntInstructores_` + persona + `" id="nombrePersIntInstructores_` + persona + `" class="form-control" onclick="validarCampoLleno('nombrePersIntInstructores_` + persona + `')" onkeyup="validarCampoLleno('nombrePersIntInstructores_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Apellido(s)</p>
					<input type="text" name="apellidosPersIntInstructores_` + persona + `" id="apellidosPersIntInstructores_` + persona + `" class="form-control" onclick="validarCampoLleno('apellidosPersIntInstructores_` + persona + `')" onkeyup="validarCampoLleno('apellidosPersIntInstructores_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Identificacion</p>
					<input type="number" name="identificacionPersIntInstructores_` + persona + `" id="identificacionPersIntInstructores_` + persona + `" class="form-control" onclick="validarCampoLleno('identificacionPersIntInstructores_` + persona + `')" onkeyup="validarCampoLleno('identificacionPersIntInstructores_` + persona + `')"/>
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumPersonasInternasInstructores').html(htmlPersonalIntInstructores)
}
// perosonal interno Otros
function numeroPersonaInternasOtros() {
	var num = $('#numPersonalIntOtros').val();
	var htmlPersonalIntOtros = '';
	for (var persona = 1; persona <= num; persona++) {
		htmlPersonalIntOtros += `<div class="col-xs-12">
			<p>Persona # ` + persona + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + persona + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre(s)</p>
					<input type="text" name="nombrePersIntOtros_` + persona + `" id="nombrePersIntOtros_` + persona + `" class="form-control" onclick="validarCampoLleno('nombrePersIntOtros_` + persona + `')" onkeyup="validarCampoLleno('nombrePersIntOtros_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Apellido(s)</p>
					<input type="text" name="apellidosPersIntOtros_` + persona + `" id="apellidosPersIntOtros_` + persona + `" class="form-control" onclick="validarCampoLleno('apellidosPersIntOtros_` + persona + `')" onkeyup="validarCampoLleno('apellidosPersIntOtros_` + persona + `')"/>
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Identificacion</p>
					<input type="number" name="identificacionPersIntOtros_` + persona + `" id="identificacionPersIntOtros_` + persona + `" class="form-control" onclick="validarCampoLleno('identificacionPersIntOtros_` + persona + `')" onkeyup="validarCampoLleno('identificacionPersIntOtros_` + persona + `')"/>
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumPersonasInternasOtros').html(htmlPersonalIntOtros)
}

// aliados
// personal externo
function numeroAliadosExternos() {
	var num = $('#numAliadosExternos').val();
	console.log("num", num);
	var htmlAliadosExternos = '';
	for (var aliado = 1; aliado <= num; aliado++) {
		htmlAliadosExternos += `<div class="col-xs-12">
			<p>Aliado # ` + aliado + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + aliado + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre</p>
					<input type="text" name="nombreAliadoExt_` + aliado + `" id="nombreAliadoExt_` + aliado + `" class="form-control" onclick="validarCampoLleno('nombreAliadoExt_` + aliado + `')" onkeyup="validarCampoLleno('nombreAliadoExt_` + aliado + `')" />
				</div>
				<div class="col-xs-12 col-md-4">
					<p>NIT</p>
					<input type="text" name="nitAliadoExt_` + aliado + `" id="nitAliadoExt_` + aliado + `" class="form-control" onclick="validarCampoLleno('nitAliadoExt_` + aliado + `')" onkeyup="validarCampoLleno('nitAliadoExt_` + aliado + `')" />
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Recursos en Especie ($COP):</p>
					<input type="number" name="recursoAliadoExt_` + aliado + `" id="recursoAliadoExt_` + aliado + `" class="form-control" onclick="validarCampoLleno('recursoAliadoExt_` + aliado + `')" onkeyup="validarCampoLleno('recursoAliadoExt_` + aliado + `')" />
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumAliadosExternos').html(htmlAliadosExternos)
}

// aliados internos
function numeroAliadosInternos() {
	var num = $('#numAliadosInternos').val();
	var htmlAliadosExternos = '';
	for (var aliado = 1; aliado <= num; aliado++) {
		htmlAliadosExternos += `<div class="col-xs-12">
			<p>Aliado # ` + aliado + `</p>
			<div class="col-xs-12 col-md-12 contentAutor_` + aliado + `">
				<div class="col-xs-12 col-md-4">
					<p>Nombre</p>
					<input type="text" name="nombreAliadoInterno_` + aliado + `" id="nombreAliadoInterno_` + aliado + `" class="form-control" onclick="validarCampoLleno('nombreAliadoInterno_` + aliado + `')" onkeyup="validarCampoLleno('nombreAliadoInterno_` + aliado + `')" />
				</div>
				<div class="col-xs-12 col-md-4">
					<p>NIT</p>
					<input type="text" name="nitAliadoInterno_` + aliado + `" id="nitAliadoInterno_` + aliado + `" class="form-control" onclick="validarCampoLleno('nitAliadoInterno_` + aliado + `')" onkeyup="validarCampoLleno('nitAliadoInterno_` + aliado + `')" />
				</div>
				<div class="col-xs-12 col-md-4">
					<p>Recursos en Especie ($COP):</p>
					<input type="number" name="recursoAliadoInterno_` + aliado + `" id="recursoAliadoInterno_` + aliado + `" class="form-control" onclick="validarCampoLleno('recursoAliadoInterno_` + aliado + `')" onkeyup="validarCampoLleno('recursoAliadoInterno_` + aliado + `')" />
				</div>
			</div>
		</div>
		<div class="col-xs-12"><hr /></div>`;
	}
	$('#contentNumAliadosInternos').html(htmlAliadosExternos)
}

// ciudades
function numeroCiudadesInfluencia() {
	var num = $('#numCiudadesInfluencia').val();
	var htmlCiudadesInfluencia = '';
	for (var ciudad = 1; ciudad <= num; ciudad++) {
		htmlCiudadesInfluencia += `<div class="col-xs-6">
			<p>Ciudad # ` + ciudad + `</p>
			<input type="text" name="nombreCiudadInfluencia_` + ciudad + `" id="nombreCiudadInfluencia_` + ciudad + `" class="form-control" placeholder="Nombre ciudad" onclick="validarCampoLleno('nombreCiudadInfluencia_` + ciudad + `')" onkeyup="validarCampoLleno('nombreCiudadInfluencia_` + ciudad + `')" />
		</div>
		`;
	}
	$('#contentNumCiudadesInfluencia').html(htmlCiudadesInfluencia)
}
// Municipios
function numeroMunicipiosInfluencia() {
	var num = $('#numMunicipiosInfluencia').val();
	var htmlMunicipiosInfluencia = '';
	for (var municipios = 1; municipios <= num; municipios++) {
		htmlMunicipiosInfluencia += `<div class="col-xs-6">
			<p>Municipio # ` + municipios + `</p>
			<input type="text" name="nombreMunicipioInfluencia_` + municipios + `" id="nombreMunicipioInfluencia_` + municipios + `" class="form-control" placeholder="Nombre Municipio" onclick="validarCampoLleno('nombreMunicipioInfluencia_` + municipios + `')" onkeyup="validarCampoLleno('nombreMunicipioInfluencia_` + municipios + `')"  />
		</div>
		`;
	}
	$('#contentNumMunicipiosInfliencia').html(htmlMunicipiosInfluencia)
}

function agregarArchivo(mas){
	var n = $('#numArchivos').val();
	console.log("n", n);

	if (n>1) {
	}

	var lim = parseInt(n)+parseInt(mas); 

	var html = $('#contentArchivos').html();
	for (var i = n; i<lim; i++ ) {
		console.log("parseInt(n+mas)", parseInt(n+mas));
		$('#menosArchivos').show('slow');
		console.log("i", i);
		html += `<input type="file" name="recursos_`+i+`" class="form-control">`;
	}
	$('#contentArchivos').html(html);


}

// recursos sennova
function sinoRadios(campo, valor) {
	if (valor == 1) {
		$('#' + campo).show();
	} else {
		$('#' + campo).hide();
	}
}
// desplegar opciones
function desplegarRecursosHumanos() {
	var opt = $('#valorReHu').val();
	if (opt == 0) {
		$('#contentRecursosHumanos').show('slow');
		$('#btn-recursosH').html('<i class="fa fa-angle-up"></i>')
		$('#valorReHu').val(1);
	} else {
		$('#contentRecursosHumanos').hide('slow');
		$('#btn-recursosH').html('<i class="fa fa-angle-down"></i>')
		$('#valorReHu').val(0);
	}
}

function desplegarAliados() {
	var opt = $('#valorAliados').val();
	if (opt == 0) {
		$('#contenAliados').show('slow');
		$('#btn-aliados').html('<i class="fa fa-angle-up"></i>')
		$('#valorAliados').val(1);
	} else {
		$('#contenAliados').hide('slow');
		$('#btn-aliados').html('<i class="fa fa-angle-down"></i>')
		$('#valorAliados').val(0);
	}
}

function deplegarRecursosSennova() {
	var opt = $('#valorReSe').val();
	if (opt == 0) {
		$('#contentRecursosSennova').show('slow');
		$('#btn-recursosS').html('<i class="fa fa-angle-up"></i>')
		$('#valorReSe').val(1);
	} else {
		$('#contentRecursosSennova').hide('slow');
		$('#btn-recursosS').html('<i class="fa fa-angle-down"></i>')
		$('#valorReSe').val(0);
	}
}

// function multi depliegue
function multiDespliegue(btn, input, content) {
	var opt = $('#' + input).val();
	if (opt == 0) {
		$('#' + content).show('slow');
		$('#' + btn).html('<i class="fa fa-angle-up"></i>')
		$('#' + input).val(1);
	} else {
		$('#' + content).hide('slow');
		$('#' + btn).html('<i class="fa fa-angle-down"></i>')
		$('#' + input).val(0);
	}
}
// volver a menus
function volverPaso(content1, content2, content3) {
	$('.' + content2).hide('slow');
	$('.' + content3).hide('slow');
	$('.' + content1).show('slow');
}
