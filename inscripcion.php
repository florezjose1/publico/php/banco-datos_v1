<?php
   session_start();
   if(isset($_SESSION['id'])) {
      echo '<script> window.location="Views/login.php"; </script>';
   }

   $cod = time();
	$_SESSION['registro'] = $cod;

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Banco Proyectos Sennova</title>
	<link rel="stylesheet" href="Public/bootstrap/css/fonts.css">
	<link rel="stylesheet" href="Public/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="Public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="Public/bootstrap/css/compiled.css">
	<link rel="stylesheet" href="Public/css/style-index.css">
	<link rel="stylesheet" href="Public/css/style.css">
	<link rel="stylesheet" href="Public/plugins/alerts/css/sweetalert.css">
	<!--<link rel="stylesheet" href="Hand_Of_Sean_Demo.ttf">-->
	
	
	<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
	
</head>
<body onload="atras();" >

	<div class="row" style=" position: absolute; width: 100%;top:0;">
	   <div class="col-xs-12">
	      <h2 class="header" style=""><b>Banco</b><br> De Proyectos</h2>

	      <img src="Public/img/header/2.png" id="img_2" class="animated img-responsive pull-right" alt="">
	      <img src="Public/img/header/3.png" id="img_3" class="animated img-responsive pull-right" alt="">
	      <img src="Public/img/header/4.png" id="img_4" class="animated img-responsive pull-right" alt="">
	      <img src="Public/img/header/5.png" id="img_5" class="animated img-responsive pull-right" alt="">
	   </div>
	</div>

	<div class="container-fluid menu" id="botones-inicio" >
    	<ul class="list-inline">
        	<li>
            <a href="index.php" id="inicio" class="btn-header">Inicio</a>
        	</li>
        	<li>
            <a href="beneficios.php" id="beneficios" class="btn-header">Beneficios</a>
        	</li>
        	<li>
            <a href="inscripcion.php" id="inscripcion" class="btn-header active">Inscripcion</a>
        	</li>
        	<li>
            <a href="#" data-toggle="modal" data-target="#modal-login" id="sesion" class="btn-header ">Iniciar Sesion</a>
        	</li>
    	</ul>
	</div>


	
	<!-- fin header-->

   <div class="container-fluid" style="margin-top: 170px;overflow:hidden;">
         
         <div class="col-xs-12 col-md-6" style="padding: 0;">
            <!--Collection card-->
            <a href="#" data-toggle="modal" data-target="#modalFormarse">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  img-inscripcion">
                     <img src="Public/img/inscripcion/observatorio.jpg" style="width:100%;" class="img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="#" data-toggle="modal" data-target="#modalFormarse">
                           <p style="font-size: 25px;color:#fff;"><b>OBSERVATORIO</b> 
                           <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Formula tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>

         <div class="col-xs-12 col-md-6" style="padding: 0;">
            <!--Collection card-->
            <a href="#" data-toggle="modal" data-target="#modalIncripcionDirecta">
               <div class="card collection-card">
                  <!--Card image-->
                  <div class="view  img-inscripcion">
                     <img src="Public/img/inscripcion/inscripcion.jpg" style="width:100%;" class=" img-fluid" alt="">
                     <div class="stripe dark " style=""> <!-- dark -->
                        <a href="#" data-toggle="modal" data-target="#modalIncripcionDirecta">
                           <p style="font-size: 25px;color:#fff;"><b>INSCRIPCIÓN</b> 
                           <i class="animated rubberBand infinite fa fa-chevron-right"></i></p>
                        </a>
                        <p style="color:#fff;"> Inscribe tu Proyecto.</p>
                     </div>
                  </div>
               </div>
            </a>
         </div>
      
	</div>





	<?php 
		include 'Public/inc/modales.php';
	?>

	<script src="Public/plugins/jquery/jquery-2.2.3.min.js"></script>


	<script src="Public/plugins/alerts/js/functions.js"></script>
  	<script src="Public/plugins/alerts/js/sweetalert.min.js"></script>

	<script src="Public/bootstrap/js/bootstrap.min.js"></script>
	<script src="Public/bootstrap/js/mdb.min.js"></script>
	<script src="Public/js/main.js"></script>
	<script src="Public/js/usuarios.js"></script>

	<script>
        
    $(function(){
        setTimeout(function(){
            $('#img_2').addClass('fadeInRight');
            $('#img_2').css('display','block');

            setTimeout(function(){
                $('#img_3').addClass('fadeInRight');
                $('#img_3').css('display','block');

                setTimeout(function(){
                    $('#img_4').addClass('fadeInRight');
                    $('#img_4').css('display','block');

                    setTimeout(function(){
                        $('#img_5').addClass('fadeInRight');
                        $('#img_5').css('display','block');

                        /*setTimeout(function(){
                            
                            //$('#botones-inicio').addClass('sombra');
                            $('#botones-inicio').addClass('fadeInLeft');
                                       
                        },600)*/
                    },600)
                },600)
            },600)
        },600)
    });

    </script>
	
	<script type="text/javascript">
		//window.history.forward();
		function atras(){
			//window.location.hash = "no-back-button";
			//window.location.hash = "Again-No-back-button"; // chrome
			//window.onhashchange = function(){window.location.hash="no-back-button"};
			////window.history.forward();
		}
		//function adelante(){
			//window.history.back();
		//}

		$(function () {
			var html = '<img src="Public/img/facebook.png" alt="">	';
			setTimeout(function(){
				
				/*swal({
			        title: "Banco de Proyectos Sennova",
			        text: html,
			        timer: 5000,
			        showConfirmButton: false
			    });*/
			},1000);
		});
	</script>
</body>
</html>