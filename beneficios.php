<?php
    session_start();
    if(isset($_SESSION['id'])) {
        echo '<script> window.location="Views/login.php"; </script>';
    }

    $cod = time();
	$_SESSION['registro'] = $cod;

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<title>Banco Proyectos Sennova</title>
	<link rel="stylesheet" href="Public/bootstrap/css/fonts.css">
	<link rel="stylesheet" href="Public/css/style-index.css">
	<link rel="stylesheet" href="Public/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="Public/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="Public/bootstrap/css/compiled.css">
	<link rel="stylesheet" href="Public/plugins/alerts/css/sweetalert.css">
	<!--<link rel="stylesheet" href="Hand_Of_Sean_Demo.ttf">-->
	
	
	<link href="https://fonts.googleapis.com/css?family=Aclonica" rel="stylesheet">
	
</head>
<body onload="atras();" >

	<div class="redes hidden-xs hidden-sm">
		<a href="">
			<img src="Public/img/facebook.png" alt="">	
		</a>
		<a href="">
			<img src="Public/img/google.png" alt="">	
		</a>
		<a href="">
			<img src="Public/img/twiter.png" alt="">	
		</a>
		<a href="">
			<img src="Public/img/message.png" alt="">	
		</a>
	</div>
	
	<div class="row" style=" position: absolute; width: 100%;top:0;">
	   <div class="col-xs-12">
	      <h2 class="header" style=""><b>Banco</b><br> De Proyectos</h2>

	      <img src="Public/img/header/2.png" id="img_2" class="animated img-responsive pull-right" alt="">
	      <img src="Public/img/header/3.png" id="img_3" class="animated img-responsive pull-right" alt="">
	      <img src="Public/img/header/4.png" id="img_4" class="animated img-responsive pull-right" alt="">
	      <img src="Public/img/header/5.png" id="img_5" class="animated img-responsive pull-right" alt="">
	   </div>
	</div>

	<div class="container-fluid menu" id="botones-inicio" >
    	<ul class="list-inline">
        	<li>
            <a href="index.php" id="inicio" class="btn-header">Inicio</a>
        	</li>
        	<li>
            <a href="beneficios.php" id="beneficios" class="btn-header active">Beneficios</a>
        	</li>
        	<li>
            <a href="inscripcion.php" id="inscripcion" class="btn-header ">Inscripcion</a>
        	</li>
        	<li>
            <a href="#" data-toggle="modal" data-target="#modal-login" id="sesion" class="btn-header ">Iniciar Sesion</a>
        	</li>
    	</ul>
	</div>


	
	<!-- fin header-->

	<div class="container-fluid">
	   <div class="title">
	      <h3>Beneficios del Banco de Proyectos <b>SENNOVA</b></h3>
	   </div>
	   
	   <div class="content-beneficios">
        	<div class="row">
            <div class="col-xs-12 col-md-3 beneficio_1">
               <!--Card-->
               <div class="card card-cascade wider img-efecto">
                  <!--Card image-->
                  <div class="card-img view overlay hm-white-slight">
                     <img src="Public/img/beneficios/beneficio1.jpg" class="img-fluid">
                     <a href="#">
                        <div class="mask waves-effect waves-light"></div>
                     </a>
                  </div>
                  <!--/Card image-->

                  <!--Card content-->
                  <div class="card-block text-center">
                     <!--Title-->
                     <h5 class="card-title"><strong>Laborar</strong></h5>

                     <p class="card-text" style="text-align: justify; font-size: 16px;">Los administradores, aprendices e instructores se pueden vincular a un semillero mediante un contrato laboral por proyectos aprobados. </p>
                  </div>
                  <!--/.Card content-->
               </div>
               <!--/.Card-->
            </div>
            <div class="col-xs-12 col-md-3 beneficio_2">
               <!--Card-->
               <div class="card card-cascade wider img-efecto" style="border: 1px solid #eee;">

                  <!--Card image-->
                  <div class="view overlay hm-white-slight" style="padding: 20px;">
                     <img src="Public/img/beneficios/beneficio2.jpg" class="img-fluid">
                     <a href="#">
                        <div class="mask waves-effect waves-light"></div>
                     </a>
                  </div>
                  <!--/Card image-->

                  <!--Card content-->
                  <div class="card-block text-center">
                     <!--Title-->
                     <h5 class="card-title"><strong>Económicos</strong></h5>

                     <p class="card-text" style="text-align: justify; font-size: 16px;">Sennova aporta infraestructura y recursos humanos, financieros y materiales necesarios para el desarrollo de su proyecto de investigación </p>
                  </div>
                  <!--/.Card content-->
               </div>
               <!--/.Card-->
            </div>
            <div class="col-xs-12 col-md-3 beneficio_3">
               <!--Card-->
               <div class="card card-cascade wider img-efecto">

                  <!--Card image-->
                  <div class="card-img view overlay hm-white-slight">
                     <img src="Public/img/beneficios/beneficio3.jpg" class="img-fluid">
                     <a href="#">
                        <div class="mask waves-effect waves-light"></div>
                     </a>
                  </div>
                 	<!--/Card image-->

                  <!--Card content-->
                  <div class="card-block text-center">
                     <!--Title-->
                     <h5 class="card-title"><strong>Desarrollo integral</strong></h5>

                     <p class="card-text" style="text-align: justify; font-size: 16px;">Representación a nivel nacional por medio del proyecto aprobado en eventos de divulgación y desarrollo tecnológico, innovación e investigación. </p>
                  </div>
                  <!--/.Card content-->
               </div>
               <!--/.Card-->
            </div>
            <div class="col-xs-12 col-md-3 beneficio_4">
               <!--Card-->
               <div class="card card-cascade wider img-efecto">
                  <!--Card image-->
                  <div class="card-img view overlay hm-white-slight">
                     <img src="Public/img/beneficios/beneficio1.jpg" class="img-fluid">
                     <a href="#">
                        <div class="mask waves-effect waves-light"></div>
                     </a>
                  </div>
                  <!--/Card image-->

                  <!--Card content-->
                  <div class="card-block text-center">
                     <!--Title-->
                     <h5 class="card-title"><strong>Gestión de conocimiento </strong></h5>

                     <p class="card-text" style="text-align: justify; font-size: 15px;">Integración del conocimiento para apoyar la transformación productiva y social de los actores empresariales regionales permitiendo generar ventajas sustentables y competitivas en un entorno dinámico. </p>
                  </div>
                  <!--/.Card content-->
               </div>
               <!--/.Card-->
            </div>
        	</div>
    	</div>
	</div>





	<?php 
		include 'Public/inc/modales.php';
	?>

	<script src="Public/plugins/jquery/jquery-2.2.3.min.js"></script>


	<script src="Public/plugins/alerts/js/functions.js"></script>
  	<script src="Public/plugins/alerts/js/sweetalert.min.js"></script>

	<script src="Public/bootstrap/js/bootstrap.min.js"></script>
	<script src="Public/bootstrap/js/mdb.min.js"></script>
	<script src="Public/js/main.js"></script>
	<script src="Public/js/usuarios.js"></script>

	<script>
        
    $(function(){
        setTimeout(function(){
            $('#img_2').addClass('fadeInRight');
            $('#img_2').css('display','block');

            setTimeout(function(){
                $('#img_3').addClass('fadeInRight');
                $('#img_3').css('display','block');

                setTimeout(function(){
                    $('#img_4').addClass('fadeInRight');
                    $('#img_4').css('display','block');

                    setTimeout(function(){
                        $('#img_5').addClass('fadeInRight');
                        $('#img_5').css('display','block');

                        /*setTimeout(function(){
                            
                            //$('#botones-inicio').addClass('sombra');
                            $('#botones-inicio').addClass('fadeInLeft');
                                       
                        },600)*/
                    },600)
                },600)
            },600)
        },600)
    });

    </script>
	
	<script type="text/javascript">
		//window.history.forward();
		function atras(){
			//window.location.hash = "no-back-button";
			//window.location.hash = "Again-No-back-button"; // chrome
			//window.onhashchange = function(){window.location.hash="no-back-button"};
			////window.history.forward();
		}
		//function adelante(){
			//window.history.back();
		//}

		$(function () {
			var html = '<img src="Public/img/facebook.png" alt="">	';
			setTimeout(function(){
				
				/*swal({
			        title: "Banco de Proyectos Sennova",
			        text: html,
			        timer: 5000,
			        showConfirmButton: false
			    });*/
			},1000);
		});
	</script>
</body>
</html>